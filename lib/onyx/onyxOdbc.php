<?php
/*
  Copyright (c) 2008, Print and Mail Production Center, Brigham Young University
  All rights reserved.

  Redistribution and use in source and binary forms, with or without modification,
  are permitted provided that the following conditions are met:

  Redistributions of source code must retain the above copyright notice,
  this list of conditions and the following disclaimer.
  Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.
  Neither the name of Brigham Young University nor the names of its contributors
  may be used to endorse or promote products derived from this software without
  specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
//------------------------------------------------------------------------------
//The following define check is to allow
//require_once('SomeScript.php') and require_once('..\SomeScript.php')
//to work when they are the same file. This happens when scripts are in subfolders
if(defined('onyxOdbc.php'))
    return;
define('onyxOdbc.php', '');
//------------------------------------------------------------------------------
function ODBCConnectElseErr($UserName, $Password, $ConnectionString) {
    $Result = odbc_connect($ConnectionString, $UserName, $Password);
    Errif($Result == false, "Connection Failed");
    $_SESSION['ODBCInTransaction'] = FALSE;
    return $Result;
}
//------------------------------------------------------------------------------
function ODBCRunSql($Conn, $SQLStr) {//
    $SQLResult = odbc_exec($Conn, $SQLStr);
    ErrIf($SQLResult == false, "SQL Error In Query: $SQLStr");
    return $SQLResult;
}
//------------------------------------------------------------------------------
function ODBCGetPrimaryKey($Conn) {//
    $SQLResult = ODBCRunSql($Conn, "SELECT @@IDENTITY PKEY");
    $PrimaryKey = odbc_result($SQLResult, "PKEY");
    return $PrimaryKey;
}
//------------------------------------------------------------------------------
function ODBCInTransaction() {//
    return $_SESSION['ODBCInTransaction'];
}
//------------------------------------------------------------------------------
function ODBCStartTransaction($Connection) {//
    ErrIf(ODBCInTransaction(), 'Start transaction failed. Already in a transaction');
    ErrIf(odbc_autocommit($Connection, TRUE), 'Start transaction failed.');
    $_SESSION['ODBCInTransaction'] = TRUE;
}
//------------------------------------------------------------------------------
function ODBCCommitElseErr($Connection) {//
    Verify(ODBCInTransaction(), 'Commit failed. Not in a transaction');
    $Result = odbc_Commit($Connection);
    ErrIf($Result == false, 'Commit failed');
    $_SESSION['ODBCInTransaction'] = FALSE;
    return $Result;
}
//------------------------------------------------------------------------------
function ODBCRollbackElseErr($Connection) {//
    Verify(ODBCInTransaction(), 'Rollback failed. Not in a transaction');
    $Result = ODBCRollback($Connection);
    ErrIf($Result == false, 'Rollback failed');
    $_SESSION['ODBCInTransaction'] = FALSE;
    return $Result;
}
//------------------------------------------------------------------------------
function ODBCFetchRow($Conn, $SQLText) {
    $SQLResult = ODBCRunSql($Conn, $SQLText);
    $Row = odbc_fetch_array($SQLResult);
    return $Row;
}
//------------------------------------------------------------------------------
function ODBCFetchColumn($Conn, $SQLText, $Column) {
    $ReturnArray = array();
    $SQLResult = ODBCRunSql($Conn, $SQLText);
    $Row = odbc_fetch_array($SQLResult);
    while($Row != false) {
        $ReturnArray[] = $Row[$Column];
        $Row = odbc_fetch_array($SQLResult);
    }
    return $ReturnArray;
}
//------------------------------------------------------------------------------
function ODBCFetchValue($Connection, $SQLText) {//
    $result = ODBCRunSql($Connection, $SQLText);
    return odbc_result($result, 1);
}
//------------------------------------------------------------------------------
function ODBCFetchAll($Connection, $SQLText, $NumberOfRows = -1) {
    $ReturnArray = array();
    $OracleStatement = ODBCRunSQL($Connection, $SQLText);
    $Row = odbc_fetch_array($OracleStatement);
    $Count = 1;

    while(($Row != false) and (($NumberOfRows = -1) or ($Count < $NumberOfRows))) {
        $ReturnArray[] = $Row;
        $Row = odbc_fetch_array($OracleStatement);
        $Count++;
    }
    return $ReturnArray;
}
//------------------------------------------------------------------------------
function ODBCCloseElseErr($Connection) {
    ErrIf(ODBCInTransaction(), 'Close attempted during transaction');
    $CloseResult = odbc_close($Connection);
    ErrIf($CloseResult === false, 'Close of database failed');
    return $CloseResult;
}
?>