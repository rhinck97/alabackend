<?php
if(defined('onyxCore.php')) {
    return;
}
define('onyxCore.php', '');
//------------------------------------------------------------------------------
define('cNoDefault', '~~NoDefault~~');
define('cStrInvalidSqlChars', "'");
define('DS', DIRECTORY_SEPARATOR);
//------------------------------------------------------------------------------
define('cException', 'Exception');
define('cENullValueInvalid', 'ENullValueInvalid');
define('cEKeyNotFound', 'EKeyNotFound');
define('cEParseErr', 'EParseErr');
define('cEEmptyStrInvalid', 'EEmptyStrInvalid');
define('cEErrFailedToThrow', 'EErrFailedToThrow');
define('cTimeMinsPerDay', 60*24);
define('cTimeDaysPerMin', 1/cTimeMinsPerDay);
//------------------------------------------------------------------------------
class ENullValueInvalid extends Exception {

}
class EKeyNotFound extends Exception {

}
class EParseErr extends Exception {

}
class EEmptyStrInvalid extends Exception {

}
class EErrFailedToThrow extends Exception {

}
//------------------------------------------------------------------------------
function err($errMessage, $errToThrow = cException) {//
    if(is_null($errToThrow)) {
        err('Err type provided is null', cENullValueInvalid);
    }
    throw new $errToThrow($errMessage);
}
//------------------------------------------------------------------------------
function errTest() {
    //no test for now if ever
}
//------------------------------------------------------------------------------
function errIf($errCondition, $errMessage = "Error condition met", $errToThrow = cException) {//
    if(!is_bool($errCondition)) {
        err('Condition must be of type boolean. Do not send truthy/falsey conditions');
    }
    if($errCondition === true) {
        err($errMessage, $errToThrow);
    }
}
//------------------------------------------------------------------------------
function errIfTest() {
    errIf(1 === 2);
    testNoErrWasThrown();
    try {
        errIf(1 === 1);
        errFailedToThrow();
    }
    catch(Exception $e) {
        testErrWasThrown($e, 'basic test');
    }
}
//------------------------------------------------------------------------------
function verify($errCondition, $errMessage = "Verify failed", $errToThrow = cException) {//
    if(!is_bool($errCondition)) {
        err('Condition must be of type boolean. Do not send truthy/falsey conditions');
    }
    if($errCondition === false) {
        err($errMessage, $errToThrow);
    }
}
//------------------------------------------------------------------------------
function verifyTest() {
    //no test for now if ever
}
//------------------------------------------------------------------------------
function errFailedToThrow() {//
    err('error failed to throw', cEErrFailedToThrow);
}
//------------------------------------------------------------------------------
function errFailedToThrowTest() {
    //no test for now if ever
}
//------------------------------------------------------------------------------
function verifyIsBool($condition) {//
    verify(is_bool($condition), 'Condition must be of type boolean. Do not send truthy/falsey conditions');
}
//------------------------------------------------------------------------------
function errIfDisallowedKeys($newFieldValues, $disAllowedKeys) {
    foreach($newFieldValues as $key=> $value) {
        foreach($disAllowedKeys as $item) {
            errIf($item === $key, '$key not editable');
        }
    }
    return;
}
//------------------------------------------------------------------------------
function verifyIsBoolTest() {//
    verifyIsBool(true);
    testNoErrWasThrown();
    try {
        verifyIsBool(null);
        errFailedToThrow();
    }
    catch(Exception $e) {
        testErrWasThrown($e, 'basic test');
    }
}
//------------------------------------------------------------------------------
$GLOBALS['lastFunctionNameTested'] = '';
//------------------------------------------------------------------------------
function testTrue($condition, $testDescription) {//
    verifyIsBool($condition);
    $levelsUp = 0;
    do {
        $functionName = backtraceGet($levelsUp, 'function');
        $levelsUp++;
    } while(strHasPrefix('test', $functionName));

    $functionName = strDeleteSuffix('Test', $functionName);
    $testDescription = $functionName.' --- '.$testDescription;
    verifyIsBool($condition);
    if($functionName !== $GLOBALS['lastFunctionNameTested']) {
        $GLOBALS['lastFunctionNameTested'] = $functionName;
        echoStr('-------------');
    }
    if($condition) {
        echoStr("<span style='background-color:#00ff00; color:#000000'>Passed</span>---$testDescription");
    }
    else {
        echoStr("<span style='background-color:#ff0000; color:#ffffff'>Failed</span>---$testDescription");
    }
}
//--------------------------------------------------------------------------
function testTrueTest() {
    //no test for now if ever
}
//------------------------------------------------------------------------------
function testFalse($condition, $testDescription) {
    verifyIsBool($condition);
    testTrue(!$condition, $testDescription);
}
//--------------------------------------------------------------------------
function testFalseTest() {
    //no test for now if ever
}
//------------------------------------------------------------------------------
function testErrWasThrown($e, $testDescription = 'Error was thrown like it should test') {//
    testTrue(get_class($e) <> cEErrFailedToThrow, $testDescription);
}
//--------------------------------------------------------------------------
function testErrWasThrownTest() {
    //no test for now if ever
}
//------------------------------------------------------------------------------
function testCorrectErrWasThrown($ex, $typeErrShouldBe, $testDescription = 'correct error was thrown test') {//
    $className = get_class($ex);
    $matchStr = 'Exception is "'.$className.'", "'.$typeErrShouldBe.'" observed: --- ';
    if($className === cEErrFailedToThrow) {
        testTrue(false, "$testDescription $matchStr");
    }
    else {
        testTrue($className === $typeErrShouldBe, "$matchStr $testDescription");
    }
}
//--------------------------------------------------------------------------
function testCorrectErrWasThrownTest() {
    //no test for now if ever
}
//--------------------------------------------------------------------------
function testNoErrWasThrown($testDescription = 'no error was thrown test') {
    testTrue(true, $testDescription);
}
//--------------------------------------------------------------------------
function testNoErrWasThrownTest() {
    //no test for now if ever
}
//--------------------------------------------------------------------------
function arrayUnsetAllKeys(&$array) {//
    foreach($array as $key=> $toss) {
        $toss = $toss; //keep unused warning from showing
        unset($array[$key]);
    }
}
//--------------------------------------------------------------------------
function arrayUnsetAllKeysTest() {
    $array = [];
    $array['fruit'] = 'pear';
    $array['color'] = 'blue';
    arrayUnsetAllKeys($array);
    testTrue(count($array) === 0, 'basic test');
}
//--------------------------------------------------------------------------
$GLOBALS['latestOutput'] = '';
//--------------------------------------------------------------------------
function testingOutputPrepClear() {//
    ob_flush();
    ob_start();
    arrayUnsetAllKeys($_GET);
    arrayUnsetAllKeys($_POST);
    arrayUnsetAllKeys($_REQUEST);
    $GLOBALS['latestOutput'] = '';
}
//--------------------------------------------------------------------------
function testingOutputPrepClearTest() {
    //no test for now if ever
}
//--------------------------------------------------------------------------
function testingGet() {//
    testingOutputPrepClear();
    $_SERVER['REQUEST_METHOD'] = 'GET';
}
//--------------------------------------------------------------------------
function testingGetTest() {
    verify($_SERVER['REQUEST_METHOD'] === 'GET', 'basic test'); //no test for now if ever
}
//--------------------------------------------------------------------------
function testingPost($data = null) {//
    testingOutputPrepClear();
    $_SERVER['REQUEST_METHOD'] = 'POST';
    if($data !== null) {
        postPseudoInputStream($data);
    }
}
//--------------------------------------------------------------------------
function testingPostTest() {
    verify($_SERVER['REQUEST_METHOD'] === 'POST', 'basic test'); //no test for now if ever
}
//--------------------------------------------------------------------------
function testingDelete() {//
    testingOutputPrepClear();
    $_SERVER['REQUEST_METHOD'] = 'DELETE';
}
//--------------------------------------------------------------------------
function testingDeleteTest() {
    verify($_SERVER['REQUEST_METHOD'] === 'DELETE', 'basic test'); //no test for now if ever
}
//--------------------------------------------------------------------------
/* function testingDelete($data = null) {//
  testingOutputPrepClear();
  $_SERVER['REQUEST_METHOD'] = 'DELETE';
  if($data !== null) {
  postPseudoInputStream($data);
  }
  } */
//--------------------------------------------------------------------------
function echoedOutput() {//
    if($GLOBALS['latestOutput'] === '') {
        $GLOBALS['latestOutput'] = ob_get_contents();
        //ob_clean();
        ob_end_clean();
        ob_start();
    }
    return $GLOBALS['latestOutput'];
}
//--------------------------------------------------------------------------
function testingCaptureOutputTest() {
    //no test for now if ever
}
//--------------------------------------------------------------------------
function testOutputMatches($expectedValue, $testDescription) {//
    $latestOutput = echoedOutput();
    testTrue($latestOutput === $expectedValue, "expected $expectedValue, $latestOutput observed --- $testDescription");
}
//--------------------------------------------------------------------------
function testOutputMatchesTest() {
    //no test for now if ever
}
//--------------------------------------------------------------------------
function testOutputNull($testDescription) {//
    testOutputMatches('null', $testDescription);
}
//--------------------------------------------------------------------------
function testOutputNullTest() {
    //no test for now if ever
}
//--------------------------------------------------------------------------
function testOutputEmptyArray($testDescription) {//
    testOutputMatches('[]', $testDescription);
}
//--------------------------------------------------------------------------
function testOutputEmptyArrayTest() {
    //no test for now if ever
}
//--------------------------------------------------------------------------
function testOutputSuccess($testDescription) {//
    testOutputMatches('"success"', $testDescription);
}
//--------------------------------------------------------------------------
function testOutputSuccessTest() {
    //no test for now if ever
}
//------------------------------------------------------------------------------
function jsonEncodePlus($value) {//
    $encodedValue = json_encode($value);
    verify(json_last_error() === JSON_ERROR_NONE, "Failed to JSON encode value", cEParseErr);
    return $encodedValue;
}
//------------------------------------------------------------------------------
function jsonEncodePlusTest() {//
    testTrue(jsonEncodePlus(null) === "null", 'null test');
    testTrue(jsonEncodePlus(1) === "1", 'number test');
    testTrue(jsonEncodePlus('hello world') === '"hello world"', 'string test');
    testTrue(jsonEncodePlus('') === '""', 'empty string test');
    testTrue(jsonEncodePlus(true) === "true", 'true test');
    testTrue(jsonEncodePlus(false) === "false", 'false test');
    testTrue(jsonEncodePlus([1, 2, 3]) === "[1,2,3]", 'array test');
}
//------------------------------------------------------------------------------
function jsonDecodePlus($value) {//
    $decodedValue = json_decode($value, true);
    verify(json_last_error() === JSON_ERROR_NONE, "Failed to JSON decode value", cEParseErr);
    return $decodedValue;
}
//------------------------------------------------------------------------------
function jsonDecodePlusTest() {//
    testTrue(jsonDecodePlus('null') === null, 'null test');
    testTrue(jsonDecodePlus('1') === 1, 'number test');
    testTrue(jsonDecodePlus('"hello world"') === 'hello world', 'string test');
    testTrue(jsonDecodePlus('""') === '', 'empty string test');
    testTrue(jsonDecodePlus("true") === true, 'true test');
    testTrue(jsonDecodePlus("false") === false, 'false test');
    testTrue(jsonDecodePlus('[1,2,3]') === [1, 2, 3], 'array test');
    try {
        jsonDecodePlus('"bad json');
        errFailedToThrow();
    }
    catch(Exception $e) {
        testCorrectErrWasThrown($e, cEParseErr);
    }
}
//--------------------------------------------------------------------------
function echoJson($value) {
    echo jsonEncodePlus($value);
}
//--------------------------------------------------------------------------
function echoJsonTest() {
    //no test for now if ever
}
//--------------------------------------------------------------------------
function echoJsonSuccess() {
    echoJson('success');
}
//--------------------------------------------------------------------------
function echoJsonSuccessTest() {
    //no test for now if ever
}
//--------------------------------------------------------------------------
function echoedOutputJsonDecode() {//
    return jsonDecodePlus(echoedOutput());
}
//--------------------------------------------------------------------------
function testingCaptureOutputJsonDecodeTest() {
    //no test for now if ever
}
//------------------------------------------------------------------------------
function errIfNull($value, $errMessage = "Null value not allowed") {//
    errIf(is_null($value), $errMessage, cENullValueInvalid);
}
//------------------------------------------------------------------------------
function errIfNullTest() {//
    errIfNull(true);
    testNoErrWasThrown();
    try {
        errIfNull(null);
        errFailedToThrow();
    }
    catch(Exception $e) {
        testCorrectErrWasThrown($e, cENullValueInvalid, 'basic test');
    }
}
//------------------------------------------------------------------------------
function errIfNullOrEmptyStr($value, $errMessage = "Value may not be null or an empty string") {//
    errIfNull($value, $errMessage);
    errIf($value === '', $errMessage, cEEmptyStrInvalid);
}
//------------------------------------------------------------------------------
function errIfNullOrEmptyStrTest() {//
    try {
        errIfNullOrEmptyStr(null);
        errFailedToThrow();
    }
    catch(Exception $e) {
        testCorrectErrWasThrown($e, cENullValueInvalid, 'null test');
    }
    try {
        errIfNullOrEmptyStr('');
        errFailedToThrow();
    }
    catch(Exception $e) {
        testCorrectErrWasThrown($e, cEEmptyStrInvalid, 'empty string test');
    }
    errIfNullOrEmptyStr('apple', 'Should throw no err');
}
//------------------------------------------------------------------------------
function emailSend($subject, $body, $fromEmail, $toEmail, $ccEmail = null, $bccEmail = null) {//
    $headers = "MIME-Version: 1.0 \r\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1 \r\n";
    if(!isNullOrEmptyStr($ccEmail)) {
        $headers .= "Cc: $ccEmail"."\r\n";
    }
    if(!isNullOrEmptyStr($bccEmail)) {
        $headers .= "Bcc: $bccEmail"."\r\n";
    }
    $headers .= "From: $fromEmail \r\n";
    $settings = error_reporting(0);
    $result = mail($toEmail, $subject, $body, $headers);
    error_reporting($settings);
    if($result === false) {
        $errMessage = 'Email send failed.';
        $errInfo = error_get_last();
        if($errInfo !== null) {
            $errMessage .= ' '.arrayGet($errInfo, 'message');
        }
        err($errMessage);
    }
}
//------------------------------------------------------------------------------
function emailSendTest() {
    emailSend('email test subject', 'email test body', 'pmpccs@gmail.com', 'pmpccs@gmail.com');
    testTrue(true, 'basic test');
    try {
        emailSend('email test subject', 'email test body', 'pmpccs@gmail.com', 'Joe_badlastname@byu.edu');
        errFailedToThrow();
    }
    catch(Exception $e) {
        testErrWasThrown($e, 'throw error on bad send test');
    }
}
//------------------------------------------------------------------------------
function valArrayKeysExist($arrayWithKeys, $arrayToCheckForKeys, $extraInfo = '') {
    foreach($arrayWithKeys as $key=> $toss) {
        $toss = $toss; //keep unused variable warning from showing
        errIf(array_key_exists($key, $arrayToCheckForKeys) === false, "key '$key' not found in target array. ".$extraInfo, cEKeyNotFound);
    }
}
//------------------------------------------------------------------------------
function valArrayKeysExistTest() {
    $arrayWithKeys['fruit'] = 'pear';
    $arrayWithKeys['color'] = 'blue';
    $arrayToCheckForKeys['fruit'] = 'bannana';
    $arrayToCheckForKeys['color'] = 'red';
    $arrayToCheckForKeys['time'] = 'morning';
    valArrayKeysExist($arrayWithKeys, $arrayToCheckForKeys);
    testNoErrWasThrown();

    $arrayWithKeys['adder'] = 'someplace';
    try {
        valArrayKeysExist($arrayWithKeys, $arrayToCheckForKeys);
        errFailedToThrow();
    }
    catch(Exception $ex) {
        testCorrectErrWasThrown($ex, cEKeyNotFound, 'throw on extra key test');
    }
}
//------------------------------------------------------------------------------
function isNullOrEmptyStr($variable) {//
    return is_null($variable) || ($variable === '');
}
//------------------------------------------------------------------------------
function isNullOrEmptyStrTest() {//
    testTrue(isNullOrEmptyStr(null), 'null test');
    testTrue(isNullOrEmptyStr(''), 'blank string test');
    testFalse(isNullOrEmptyStr("Hi There"), 'has value test');
}
//------------------------------------------------------------------------------
function valArrayAndKey($array, $key) {//
    errIfNull($array, 'Array may not be null');
    verify(is_array($array), 'Non array passed');
    errIfNull($key, 'Key may not be null');
}
//------------------------------------------------------------------------------
function valArrayAndKeyTest() {//
    valArrayAndKey(array("color"=>"red"), 'color');
    testNoErrWasThrown();
    try {
        valArrayAndKey(null, 'name');
        errFailedToThrow();
    }
    catch(Exception $e) {
        testCorrectErrWasThrown($e, cENullValueInvalid, 'Throw on null array test');
    }
    try {
        $array = [];
        valArrayAndKey($array, null);
        errFailedToThrow();
    }
    catch(Exception $e) {
        testCorrectErrWasThrown($e, cENullValueInvalid, 'Throw on null key test');
    }
    try {
        valArrayAndKey('not an array', null);
        errFailedToThrow();
    }
    catch(Exception $e) {
        testErrWasThrown($e, 'Throw on non array test');
    }
}
//------------------------------------------------------------------------------
function arrayTrueKey($array, $key, $errIfKeyMissing = true) {//
    verify(is_string($key) or is_numeric($key), 'Key must be a string or integer');

    $origKey = $key;

    if(is_numeric($key)) {
        $key = (integer) $key;
    }

    if(array_key_exists($key, $array)) {
        return $key;
    }

    $key = (string) $key;
    $key = strtoupper($key);
    if(array_key_exists($key, $array)) {
        return $key;
    }

    $key = strtolower($key);
    if(array_key_exists($key, $array)) {
        return $key;
    }

    foreach($array as $trueKey=> $toss) {
        $toss = $toss; //keep unused variable warning from showing
        if(strtolower($trueKey) === $key) {
            return $trueKey;
        }
    }

    errIf($errIfKeyMissing, "Key '$origKey' not found in array", cEKeyNotFound);
    return null;
}
//------------------------------------------------------------------------------
function arrayTrueKeyTest() {//
    $array[0] = 'zero';
    $array[1] = 'one';
    $array[2] = 'two';
    $array[23] = 'twenty three';
    testTrue(arrayTrueKey($array, 0) === 0, 'index 0 test');
    testTrue(arrayTrueKey($array, 1) === 1, 'index 1 test');
    testTrue(arrayTrueKey($array, 2) === 2, 'index 2 test');
    testTrue(arrayTrueKey($array, 23) === 23, 'index 3 test');

    $array = array('fruit'=>'apple', 'stone'=>'onyx');
    testTrue(arrayTrueKey($array, 'fruit') === 'fruit', 'match test');
    testTrue(arrayTrueKey($array, 'Fruit') === 'fruit', 'mixed test');
    testTrue(arrayTrueKey($array, 'FRUIT') === 'fruit', 'all cap test');
    try {
        arrayTrueKey($array, 'missing');
        errFailedToThrow();
    }
    catch(Exception $e) {
        testErrWasThrown($e, "throw error when key missing test");
    }
}
//------------------------------------------------------------------------------
function arrayGet($array, $key, $defaultValue = cNoDefault) {//
    valArrayAndKey($array, $key);
    $trueKey = arrayTrueKey($array, $key, $defaultValue === cNoDefault);
    if(!is_null($trueKey)) {
        return $array[$trueKey];
    }
    return $defaultValue;
}
//------------------------------------------------------------------------------
function arrayGetTest() {//
    $array = array('fruit'=>'apple', 'stone'=>'onyx');
    testTrue(arrayGet($array, 'fruit') === 'apple', "key exists test");
    testTrue(arrayGet($array, 'missing', null) === null, "null test");
    try {
        arrayGet($array, 'missing');
        errFailedToThrow();
    }
    catch(Exception $e) {
        testErrWasThrown($e, "throw error when key missing test");
    }
    try {
        arrayGet(null, 'fruit');
        errFailedToThrow();
    }
    catch(Exception $e) {
        testErrWasThrown($e, "throw error when null array test");
    }
    try {
        $nonArray = 5;
        arrayGet($nonArray, 'fruit');
        errFailedToThrow();
    }
    catch(Exception $e) {
        testErrWasThrown($e, "throw error when not array test");
    }
}
//------------------------------------------------------------------------------
function requestGet($key, $defaultValue = cNoDefault) {//
    return arrayGet($_REQUEST, $key, $defaultValue);
}
//------------------------------------------------------------------------------
function strExists($needle, $haystack)
{
	return stripos($haystack, $needle) !== false;
}
//------------------------------------------------------------------------------
function strHasPrefix($prefix, $str) {//
    if(strlen($prefix) == 0 || strlen($str) == 0) {
        return false;
    }
    return substr($str, 0, strlen($prefix)) == $prefix;
}
//------------------------------------------------------------------------------
function strHasPrefixTest() {//
    testTrue(strHasPrefix('test', 'test This string out'), 'has prefix test');
    testTrue(strHasPrefix('AND', 'AND X==Y AND 1==2'), 'Has prefix Test 2');
    testFalse(strHasPrefix('test', 'Blah Blah Blah'), 'does not have prefix test');
    testFalse(strHasPrefix('', 'Blahblasdfa'), '0 length prefix test');
    testFalse(strHasPrefix('test', ''), '0 length string test');
}
//------------------------------------------------------------------------------
function strHasSuffix($suffix, $str) {//
    if(strlen($suffix) == 0 || strlen($str) == 0) {
        return false;
    }
    return substr($str, -1 * strlen($suffix)) === $suffix;
}
//------------------------------------------------------------------------------
function strHasSuffixTest() {//
    testTrue(strHasSuffix('test', 'This is a test'), 'has suffix test');
    testTrue(strHasSuffix('AND', 'X==Y AND 1==2 AND'), 'Has Suffix Test 2');
    testFalse(strHasSuffix('test', 'Blah Blah Blah'), 'does not have suffix test');
    testFalse(strHasSuffix('', 'Blahblasdfa'), '0 length suffix test');
    testFalse(strHasSuffix('test', ''), '0 length string test');
}
//------------------------------------------------------------------------------
function strDelete($needleToDelete, $haystack) {
    return str_replace($needleToDelete, '', $haystack);
}
//------------------------------------------------------------------------------
function strDeletePrefix($prefix, $stringToDeleteFrom) {
    if(strHasPrefix($prefix, $stringToDeleteFrom)) {
        $prefixLen = strlen($prefix);
        return substr($stringToDeleteFrom, $prefixLen);
    }
    else {
        return $stringToDeleteFrom;
    }
}
//-----------------------------------------------------------------------------------
function strDeletePrefixTest() {
    testTrue(strDeletePrefix('AND', 'X==Y AND 1==2') == 'X==Y AND 1==2', 'StrDeletePrefix No Prefix Test');
    testTrue(strDeletePrefix('AND', 'AND X==Y AND 1==2') == ' X==Y AND 1==2', 'StrDeletePrefix Prefix Test');
}
//------------------------------------------------------------------------------
function strDeleteSuffix($suffix, $stringToDeleteFrom) {//
    if(strHasSuffix($suffix, $stringToDeleteFrom)) {
        $suffixLen = strlen($suffix);
        $stringLen = strlen($stringToDeleteFrom);
        return substr($stringToDeleteFrom, 0, $stringLen - $suffixLen);
    }
    else {
        return $stringToDeleteFrom;
    }
}
//------------------------------------------------------------------------------
function strDeleteSuffixTest() {//
    testTrue(strDeleteSuffix('AND', 'X==Y AND 1==2') == 'X==Y AND 1==2', 'No Suffix Test');
    testTrue(strDeleteSuffix('AND', 'X==Y AND 1==2 AND') == 'X==Y AND 1==2 ', 'Has Suffix Test');
}
//------------------------------------------------------------------------------
function strEnsurePrefix($prefix, $str) {
    if(strHasPrefix($prefix, $str)) {
        return $str;
    }
    else {
        return $prefix.$str;
    }
}
//------------------------------------------------------------------------------
function strEnsurePrefixTest() {
    testTrue(strEnsurePrefix('Test', 'Test this') == 'Test this', 'strEnsurePrefix failed do nothing test');
    testTrue(strEnsurePrefix('Test', ' this') == 'Test this', 'strEnsurePrefix failed to add prefix');
}
//------------------------------------------------------------------------------
function strEnsureSuffix($suffix, $str) {
    if(strHasSuffix($suffix, $str)) {
        return $str;
    }
    else {
        return $str.$suffix;
    }
}
//------------------------------------------------------------------------------
function strEnsureSuffixTest() {
    testTrue(strEnsureSuffix('test', 'This is a test') == 'This is a test', 'StrEnsureSuffix failed do nothing test');
    testTrue(strEnsureSuffix('test', 'This is a ') == 'This is a test', 'StrEnsureSuffix failed to add Suffix');
    testTrue(strEnsureSuffix('TeSt', 'a ') == 'a TeSt', 'StrEnsureSuffix failed basic search string test 2');
}
//------------------------------------------------------------------------------
function strDeleteCharSet($charSetToDelete, $string) {
    $length = sizeof($charSetToDelete);
    if(is_string($charSetToDelete)) {
        $length = strlen($charSetToDelete);
    }
    for($i = 0; $i < $length; $i++) {
        $string = str_replace($charSetToDelete[$i], "", $string);
    }
    return $string;
}
//------------------------------------------------------------------------------
function strDeleteCharSetTest() {
    testTrue(strDeleteCharSet(array('#', '$', '"'), '#this$is"a"#test"') == 'thisisatest', 'StrDeleteCharSet failed basic test');
    testTrue(strDeleteCharSet(array(), 'thisisatest') == 'thisisatest', 'StrDeleteCharSet failed empty set test');
    testTrue(strDeleteCharSet(array('d'), '') == '', 'StrDeleteCharSet failed nil string test');
}
//--------------------------------------------------------------------------
function strNthItem($nth, $stringOfItems, $seperator = ',') {
    $array = explode($seperator, $stringOfItems);
    if(abs($nth) > count($array) or $nth == 0) {
        return '';
    }
    $nth = numRemapNegNth($nth, count($array));
    return $array[$nth - 1];
}
//--------------------------------------------------------------------------
function numRemapNegNth($nth, $maxNth) {
    $result = $nth;
    if($result < 0) {
        $result = $maxNth + $nth + 1;
    }
    return $result;
}
//------------------------------------------------------------------------------
function strTrimStringsInArray(&$array) {
    foreach($array as &$element) {
        if(is_array($element)) {
            strTrimStringsInArray($element);
        }
        else if (is_string($element)){
            $element = trim($element);
        }
    }
}
//------------------------------------------------------------------------------
function strTrimStringsInArrayTest() {
    $array = array();
    strTrimStringsInArray($array);
    testTrue(count($array) === 0, 'empty array test');
    $array[] = 'no trim needed';
    $array[] = ' trim needed ';
    $array2 = array();
    $array2['data'] = 'test data';
    $array2['dataWithSpace'] = ' data with space ';
    $array2['years'] = 23;

    $array[] = $array2;

    strTrimStringsInArray($array);
    testTrue($array[0] === 'no trim needed', 'no trim needed test');
    testTrue($array[1] === 'trim needed', 'trim needed test');
    testTrue($array[2]['data'] === 'test data', 'nested no trim needed test');
    testTrue($array[2]['dataWithSpace'] === 'data with space', 'nested trim needed test');
    testTrue($array[2]['years'] === 23, 'non-string test');
}
//-----------------------------------------------------------------------------------
function strFormatPrice($oldPrice) {
    return '$'.number_format($oldPrice, 2, '.', ',');
}
//------------------------------------------------------------------------------
function backtraceGet($callsBack, $key, $default = cNoDefault) {//
    $maxIndex = count(debug_backtrace()) - 1;
    if($callsBack > $maxIndex) {
        return $default;
    }
    $backTrace = debug_backtrace();
    $callInfo = arrayGet($backTrace, $callsBack + 1);
    return arrayGet($callInfo, $key, $default);
}
//------------------------------------------------------------------------------
function backtraceGetTest() {//
    //no test for now if ever
}
//------------------------------------------------------------------------------
function backtraceCallLocation($callsBack = 0) {//
    $functionName = backtraceGet($callsBack + 1, 'function', 'script');
    $lineNumber = backtraceGet($callsBack, 'line', 'unknown line');
    $fileName = backtraceGet($callsBack, 'file', 'unknown file');
    return "line $lineNumber in $fileName by $functionName";
}
//------------------------------------------------------------------------------
function backtraceCallLocationTest() {//
    //no test for now if ever
}
//------------------------------------------------------------------------------
function arrayToDebugStr($array, $indentPrefix = '') {
    verify(is_array($array), 'Value provided is not an array');
    $resultString = '';
    if(is_null($array)) {
        return 'Array variable is null';
    }
    else if(count($array) === 0) {
        return '[]';
    }
    else {
        foreach($array as $key=> $value) {
            if(is_array($value)) {
                $resultString .= arrayToDebugStr($value, $indentPrefix."[$key]");
            }
            else {
                $resultString .= $indentPrefix."[$key]".toDebugStr($value)."\n";
            }
        }
    }
    return $resultString;
}
//------------------------------------------------------------------------------
function arrayToDebugStrTest() {//
    //no test for now if ever
}
//------------------------------------------------------------------------------
function toDebugStr($value) {//
    if(is_array($value)) {
        return arrayToDebugStr($value);
    }
    else if(is_string($value)) {
        return '"'.$value.'"';
    }
    else {
        return jsonEncodePlus($value);
    }
}
//------------------------------------------------------------------------------
function toDebugStrTest() {
    //no test for now if ever
}
//------------------------------------------------------------------------------
function echoStr($str) {//
    echo str_replace("\n", '<br />', $str).'<br />';
}
//------------------------------------------------------------------------------
function echoStrTest() {
    //no test for now if ever
}
//------------------------------------------------------------------------------
function echoLine($str = '') {//
    echo $str.'<br />';
}
//------------------------------------------------------------------------------
function echoLineTest() {
    //no test for now if ever
}
//------------------------------------------------------------------------------
function echoDebug($value) {//
    $callLocation = backtraceCallLocation(1);
    $header = "echoDebug called on $callLocation";
    echoStr($header);
    $debugStr = toDebugStr($value);
    echoStr($debugStr);
}
//------------------------------------------------------------------------------
function echoDebugTest() {
    //no test for now if ever
}
//------------------------------------------------------------------------------
function isoDateTime() {
    return date('Y-m-d H:i:s');
}
//------------------------------------------------------------------------------
function isoDateTimeTest() {
    //no test for now if ever
}
//------------------------------------------------------------------------------
function fileDeleteIfExists($fileName) {
    if(file_exists($fileName)) {
        fileDelete($fileName);
    }
}
//------------------------------------------------------------------------------
function fileDeleteIfExistsTest() {
    $fileName = 'Delete.txt';
    fileAppendText('test', $fileName);
    fileDeleteIfExists($fileName);
    testFalse(file_exists($fileName), 'delete test');
    fileDeleteIfExists($fileName);
    testTrue(true, 'file is not there test');
}
//------------------------------------------------------------------------------
function dirDelete($dirName) {
    verify(file_exists($dirName), "Directory not found");
	$result = rmdir($dirName);
    verify($result === true, "Delete of directory $dirName failed");
}
//------------------------------------------------------------------------------
function fileOrDirDelete($fileOrDirName) {
    if(is_dir($fileOrDirName)) {
		dirDelete($fileOrDirName);
	}
	else {
		fileDelete($fileOrDirName);
	}
}
//------------------------------------------------------------------------------
function fileDelete($filePath) {
    verify(file_exists($filePath), "File $filePath not found");
    $result = unlink($filePath);
    verify($result === true, "Delete of file $filePath failed");
}
//------------------------------------------------------------------------------
function fileDeleteTest() {
    $fileName = 'Delete.txt';
    fileAppendText('test', $fileName);
    fileDelete($fileName);
    testFalse(file_exists($fileName), 'delete test');
    try {
        fileDelete($fileName);
        errFailedToRaise;
    }
    catch(exception $e) {
        testErrWasThrown($e, 'raise exception when file is missing');
    }
}
//------------------------------------------------------------------------------
function fileAppendText($inputString, $fileName) {
    errIfNullOrEmptyStr($fileName, 'Filename bad in fileAppendText');
    $result = file_put_contents($fileName, $inputString, FILE_APPEND);
    if($result === false) {
        err("File failed to open. Rights are likely needed. ".
                "This can happen if the file was created manualy and not by a PHP script");
    }
}
//--------------------------------------------------------------------------
function strLineBreakUsed($str) {
	if(strpos($str, "\r\n") !== false) {
		return "\r\n";
	}
	else if(strpos($str, "\n\r") !== false) {
		return "\n\r";
	}
	else if(strpos($str, "\r") !== false) {
		return "\r";
	}
	else if(strpos($str, "\n") !== false) {
		return "\n";
	}
	else {
		return "\n";
	}
}
//------------------------------------------------------------------------------
function fileReadText($fileName){
	verify(file_exists($fileName), 'file "'.$fileName.'" not found');
	return file_get_contents($fileName);
}
//------------------------------------------------------------------------------
function fileWriteText($inputString, $fileName)
{
	errIfNullOrEmptyStr($fileName, 'Filename missing in FileWriteTextFile');
	if(isNullOrEmptyStr($inputString)) {
		$inputString = " ";
	}
	$handle = fopen($fileName, "w");
	errIf($handle == false, "Open of file named '$fileName' failed in FileWriteText");

	$result = fwrite($handle, $inputString);
	errIf($result == false, "Writing into named file '$fileName' failed in FileWriteTextwriteFile");

	$result = fclose($handle);
	errIf($result == false, "Closing file named '$fileName' failed in FileWriteText");
}
//------------------------------------------------------------------------------
function fileWriteTextTest()
{
	$fileName = "WriteNewFileTest.txt";
	fileDeleteIfExists($fileName);
	fileWriteText("Time to eat ice cream!", $fileName);
	$text = file($fileName);
	$result = stristr($text[0], "Time to eat ice cream!");
	testFalse($result == false, 'FileWriteTextFile create file test');
	fileWriteText("Time to eat pizza!", $fileName);
	$text = file($fileName);
	$result = stristr($text[0], "Time to eat pizza!");
	testFalse($result == false, 'FileWriteTextFile rewrite existing file test');
	fileDelete($fileName);
}
//------------------------------------------------------------------------------
function fileAppendTextTest() {
    $fileName = "AppendToFileTest.txt";
    fileDeleteIfExists($fileName);

    fileAppendText("Time to eat ice cream!", $fileName);
    $text = file_get_contents($fileName);
    testTrue($text === "Time to eat ice cream!", 'FileAppendToFile append test1');

    fileAppendText("The time is now!", $fileName);
    $text = file_get_contents($fileName);
    $result = stripos($text, "The time is now");
    testTrue($result === 22, 'FileAppendToFile append test2');

    fileDelete($fileName);
    errIf(file_exists($fileName), "The test file $fileName was not deleted properly in FileAppendToFileTest");
}
//------------------------------------------------------------------------------
function fileLog($message, $fileName = 'log.txt', $callsBack = 1) {
    errIfNullOrEmptyStr($fileName, 'Filename bad in FileLog');
    $callLocation = backtraceCallLocation($callsBack);
    $fullMessage = isoDateTime()." $message. Logged on $callLocation. \n\n";
    fileAppendText($fullMessage, $fileName);
}
//------------------------------------------------------------------------------
/* function fileLog($message, $fileName = 'log.txt', $levelsBack = 1) {
  errIfNullOrEmptyStr($fileName, 'Filename bad in FileAppendToLog');

  $returnFunctionName = debugStackTraceFunctionName($levelsBack);
  $callLocation = debugStackTraceCallLocation($levelsBack - 1);

  $fullMessage = isoDateTime()." $message. Logged by $returnFunctionName in $callLocation. \n\n";
  if(file_exists($fileName) && filesize($fileName)>10485760) {
  rename($fileName, pathInfo($fileName,PATHINFO_FILENAME) . date("Y-m-d H-i") .".". pathInfo($fileName,PATHINFO_EXTENSION));
  }
  fileAppendText($fullMessage, $fileName);
  } */
//------------------------------------------------------------------------------
function fileLogTest() {
    //no test for now
}
//------------------------------------------------------------------------------
function fileLogDebug($value, $fileName = 'log.txt') {
    $debugStr = toDebugStr($value);
    fileLog($debugStr, $fileName, 2);
}
//------------------------------------------------------------------------------
function fileLogDebugTest() {
    //no test for now
}
//------------------------------------------------------------------------------
function fileSaveUserUpload($saveDirectory = '', $uniqueId = '') {
    $tmpName = $_FILES['file']['tmp_name'];
    $fileName = basename($_FILES['file']['name']);
    $fileName = preg_replace("/\s/", "_", $fileName);
    $finalFileName = $uniqueId.$fileName;
    $finalFilePath = $saveDirectory.$finalFileName;
    $success = move_uploaded_file($tmpName, $finalFilePath);
    verify($success, "Failed to upload ".$fileName);
    return $finalFilePath;
}
//==============================================================================
function fileUniqueName($destDirectory, $ext = '.tmp', $prefix = '$')
{
	$destDirectory = dirEnsureSlash($destDirectory);
	$ext = strEnsurePrefix('.', $ext);
	verify(file_exists($destDirectory), "directory $destDirectory not found");
	do {
		$result = $destDirectory.$prefix.rand(1000000, 9999999).$ext;
	} while(file_exists($result));
	return $result;
}
//------------------------------------------------------------------------------
function fileSaveUserUploadTest() {
    //no test for now if ever
}
//------------------------------------------------------------------------------
function dirEnsureSlash($dir) {
    $dir = fileUseNativeSlashes($dir);
    if($dir === '') {
        return '';
    }
    if(strHasSuffix(DIRECTORY_SEPARATOR, $dir)) {
        return $dir;
    }
    return $dir.DIRECTORY_SEPARATOR;
}
//------------------------------------------------------------------------------
function dirEnsureSlashTest() {
    testTrue(dirEnsureSlash('..\images') === '..\images'.DIRECTORY_SEPARATOR, 'add slash test');
    testTrue(dirEnsureSlash('..\images\\') === '..\images'.DIRECTORY_SEPARATOR, 'slash already there test');
    testTrue(dirEnsureSlash('') === '', 'empty string test');
}
//------------------------------------------------------------------------------
function fileUseNativeSlashes($dir) {
    $dir = str_ireplace('\\', DIRECTORY_SEPARATOR, $dir);
    $dir = str_ireplace('/', DIRECTORY_SEPARATOR, $dir);
    return $dir;
}
//------------------------------------------------------------------------------
function fileExtractDir($fullPath) {
	$dir = pathinfo($fullPath, PATHINFO_DIRNAME);
    if((strpos($fullPath, "\\") === false) && (strpos($fullPath, '/') === false)) {
        return '';
    }
    else {
        return dirEnsureSlash($dir);
    }
}
//------------------------------------------------------------------------------
function fileExtractDirTest() {
    testTrue(fileExtractDir('nameOnly') === '', 'filename only test');
    testTrue(fileExtractDir('images\\mainPic.jpg') === 'images\\', 'basic test');
    testTrue(fileExtractDir('site\\images\\mainPic.jpg') === 'site\\images\\', 'basic test2');
    testTrue(fileExtractDir('') === '', 'empty string test');
}
//------------------------------------------------------------------------------
function fileExtractName($fullPath) {
	return pathinfo($fullPath, PATHINFO_BASENAME);
}
//------------------------------------------------------------------------------
function fileExtractNameNoExt($fullPath) {
	return pathinfo($fullPath, PATHINFO_FILENAME);
}
//------------------------------------------------------------------------------
function fileExtractExt($fullPath) {
	return pathinfo($fullPath, PATHINFO_EXTENSION);
}
//------------------------------------------------------------------------------
function fileDeleteExt($fileName) {
	$ext = fileExtractExt($fileName);
	return strDeleteSuffix($ext, $fileName);
}
//=========================================================================
/* function httpErr($exception, $httpCode) {
  $errMessage = $exception->GetMessage();
  $errInfo = get_class($exception).': '.$errMessage;
  header("HTTP/1.0 $httpCode $errInfo");
  } */

/*
//------------------------------------------------------------------------------
function fileDeleteExt($filename) {
	$ext = fileExtractExt($filename);
	return strDeleteSuffix($ext, $filename);
}
//------------------------------------------------------------------------------
function fileDeleteExtTest()
{
  testTrue(fileDeleteExt('path/path/filename.txt') == 'path/path/filename', 'FileDeleteExt with path test');
  testTrue(fileDeleteExt('path/path/filename') == 'path/path/filename', 'FileDeleteExt no extension test');
  testTrue(fileDeleteExt('path/path/filename.middle.txt') == 'path/path/filename.middle', 'FileDeleteExt dot in middle test');
  testTrue(fileDeleteExt('filename.....exe') == 'filename....', 'FileDeleteExt multiple dot test');
}
  //============================================================================================
  function strEnsurePrefix($prefix, $str) {
  if(strHasPrefix($prefix, $str)) {
  return $str;
  }
  else {
  return $prefix.$str;
  }
  }
  //------------------------------------------------------------------------------
  function fileExtractNameNoExt($fullPath) {
  return fileDeleteExt(fileExtractName($fullPath));
  }
  //------------------------------------------------------------------------------
  function fileExtractName($fullPath) {
  return basename($fullPath);
  }
  //------------------------------------------------------------------------------
  function fileExtractDir($fullPath) {
  $path = dirName($fullPath);
  if(strpos($fullPath, "\\") === false and strpos($fullPath, '/') === false) {
  return '';
  }
  else {
  return $path.DS;
  }
  }
  //------------------------------------------------------------------------------
  function fileExtractExt($fullPath) {
  $extension = pathinfo($fullPath, PATHINFO_EXTENSION);
  if(isNullOrEmptyStr($extension)) {
  return '';
  }
  else {
  return ".$extension";
  }
  }
  //------------------------------------------------------------------------------
  function fileMove($srcPath, $destDir) {
  $destDir = dirEnsureSlash($destDir);
  $destFileName = fileExtractName($srcPath);
  $result = rename($srcPath, $destDir.$destFileName);
  verify($result, "fileMove failed");
  }
  //------------------------------------------------------------------------------
  function fileCleanFileName($fileName) {
  $value = str_ireplace(
  array(" ", "'", "+", "#", "\"", "%", "&", "*", ":", ";", "<", ">", "?", "\\", "/", "{", "}", "|", "~"), array("_"), $fileName);
  return $value;
  }
  //=========================================================================
  function httpErr($exception) {
  $errMessage = $exception->GetMessage();
  $errInfo = get_class($exception).': '.$errMessage;
  header("HTTP/1.0 400 $errInfo");
  }
*/
//------------------------------------------------------------------------------
function errInvalidRequestMethod() {//
    $requestMethod = $_SERVER['REQUEST_METHOD'];
    err("'$requestMethod' not supported for this action");
}
//------------------------------------------------------------------------------
function errInvalidRequestMethodTest() {
    try {
        errInvalidRequestMethod();
        errFailedToThrow();
    }
    catch(Exception $e) {
        testErrWasThrown($e);
    }
}
//------------------------------------------------------------------------------
function postPseudoInputStream($data) {
    if(!is_string($data)) {
        $data = jsonEncodePlus($data);
    }
    $_POST['pseudoInputStream'] = $data;
}
//------------------------------------------------------------------------------
function postPseudoInputStreamTest() {
    //not test for now if ever
}
//------------------------------------------------------------------------------
function inputGet() {//
    if(array_key_exists('pseudoInputStream', $_POST) &&
            !is_null($_POST['pseudoInputStream'])) {
        return $_POST['pseudoInputStream'];
    }
    else {
        return file_get_contents("php://input");
    }
}
//------------------------------------------------------------------------------
function inputGetTest() {//
    //no test for now if ever
}
//------------------------------------------------------------------------------
function inputGetJsonDecode() {//
    return jsonDecodePlus(inputGet());
}
//------------------------------------------------------------------------------
function inputGetJsonDecodeTest() {//
    //no test for now if ever
}
//------------------------------------------------------------------------------
function strToBool($bool) {
    verify(is_string($bool) || is_bool($bool), 'non string passed in strToBool got '.jsonEncodePlus($bool).' instead');
    if(is_bool($bool)) {
        return $bool;
    }
    $firstLetter = $bool[0];
    if(stristr('TY1', $firstLetter) !== false) {
        return true;
    }
    if(stristr('FN0', $firstLetter) !== false) {
        return false;
    }
    err("Unable to convert $bool to boolean");
}
//------------------------------------------------------------------------------
function valRequestMethod($neededMethod) {
    $actualMethod = $_SERVER['REQUEST_METHOD'];
    if($actualMethod !== $neededMethod) {
        errHttp("Invalid Request Method. $neededMethod needed. received $actualMethod", 400);
    }
}
//------------------------------------------------------------------------------
function valRequestMethodPost() {
    valRequestMethod('POST');
}
//------------------------------------------------------------------------------
function valRequestMethodGet() {
    valRequestMethod('GET');
}
//------------------------------------------------------------------------------
function strToBoolTest() {
    testTrue(strToBool('true'), "'true' test");
    testTrue(strToBool('T'), "'T' test");
    testTrue(strToBool('yes'), "'yes' test");
    testTrue(strToBool('1'), "'1' test");
    testFalse(strToBool('false'), "'false' test");
    testFalse(strToBool('F'), "'F' test");
    testFalse(strToBool('no'), "'no' test");
    testFalse(strToBool('0'), "'0' test");
    try {
        testFalse(strToBool('X'), "'0' test");
    }
    catch(Exception $e) {
        testErrWasThrown($e);
    }
}
//--------------------------------------------------------------------------
function sqlSubStrSearch($value, $caseInsensitive = true) {
    if(is_string($value)) {
        if($caseInsensitive) {
            $value = strtoupper($value);
        }
        $value = "'%".$value."%'";
    }
    return $value;
}
//------------------------------------------------------------------------------
function echoStandardFormat($value) {
    $resultObject['result'] = $value;
    $resultJson = jsonEncodePlus($resultObject);
    echo $resultJson;
}
//------------------------------------------------------------------------------
function onyxCoreTest() {
    errIfNullTest();
    errIfNullOrEmptyStrTest();
    verifyIsBoolTest();
    isNullOrEmptyStrTest();
    valArrayAndKeyTest();
    valArrayKeysExistTest();
    arrayTrueKeyTest();
    arrayGetTest();
    strHasPrefixTest();
    strHasSuffixTest();
    strDeletePrefixTest();
    strDeleteSuffixTest();
    strDeleteCharSetTest();
    strEnsurePrefixTest();
    strEnsureSuffixTest();
    strTrimStringsInArrayTest();
    jsonEncodePlusTest();
    jsonDecodePlusTest();
    arrayUnsetAllKeysTest();
    errInvalidRequestMethodTest();
    fileDeleteTest();
    fileDeleteIfExistsTest();
    fileAppendTextTest();
    fileExtractDirTest();
    strToBoolTest();
    dirEnsureSlashTest();
    //emailSendTest();
}
//==============================================================================
function tableToCsv($table, $createColumnHeaders = true) {
    $csvStr = '';
    if($createColumnHeaders) {
        $firstRow = $table[0];
        $csvHeader = '';
        foreach($firstRow as $key=>$value) {
            $csvHeader = $csvHeader.$key.',';
        }
        $csvHeader = strDeleteSuffix(',', $csvHeader);
        $csvStr = $csvStr.$csvHeader;
        $csvStr = $csvStr."\n";
    }
    foreach($table as $tableRow) {
        $csvRow = '';
        foreach($tableRow as $value) {
            $encodedValue = jsonEncodePlus($value);
            if($encodedValue === 'null') {
                $encodedValue = '""';
            }
            $csvRow = $csvRow.$encodedValue.',';
        }
        $csvRow = strDeleteSuffix(',', $csvRow);
        $csvStr = $csvStr.$csvRow;
        $csvStr = $csvStr."\n";
    }
    $csvStr = strDeleteSuffix("\n", $csvStr);
    return $csvStr;
}
//==============================================================================
function filesOlderThan($fileList, $daysOld) {
	errIfNullOrEmptyStr($daysOld);
	$secsPerDay = 60*60*24;
	$result = [];
	foreach($fileList as $filePath) {
		$fileDateTime = filemtime($filePath);
		$currentDateTime = time();
		$secondesPassed = $currentDateTime - $fileDateTime;
		$daysPassed = $secondesPassed/$secsPerDay;
		if($daysPassed > $daysOld) {
			$result[] = $filePath;
		}
	}
	return $result;
}
//==============================================================================
function fileOrDirsDelete($fileList) {
	errIfNull($fileList, 'File list not provided');
	foreach($fileList as $filePath) {
		fileOrDirDelete($filePath);
	}
}
//------------------------------------------------------------------------------
function fileOrDirFind($mask, $recurse = false, $includeDirs = false, $includeFiles = true) {
    $results = [];
	$dir = fileExtractDir($mask);
	if($dir != '') {
		verify(file_exists($dir), 'directory "'.$dir.'" not found');
	}
    $fileEntities = glob($mask, GLOB_MARK+GLOB_ERR);
    errIf($fileEntities === false, 'finding files failed');
    foreach($fileEntities as $fileEntity) {
        if($includeDirs && strHasSuffix(DIRECTORY_SEPARATOR, $fileEntity)) {
            $results[] = $fileEntity;
        }
        if($includeFiles && !strHasSuffix(DIRECTORY_SEPARATOR, $fileEntity)) {
            $results[] = $fileEntity;
        }
    }

    if($recurse) {
        $maskDirPart = fileExtractDir($mask);
        $maskFilePart = fileExtractName($mask);
        $dirs = glob($maskDirPart."*", GLOB_MARK | GLOB_ONLYDIR);
        errIf($dirs === false, 'finding dirs failed');
        foreach($dirs as $dir) {
            $subDirResults = fileOrDirFind($dir.$maskFilePart, $recurse, $includeDirs, $includeFiles);
            $results = array_merge($results, $subDirResults);
        }
    }
    return $results;
}
//------------------------------------------------------------------------------
function dirEnsureExists($dirName, $recursive = false) {
    if(!file_exists($dirName)) {
		$result = mkdir($dirName, 0777, $recursive);
        verify($result, "failed to make directory $dirName");
        $result = file_exists($dirName);
        verify($result, "failed to make directory $dirName");
    }
}
//------------------------------------------------------------------------------
function fileCopy($srcPath, $destDir, $overwriteDest = false) {//reviewed to here on navigator
    verify(file_exists($srcPath), "file '$srcPath' not found");
	$destDir = dirEnsureSlash($destDir);
    verify(file_exists($destDir), "directory $destDir not found");
	$destFileName = fileExtractName($srcPath);
    $destPath = $destDir.$destFileName;
	errIf(file_exists($destPath) && (!$overwriteDest), "dest file $destPath exists");
	$result = copy($srcPath, $destPath);
	verify($result, "fileCopy from $srcPath to $destPath failed");
}
//------------------------------------------------------------------------------
function fileCopyRename($srcPath, $destPath, $overwriteDest = false) {//reviewed to here on navigator
    verify(file_exists($srcPath), "file '$srcPath' not found");
    $destDir = fileExtractDir($destPath);
    verify(file_exists($destDir), "directory $destDir not found");
    errIf(file_exists($destPath) && (!$overwriteDest), "dest file $destPath exists");

    $result = copy($srcPath, $destPath);
    verify($result, "fileCopyRename from $srcPath to $destPath failed");
}
//------------------------------------------------------------------------------
function fileMove($srcPath, $destDir, $overwriteDest = false) {//reviewed to here on navigator
    verify(file_exists($srcPath), "file '$srcPath' not found");
	$destDir = dirEnsureSlash($destDir);
    verify(file_exists($destDir), "directory $destDir not found");
	$destFileName = fileExtractName($srcPath);
    $destPath = $destDir.$destFileName;
	errIf(file_exists($destPath) && (!$overwriteDest), "dest file $destPath exists");
	$result = rename($srcPath, $destPath);
	verify($result, "fileMove from $srcPath to $destDir failed");
}
//------------------------------------------------------------------------------
function fileMoveRename($srcPath, $destPath, $overwriteDest = false) {//reviewed to here on navigator
    verify(file_exists($srcPath), "file '$srcPath' not found");
    $destDir = fileExtractDir($destPath);
    verify(file_exists($destDir), "directory $destDir not found");
    errIf(file_exists($destPath) && (!$overwriteDest), "dest file $destPath exists");

    $result = rename($srcPath, $destPath);
    verify($result, "fileMoveRename from $srcPath to $destPath failed");
}
//------------------------------------------------------------------------------
function fileRename($startPath, $newName, $overwriteDest = false) {//reviewed to here on navigator
    verify(file_exists($startPath), "file '$startPath' not found");
    verify(fileExtractDir($newName) == '', "new file name $newName cannot be a directory");

    $dir = fileExtractDir($startPath);
    $newPath = $dir.$newName;
    errIf(file_exists($newPath) && (!$overwriteDest), "file $newPath exists");
	$result = rename($startPath, $newPath);
	verify($result, "fileRename from $startPath to $newPath failed");
}
//==============================================================================
function dirMoveContents($srcDir, $destDir, $overwriteDestFiles = false, $recurse = false) {
    $srcDir = dirEnsureSlash($srcDir);
    $destDir = dirEnsureSlash($destDir);
    $srcList = fileOrDirFind($srcDir.'*.*', $recurse);
    foreach($srcList as $srcPath) {
        $pathFromSrcDir = strDeletePrefix($srcDir, $srcPath);
        $destPath = $destDir.$pathFromSrcDir;
        $finalDestDir = fileExtractDir($destPath);
        dirEnsureExists($finalDestDir);
        fileMove($srcPath, $finalDestDir, $overwriteDestFiles);
    }
}
//------------------------------------------------------------------------------
function serverName() {
    return $_SERVER['SERVER_NAME'];
}
//------------------------------------------------------------------------------
function isDirWritable($dir) { // todo review
    $path = dirEnsureSlash($dir)."temp.txt";
    try {
        fileWriteText("", $path);
    }
    catch(Exception $e) {
        return false;
    }
    fileDelete($path);  // should this be inside the try block?
    return true;
}
//------------------------------------------------------------------------------
//Not Officially part of onyx
//-----------------------------------------------------------------
function fileServerToDriveHack($path, $serverName=null, $driveLetter='e:') {
    if(file_exists($path)) {
        return $path;
    }
	if($serverName === null) {
		$serverName = serverName();
	}
    $driveLetterPlusColon = strEnsureSuffix(':', $driveLetter);
    $path = str_ireplace('\\\\'.$serverName, $driveLetterPlusColon, $path);
	$path = fileUseNativeSlashes($path);
	return $path;
}
//------------------------------------------------------------------------------
//not sure what to do here
//------------------------------------------------------------------------------
function configGet($settingName, $configFileName = 'config.ini', $sectionName = '') {
    verify(File_Exists($configFileName), "Config file not found");
    errIf(isNullOrEmptyStr($settingName), 'SettingName may not be null');

    if(isNullOrEmptyStr($sectionName)) {
        $settings = parse_ini_file($configFileName, false);
        errIf($settings === false, ".ini non-section parse failed where setting = $settingName in $configFileName");
    }
    else {
        $sections = parse_ini_file($configFileName, true);
        errIf($sections === false, ".ini section parse failed where setting = $settingName and section = $sectionName in $configFileName");
        // verify(arrayKeyExists($sections, $sectionName), 'Section '.$sectionName.' not found in ');
        $settings = arrayGet($sections, $sectionName);
    }
    // verify(arrayKeyExists($settings, $settingName), 'Setting '.$settingName.' not found in section '.$sectionName);
    $setting = arrayGet($settings, $settingName);
    return $setting;
}
