<?php
if(defined('onyxYii.php')) {
    return;
}
define('onyxYii.php', '');
//--------------------------------------------------------------------------
function queryScalar($sql) {
    return Yii::app()->db->createCommand($sql)->queryScalar();
}
//--------------------------------------------------------------------------
function queryColumn($sql) {
    return Yii::app()->db->createCommand($sql)->queryColumn();
}
//--------------------------------------------------------------------------
function queryRow($sql) {
    return Yii::app()->db->createCommand($sql)->queryRow();
}
//--------------------------------------------------------------------------
function queryAll($sql) {
    return Yii::app()->db->createCommand($sql)->queryAll();
}
//--------------------------------------------------------------------------
function executeSql($sql) {
    return Yii::app()->db->createCommand($sql)->execute();
}
//--------------------------------------------------------------------------
function activeRecordsToAttributes(&$value) {
    if(is_a($value, 'CActiveRecord')) {
        $value = $value->attributes;
    }
    if(is_array($value)) {
        foreach($value as &$element) {
            activeRecordsToAttributes($element);
        }
    }
}
//--------------------------------------------------------------------------
function jsonEncodePlusYii($value) {
    activeRecordsToAttributes($value);
    return jsonEncodePlus($value);
}
//------------------------------------------------------------------------------
function toDebugStrYii($value) {//
    if(is_array($value)) {
        return arrayToDebugStr($value);
    }
    else if(is_string($value)) {
        return '"'.$value.'"';
    }
    else {
        return jsonEncodePlusYii($value);
    }
}
//------------------------------------------------------------------------------
function fileLogDebugYii($value, $fileName = 'log.txt') {
    $debugStr = toDebugStrYii($value);
    fileLog($debugStr, $fileName, 2);
}
//--------------------------------------------------------------------------
/* function jsonEncodeActiveRecord($record) {
  if(is_null($record)) {
  return jsonEncodePlus($record);
  }
  else if(is_a($record, 'CActiveRecord')) {
  return jsonEncodePlus($record->attributes);
  }
  else {
  err('value must be null or CActiveRecord');
  }
}
//--------------------------------------------------------------------------
  function jsonEncodeActiveRecords($records) {
  $result = '[';
  foreach($records as $record) {
  $result = $result.jsonEncodeActiveRecord($record).',';
  }
  $result = strDeleteSuffix(',', $result);
  $result = $result.']';
  return $result;
  } */
//--------------------------------------------------------------------------
function testRecordMatches($fields, $testName = 'saved data test') {
    $record = echoedOutputJsonDecode();
    if(is_string($fields)) {
        $fields = jsonDecodePlus($fields);
    }
    $diff = array_diff_assoc($fields, $record);
    if($diff === []) {
        testTrue(true, $testName);
    }
    else {
        testTrue(false, toDebugStr($diff));
    }
}
//--------------------------------------------------------------------------
function matchValue($columnValueArray) {
    $sql = '';
    foreach($columnValueArray as $key=>$value) {
        if(is_string($value)) {
            $value = sqlSubStrSearch($value);
            $sql .= " upper($key) LIKE $value ";
        }
        else {
            $sql .= " $key = $value ";
        }
        $sql .= ' AND ';
    }
    $sql = strDeleteSuffix(' AND ', $sql);
    return $sql;
}
//--------------------------------------------------------------------------
function matchDateRange($dateRangeArray) {
    $sql = '';
    foreach($dateRangeArray as $key=> $value) {
        $sql .= "$key >= "."to_date('".$value[0]."','YYYY-MM-DD')".
                " AND $key <= "."to_date('".$value[1]."','YYYY-MM-DD')";
        $sql .= ' AND ';
    }
    $sql = strDeleteSuffix(' AND ', $sql);
    return $sql;
}
//--------------------------------------------------------------------------
function matchSetValue($setArray) {
    $sql = '';
    foreach($setArray as $key=>$value) {
        $sql .= "$key in ('";
        foreach($value as $val) {
            $sql .= $val."','";
        }
        $sql = strDeleteSuffix(",'", $sql);

        $sql .= ') AND ';
    }
    $sql = strDeleteSuffix(' AND ', $sql);
    return $sql;
}
//--------------------------------------------------------------------------
function findAllCaseInsensitiveSubstring($tableName, $columnValueArray) {
    $sql = "SELECT * FROM ".$tableName." WHERE ";
    $sql .= matchValue($columnValueArray);
    $result = queryAll($sql);
    return $result;
}
//--------------------------------------------------------------------------
//todo make this a little more generic should this use the matchSetValue function?
function countAllCaseInsensitiveSubstring($tableName, $columnValueArray) {
    $sql = "SELECT count(*) FROM ".$tableName." WHERE ";
    foreach($columnValueArray as $key=> $value) {
        if(is_string($value)) {
            $value = sqlSubStrSearch($value);
            $sql .= " upper($key) LIKE $value ";
        }
        else {
            $sql .= " $key = $value ";
        }
        $sql .= ' AND ';
    }
    $sql = strDeleteSuffix(' AND ', $sql);
    $result['result'] = (int) queryScalar($sql);
    return $result;
}
//--------------------------------------------------------------------------
function defaultGet($model) {
    valRequestMethodGet();
    $count = arrayGet($_GET, 'count', false); //todo change this to countFlag or something
    $count = strToBool($count);
    $exactMatch = arrayGet($_GET, 'exactMatch', false);
    $exactMatch = strToBool($exactMatch);
    $filter = arrayGet($_GET, 'filter', null);
    $filter = jsonDecodePlus($filter);
    if(!is_null($filter)) {
        $filter = array_change_key_case($filter, CASE_UPPER);
        if($count) {
            if($exactMatch === true) {
                err('count on exact match not supported yet');
            }
            else {
                $result = countAllCaseInsensitiveSubstring($model->tableName(), $filter);
            }
            echo jsonEncodePlus($result);
        }
        else {
            if($exactMatch === true) {
                $result = $model->findAllByAttributes($filter);
            }
            else {
                $result = findAllCaseInsensitiveSubstring($model->tableName(), $filter);
            }
            echo jsonEncodePlusPlus($result);
        }
    }
    else {
        if($count === true) {
            $modelTableName = $model->tableName();
            $recordCount = (int) queryScalar("select count(*) from $modelTableName");
            echoStandardFormat($recordCount);
        }
        else {
            $result = $model->findAll();
            echo jsonEncodePlusPlus($result);
        }
    }
}
//------------------------------------------------------------------------------
function errHttp($errMessage, $httpCode = 400) {
    errIfNull($httpCode, 'Http code provided is null');
    throw new CHttpException($httpCode, $errMessage, $httpCode);
}
//------------------------------------------------------------------------------
/*
  //--------------------------------------------------------------------------
  function AstroidBelt.giveDamage($obj)
  {
  $obj.hitpoints = $obj.hitpoints- 5;
  //remember all ships it ever hurt in some array or something
  //also remember the hit points before damage
  }
  //--------------------------------------------------------------------------
  /*function AstroidBelt.strongestShipToEnter(&$strongestShip)
  {
  strongestSpaceShip = //find the best ship to enter in some way that it remebers
  return //find the best hitpoints before damage
  }

  $spaceShip1.hitpoints = 7;
  $spaceShip2.hitpoints = 77;
  $spaceShip3.hitpoints = 777;
  $astroidBelt.giveDamage($spaceShip1);
  $astroidBelt.giveDamage($spaceShip3);

  $strongestHitPoints = $astroidBelt.strongestShipToEnter($strongestSpaceShip);
 */
