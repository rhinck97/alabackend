<?php
class OnyxPdf extends TCPDF {
    //----------------------------------
    /*
      This is, basically, a for loop around MultiCellByRatio (note that's CELL not CELLS).
      It'll set the font-family and color using data inside the variable $cellsInfo and
      functions inherited from TCPDF.  It then calls MultiCellByRatio, defined below.
     */
    function MultiCellsByRatio($cellsInfo, $pdfHeight, $spacingAdjustmentRatio) {
        foreach($cellsInfo as $cellInfo) {
            $this->setFont(arrayGet($cellInfo, 'fontFamily', 'Helvetica'), arrayGet($cellInfo, 'fontStyle', ''), round(arrayGet($cellInfo, 'fontSize') * $pdfHeight * 72));
//
//            //TEST STUFF
//            $this->setFont('malgunbd','',round(arrayGet($cellInfo, 'fontSize') * $pdfHeight * 72),'',false);
//            //END TEST STUFF
//            
            $heightPerSpacingInInches = .1875;  //This number is subject to change
            $spacingInInches = (arrayGet($cellInfo, 'fontSize') * $pdfHeight) * $heightPerSpacingInInches;
            $spacingAdjustment = ($spacingInInches * $spacingAdjustmentRatio) - $spacingInInches;
            $this->setFontSpacing($spacingAdjustment);
            $alignment = arrayGet($cellInfo, 'alignment', 'L');
            $this->SetTextColor(arrayGet($cellInfo, 'fontR', 0), arrayGet($cellInfo, 'fontG', 0), arrayGet($cellInfo, 'fontB', 0));

            $this->MultiCellByRatio(arrayGet($cellInfo, 'cellWidth'), arrayGet($cellInfo, 'cellHeight'), arrayGet($cellInfo, 'cellText'), 0, $alignment, false, 1, arrayGet($cellInfo, 'cellLeft'), arrayGet($cellInfo, 'cellTop'));
        }
    }
    //----------------------------------
    /*
      MultiCellByRatio is a slightly more friendly encapsulation of TCPDF's MultiCell function,
      which places text inside a cell.

      $width: a percentange of the document's width (in decimal form)
      $height: a percentage of the document's height (in decimal form)
      $text: the string to be placed on the PDF
      $border: the size of the border around the cell (much like CSS border)
      $align: a char denoting how to align the text.  'L' for left align, 'R' for right, and so on
      $fill: boolean, indicates if the cell background must be painted (true) or transparent (false)
      $ln: (int) Indicates where the current position should go after the call. Possible values are:
      0: to the right
      1: to the beginning of the next line
      2: below
      $left: the left position of the top-left corner of the cell - is a percentage of the document's width
      $top: the top position of the top-left corner of the cell - is a percentage of the document's height

      TCPDF's MultiCell function accepts addtional variables, but we've yet to support or need them
     */
    function MultiCellByRatio($width, $height, $text, $border, $align, $fill, $ln, $left, $top) {
        $this->MultiCell(($this->getPageWidth()), $height * $this->getPageHeight(), $text, $border, $align, $fill, $ln, $left * $this->getPageWidth(), $top * $this->getPageHeight());
    }
    //----------------------------------
    /*
      ImageByRatio is a slightly more friendly encapsulation of TCPDF's Image function,
      which places an image on the PDF.

      $file: path to the image.
      $left: the left position of the top-left corner of the cell - is a percentage of the document's width
      $top: the top position of the top-left corner of the cell - is a percentage of the document's height
      $w: a percentange of the document's width (in decimal form)
      $h: a percentage of the document's height (in decimal form)
      $type:
      $link:
      $align: a char denoting how to align the text.  'L' for left align, 'R' for right, and so on
      $resize:
      $dpi:
      $palign:
      $ismask:
      $imgmask:
      $border:
      $fitbox:
      $hidden:
      $fitonpage:
      $alt:

      TCPDF's MultiCell function accepts addtional variables, but we've yet to support or need them
     */
    function ImageByRatio($file, $left = '', $top = '', $w = 0, $h = 0, $type = '', $link = '', $align = '', $resize = false, $dpi = 300, $palign = '', $ismask = false, $imgmask = false, $border = 0, $fitbox = false, $hidden = false, $fitonpage = false, $alt = false) {
        $this->Image($file, $left * $this->getPageWidth(), $top * $this->getPageHeight(), $w * $this->getPageWidth(), $h * $this->getPageHeight(), $type, $link, $align, $resize, $dpi, $palign, $ismask, $imgmask, $border, $fitbox, $hidden, $fitonpage, $alt);
    }
    //----------------------------------
    /*
      This is, basically, a for loop around ImageByRatio (note that's IMAGE not IMAGES).
      It allows the programmer to have an array of images they wish to place in a document.
     */
    function ImagesByRatio($images) {
        foreach($images as $image) {
            $this->ImageByRatio("protected/images/".arrayGet($image, 'lang')."/".arrayGet($image, 'name'), arrayGet($image, 'left'), arrayGet($image, 'top'), arrayGet($image, 'width'), arrayGet($image, 'height'));
        }
    }
    //----------------------------------
    function ColorBoxByRatio($colorBoxes) {
        foreach($colorBoxes as $colorBox) {
            $this->Rect($colorBox['left'] * $this->getPageWidth(), $colorBox['top'] * $this->getPageHeight(), $colorBox['width'] * $this->getPageWidth(), $colorBox['height'] * $this->getPageHeight(), 'F', '', $fill_color = array($colorBox['r'], $colorBox['g'], $colorBox['b']));
        }
    }
    function setBleeds($bleedSize, $width, $height) {
        $format['MediaBox'] = [];
        $format['MediaBox']['llx'] = 0;
        $format['MediaBox']['lly'] = 0;
        $format['MediaBox']['urx'] = $width;
        $format['MediaBox']['ury'] = $height;

        $format['CropBox'] = [];
        $format['CropBox']['llx'] = 0;
        $format['CropBox']['lly'] = 0;
        $format['CropBox']['urx'] = $width;
        $format['CropBox']['ury'] = $height;

        $format['BleedBox'] = [];
        $format['BleedBox']['llx'] = 0;
        $format['BleedBox']['lly'] = 0;
        $format['BleedBox']['urx'] = $width;
        $format['BleedBox']['ury'] = $height;

        $format['ArtBox'] = [];
        $format['ArtBox']['llx'] = 0;
        $format['ArtBox']['lly'] = 0;
        $format['ArtBox']['urx'] = $width;
        $format['ArtBox']['ury'] = $height;

        $format['TrimBox'] = [];
        $format['TrimBox']['llx'] = $bleedSize;
        $format['TrimBox']['lly'] = $bleedSize;
        $format['TrimBox']['urx'] = $width - $bleedSize;
        $format['TrimBox']['ury'] = $height - $bleedSize;

        $orientation = ($height > $width) ? 'P' : 'L';

        $this->setPageFormat($format, $orientation);
    }
}
//-----------------------------------------------
/*

 */
function createPdf($fileName, $unit, $width, $height, $bleedSize, $textCells, $images, $colorBoxes, $fontAdjust, $fontAdjustmentRatio, $pages, $doubleSided) {
    $orientation = ($height > $width) ? 'P' : 'L';
    $pdf = new OnyxPdf($orientation, $unit, [$height, $width], 'UTF-8', false);
    $pdf->setBleeds($bleedSize, $width, $height);

    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);
    $pdf->setCellPaddings(0, 0, 0, 0);
    $pdf->setCellMargins(0, 0, 0, 0);
    $pdf->SetAutoPageBreak(false, 0);
    $pdf->AddPage();
    $pdf->ColorBoxByRatio($colorBoxes);
    $pdf->ImagesByRatio($images);
    $pdf->MultiCellsByRatio($textCells, $height, $fontAdjustmentRatio);
    if($doubleSided == "true") {
        if(is_array($pages)) {
            fileLogDebug($pages);
            foreach($pages as $page) {
                $pdf->AddPage();
                $pdf->ImageByRatio("protected/images/".arrayGet($page, 'lang')."/".arrayGet($page, 'name'), arrayGet($page, 'left'), arrayGet($page, 'top'), arrayGet($page, 'width'), arrayGet($page, 'height'));
            }
        }
        else {
            //Maybe throw a useful error here
            fileLogDebug(gettype($pages));
        }
    }
    $pdf->Output($fileName, 'F');
    $pdf->Output("latest.pdf", 'F');
}
