<?php
if(defined('onyxYiiCore.php'))
    return;
define('onyxYiiCore.php', '');
define("cIsoDateTimeFormat", 'Y-m-d H:i:s');
define('cStrDigitChars', '1234567890');
define('cStrSymbolChars', '!@#$%^&*()-+={[}]|\:;"<,>.?/'."'");
define("cIsoDateTimeRegEx", "/^([0-9]{4})-([0-9]{2})-([0-9]{2})([ ]|$|T[0-9]{2}:[0-9]{2}:[0-9]{2})/");
//-----------------------------------------------------------------------------------
function strFormatPrice($oldPrice) {
    return '$'.number_format($oldPrice, 2, '.', ',');
}
//-----------------------------------------------------------------------------------
function strFormatPriceNoDollarSign($oldPrice) {
    return number_format($oldPrice, 2, '.', ',');
}
//=========================================================================
function isValidEmailAddress($email) {
    $pattern = '/^[a-zA-Z0-9!#$%&\'*+\\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&\'*+\\/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/';
    $emails = explode(',', $email);
    foreach($emails as $email) {
        if(preg_match($pattern, $email) == 0)
            return false;
    }
    return true;
}
//=========================================================================
function httpLoad($URL, $data = null, $verb = 'POST') {//serious cleanup needed xxx
    $URL_Info = parse_url($URL);

    if($data != "") {
        foreach($data as $key=> $value)
            $values[] = "$key=".urlencode($value);
        $data_string = implode("&", $values);
    }
    else
        $data_string = "";

    if(!isset($URL_Info["port"]))
        $URL_Info["port"] = 80;

    $path = isset($URL_Info["path"]) ? $URL_Info["path"] : "/";
    /* echoValue($data); */
    // building POST-request:
    $request = "$verb ".$path." HTTP/1.1\n";
    $request.="Host: ".$URL_Info["host"]."\n";
    $request.="Referer: \n";
    if($verb == 'POST') {
        $request.="Content-type: application/x-www-form-urlencoded\n";
        $request.="Content-length: ".strlen($data_string)."\n";
    }
    $request.="Connection: close\n";
    $request.="\n";
    $request.=$data_string."\n";

    $sock = fsockopen($URL_Info["host"], 80);

    fwrite($sock, $request);
    $result = '';
    while(!feof($sock)) {
        $result .= fgets($sock);
    }
    fclose($sock);
    return $result;
}
//=========================================================================
function arraySet(&$array, $key, $value, $errIfKeyMissing = false) {
    valArrayAndKey($array, $key);
    $trueKey = arrayTrueKey($array, $key, $errIfKeyMissing);
    if(is_null($trueKey))
        $trueKey = $key;
    $array[$trueKey] = $value;
}
//=========================================================================
function requestGet($key, $defaultValue = cNoDefault) {
    return arrayGet($_REQUEST, $key, $defaultValue);
}
//=========================================================================
function strDeleteCharSet($charSetToDelete, $string) {
    $length = sizeof($charSetToDelete);
    if(is_string($charSetToDelete))
        $length = strlen($charSetToDelete);
    for($i = 0; $i < $length; $i++)
        $string = str_replace($charSetToDelete[$i], "", $string);
    return $string;
}
//------------------------------------------------------------------------------
function strDeleteCharSetTest() {
    testTrue(strDeleteCharSet(array('#', '$', '"'), '#this$is"a"#test"') == 'thisisatest', 'StrDeleteCharSet failed basic test');
    testTrue(strDeleteCharSet(array(), 'thisisatest') == 'thisisatest', 'StrDeleteCharSet failed empty set test');
    testTrue(strDeleteCharSet(cStrUndesiredChars, 'c"St&r?Und#esire?dC=h#a&rs') == 'cStrUndesiredChars', 'StrDeleteCharSet Another test');
    testTrue(strDeleteCharSet(array('d'), '') == '', 'StrDeleteCharSet failed nil string test');
}
//=========================================================================
function strToCamelCase($string) {
    $string = strtolower($string);
    $string = ucwords($string);
    $string[0] = strtolower($string[0]);
    $string = strDelete(" ", $string);
    return $string;
}
//=========================================================================
function echoLine($value) {
    echo "$value<br>";
}
//------------------------------------------------------------------------------
function echoLineTest() {
    testEcho('echoLine("Echo with a line break")', 'Echo with a line break');
    echoLine("Echo with a line break");
}
//=========================================================================
/**
 * dateIsExpired will tell you if the date put in is expired
 * @param String $eString - The string for the expiration date
 * @return boolean - if expired returns true, else returns false
 */
function dateIsExpired($eString) {
    $c = new DateTime();
    $e = new DateTime($eString);
    //I think there is a simpler way to do this. 
    //return $c->getTimeStamp() > $e->getTimeStamp //Sterling
    $cYear = (int) $c->format('Y');
    $eYear = (int) $e->format('Y');
    $cMonth = (int) $c->format('m');
    $eMonth = (int) $e->format('m');
    $cDay = (int) $c->format('d');
    $eDay = (int) $e->format('d');

    if($cYear > $eYear)
        return true;
    else if($cYear < $eYear)
        return false;
    else {
        if($cMonth > $eMonth)
            return true;
        else if($cMonth < $eMonth)
            return false;
        else {
            if($cDay < $eDay)
                return false;
            else
                return true;
        }
    }
}
//=========================================================================
function requestIsGet() {
    return count($_GET) > 0;
}
//=========================================================================
function requestIsPost() {
    return count($_POST) > 0;
}
//--------------------------------------------------------------------------
function requestQuery() {
    $thisQuery = '';
    if(requestIsGet())
        $thisQuery = http_build_query($_GET);
    else if(requestIsPost())
        $thisQuery = http_build_query($_POST);
    if($thisQuery != '')
        $thisQuery = "?$thisQuery";
    return $thisQuery;
}
//=========================================================================
function jsonEncodePlus($value) {
    if(is_array($value))
        array_walk_recursive($value, 'isoDateTimeEncodeRef');
    else
        isoDateTimeEncodeRef($value);
    $value = json_encode($value);
    return $value;
}
//=========================================================================
function isoDateTime() {
    return date('Y-m-d H:i:s');
}
//=========================================================================
function isoDateTimeEncodeRef(&$value) {
    if($value instanceof DateTime)
        $value = $value->format(cIsoDateTimeFormat);
}
//=========================================================================
function renderServiceOutput($data) {//todo: this shouln't be here
    Yii::app()->getController()->renderPartial('../system/jsonOutput', array("data"=>$data));
}
//------------------------------------------------------------------------------
function fileExtractNameNoExt($fullPath) {
    return fileDeleteExt(fileExtractName($fullPath));
}
//------------------------------------------------------------------------------
function fileExtractName($fullPath) {
    return basename($fullPath);
}
//------------------------------------------------------------------------------
function fileExtractExt($fullPath) {
    $extension = pathinfo($fullPath, PATHINFO_EXTENSION);
    if(isNullOrEmptyStr($extension))
        return '';
    else
        return ".$extension";
}
//------------------------------------------------------------------------------
function fileCleanFileName($fileName) {
    $value = str_ireplace(
            array(" ", "'", "+", "#", "\"", "%", "&", "*", ":", ";", "<", ">", "?", "\\", "/", "{", "}", "|", "~"), array("_"), $fileName);
    return $value;
}
//=========================================================================
function httpErr($exception) {
    $errMessage = $exception->GetMessage();
    $errInfo = get_class($exception).': '.$errMessage;
    header("HTTP/1.0 400 $errInfo");
}
//-------------------------------------------------------
function isoDateTimeDecodeRef(&$value) {
    if(isoDateTimeMatch($value, $parts))
        $value = new DateTime($value);
}
//-------------------------------------------------------
function isoDateTimeMatch($value, &$parts) {
    if(is_string($value) && preg_match(cIsoDateTimeRegEx, $value, $parts))
        return true;
    else
        return false;
}
?>