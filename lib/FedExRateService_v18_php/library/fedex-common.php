<?php
define('Newline', "<br />");
//----------------------------------------------
function printReply($client, $response) {
    $highestSeverity = $response->HighestSeverity;
    if($highestSeverity == "SUCCESS") {
        echo '<h2>The transaction was successful.</h2>';
    }
    if($highestSeverity == "WARNING") {
        echo '<h2>The transaction returned a warning.</h2>';
    }
    if($highestSeverity == "ERROR") {
        echo '<h2>The transaction returned an Error.</h2>';
    }
    if($highestSeverity == "FAILURE") {
        echo '<h2>The transaction returned a Failure.</h2>';
    }
    echo "\n";
    printNotifications($response->Notifications);
    printRequestResponse($client, $response);
}
//----------------------------------------------
function printRequestResponse($client) {
    echo '<h2>Request</h2>'."\n";
    echo '<pre>'.htmlspecialchars($client->__getLastRequest()).'</pre>';
    echo "\n";

    echo '<h2>Response</h2>'."\n";
    echo '<pre>'.htmlspecialchars($client->__getLastResponse()).'</pre>';
    echo "\n";
}
//----------------------------------------------
function printFault($exception, $client) {
    echo '<h2>Fault</h2>'."<br>\n";
    echo "<b>Code:</b>{$exception->faultcode}<br>\n";
    echo "<b>String:</b>{$exception->faultstring}<br>\n";
    writeToLog($client);

    echo '<h2>Request</h2>'."\n";
    echo '<pre>'.htmlspecialchars($client->__getLastRequest()).'</pre>';
    echo "\n";
}
//----------------------------------------------
function writeToLog($client) {
    /**
     * __DIR__ refers to the directory path of the library file.
     * This location is not relative based on Include/Require.
     */
    if(!$logfile = fopen(__DIR__.'/fedextransactions.log', "a")) {
        error_func("Cannot open ".__DIR__.'/fedextransactions.log'." file.\n", 0);
        exit(1);
    }
    fwrite($logfile, sprintf("\r%s:- %s", date("D M j G:i:s T Y"), $client->__getLastRequest()."\r\n".$client->__getLastResponse()."\r\n\r\n"));
}
//----------------------------------------------
function printNotifications($notes) {
    foreach($notes as $noteKey=> $note) {
        if(is_string($note)) {
            echo $noteKey.': '.$note.Newline;
        }
        else {
            printNotifications($note);
        }
    }
    echo Newline;
}
?>