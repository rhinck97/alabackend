<?php
function returnLanguageCaptions($languageCode){
    if ($languageCode == 'en') {
        $languageInformation = [];
        $languageInformation['title'] ='Order Receipt';
        $languageInformation['buttonPrintFriendly'] ='Printer Friendly Version';
        $languageInformation['thanksText'] ='Thank you, ';
        $languageInformation['thanksTextPt2'] =' for shopping with us. Your order summary is listed below.';
        $languageInformation['subtotal'] ='SubTotal: ';
        $languageInformation['shipping'] ='Shipping: ';
        $languageInformation['shippingMethod'] ='Shipping Method: ';
        $languageInformation['tax'] ='Tax: ';
        $languageInformation['total'] ='Total: ';
        $languageInformation['moneySymbol'] ='$';
        $languageInformation['paymentType'] ='Payment Type: ';
        $languageInformation['businessUnitNumber'] ='Business Unit Number: ';
        $languageInformation['businessUnitID'] ='Business Unit ID: ';
        $languageInformation['departmentID'] ='Department ID: ';
        $languageInformation['accountCode'] ='Account Code: ';
        $languageInformation['projectCode'] ='Project Code: ';
        $languageInformation['activityCode'] ='Activity Code: ';
        $languageInformation['unitNumber'] ='Unit Number: ';
        $languageInformation['leaderLastName'] ='Leader Last Name: ';
        $languageInformation['firstName'] ='First Name: ';
        $languageInformation['lastName'] ='Last Name: ';
        $languageInformation['email'] ='Email: ';
        $languageInformation['phone'] ='Phone: ';
        $languageInformation['addressWithSpace'] ='Address ';
        $languageInformation['information'] ='Information: ';
        $languageInformation['itemsShippingToAddress'] ='Items Shipping To This Address: ';
        $languageInformation['itemQuantityInfo'] ='Item Quantity Information: ';
        $languageInformation['paymentInfo'] ='Payment Info';
        $languageInformation['noShippingInfo'] ='No Shipping Information';
        $languageInformation['shippingInfo'] ='Shipping Information';
        $languageInformation['address'] ='Address';
        $languageInformation['information'] ='Information';
        $languageInformation['orderLink'] ='Order Link: ';
        $languageInformation['jobNumber'] ='Job Number';

        //delivery methods
        $languageInformation['NEXTDAYPRIORITY'] ='Next Business Day Priority';
        $languageInformation['STANDARD'] ='Standard Shipping';
        $languageInformation['DELIVER'] ='Campus Delivery';
        $languageInformation['DELIVERINTERNALLY'] ='Interoffice Delivery';
        $languageInformation['NEXTDAYSTANDARD'] ='Next Business Day Standard';
        $languageInformation['INTLECONOMY'] ='International Economy';
        $languageInformation['PICKUP'] ='Customer Pickup';
        $languageInformation['PREPAID'] ='Prepaid Shipping Label';
        $languageInformation['INTLPRIORITY'] ='International Priority';
        $languageInformation['SECONDDAY'] ='Second Day';

        //payment methods
        $languageInformation['CF'] ='Church Unit No.';
        $languageInformation['CASHNET'] ='Credit Card';
        $languageInformation['DEPTACCT'] ='Department Account Number';
        $languageInformation['BT'] ='Budget Transfer';
        $languageInformation['CASH'] ='Pay On Pickup';
        $languageInformation['NB'] ='Not Billed';
        $languageInformation['AC'] ='Account Code';
        $languageInformation['BUN'] ='Business Unit Number';
    }
    return $languageInformation;
}