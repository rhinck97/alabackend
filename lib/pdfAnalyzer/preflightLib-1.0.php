<?php
require_once dirname(__FILE__).'/../onyx/onyxCore.php';
//==============================================================================
function runPreflight($pdfPath) {
    $fontLists = getFontList($pdfPath);
    $embedded = $fontLists[0];
    $notEmbedded = $fontLists[1];
    $pagesLists = getColorPages($pdfPath);
    $colorPages = $pagesLists[1];
    $bwPages = $pagesLists[0];

	$landscapePages = getOrientation($pdfPath);
	$allPortraitPages = true;
    $result = array();
    $result['bwPages'] = $bwPages;
    $result['colorPages'] = $colorPages;
    $result['embedded'] = $embedded;
    $result['notEmbedded'] = $notEmbedded;
    $result['bwPageCount'] = sizeof($bwPages);
    $result['colorPageCount'] = sizeof($colorPages);
    $result['totalPageCount'] = sizeof($bwPages) + sizeof($colorPages);
    $result['embeddedFontCount'] = sizeof($embedded);
    $result['notEmbeddedFontCount'] = sizeof($notEmbedded);
	$result['landscapePages'] = $landscapePages;
    return $result;
}
//==============================================================================
function getOrientation($pdfPath) {
	$retArray = array();
	$noLandscape = true;
    exec("./oriental \"".$pdfPath."\"", $retArray);
	// fileLogValue($retArray);
	$landscapeArr = array();
	$dimensionArr = array();
	foreach ($retArray as $e)
	{
        if (strpos($e, 'size') !== false)
        {
        	$sizeStrPos = strpos($e, 's')-2;
    		$currPos = $sizeStrPos;
    		$pageStr = $e[$currPos];
    		$stillrunning = true;
    		while($stillrunning)
    		{
    			$currPos -= 1;
    			$currChar = $e[$currPos];
    			if (!is_numeric($currChar))
    				$stillrunning = false;
    			else
    				$pageStr = $currChar.$pageStr;
    		}

    		//fileLogValue($pageStr);
    		$colon = strpos($e, ':')+1;
    		$x = strpos($e, 'x')-1;
    		$paren = strpos($e, 'p')-1;
    		$w = intval(substr($e, $colon, $x-$colon));
    		$h = intval(substr($e, $x+2, $paren-($x+2)));
        }
        if (strpos($e, 'rot') !== false)
        {
		        $colon = strpos($e, ':')+1;
            $rot = substr($e, $colon+2);
            if ($rot === '90' || $rot === '270')
            {
                $hold = $w;
                $w = $h;
                $h = $hold;
            }

            if ($w >= $h)
            {
                $noLandscape = false;
                $dimensions = "$w x $h";
                array_push($dimensionArr, $dimensions);
                array_push($landscapeArr, $pageStr);
            }
        }
	}
	$retArr = array($noLandscape, $landscapeArr, $dimensionArr);
	return $retArr;
}
//==============================================================================
function getFontList($pdfPath) {
    $results = array();
    $execStr = "pdffonts \"" . $pdfPath . "\"";
    exec($execStr, $results);
	
    $embedded = array();
    $notEmbedded = array();
	
    $embPos = strPos($results[0], 'emb');

    for ($x = 2; $x < sizeof($results); $x++) {
        $curFontSpecs = preg_split("/[\s]+/", $results[$x]);
	$result = $curFontSpecs[count($curFontSpecs)-5];
        if ($curFontSpecs[0] != '[none]') {
            if ($result == 'yes')
                $embedded[] = $curFontSpecs[0];
            else
                $notEmbedded[] = $curFontSpecs[0];
        }
    }

    $fontLists = array();
    $fontLists[] = $embedded;
    $fontLists[] = $notEmbedded;
    return $fontLists;
}
//==============================================================================
function getColorPages($pdfPath) {
    $results = array();
    $execStr = "pdfcolor 2>&1 \"" . $pdfPath . "\"";
    exec($execStr, $results);
	
    for ($x = 0; $x < sizeof($results); $x++) {
        if (substr($results[$x], 0, 5) == 'Color') {
            $colorPages = explode(" ", substr($results[$x], 7));
            if ($colorPages[0] == '')
                $colorPages = array();
        }

        if (substr($results[$x], 0, 2) == 'BW') {
          
            $bwPages = explode(" ", substr($results[$x], 4));
            
            if ($bwPages[0] == '')
                $bwPages = array();
        }
    }
    $pagesList = array();
    $pagesList[] = $bwPages;
    $pagesList[] = $colorPages;

    return $pagesList;
}
//==============================================================================