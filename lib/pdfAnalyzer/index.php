<?php
require_once 'preflightLib.php';

$fileName = filter_input(INPUT_GET,'fileName');
$filePath = filter_input(INPUT_GET,'filePath');
$filePath = $filePath . $fileName;
file_put_contents($filePath."test.txt", $fileName);

if (!file_exists($filePath)) {
	echo 'File path does not exist: ' . $filePath;
}

$result = pam\preflight\runPreflight($filePath);

//echoJsonOnyxOutput($result);
echo json_encode($result);