/* using GS DLL as a ps colorpage separator.
	Carsten Hammer Oc�-Deutschland GmbH
	Hammer.Carsten@oce.de			*/
#if defined(_WIN32) && !defined(_Windows)
# define _Windows
#endif
#ifdef _Windows
	/* Compile with:
	 * cl -D_Windows -Isrc -Febin\pscolor.exe test.c lex.yy.c bin\gsdll32.lib
	 */
	#include <windows.h>
	#define GSDLLEXPORT __declspec(dllimport)
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ierrors.h"
#include "iapi.h"
#include "gdevdsp.h"

/* Maximum number of pages that we can analyze */
#define MAXPAGES 16000

char iscolourpage[MAXPAGES];

unsigned char *myimage;
int breite,hoehe,seite=0,myraster;
FILE *color_fd,*black_fd,*temp_fd,*choose;
FILE *read_fd;

/* Enable this line if you wish to monitor ghostscripts display callbacks */
//#define DISPLAY_DEBUG

/* New device has been opened */
/* This is the first event from this device. */
static int display_open(void *handle, void *device)
{
#ifdef DISPLAY_DEBUG
    char buf[256];
    sprintf(buf, "display_open(0x%x, 0x%x)\n", handle, device);
    fprintf(stderr, buf);
#endif
    return 0;
}

/* Device is about to be closed. */
/* Device will not be closed until this function returns. */
static int display_preclose(void *handle, void *device)
{
#ifdef DISPLAY_DEBUG
    char buf[256];
    sprintf(buf, "display_preclose(0x%x, 0x%x)\n", handle, device);
    fprintf(stderr, buf);
#endif
    /* do nothing - no thread synchonisation needed */
    return 0;
}

/* Device has been closed. */
/* This is the last event from this device. */
static int display_close(void *handle, void *device)
{
#ifdef DISPLAY_DEBUG
    char buf[256];
    sprintf(buf, "display_close(0x%x, 0x%x)\n", handle, device);
    fprintf(stderr, buf);
#endif
    return 0;
}

/* Device is about to be resized. */
/* Resize will only occur if this function returns 0. */
static int display_presize(void *handle, void *device, int width, int height,
	int raster, unsigned int format)
{
#ifdef DISPLAY_DEBUG
    char buf[256];
    sprintf(buf, "display_presize(0x%x, 0x%x, width=%d height=%d raster=%d format=%d)\n",
       handle, device, width, height, raster, format);
    fprintf(stderr, buf);
#endif
    return 0;
}

/* Device has been resized. */
/* New pointer to raster returned in pimage */
static int display_size(void *handle, void *device, int width, int height,
	int raster, unsigned int format, unsigned char *pimage)
{

#ifdef DISPLAY_DEBUG
    char buf[256];
    sprintf(buf, "display_size(0x%x, 0x%x, width=%d height=%d raster=%d format=%d image=0x%x)\n",
       handle, device, width, height, raster, format, pimage);
    fprintf(stderr, buf);
#endif
	myimage=pimage;
	breite=width;
	hoehe=height;
	myraster=raster;
	   return 0;
}

/* flushpage */
static int display_sync(void *handle, void *device)
{

#ifdef DISPLAY_DEBUG
    char buf[256];
    sprintf(buf, "display_sync(0x%x, 0x%x)\n", handle, device);
    fprintf(stderr, buf);
#endif

    return 0;
}

/* showpage */
/* If you want to pause on showpage, then don't return immediately */
static int display_page(void *handle, void *device, int copies, int flush)
{
	int i,t,color=0;//,c;
#ifdef DISPLAY_DEBUG
    char buf[256];
    sprintf(buf, "display_page(0x%x, 0x%x, copies=%d flush=%d)\n",
	handle, device, copies, flush);
    fprintf(stderr, buf);
#endif
	seite++;

	if (seite>=MAXPAGES)
	  {
	    fprintf(stderr,"ERROR: Maximum page count (%i) exceeded!\nYou must change MAXPAGES and recompile to increase this limit.\n\n",MAXPAGES);
	    exit(EXIT_FAILURE);
	  }

	for(i=hoehe-1;i>=0;i=i-1){
		for(t=breite-1;t>=0;t=t-1){
            int red = myimage[i*myraster+t*3+0];
            int green = myimage[i*myraster+t*3+1];
            int blue = myimage[i*myraster+t*3+2];
            int colorB = ((abs(red - green) > 5)||(abs(red - blue) > 5)||(abs(blue - green) > 5));


			if(colorB){
				color=1;
				//fprintf(stderr,"Found a colour pixel at coords (%d,%d) with R=%x,G=%x,B=%x\n",i,t,myimage[i*myraster+t*3+2],myimage[i*myraster+t*3+1],myimage[i*myraster+t*3+0]);
				goto out;
			}
		}
	}
out:
	if(color){
	  iscolourpage[seite-1]=1;
	  fprintf(stderr,"[%d]Colour\n",seite);
	} else {
	  iscolourpage[seite-1]=0;
	  fprintf(stderr,"[%d]BlackAndWhite\n",seite);
	}

    return 0;
}

display_callback display = {
    sizeof(display_callback),
    DISPLAY_VERSION_MAJOR,
    DISPLAY_VERSION_MINOR,
    display_open,
    display_preclose,
    display_close,
    display_presize,
    display_size,
    display_sync,
    display_page,
    NULL,    /* display_update: we do not need progressive rendering */
    NULL,	/* we do not need our own memalloc */
    NULL	/* we do not need our own memfree */
};

//gs_main_instance *minst; This type has been changed to void * in ghostscript http://svn.ghostscript.com/viewvc?view=rev&revision=5251
void *minst;
const char start_string[] = "systemdict /start get exec\n";


int main(int argc, char *argv[])
{
    int code;
    int exit_code;
    char * gsargv[10];
    char arg[64];
    int gsargc;
    int idx;
    gsargv[0] = "pdfcolour";	/* actual value doesn't matter */
    gsargv[1] = "-dNOPAUSE";
    gsargv[2] = "-dBATCH";
    gsargv[3] = "-dNOSAFER";
//    gsargv[4] = "-sDEVICE=pdfwrite";
    gsargv[4] = "-sDEVICE=display";
    gsargv[5] = "-dDisplayHandle=0";
    sprintf(arg,"-dDisplayFormat=%d",DISPLAY_COLORS_RGB|DISPLAY_DEPTH_8|DISPLAY_LITTLEENDIAN|DISPLAY_BOTTOMFIRST);
    gsargv[6] = "-E-";
		gsargv[7] = arg;
    //    gsargv[7] = "-sOutputFile=out.pdf";
    //    gsargv[8] = "-c";
    //    gsargv[9] = ".setpdfwrite";
		//	|DISPLAY_ALPHA_NONE
    gsargv[8] = "-f";

    if (argc==2) /* A single command line argument */
      {
	gsargv[9] = argv[1];
      }
    else
      {
	gsargv[9] = "-";
      }
    //    gsargv[8] = "input.ps";
    //    gsargv[8] = "-";


    gsargc=10;

    printf("Starting pdfcolor version $Id: pdfcolor.c 2724 2009-08-17 16:34:20Z crodgers $\n\n");

    code = gsapi_new_instance(&minst, NULL);
    if (code < 0)
			return 1;
    gsapi_set_display_callback(minst, &display);

    code = gsapi_init_with_args(minst, gsargc, gsargv);
    gsapi_exit(minst);

    gsapi_delete_instance(minst);

    printf("\n\nColor:");
    for (idx=0;idx<seite;idx++)
      {
	if (iscolourpage[idx])
	  {
	    printf(" %i",idx+1);
	  }
      }

    printf("\n\nBW:");
    for (idx=0;idx<seite;idx++)
      {
	if (!iscolourpage[idx])
	  {
	    printf(" %i",idx+1);
	  }
      }
    printf("\n\n");

    if ((code == 0) || (code == e_Quit))
			return 0;
    return 1;
}
