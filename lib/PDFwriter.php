<?php
require_once('/tcpdf/tcpdf.php');
require_once('/tcpdf/fpdi.php');
//require_once Yii::app()->basePath.'/lib/tcpdf/fpdi.php'; //added for createPdf
//require_once('pam2Core.php');
//require_once 'onyxCore.php';
class PDFWriter extends FPDI {
    private $gWidth;
    private $gHeight;
    private $gOrientation;
    private $gPageBoxes;
    /**
     * 
     * @param type $elArray
     * ['pages'][]['height']*
     *          []['width']*
     *          []['orientation']* //needs to be set on EVERY page that differs from the document wide setting
     *          []['elements'][]  //REQUIRED, even if no elements are to be added to the page
     * 						  []['type'] element type
     * 						  []['posX'] //required for types: TEXT,IMAGE,IMAGEPDF
     * 						  []['posY'] //required for types: TEXT,IMAGE,IMAGEPDF
     * 						  []['srcFile'] //required for types: IMAGE,IMAGEPDF
     * 						  []['font'] font definition array //required for types: TEXT
     * 						  []['border'] //required for: TEXT
     * 						  []['width'] //required for: IMAGE,IMAGEPDF
     * 						  []['height'] //required for: IMAGE,IMAGEPDF
     * 						  []['textWidth'] //required for: TEXT
     * 						  []['textHeight'] //required for: TEXT
     *          []['pageBoxes'][]
     * ['height'] // measured in the user defined unit set below
     * ['width'] // measured in the user defined unit set below
     * ['orientation']* //defaults to 'P' if not defined
     * ['unit']* //will default to 'in' if not supplied
     * ['pageBoxes'][]* //defaults to empty array
     *              [boxName]['llx']
     * 				[boxName]['lly']
     * 				[boxName]['urx']
     * 				[boxName]['ury']
     * 
     * a * indicates that this field is optional
     * @return type
     */
    public static function createFromElementArray($elArray, $saveFilePath) {
        $pdf = new PDFWriter();
        $pdf->initializePdf();
        $pdf->gHeight = (array_key_exists("height", $elArray)) ? $elArray['height'] : 8.5;
        $pdf->gWidth = (array_key_exists("width", $elArray)) ? $elArray['width'] : 11;

        if(array_key_exists("orientation", $elArray)) {
            $pdf->gOrientation = $elArray['orientation'];
        }
        else {
            $pdf->gOrientation = ($pdf->gHeight > $pdf->gWidth) ? 'P' : 'L';
        }
        $pdf->setPageUnit((array_key_exists("unit", $elArray)) ? $elArray['unit'] : 'in');
        $pdf->gPageBoxes = (array_key_exists("pageBoxes", $elArray)) ? $elArray['pageBoxes'] : array();
        //save global page Box array
        foreach($elArray['pages'] as $page) {
            $pdf->addPageFromArray($page);
        }
        $pdf->Output($saveFilePath, 'F');
    }
    private function addPageFromArray($pageDefinition) {
        $this->AddPage();
        $width = (array_key_exists('width', $pageDefinition)) ? $pageDefinition['width'] : $this->gWidth;
        $height = (array_key_exists('height', $pageDefinition)) ? $pageDefinition['height'] : $this->gHeight;

        $orientation = (array_key_exists('orientation', $pageDefinition)) ? $pageDefinition['orientation'] : $this->gOrientation;
        $pageBoxes = (array_key_exists('pageBoxes', $pageDefinition)) ? array_merge($this->gPageBoxes, $pageDefinition['pageBoxes']) : $this->gPageBoxes;

        $this->setPageFormat($pageBoxes, $orientation);

        foreach($pageDefinition['elements'] as $element) {
            switch($element['type']) {
                case 'IMAGE':
                    $this->addImageElement($element);
                    break;
                case 'IMAGEPDF':
                    $this->addImagePdfElement($element);
                    break;
                case 'SVG':
                    $this->addSvgImageElement($element);
                    break;
                case 'TEXT':
                    $this->addTextElement($element);
                    break;
                case 'TEXTLINE':
                    $this->addTextLineElement($element);
                    break;
                case 'COLORBOX':
                    $this->addColorBoxElement($element);
                    break;
            }
        }
    }
    private function initializePDF() {
        $this->SetAutoPageBreak(true);
        $this->setPrintHeader(false);
        $this->setPrintFooter(false);
        $this->SetMargins(0, 0, 0, true);
        $this->setCellPadding(0);
        $this->setFontSubsetting(false);
        $this->AddSpotColor("PANTONE 648 C", 100, 85, 34, 23);
        $this->AddSpotColor("PANTONE 647 C", 85, 48, 0, 20);
        $this->AddSpotColor("PANTONE 7419 U", 0, 60, 45, 18);
        $this->AddSpotColor("PANTONE 7448 C", 32, 42, 0, 55);
        $this->AddSpotColor("PANTONE 469 U", 0, 52, 100, 62);
        $this->AddSpotColor("PANTONE 549 U", 52, 6, 0, 25);
        $this->AddSpotColor("PANTONE Cool Gray 11 U", 0, 2, 0, 68);
        $this->AddSpotColor("PANTONE 2955 C", 100, 43, 0, 34);
        $this->AddSpotColor("PANTONE 2955 CVU", 100, 43, 0, 34);
        $this->AddSpotColor("PANTONE 343 U", 98, 0, 72, 61);
        $this->AddSpotColor("PANTONE 301 CV", 100, 43, 0, 18);
        $this->AddSpotColor("PANTONE 7448 C", 32, 42, 0, 55);
        $this->AddSpotColor("PANTONE 3282 U", 100, 0, 46, 15);
    }
    /*
      @param type $element
      ['posX']
      ['posY']
      ['width']
      ['height']
      ['style']*
      ['borderStyle']*
      [fillColor]* Format: array(GREY), array(R,G,B), array(C,M,Y,K), or array(C,M,Y,K,SpotColorName)  Defaults to empty array

      a * indicates that this field is optional

      @return type
     */
    private function addColorBoxElement($element) {
        $this->Rect(
                $element['posX'], $element['posY'], $element['width'], $element['height'], 'F', '', $element['fillColor']);
    }
    private function addImageElement($element) {
        $this->Image($element['srcFile'], $element['posX'], $element['posY'], $element['width'], $element['height'], '', //type
                     '', //link
                     '', //align
                     true, //resize
                     300, //dpi
                     '', //p-align
                     false, //is mask
                     false, //img mask
                     0, //border
                     false, //fit box
                     false, //hidden
                     false, //fit on page
                     false, //alt
                     array()  //alt imgs
        );
    }
    private function addImagePdfElement($element) {
        $this->setSourceFile($element['srcFile']);
        $PID = $this->importPage($element['pageNo'], '/MediaBox');
        $this->useTemplate($PID, $element['posX'], $element['posY'], $element['width']); //original function accepted height but never used it...
    }
    private function addSvgImageElement($element) {
        $this->ImageSVG(
                $element['srcFile'], $element['posX'], $element['posY'], $element['width'], $element['height'], '', //link
                '', //align
                '', //p-align
                0, //border
                false      //fit on page
        );
    }
    private function addTextElement($element) {
        $this->setLineProperties($element['font']);
        $width = $this->GetStringWidth($element['text'], $element['font']['family'], $element['font']['style'], $element['font']['size']);
        $this->MultiCell(
                $width, //$element['textWidth'],// (float) width of cells. If 0 extend to right margin
                $element['textHeight'], //(float) cell minimum height. extends automatically
                $element['text'], //(string) Text to print
                $element['border'], $element['alignment'], FALSE, //(boolean) true if cell background must be painted
                0, //(int) where current position should go after the call. 0 (to the right) 1 (to beginning of next line) 2 (below)
                $element['posX'], $element['posY'], TRUE, //(boolean) if true reset last cell height
                0, //(int) font stretch mode. 0 (disabled) 1 (horizontal scaling only if longer than width) 2 (forced to fit cell width) 3 (character spacing only if larger than cell width) 4(forced character spacing to fit cell)
                FALSE, //(boolean) DONT USE! INSTEAD USE writeHTMLCell() or writeHtml()
                TRUE, $element['textHeight'], //(float) should be >= $height and < remaining space to bottom of page. 0 to disable
                'M', true);
        /** MultiCell($width,$height,$text,$border,$alignment,$fill,$ln,$positionX,$positionY,
          $resetHeight,$stretchHeight,$isHtml,$autopadding,$maxHeight,$verticalAlign,$fitcell)
          @params:
          $width = (float) width of cells. If 0 extend to right margin
          $height = (float) cell minimum height. extends automatically
          $text = (string) Text to print
          $border = (mixed- int||string)  0 means no border
          1 means border.
          'L' (left border)
          'R' (right border)
          'T' (top border)
          'B' (bottom border)
          $alignment = (string) 'L' or '' (left align)
          'C' (center align)
          'R' (right align)
          'J' justified
          $fill = (boolean) true if cell background must be painted
          $ln = (int) where current position should go after the call. 0 (to the right) 1 (to beginning of next line) 2 (below)
          $positionX = (float) x postion in user units (defined in FPDI constructor)
          $positionY = (float) y postion in user units (defined in FPDI constructor)
          $resetHeight = (boolean) if true reset last cell height
          $stretchHeight = (int) font stretch mode. 0 (disabled) 1 (horizontal scaling only if longer than width) 2 (forced to fit cell width) 3 (character spacing only if larger than cell width) 4(forced character spacing to fit cell)
          $isHtml = (boolean) DONT USE! INSTEAD USE writeHTMLCell() or writeHtml()
          $autopadding = (boolean) true means automatically addusts internal padding to acount for line witdh
          $maxHeight = (float) should be >= $height and < remaining space to bottom of page. 0 to disable
          $verticalAlign = (string) requires $maxHeight = $height > 0.
          'T' (top)
          'M' (middle)
          'B' (bottom)
          $fitcell = (boolean) if true attempt to fit all text in cell by reducing font size.

          @return: number of cells or 1 for html mode.
         */
    }
    private function addTextLineELement($element) {
        $this->setLineProperties($element['font']);
        //$this->setXY($element['posX'], $element['posY'], true);
        /* $this->Cell(
          $element['textWidth'],
          $element['textHeight'],
          $element['text'],
          $element['border'],
          0,
          $element['alignment'],
          FALSE,
          '',
          0,
          false,
          'T',
          'M'
          ); */
        $this->Text($element['posX'], $element['posY'], $element['text'], false, false, true, 0, 0, $element['alignment'], false, '', 0, false, 'T', 'M', true);
    }
    private function setLineProperties($fontDefinition) {
        $fontFamily = $fontDefinition['family']; // can be a predefined font or you can add the font using $pdf->AddFont;
        $fontStyle = $fontDefinition['style']; // '' (regular), 'B' (bold), 'I' (italic), 'U' (underline), 'D' (line through) , 'O' (overline)
        $fontSize = $fontDefinition['size']; // (float) Font size in points defaults to 12 or last set font size when exists
        $fontColor = $fontDefinition['color'];
        $fontSpacing = $fontDefinition['spacing'];
        /* AddFont($fontFamily, $style, $fontFile)
          $fontFamily = (string) can be chosen arbitrarily, if standard family name overrides that font.
          $fontStyle = (string) '' (regular)
          'B' (bold)
          'I' (italic)
          'BI' or 'IB' (bold italic)
          $fontFile = (string) name of the font file that is stored in the tcpdf
         */
        /* SetFont($fontFamily, $style, $fontSize)
          $fontFamily = (string) can be chosen arbitrarily overrides
          $fontStyle = (string) '' (regular)
          'B' (bold)
          'I' (italic)
          'U' (underline)
          'D' (line through)
          'O' (overline)
          note: you may combine multiple of the above.
          $fontSize = (float) Font size in points. The default value is the current size. If no size has been specified since the beginning of the document, the value taken is 12
         */

        $this->AddFont($fontFamily, '', $fontFamily.'.php'); //works for now, but checking for the fonts existence might keep us from doing stuff unnecessarily
        $this->SetFont($fontFamily, $fontStyle, $fontSize);
        if(array_key_exists('fontSpotColor', $fontDefinition))
            $this->SetTextSpotColor($fontDefinition['fontSpotColor'], 100);
        else {
            $color = html2cmyk($fontColor);
            $this->SetTextColor($color["C"], $color["M"], $color["Y"], $color["K"]);
        }
        $this->setFontSpacing($fontSpacing);
    }
}
function html2cmyk($color) {
    $color = ($color[0] == "#") ? substr($color, 1) : $color;

    $r = hexdec(substr($color, 0, 2));
    $g = hexdec(substr($color, 2, 2));
    $b = hexdec(substr($color, 4, 2));

    // BLACK
    if($r == 0 && $g == 0 && $g == 0) {
        $computedK = 100;
        return array("C"=>0, "M"=>0, "Y"=>0, "K"=>$computedK);
    }

    $computedC = 1 - ($r / 255);
    $computedM = 1 - ($g / 255);
    $computedY = 1 - ($b / 255);

    $minCMY = min($computedC, min($computedM, $computedY));

    $computedC = 100 * (($computedC - $minCMY) / (1 - $minCMY));
    $computedM = 100 * (($computedM - $minCMY) / (1 - $minCMY));
    $computedY = 100 * (($computedY - $minCMY) / (1 - $minCMY));
    $computedK = 100 * $minCMY;

    return array("C"=>$computedC, "M"=>$computedM, "Y"=>$computedY, "K"=>$computedK);
}
?>