<?php
require_once 'protected/lib/onyx/onyxCore.php';
require_once 'protected/lib/pam/pamCore.php';

//------------------------------------------------------------------------------
class FedExShipment {
    public $pamMailCode = '';
    public $recipientName = '';
    public $recipientCompany = '';
    public $recipientAddress1 = '';
    public $recipientAddress2 = '';
    public $recipientCity = '';
    public $recipientState = '';
    public $recipientPostalCode = '';
    public $recipientCountryCode = '';
    public $isResidential = false;
    public $lineItems = array();
    //-------------------------------------------------
    public function addLineItem($lineItem) {
        verify($lineItem instanceof FedExShipmentLineItem, 'Invalid LineItem type');
        $this->lineItems[] = $lineItem;
    }
    //-------------------------------------------------
    public function clearLineItems() {
        $this->lineItems = array();
    }
    //-------------------------------------------------
    public function getLineItems() {
        return $this->lineItems;
    }
    //-------------------------------------------------
    public function getPackageCount() {
        return count($this->lineItems);
    }
}
//-------------------------------------------------
class FedExShipmentLineItem {
    public $weightInPounds = 0;
    public $widthInInches = 0;
    public $heightInInches = 0;
    public $lengthInInches = 0;
}
//-------------------------------------------------
class PamFedExShipment extends FedExShipment {
    public function addLineItemFromOunces($ounces) {
        errIfNull($ounces, 'Ounces must be provided');
        $lineItem = new FedExShipmentLineItem();
        $lineItem->weightInPounds = $ounces / 16;
        $this->addLineItem($lineItem);
    }
    //----------------------------------------------------------------------------
    public function addLineItemsFromOunces($totalOunces) {
        errIfNull($totalOunces, 'Ounces missing');
        $maxOuncesPerBox = 54 * 16;
        $remainingOunces = $totalOunces;
        while($remainingOunces >= $maxOuncesPerBox) {
            $this->addLineItemFromOunces($maxOuncesPerBox);
            $remainingOunces -= $maxOuncesPerBox;
        }
        if($remainingOunces > 0) {
            $this->addLineItemFromOunces($remainingOunces);
        }
    }
    //-------------------------------------------------
    function retrieveMailRates() {
        if(isNullOrEmptyStr($this->pamMailCode)) {
            $results = retrieveFedExMailRatesList($this);
            try {
                $smartPostResults = array();
                $smartPostResults = retrieveFedExMailRatesList($this, 'SMART_POST');
            }
            catch(Exception $e) {
                return $results;
            }
            return array_merge($results, $smartPostResults);
        }
        else {
            $fedExShippingMethod = pamShippingMethodToFedExShippingMethod($this->pamMailCode, $this->isResidential);
            return retrieveFedExMailRatesList($this, $fedExShippingMethod);
        }
    }
}
//------------------------------------------------------------------------------
function pamShippingMethodToFedExShippingMethod($pamShippingMethod, $isResidential) {
    if(($pamShippingMethod === cMailNextDayPriority) || ($pamShippingMethod === cMailNextDayRound)) {
        return 'PRIORITY_OVERNIGHT';
    }
    if($pamShippingMethod === cMailNextDayStandard) {
        return 'STANDARD_OVERNIGHT';
    }
    if($pamShippingMethod === cMailSecondDay) {
        return 'FEDEX_2_DAY';
    }
    if($pamShippingMethod === cMailIntlEconomy) {
        return 'INTERNATIONAL_ECONOMY';
    }
    if($pamShippingMethod === cMailIntlPriority) {//TODO review the intl fedex
        return 'INTERNATIONAL_PRIORITY';
    }
    if($pamShippingMethod === cMailStandard) {
        if($isResidential) {
            return 'GROUND_HOME_DELIVERY';
        }
        else {
            return 'FEDEX_GROUND';
        }
    }
    err($pamShippingMethod." doesn't map to a FedEx shipping method");
}
//-------------------------------------------------
function fedExShippingMethodToPamShippingMethod($fedExShippingMethod) {
    if($fedExShippingMethod == 'PRIORITY_OVERNIGHT') {
        return cMailNextDayPriority;
    }
    if($fedExShippingMethod == 'STANDARD_OVERNIGHT') {
        return cMailNextDayStandard;
    }
    elseif($fedExShippingMethod == 'FEDEX_2_DAY'){
        return cMailSecondDay;
    }
    elseif($fedExShippingMethod == 'INTERNATIONAL_ECONOMY'){
        return cMailIntlEconomy;
    }
    elseif($fedExShippingMethod == 'INTERNATIONAL_PRIORITY'){
        return cMailIntlPriority;
    }
    elseif($fedExShippingMethod == 'GROUND_HOME_DELIVERY' || $fedExShippingMethod == 'FEDEX_GROUND'){
        return cMailStandard;
    }
    err($fedExShippingMethod." doesn't map to a Print and Mail delivery method");
}
//------------------------------------
// Copyright 2009, FedEx Corporation. All rights reserved.
// Version 9.0.0
//------------------------------------
function retrieveFedExMailRatesList($shipment, $serviceType = '') {
    $path_to_wsdl = dirname(__file__).'/../fedexWsdl/RateService_v10.wsdl';
    ini_set("soap.wsdl_cache_enabled", "0");

    $client = new SoapClient($path_to_wsdl, array('trace'=>1));

    $request['WebAuthenticationDetail'] = array(
            'UserCredential'=>array(
                    'Key'=>'ynKJIg4svr6mGJVj',
                    'Password'=>'sMa2zv6kpLuWQKG8eHgq98iC6'
            )
    );

    $request['ClientDetail'] = array(
            'AccountNumber'=>'128165428',
            'MeterNumber'=>'103196341'
    );

    $request['TransactionDetail'] = array('CustomerTransactionId'=>1);
    $request['Version'] = array('ServiceId'=>'crs', 'Major'=>'10', 'Intermediate'=>'0', 'Minor'=>'0');

    $request['ReturnTransitAndCommit'] = true;
    $request['RequestedShipment']['DropoffType'] = 'REGULAR_PICKUP'; // valid values REGULAR_PICKUP, REQUEST_COURIER, ...
    $request['RequestedShipment']['ShipTimestamp'] = date('c'); //'2011-08-31T17:00:00-04:00';

    if($serviceType != '') {
        $request['RequestedShipment']['ServiceType'] = $serviceType; // valid values STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND, ...
        //if($serviceType == 'SMART_POST')
        //{
        $request['RequestedShipment']['SmartPostDetail'] = array(
                'Indicia'=>'PARCEL_SELECT',
                'AncillaryEndorsement'=>'CARRIER_LEAVE_IF_NO_RESPONSE',
                'SpecialServices'=>'USPS_DELIVERY_CONFIRMATION',
                'HubId'=>'5843',
                //'CustomerManifestId' => 'XXX'
        );
        //}
    }
    $request['RequestedShipment']['TotalInsuredValue'] = array('Amount'=>100, 'Currency'=>'USD');
    $request['RequestedShipment']['Shipper'] = addShipper();
    $request['RequestedShipment']['Recipient'] = addRecipient($shipment);
    $request['RequestedShipment']['ShippingChargesPayment'] = array(
            'PaymentType'=>'SENDER', // valid values RECIPIENT, SENDER and THIRD_PARTY
            'Payor'=>array('AccountNumber'=>128165428,
                    'CountryCode'=>'US'));

    $request['RequestedShipment']['RateRequestTypes'] = 'ACCOUNT';
    $request['RequestedShipment']['PackageCount'] = $shipment->getPackageCount();//# of items in this shipment
    $request['RequestedShipment']['RequestedPackageLineItems'] = addPackageLineItem($shipment->getLineItems());//calculate dimensions?

    try {
//        if(setEndpoint('changeEndpoint'))
//            $newLocation = $client->__setLocation(setEndpoint('endpoint'));
        $response = $client->getRates($request);

        if($response->HighestSeverity != 'FAILURE' && $response->HighestSeverity != 'ERROR') {
            $rateReply = objectToArray($response->RateReplyDetails);
			
            if(array_key_exists(0, $rateReply)){
                $rateEntries = $rateReply;
            }
            else {
                $rateEntries[] = $rateReply;
            }

            $results = array();
            foreach($rateEntries as $rateEntry) {
                $serviceType = arrayGet($rateEntry, 'ServiceType');
                $pamShippingMethod = fedExShippingMethodToPamShippingMethod($serviceType);
                if($pamShippingMethod === null) {
                    continue;
                }
                $result['SERVICETYPE'] = $pamShippingMethod;
                $ratedShipmentDetails = arrayGet($rateEntry, 'RatedShipmentDetails');
                if(array_key_exists(0, $ratedShipmentDetails)) {
                    $result['AMOUNT'] = number_format($ratedShipmentDetails[0]['ShipmentRateDetail']['TotalNetCharge']['Amount'], 2, ".", ",");
                }
                else {
                    $result['AMOUNT'] = number_format($ratedShipmentDetails['ShipmentRateDetail']['TotalNetCharge']['Amount'], 2, ".", ",");
                }
                if(array_key_exists('DeliveryTimestamp', $rateEntry)) {
                    $result['DELIVERYDATE'] = arrayGet($rateEntry, 'DeliveryTimestamp');
                }
                else if(array_key_exists('TransitTime', $rateEntry)) {
                    $result['DELIVERYDATE'] = arrayGet($rateEntry, 'TransitTime');
                }
                else {
                    $result['DELIVERYDATE'] = '';
                }
                $results[] = $result;
            }
            return $results;
        }
        else {
            throwFedExError($response->Notifications);
        }
    }
    catch(SoapFault $exception) {
        err("{$exception->faultcode}: {$exception->faultstring}");
    }
}
//------------------------------------
function objectToArray($d) {
    if(is_object($d)) {
        $d = get_object_vars($d);
    }

    if(is_array($d)) {
        return array_map(__FUNCTION__, $d);
    }
    else {
        return $d;
    }
}
//------------------------------------
function addShipper() {
    $shipper = array(
            'Contact'=>array(
                    'PersonName'=>'',
                    'CompanyName'=>'BYU Print and Mail',
                    'PhoneNumber'=>'8014222855'),
            'Address'=>array(
                    'StreetLines'=>array('701 E University Pkwy'),
                    'City'=>'Provo',
                    'StateOrProvinceCode'=>'UT',
                    'PostalCode'=>'84602',
                    'CountryCode'=>'US')
    );
    return $shipper;
}
//------------------------------------
function addRecipient($shipment) {
    $recipient = array(
            'Contact'=>array(
            //'PersonName' => '',
            //'CompanyName' => '',
            //'PhoneNumber' => ''
            ),
            'Address'=>array(
                    //'StreetLines' => array('P.O. Box 123'),
                    //'City' => '',
                    //'StateOrProvinceCode' => '',
                    'PostalCode'=>$shipment->recipientPostalCode,
                    'CountryCode'=>$shipment->recipientCountryCode,
                    'Residential'=>$shipment->isResidential)
    );
    return $recipient;
}
//------------------------------------
function addShippingChargesPayment() {
    $shippingChargesPayment = array(
            'PaymentType'=>'SENDER',
            'Payor'=>array(
                    'AccountNumber'=>'128165428',
                    'CountryCode'=>'US')
    );
    return $shippingChargesPayment;
}
//------------------------------------
function addLabelSpecification() {
    $labelSpecification = array(
            'LabelFormatType'=>'COMMON2D', // valid values COMMON2D, LABEL_DATA_ONLY
            'ImageType'=>'PDF', // valid values DPL, EPL2, PDF, ZPLII and PNG
            'LabelStockType'=>'PAPER_7X4.75');
    return $labelSpecification;
}
//------------------------------------
function addSpecialServices() {
    $specialServices = array(
            'SpecialServiceTypes'=>array('COD'),
            'CodDetail'=>array(
                    'CodCollectionAmount'=>array('Currency'=>'USD', 'Amount'=>150),
                    'CollectionType'=>'ANY')// ANY, GUARANTEED_FUNDS
    );
    return $specialServices;
}
//------------------------------------
function addPackageLineItem($lineItems) {//all items in shipment

    $itemNumber = 0;
    $packageLineItems = array();
    foreach($lineItems as $lineItem) {
        $itemNumber++;
        $packageLineItem = array(
                'SequenceNumber'=>$itemNumber,
                'GroupPackageCount'=>1,
                'Weight'=>array('Value'=>$lineItem->weightInPounds, 'Units'=>'LB')
        );

        //does it even get in here? lengthInInches always = 0?
        if($lineItem->lengthInInches > 0 && $lineItem->widthInInches > 0 && $lineItem->heightInInches > 0) {
            $packageLineItem['Dimensions'] = array(
                    'Length'=>$lineItem->lengthInInches,
                    'Width'=>$lineItem->widthInInches,
                    'Height'=>$lineItem->heightInInches,
                    'Units'=>'IN'
            );
        }

        $packageLineItems[] = $packageLineItem;
    }
    return $packageLineItems;
}
//------------------------------------
function throwFedExError($notes) {
    foreach($notes as $noteKey=> $note) {
        if(strcasecmp($noteKey, 'Message') == 0) {
            err($note);
        }
    }
}
//=========================================
//pam again
//=========================================
function getFedExMailRate($deliveryMethod, $shipToName, $address1, $address2, $city, $state, 
                            $postalCode, $country, $isResidential, $ounces) {
    $fedExShipment = new PamFedExShipment;
    $fedExShipment->pamMailCode = $deliveryMethod;
    $fedExShipment->recipientName = $shipToName;
    $fedExShipment->recipientAddress1 = $address1;
    $fedExShipment->recipientAddress2 = $address2;
    $fedExShipment->recipientCity = $city;
    $fedExShipment->recipientState = $state;
    $fedExShipment->recipientPostalCode = $postalCode;
    $fedExShipment->recipientCountryCode = $country;
    $fedExShipment->isResidential = $isResidential;

    $fedExShipment->addLineItemsFromOunces($ounces);
    $result = $fedExShipment->retrieveMailRates();
    $shippingCharge = strDeleteCharSet('$,', $result[0]['AMOUNT']);
    $shippingCharge = floatval($shippingCharge);
    return $shippingCharge;
}
