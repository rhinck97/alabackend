<?php
require_once 'protected/lib/onyx/onyxCore.php';
require_once 'protected/lib/onyx/onyxYii.php';
require_once 'protected/lib/pam/pamCore.php';
require_once 'protected/lib/pam/pamFedExShipment2.php';
//require_once 'protected/lib/pam/pamDhlShipment.php';

define('cMailNextDayStandard', 'NEXTDAYSTANDARD');//in use
define('cMailNextDayRound', 'NEXTDAYROUND');//in use but not in checkout
define('cMailIntlEconomy', 'INTLECONOMY');//in use
define('cMailSecondDay', 'SECONDDAY');//in use
define('cMailStandard', 'STANDARD');//in use
define('cMailDeliver', 'DELIVER');//in use
define('cMailCustomerPickup', 'PICKUP');//in use
define('cMailFreight', 'FREIGHT');//?
define('cMailMissing', 'MISSING');//in use
define('cMediaMail', 'MEDIAMAIL');//?

//------------------------------------------------------------------------------
function pamDeliveryMethodToUpsShippingMethod($pamShippingMethod) {
    if($pamShippingMethod === cMailNextDayStandard) {
        return '01';//UPS Next Day Air
    }
    if($pamShippingMethod === cMailSecondDay) {
        return '02';//UPS Second Day Air
    }
    if($pamShippingMethod === cMailStandard) {
        return '03';//UPS Ground
    }
    err($pamShippingMethod." doesn't map to a UPS shipping method");
}
//------------------------------------------------------------------------------
function fetchUpsMailRate($stateCode, $postalCode, $deliveryMethod, $ounces, $lengthInInches, $widthInInches, $heightInInches) {
    $service = pamDeliveryMethodToUpsShippingMethod($deliveryMethod);
	// This script was written by Mark Sanborn at http://www.marksanborn.net
	// If this script benefits you are your business please consider a donation
	// You can donate at http://www.marksanborn.net/donate.
	// ========== CHANGE THESE VALUES TO MATCH YOUR OWN ===========
	$accessLicenseNumber = '3D2AECE63D95F358'; // Your license number
	$userId = 'byumailservices'; // Username
	$password = 'supplies'; // Password
	$shipperNumber = 'AW0351'; // Your UPS shipper number
    $fromPostalCode = '84602'; // Zipcode you are shipping FROM
    $pounds = intval($ounces) / 16;
        // =============== xml start ===============
    	$data ="<?xml version=\"1.0\"?>
    	<AccessRequest xml:lang=\"en-US\">
    		<AccessLicenseNumber>$accessLicenseNumber</AccessLicenseNumber>
    		<UserId>$userId</UserId>
    		<Password>$password</Password>
    	</AccessRequest>
    	<?xml version=\"1.0\"?>
    	<RatingServiceSelectionRequest xml:lang=\"en-US\">
    		<Request>
    			<TransactionReference>
    				<CustomerContext>Bare Bones Rate Request</CustomerContext>
    				<XpciVersion>1.0001</XpciVersion>
    			</TransactionReference>
    			<RequestAction>Rate</RequestAction>
    			<RequestOption>Rate</RequestOption>
    		</Request>
    	<PickupType>
    		<Code>01</Code>
    	</PickupType>
    	<Shipment>
    		<Shipper>
    			<Address>
    				<PostalCode>$fromPostalCode</PostalCode>
    				<CountryCode>US</CountryCode>
    			</Address>
			<ShipperNumber>$shipperNumber</ShipperNumber>
    		</Shipper>
    		<ShipTo>
    			<Address>
    				<PostalCode>$postalCode</PostalCode>
						<StateProvinceCode>$stateCode</StateProvinceCode>
    				<CountryCode>US</CountryCode>
				<ResidentialAddressIndicator/>
    			</Address>
    		</ShipTo>
    		<ShipFrom>
    			<Address>
    				<PostalCode>$fromPostalCode</PostalCode>
						<StateProvinceCode>UT</StateProvinceCode>
    				<CountryCode>US</CountryCode>
    			</Address>
    		</ShipFrom>
    		<Service>
    			<Code>$service</Code>
    		</Service>";
            // Handle number of packages
        if ($pounds > 54) {
            $remainingPounds = $pounds;
            while ($remainingPounds >= 54) {
                $data .= "
                <Package>
        			<PackagingType>
        				<Code>02</Code>
        			</PackagingType>
        			<Dimensions>
        				<UnitOfMeasurement>
        					<Code>IN</Code>
        				</UnitOfMeasurement>
        				<Length>$lengthInInches</Length>
        				<Width>$widthInInches</Width>
        				<Height>$heightInInches</Height>
        			</Dimensions>
        			<PackageWeight>
        				<UnitOfMeasurement>
        					<Code>LBS</Code>
        				</UnitOfMeasurement>
        				<Weight>54</Weight>
        			</PackageWeight>
        		</Package>";
                $remainingPounds -= 54;
            }
            if ($remainingPounds > 0) {
                $data .=
                "<Package>
        			<PackagingType>
        				<Code>02</Code>
        			</PackagingType>
        			<Dimensions>
        				<UnitOfMeasurement>
        					<Code>IN</Code>
        				</UnitOfMeasurement>
        				<Length>$lengthInInches</Length>
        				<Width>$widthInInches</Width>
        				<Height>$heightInInches</Height>
        			</Dimensions>
        			<PackageWeight>
        				<UnitOfMeasurement>
        					<Code>LBS</Code>
        				</UnitOfMeasurement>
        				<Weight>$remainingPounds</Weight>
        			</PackageWeight>
        		</Package>";
            }
        }
        else {
            $data .=
            "<Package>
                <PackagingType>
                    <Code>02</Code>
                </PackagingType>
                <Dimensions>
                    <UnitOfMeasurement>
                        <Code>IN</Code>
                    </UnitOfMeasurement>
                    <Length>$lengthInInches</Length>
                    <Width>$widthInInches</Width>
                    <Height>$heightInInches</Height>
                </Dimensions>
                <PackageWeight>
                    <UnitOfMeasurement>
                        <Code>LBS</Code>
                    </UnitOfMeasurement>
    				<Weight>$pounds</Weight>
    			</PackageWeight>
            </Package>";
        }
        // End handle number of packages

	$data .="<RateInformation>
					<NegotiatedRatesIndicator/>
				</RateInformation>
    	</Shipment>
    	</RatingServiceSelectionRequest>";
        // =============== xml end ===============
	$ch = curl_init("https://www.ups.com/ups.app/xml/Rate");
	curl_setopt($ch, CURLOPT_HEADER, 1);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_TIMEOUT, 60);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
	curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$data);
	$result = curl_exec($ch);
    $first = strNthItem(2, $result, '<ResponseStatusDescription>');
    $afterFirst = $first;
    $responseStatus = strNthItem(1, $afterFirst, '</ResponseStatusDescription>');
    if ($responseStatus == 'Success') {
            $afterNegOpenTag = strNthItem(2, $result, '<NegotiatedRates>');
    		$negTagContents = strNthItem(1, $afterNegOpenTag, '</NegotiatedRates>');
            $afterMonOpenTag = strNthItem(2, $negTagContents, '<MonetaryValue>');
    		$negRate = strNthItem(1, $afterMonOpenTag, '</MonetaryValue>');
            errIf($negRate <= 0, 'UPS returned rate <= 0. Full output... '.$result);
            return $negRate;
    }
    else if ($responseStatus == 'Failure') {
            $afterOpen = strNthItem(2, $result, '<ErrorDescription>');
        $errorMessage = strNthItem(1, $afterOpen, '</ErrorDescription>');
        err('UPS returned error... '.$errorMessage);
    }
}
//------------------------------------------------------------------------------
function pamDeliveryMethodToUspsShippingMethod($pamDeliveryMethod, $ounces) {
    if($pamDeliveryMethod === cMailNextDayStandard) {
        return 'PRIORITY MAIL EXPRESS';
    }
    if($pamDeliveryMethod === cMailStandard || $pamDeliveryMethod === cMailSecondDay) {
        if($ounces <= 13) {
            return 'FIRST CLASS';
        }
        else {
            return 'PRIORITY';
        }
    }
    err($pamDeliveryMethod." doesn't map to a USPS shipping method");
}
//============================================================================
function fetchUspsMailRate($postalCode, $countryCode, $pamDeliveryMethod, $ounces) {
    if ($pamDeliveryMethod === cMailIntlEconomy) {
        return fetchUspsIntlMailRate($countryCode, $ounces);
    }
    else if (($pamDeliveryMethod === cMailNextDayStandard) ||
            ($pamDeliveryMethod === cMailSecondDay) ||
            ($pamDeliveryMethod === cMailStandard)) {
        return fetchUspsNonIntlMailRate($postalCode, $pamDeliveryMethod, $ounces);
    }
    else {
        err($pamDeliveryMethod." doesn't map to a USPS shipping method");
    }
}
//==============================================================================
function fetchUspsNonIntlMailRate($postalCode, $pamDeliveryMethod, $ounces) {
    // First Class delivery method can't be greater than 13 oz. Otherwise the API gives an error.
    $uspsService = pamDeliveryMethodToUspsShippingMethod($pamDeliveryMethod, $ounces);
    $host = 'http://production.shippingapis.com/ShippingApi.dll';
    $paramName = '?API=RateV4&XML=';
    $paramValue = '<RateV4Request USERID="815BYUPR5306">'.
                    "<Revision>2</Revision>".
                    '<Package ID="0">'.
                        "<Service>$uspsService</Service>".
                        //"<Service>PRIORITY COMMERCIAL</Service>".
                        //"<Service>PRIORITY</Service>".
                        "<FirstClassMailType>PARCEL</FirstClassMailType>".
                        "<ZipOrigination>84602</ZipOrigination>".
                        "<ZipDestination>$postalCode</ZipDestination>".
                        "<Pounds>0</Pounds>".
                        "<Ounces>$ounces</Ounces>".
                        "<Container>VARIABLE</Container>".
                        "<Size>Regular</Size>".
                        "<Width>0</Width>".
                        "<Length>0</Length>".
                        "<Height>0</Height>".
                        "<Girth>0</Girth>".
                        "<Machinable>TRUE</Machinable>".
                    "</Package>".
                "</RateV4Request>";
    $paramValue = urlencode($paramValue);
    $url = $host.$paramName.$paramValue;
    $result = file_get_contents($url);
    if (strExists("<Error>", $result)) {
        $strAfterOpenDescriptionTag = strNthItem(2, $result, '<Description>');
        $description = strNthItem(1, $strAfterOpenDescriptionTag, '</Description>');
        err('USPS returned error... '.$description);
    }

    //$strAfterOpenMailServiceTag = strNthItem(2, $result, '<MailService>');
    //$mailService = strNthItem(1, $strAfterOpenMailServiceTag, '</MailService>');
//fileLog($mailService);

    $strAfterOpenRateTag = strNthItem(2, $result, '<Rate>');
    $rate = strNthItem(1, $strAfterOpenRateTag, '</Rate>');
    errIf($rate <= 0, 'USPS returned rate <= 0 Full output... '.$result);
    return $rate;
}
//============================================================================================
function fetchUspsIntlMailRate($countryCode, $ounces) {
    // USPS International rate calculator automatically chooses delivery method based on weight.
    // $uspsService = pamShippingMethodToUspsShippingMethod($deliveryMethod, $ounces);
    $countries = array
    (
        'AF' => 'Afghanistan',
        'AX' => 'Aland Islands',
        'AL' => 'Albania',
        'DZ' => 'Algeria',
        'AS' => 'American Samoa',
        'AD' => 'Andorra',
        'AO' => 'Angola',
        'AI' => 'Anguilla',
        'AQ' => 'Antarctica',
        'AG' => 'Antigua And Barbuda',
        'AR' => 'Argentina',
        'AM' => 'Armenia',
        'AW' => 'Aruba',
        'AU' => 'Australia',
        'AT' => 'Austria',
        'AZ' => 'Azerbaijan',
        'BS' => 'Bahamas',
        'BH' => 'Bahrain',
        'BD' => 'Bangladesh',
        'BB' => 'Barbados',
        'BY' => 'Belarus',
        'BE' => 'Belgium',
        'BZ' => 'Belize',
        'BJ' => 'Benin',
        'BM' => 'Bermuda',
        'BT' => 'Bhutan',
        'BO' => 'Bolivia',
        'BA' => 'Bosnia And Herzegovina',
        'BW' => 'Botswana',
        'BV' => 'Bouvet Island',
        'BR' => 'Brazil',
        'IO' => 'British Indian Ocean Territory',
        'BN' => 'Brunei Darussalam',
        'BG' => 'Bulgaria',
        'BF' => 'Burkina Faso',
        'BI' => 'Burundi',
        'KH' => 'Cambodia',
        'CM' => 'Cameroon',
        'CA' => 'Canada',
        'CV' => 'Cape Verde',
        'KY' => 'Cayman Islands',
        'CF' => 'Central African Republic',
        'TD' => 'Chad',
        'CL' => 'Chile',
        'CN' => 'China',
        'CX' => 'Christmas Island',
        'CC' => 'Cocos (Keeling) Islands',
        'CO' => 'Colombia',
        'KM' => 'Comoros',
        'CG' => 'Congo',
        'CD' => 'Congo, Democratic Republic',
        'CK' => 'Cook Islands',
        'CR' => 'Costa Rica',
        'CI' => 'Cote D\'Ivoire',
        'HR' => 'Croatia',
        'CU' => 'Cuba',
        'CY' => 'Cyprus',
        'CZ' => 'Czech Republic',
        'DK' => 'Denmark',
        'DJ' => 'Djibouti',
        'DM' => 'Dominica',
        'DO' => 'Dominican Republic',
        'EC' => 'Ecuador',
        'EG' => 'Egypt',
        'SV' => 'El Salvador',
        'GQ' => 'Equatorial Guinea',
        'ER' => 'Eritrea',
        'EE' => 'Estonia',
        'ET' => 'Ethiopia',
        'FK' => 'Falkland Islands (Malvinas)',
        'FO' => 'Faroe Islands',
        'FJ' => 'Fiji',
        'FI' => 'Finland',
        'FR' => 'France',
        'GF' => 'French Guiana',
        'PF' => 'French Polynesia',
        'TF' => 'French Southern Territories',
        'GA' => 'Gabon',
        'GM' => 'Gambia',
        'GE' => 'Georgia',
        'DE' => 'Germany',
        'GH' => 'Ghana',
        'GI' => 'Gibraltar',
        'GR' => 'Greece',
        'GL' => 'Greenland',
        'GD' => 'Grenada',
        'GP' => 'Guadeloupe',
        'GU' => 'Guam',
        'GT' => 'Guatemala',
        'GG' => 'Guernsey',
        'GN' => 'Guinea',
        'GW' => 'Guinea-Bissau',
        'GY' => 'Guyana',
        'HT' => 'Haiti',
        'HM' => 'Heard Island & Mcdonald Islands',
        'VA' => 'Holy See (Vatican City State)',
        'HN' => 'Honduras',
        'HK' => 'Hong Kong',
        'HU' => 'Hungary',
        'IS' => 'Iceland',
        'IN' => 'India',
        'ID' => 'Indonesia',
        'IR' => 'Iran, Islamic Republic Of',
        'IQ' => 'Iraq',
        'IE' => 'Ireland',
        'IM' => 'Isle Of Man',
        'IL' => 'Israel',
        'IT' => 'Italy',
        'JM' => 'Jamaica',
        'JP' => 'Japan',
        'JE' => 'Jersey',
        'JO' => 'Jordan',
        'KZ' => 'Kazakhstan',
        'KE' => 'Kenya',
        'KI' => 'Kiribati',
        'KR' => 'Korea',
        'KW' => 'Kuwait',
        'KG' => 'Kyrgyzstan',
        'LA' => 'Lao People\'s Democratic Republic',
        'LV' => 'Latvia',
        'LB' => 'Lebanon',
        'LS' => 'Lesotho',
        'LR' => 'Liberia',
        'LY' => 'Libyan Arab Jamahiriya',
        'LI' => 'Liechtenstein',
        'LT' => 'Lithuania',
        'LU' => 'Luxembourg',
        'MO' => 'Macao',
        'MK' => 'Macedonia',
        'MG' => 'Madagascar',
        'MW' => 'Malawi',
        'MY' => 'Malaysia',
        'MV' => 'Maldives',
        'ML' => 'Mali',
        'MT' => 'Malta',
        'MH' => 'Marshall Islands',
        'MQ' => 'Martinique',
        'MR' => 'Mauritania',
        'MU' => 'Mauritius',
        'YT' => 'Mayotte',
        'MX' => 'Mexico',
        'FM' => 'Micronesia, Federated States Of',
        'MD' => 'Moldova',
        'MC' => 'Monaco',
        'MN' => 'Mongolia',
        'ME' => 'Montenegro',
        'MS' => 'Montserrat',
        'MA' => 'Morocco',
        'MZ' => 'Mozambique',
        'MM' => 'Myanmar',
        'NA' => 'Namibia',
        'NR' => 'Nauru',
        'NP' => 'Nepal',
        'NL' => 'Netherlands',
        'AN' => 'Netherlands Antilles',
        'NC' => 'New Caledonia',
        'NZ' => 'New Zealand',
        'NI' => 'Nicaragua',
        'NE' => 'Niger',
        'NG' => 'Nigeria',
        'NU' => 'Niue',
        'NF' => 'Norfolk Island',
        'MP' => 'Northern Mariana Islands',
        'NO' => 'Norway',
        'OM' => 'Oman',
        'PK' => 'Pakistan',
        'PW' => 'Palau',
        'PS' => 'Palestinian Territory, Occupied',
        'PA' => 'Panama',
        'PG' => 'Papua New Guinea',
        'PY' => 'Paraguay',
        'PE' => 'Peru',
        'PH' => 'Philippines',
        'PN' => 'Pitcairn',
        'PL' => 'Poland',
        'PT' => 'Portugal',
        'PR' => 'Puerto Rico',
        'QA' => 'Qatar',
        'RE' => 'Reunion',
        'RO' => 'Romania',
        'RU' => 'Russian Federation',
        'RW' => 'Rwanda',
        'BL' => 'Saint Barthelemy',
        'SH' => 'Saint Helena',
        'KN' => 'Saint Kitts And Nevis',
        'LC' => 'Saint Lucia',
        'MF' => 'Saint Martin',
        'PM' => 'Saint Pierre And Miquelon',
        'VC' => 'Saint Vincent And Grenadines',
        'WS' => 'Samoa',
        'SM' => 'San Marino',
        'ST' => 'Sao Tome And Principe',
        'SA' => 'Saudi Arabia',
        'SN' => 'Senegal',
        'RS' => 'Serbia',
        'SC' => 'Seychelles',
        'SL' => 'Sierra Leone',
        'SG' => 'Singapore',
        'SK' => 'Slovakia',
        'SI' => 'Slovenia',
        'SB' => 'Solomon Islands',
        'SO' => 'Somalia',
        'ZA' => 'South Africa',
        'GS' => 'South Georgia And Sandwich Isl.',
        'ES' => 'Spain',
        'LK' => 'Sri Lanka',
        'SD' => 'Sudan',
        'SR' => 'Suriname',
        'SJ' => 'Svalbard And Jan Mayen',
        'SZ' => 'Swaziland',
        'SE' => 'Sweden',
        'CH' => 'Switzerland',
        'SY' => 'Syrian Arab Republic',
        'TW' => 'Taiwan',
        'TJ' => 'Tajikistan',
        'TZ' => 'Tanzania',
        'TH' => 'Thailand',
        'TL' => 'Timor-Leste',
        'TG' => 'Togo',
        'TK' => 'Tokelau',
        'TO' => 'Tonga',
        'TT' => 'Trinidad And Tobago',
        'TN' => 'Tunisia',
        'TR' => 'Turkey',
        'TM' => 'Turkmenistan',
        'TC' => 'Turks And Caicos Islands',
        'TV' => 'Tuvalu',
        'UG' => 'Uganda',
        'UA' => 'Ukraine',
        'AE' => 'United Arab Emirates',
        'GB' => 'United Kingdom',
        'US' => 'United States',
        'UM' => 'United States Outlying Islands',
        'UY' => 'Uruguay',
        'UZ' => 'Uzbekistan',
        'VU' => 'Vanuatu',
        'VE' => 'Venezuela',
        'VN' => 'Viet Nam',
        'VG' => 'Virgin Islands, British',
        'VI' => 'Virgin Islands, U.S.',
        'WF' => 'Wallis And Futuna',
        'EH' => 'Western Sahara',
        'YE' => 'Yemen',
        'ZM' => 'Zambia',
        'ZW' => 'Zimbabwe',
    );
    $country = $countries[$countryCode];

    $host = 'http://production.shippingapis.com/ShippingApi.dll';
    $paramName = '?API=IntlRateV2&XML=';
    $paramValue = '<IntlRateV2Request USERID="815BYUPR5306">'.
                    "<Revision>2</Revision>".
                    '<Package ID="0">'.
                        //"<Service>$uspsService</Service>".
                        //"<Service>PRIORITY COMMERCIAL</Service>".
                        //"<Service>PRIORITY</Service>".
                        "<Pounds>0</Pounds>".
                        "<Ounces>$ounces</Ounces>".
                        "<MailType>PACKAGE</MailType>".
                        "<ValueOfContents>20.00</ValueOfContents>".
                        "<Country>$country</Country>".
                        "<Container>RECTANGULAR</Container>".
                        "<Size>REGULAR</Size>".
                        "<Width>0</Width>".
                        "<Length>0</Length>".
                        "<Height>0</Height>".
                        "<Girth>0</Girth>".
                        "<OriginZip>84602</OriginZip>".
                    "</Package>".
                "</IntlRateV2Request>";
    $paramValue = urlencode($paramValue);
    $url = $host.$paramName.$paramValue;
    $result = file_get_contents($url);
    if (strExists("<Error>", $result)) {
        $strAfterOpenDescriptionTag = strNthItem(2, $result, '<Description>');
        $description = strNthItem(1, $strAfterOpenDescriptionTag, '</Description>');
        err('USPS returned error... '.$description);
    }

    if ($ounces <= 64) {
//      First Class International
        if (strExists('<Service ID="15">', $result)) {
            $tag = '<Service ID="15">';
        }
        else if (strExists('<Service ID="12">', $result)) {
            $tag = '<Service ID="12">';
        }
        else {
            err('service id 15 or 12 not found in output... '.$result);
        }
        $strAfterOpenTag = strNthItem(2, $result, $tag);
        $serviceContents = strNthItem(1, $strAfterOpenTag, '</Service>');
        $strAfterPostageTag = strNthItem(2, $serviceContents, '<Postage>');
        $rate = strNthItem(1, $strAfterPostageTag, '</Postage>');
    }
    else if ($ounces > 64) {
//      Priority Mail International
        if (strExists('<Service ID="2">', $result)) {
            $tag = '<Service ID="2">';
        }
        else if (strExists('<Service ID="12">', $result)) {
            $tag = '<Service ID="12">';
        }
        else {
            err('service id 15 or 12 not found in output... '.$result);
        }
        $strAfterOpenTag = strNthItem(2, $result, $tag);
        $serviceContents = strNthItem(1, $strAfterOpenTag, '</Service>');
        $strAfterPostageTag = strNthItem(2, $serviceContents, '<Postage>');
        $rate = strNthItem(1, $strAfterPostageTag, '</Postage>');
    }
    errIf($rate <= 0, 'USPS returned rate <= 0 Full output... '.$result);
    return $rate;
}
//============================================================================================
function valAddress($address1, $address2, $city, $state, $postalCode) {
    $host = 'http://production.shippingapis.com/ShippingApi.dll';
    $paramName = '?API=Verify&XML=';
    $paramValue = '<AddressValidateRequest USERID="815BYUPR5306">'.
                    "<Revision>1</Revision>".
                    '<Address ID="0">'.
                        "<Address1>$address1</Address1>".
                        "<Address2>$address2</Address2>".
                        "<City>$city</City>".
                        "<State>$state</State>".
                        "<Zip5>$postalCode</Zip5>".
                        "<Zip4></Zip4>".
                    "</Address>".
                "</AddressValidateRequest>";
    $paramValue = urlencode($paramValue);
    $url = $host.$paramName.$paramValue;
// fileLogDebug($url);
    $result = file_get_contents($url);
// fileLogDebug($result);
    if (strExists("<Error>", $result)) {
        $strAfterOpenDescriptionTag = strNthItem(2, $result, '<Description>');
        $description = strNthItem(1, $strAfterOpenDescriptionTag, '</Description>');
        err('USPS returned error... '.$description);
    }

    // Get Deliverable Status, figured by DPVConfirmation Tag being Y or N
    // S Status = Possible confusion with reading Address1. (Ex. rd. = Road, bd. = Boulevard)
    // D Status = Address 2 missing or invalid. (Ex. no apt number when required)
    $strAfterDeliverableTag = strNthItem(2, $result, '<DPVConfirmation>');
    $deliverableStatus = strNthItem(1, $strAfterDeliverableTag, '</DPVConfirmation>');

    // Get Business or Residential Status, figured by the Business tag being Y or N
    $strAfterBusinessTag = strNthItem(2, $result, '<Business>');
    $businessStatus = strNthItem(1, $strAfterBusinessTag, '</Business>');

    // Get confirmation on whether the address is a PO Box from the isPoBox function below
    $valPoBox = isPoBox($address1);

    // Get +4 postal code for additional address clarity
    $strAfterZip4Tag = strNthItem(2, $result, '<Zip4>');
    $zip4 = strNthItem(1, $strAfterZip4Tag, '</Zip4>');

    $validationInfo = array ($deliverableStatus, $businessStatus, $valPoBox, $zip4);
    return $validationInfo;
}
//============================================================================================
function isPoBox($addressLine1) {
    if (stripos($addressLine1, 'PO Box') !== false || stripos($addressLine1, 'P.O. Box') !== false ||
        stripos($addressLine1, 'P.O box') !== false || stripos($addressLine1, 'PO. box') !== false ||
        stripos($addressLine1, 'PO.box') !== false || stripos($addressLine1, 'Post Office box') !== false ||
        stripos($addressLine1, 'PObox') !== false || stripos($addressLine1, 'P O box') !== false ||
        stripos($addressLine1, 'P.O.Box') !== false) {
            return 'T';
    }
    return 'F';
}
//==============================================================================
function fetchMailRate($address1, $address2, $city, $stateCode, $postalCode, $countryCode,
                        $pamDeliveryMethod, $isResidential, $ounces, $lengthInInches, $widthInInches, $heightInInches) {
fileLogDebug(' '.$address1.' '.$address2.' '.$city.' '.$stateCode.' '.$postalCode.' '.$countryCode.
            ' '.$pamDeliveryMethod.' res '.$isResidential.' ounces '.$ounces.' len '.$lengthInInches.' wid '.$widthInInches.' heig '.$heightInInches);
    errIf(isNullOrEmptyStr($postalCode), "Postal code not found.");
    errIf(isNullOrEmptyStr($pamDeliveryMethod), "Delivery method not found.");
    errIf(isNullOrEmptyStr($ounces), "Ounces not found.");
    errIf(isNullOrEmptyStr($countryCode), "Country code not found.");
    errIf($countryCode == "US" && $pamDeliveryMethod === cMailIntlEconomy, 'US shipments may not go international');
    errIf($countryCode !== "US" && $pamDeliveryMethod !== cMailIntlEconomy, 'Non US shipments must go international');
    
    $errMessage = '';
    $allErrMessages = '';

    $mailRates = [];
    try {
fileLogDebug('USPS');
        $uspsMailRate = fetchUspsMailRate($postalCode, $countryCode, $pamDeliveryMethod, $ounces);
        $mailRates[] = $uspsMailRate;
    }
    catch(Exception $e) {
        $errMessage = $e->getMessage();
        fileLog($errMessage);
        $allErrMessages = $errMessage;
    }

    try {
fileLogDebug('FedEx');
        $fedExMailRate = fetchFedExMailRate($address1, $address2, $city, $stateCode,
                                    $postalCode, $countryCode, $pamDeliveryMethod, $isResidential, $ounces);
        $mailRates[] = $fedExMailRate;
    }
    catch(Exception $e) {
        $errMessage = $e->getMessage();
        fileLog($errMessage);
        $allErrMessages = $allErrMessages."\n".$errMessage;
    }

    if($pamDeliveryMethod == cMailIntlEconomy) {
//Brian would like to not use DHL so disabled for now
/*        try {
fileLogDebug('DHL');
            $dhlMailRate = fetchDhlMailRate($city, $postalCode, $countryCode, $ounces, $lengthInInches, $widthInInches, $heightInInches);
            $mailRates[] = $dhlMailRate;
        }
        catch(Exception $e) {
            fileLog($e->getMessage());
        }*/
    }
    else {
        try {
fileLogDebug('UPS');
            $upsMailRate = fetchUpsMailRate($stateCode, $postalCode, $pamDeliveryMethod, $ounces, $lengthInInches, $widthInInches, $heightInInches);
            $mailRates[] = $upsMailRate;
        }
        catch(Exception $e) {
            $errMessage = $e->getMessage();
            fileLog($errMessage);
            $allErrMessages = $allErrMessages."\n".$errMessage;
        }
    }
    errIf(count($mailRates) == 0, "all carrier calculation attempts failed \n".$allErrMessages);
fileLogDebug($mailRates);
    $result = min($mailRates);
fileLogDebug($result);
    return $result;
}
//==============================================================================
function fetchMailRates($address1, $address2, $city, $stateCode, $postalCode, $countryCode,
                        $isResidential, $ounces, $lengthInInches, $widthInInches, $heightInInches) {
    if($countryCode == 'US') {
        $pamDeliveryMethods = [cMailNextDayStandard, cMailSecondDay, cMailStandard];
    }
    else {
        $pamDeliveryMethods = [cMailIntlEconomy];
    }
    $rates = [];
    foreach($pamDeliveryMethods as $pamDeliveryMethod){
fileLog($pamDeliveryMethod);
        $individualRate = fetchMailRate($address1, $address2, $city, $stateCode, $postalCode, $countryCode,
                                        $pamDeliveryMethod, $isResidential, $ounces,
                                        $lengthInInches, $widthInInches, $heightInInches);
        $rates[$pamDeliveryMethod] = $individualRate;
    }
fileLogDebug($rates);
	return $rates;
}
