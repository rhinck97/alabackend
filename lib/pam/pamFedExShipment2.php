<?php
require_once 'protected/lib/onyx/onyxCore.php';
require_once 'protected/lib/pam/pamCore.php';
require_once 'protected/lib/FedExRateService_v18_php/library/fedex-common.php';
// require_once 'protected/lib/FedExRateService_v18_php/AddressValidation/examples/bootstrap.php';
// require_once 'protected/lib/FedExRateService_v18_php/AddressValidation/';
//------------------------------------
// Copyright 2009, FedEx Corporation. All rights reserved.
// Version 12.0.0
// The following function wraps a sample script from fedex. The code has been modified
// to used values from passed in parameters
//------------------------------------

// use FedEx\AddressValidationService;
// use FedEx\AddressValidationService\ComplexType;
// use FedEx\AddressValidationService\SimpleType;

function fetchFedExMailRatesSoap($address1, $address2, $city, $state, $postalCode,
                                    $countryCode, $fedExServiceType, $isResidential,
                                    $lineItemsWeightsInPounds) {


    //The soap client object takes a bunch of data formatted in the following structure
    //certain values are now set from the data passed in or hard coded for things like the
    //key, password, and contact information
    $request['WebAuthenticationDetail'] = array(
            //'ParentCredential'=>array(
            //        'Key'=>getProperty('parentkey'),
            //        'Password'=>getProperty('parentpassword')
            //),
            'UserCredential'=>array(
                    'Key'=>'ynKJIg4svr6mGJVj',
                    'Password'=>'sMa2zv6kpLuWQKG8eHgq98iC6'
            )
    );
    $request['ClientDetail'] = array(
            'AccountNumber'=>'128165428',
            'MeterNumber'=>'103196341'
    );
    $request['TransactionDetail'] = array('CustomerTransactionId'=>' *** Rate Request using PHP ***');
    $request['Version'] = array(
            'ServiceId'=>'crs',
            'Major'=>'18',
            'Intermediate'=>'0',
            'Minor'=>'0'
    );
    $request['ReturnTransitAndCommit'] = true;
    $request['RequestedShipment']['DropoffType'] = 'REGULAR_PICKUP'; // valid values REGULAR_PICKUP, REQUEST_COURIER, ...
    $request['RequestedShipment']['ShipTimestamp'] = date('c');
    $request['RequestedShipment']['ServiceType'] = $fedExServiceType; // valid values STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND, ...
    $request['RequestedShipment']['PackagingType'] = 'YOUR_PACKAGING'; // valid values FEDEX_BOX, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, ...
    $request['RequestedShipment']['TotalInsuredValue'] = array(
            'Ammount'=>100,
            'Currency'=>'USD'
    );
    $request['RequestedShipment']['Shipper'] = buildShipper();
    $request['RequestedShipment']['Recipient'] = buildRecipient($state, $postalCode, $countryCode, $isResidential);
    $request['RequestedShipment']['ShippingChargesPayment'] = buildShippingChargesPayment();
    $request['RequestedShipment']['PackageCount'] = count($lineItemsWeightsInPounds);
    //$request['RequestedShipment']['PackageCount'] = '1';
    $request['RequestedShipment']['RequestedPackageLineItems'] = buildPackageLineItems($lineItemsWeightsInPounds);

    //The request object is now set up time to pass to the soap client
    try {
        $path_to_wsdl = "protected/lib/FedExRateService_v18_php/RateService_v18.wsdl";
        ini_set("soap.wsdl_cache_enabled", "0");
        $client = new SoapClient($path_to_wsdl, array('trace'=>1)); // Refer to http://us3.php.net/manual/en/ref.soap.php for more information
        $response = $client->getRates($request);
    }
    catch(SoapFault $exception) {
        printFault($exception, $client);
        //errHttp();
    }

    if($response->HighestSeverity == 'FAILURE' || $response->HighestSeverity == 'ERROR') {
        $str = notificationsToStr($response->Notifications);
        err('FedEx returned error... '.$response->HighestSeverity.' '.$str);
    }

    //create a visual table
    $rateReply = $response->RateReplyDetails;
    /*echo '<table border="1">';
            echo '<tr><td>Service Type</td><td>Amount</td><td>Delivery Date</td></tr><tr>';
            $serviceType = '<td>'.$rateReply->ServiceType.'</td>';
            if($rateReply->RatedShipmentDetails && is_array($rateReply->RatedShipmentDetails)) {
                $amount = '<td>$'.number_format($rateReply->RatedShipmentDetails[0]->ShipmentRateDetail->TotalNetCharge->Amount, 2, ".", ",").'</td>';
            }
            elseif($rateReply->RatedShipmentDetails && !is_array($rateReply->RatedShipmentDetails)) {
                $amount = '<td>$'.number_format($rateReply->RatedShipmentDetails->ShipmentRateDetail->TotalNetCharge->Amount, 2, ".", ",").'</td>';
            }
            if(array_key_exists('DeliveryTimestamp', $rateReply)) {
                $deliveryDate = '<td>'.$rateReply->DeliveryTimestamp.'</td>';
            }
            else if(array_key_exists('TransitTime', $rateReply)) {
                $deliveryDate = '<td>'.$rateReply->TransitTime.'</td>';
            }
            else {
                $deliveryDate = '<td>&nbsp;</td>';
            }
            echo $serviceType.$amount.$deliveryDate;
            echo '</tr>';
    echo '</table>';*/

    //get the data
    if($rateReply->RatedShipmentDetails && is_array($rateReply->RatedShipmentDetails)) {
        $amount = $rateReply->RatedShipmentDetails[0]->ShipmentRateDetail->TotalNetCharge->Amount;
    }
    elseif($rateReply->RatedShipmentDetails && !is_array($rateReply->RatedShipmentDetails)) {
        $amount = $rateReply->RatedShipmentDetails->ShipmentRateDetail->TotalNetCharge->Amount;
    }

    //printReply($client, $response);
	//writeToLog($client);	// Write to log file
    //errIf($amount <= 0, 'FedEx returned bad rate');
    errIf($amount <= 0, 'FedEx returned rate <= 0... '.$amount);
    return $amount;
}
//----------------------------------------------
function notificationsToStr($notes) {
    $result = '';
    foreach($notes as $noteKey=> $note) {
        if(is_string($note)) {
            $result = $noteKey.': '.$note.Newline;
        }
        else {
            $result .= notificationsToStr($note);
        }
    }
    return $result;
}
//------------------------------------
//this function is modified from the fedex sample
//------------------------------------
function buildShipper() {//this should be fine or close to fine
    $shipper = array(
            'Contact'=>array(
                    'PersonName'=>'',
                    'CompanyName'=>'BYU Print and Mail',
                    'PhoneNumber'=>'8014222855'),
            'Address'=>array(
                    'StreetLines'=>array('701 E University Pkwy'),
                    'City'=>'Provo',
                    'StateOrProvinceCode'=>'UT',
                    'PostalCode'=>'84602',
                    'CountryCode'=>'US')
    );
    return $shipper;
}
//------------------------------------
//this function is modified from the fedex sample
//------------------------------------
function buildRecipient($state, $postalCode, $countryCode, $isResidential) {//this should be fine or close to fine
    $recipient = array(
            'Contact'=>array(
                    'PersonName'=>'',
                    'CompanyName'=>'',
                    'PhoneNumber'=>''
            ),
            'Address'=>array(
                    'StreetLines'=>array(''),
                    'City'=>'',
                    'StateOrProvinceCode'=>$state,
                    'PostalCode'=>$postalCode,
                    'CountryCode'=>$countryCode,
                    'Residential'=>$isResidential)
    );
    return $recipient;
}
//------------------------------------
//this function is modified from the fedex sample
//------------------------------------
function buildShippingChargesPayment() {//this should be fine or close to fine
    $shippingChargesPayment = array(
            'PaymentType'=>'SENDER', // valid values RECIPIENT, SENDER and THIRD_PARTY
            'Payor'=>array(
                    'ResponsibleParty'=>array(
                            'AccountNumber'=>'128165428',
                            'CountryCode'=>'US'
                    )
            )
    );
    return $shippingChargesPayment;
}
//------------------------------------
//this function is modified from the fedex sample
//------------------------------------
function buildPackageLineItem($weightInPounds, $sequenceNumber) {//this should be fine or close to fine
    $packageLineItem = array(
            'SequenceNumber'=>$sequenceNumber,
            'GroupPackageCount'=>1,
            'Weight'=>array(
                    'Value'=>$weightInPounds,
                    'Units'=>'LB'
            )
    );
    return $packageLineItem;
}
//------------------------------------
// the following functions are to help in calling the fedex functions above
//------------------------------------
function buildPackageLineItems($lineItemsWeights) {
    $itemNumber = 0;
    $packageLineItems = array();
    foreach($lineItemsWeights as $lineItemWeight) {
        $itemNumber++;
        $packageLineItem = buildPackageLineItem($lineItemWeight, $itemNumber);
        $packageLineItems[] = $packageLineItem;
    }
    return $packageLineItems;
}
//----------------------------------------------------
// the following functions help convert inputs to values fedex needs
//----------------------------------------------------
function ouncesToLineItemWeightsInPounds($totalOunces) {
    errIfNull($totalOunces, 'Ounces missing');
    $lineItemsWeightsInPounds = array();
    $maxOuncesPerBox = 54 * 16;
    $remainingOunces = $totalOunces;
    while($remainingOunces >= $maxOuncesPerBox) {
        $weightInPounds = $maxOuncesPerBox / 16;
        $lineItemsWeightsInPounds[] = $weightInPounds;
        $remainingOunces -= $maxOuncesPerBox;
    }
    if($remainingOunces > 0) {
        $weightInPounds = $remainingOunces / 16;
        $lineItemsWeightsInPounds[] = $weightInPounds;
    }
    return $lineItemsWeightsInPounds;
}
//------------------------------------------------------------------------------
//the above code is mostly fedexcode with few changes by print and mail
//everything below is new functions to aid in using the above written by print and mail
//------------------------------------------------------------------------------
function pamDeliveryMethodToFedExServiceType($pamDeliveryMethod, $isResidential) {//this should be fine or close to fine
    if($pamDeliveryMethod === cMailIntlEconomy) {
        return 'INTERNATIONAL_ECONOMY';
    }
    else if($pamDeliveryMethod === cMailNextDayStandard) {
        return 'STANDARD_OVERNIGHT';
    }
    else if($pamDeliveryMethod === cMailSecondDay) {
        return 'FEDEX_2_DAY';
    }
    else if($pamDeliveryMethod === cMailStandard) {
        if($isResidential) {
            return 'GROUND_HOME_DELIVERY';
        }
        else {
            return 'FEDEX_GROUND';
        }
    }
    err($pamDeliveryMethod." doesn't map to a FedEx shipping method");
}
//=========================================
function fetchFedExMailRate($address1, $address2, $city, $stateCode,
                            $postalCode, $countryCode, $deliveryMethod, $isResidential, $ounces) {
    $fedExServiceType = pamDeliveryMethodToFedExServiceType($deliveryMethod, $isResidential);
    $lineItemsWeightsInPounds = ouncesToLineItemWeightsInPounds($ounces);
    $rate = fetchFedExMailRatesSoap($address1, $address2, $city,
                                $stateCode, $postalCode, $countryCode,
                                $fedExServiceType, $isResidential,
                                $lineItemsWeightsInPounds);
    return $rate;
}
//=========================================
function fetchFedExMailRates($address1, $address2, $city, $stateCode, $postalCode, $countryCode, $isResidential, $ounces) {
        if ($countryCode === "US"){
            $validDeliveryMethods = [cMailNextDayStandard, cMailSecondDay, cMailStandard];
        }
        else {
            $validDeliveryMethods = [cMailIntlEconomy];
        }
        $rates = [];

        foreach ($validDeliveryMethods as $deliveryMethod){
            $individualRate = fetchFedExMailRate($address1, $address2, $city, $stateCode, $postalCode, $countryCode, $deliveryMethod, $isResidential, $ounces);
            $rates[$deliveryMethod] = $individualRate;
        }

	return $rates;
}
