<?php
require_once("protected/lib/onyx/onyxYii.php");
class PricingAvanti extends Pricing {
    public function calcJobCharge($job) {//
        if(!toBool($job->productType->INPRODUCTTABLE)) {
            return 0;
        }

        $productCount = $job->PRODUCTCOUNT;
        $productDtls = $job->product->productDtls();
        $avantiPrice = 0;
        foreach($productDtls as $productDtl) {
            $avantiPriceSql = "select whatever from sometable where id = $productDtl->ITEM";
            $avantiPrice += queryScalar($avantiPriceSql);
        }
        return $avantiPrice;
    }
}