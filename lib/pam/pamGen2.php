<?php
//needs a code review. this should have models
//------------------------------------------------------------------------------
//The following define check is to allow
//require_once('SomeScript.php') and require_once('..\SomeScript.php')
//to work when they are the same file. This happens when scripts are in
//subfolders
if(defined('pamGen2.php')) {
    return;
}
define('pamGen2.php', '');
//------------------------------------------------------------------------------
require_once 'pamCore.php';
require_once 'onyxYiiOdbc.php';
//------------------------------------------------------------------------------
define('cGen2UspsCode', 1);
define('cGen2FedExCode', 3);
define('cGen2DhlCode', 4);

define('cGen2Usps1stClassLetter', 2000);
define('cGen2UspsPriorityMail', 2051);
define('cGen2UspsMediaMail', 2510);

define('cGen2FedExGround', 1000);//this is business
define('cGen2FedExHomeDelivery', 1001);
define('cGen2FedEx2Day', 3000);
define('cGen2FedExPriorityOvernight', 3001);
define('cGen2FedExStandardOvernight', 3002);
define('cGen2FedExInternationalFirst', 3010);
define('cGen2FedExInternationalPriority', 3011);
define('cGen2FedExInternationalEconomy', 3012);

define('cGen2DhlInternationalDocument', 8000);
define('cGen2DhlWorldwidePriorityExpress', 8001);

//TODO cDhlInternationalFuelSurCharge make dynamic
define('cGen2DhlInternationalFuelSurCharge', 0.115);
//------------------------------------------------------------------------------
function yGen2ConnectElseErr() {
    $connectionString = Yii::app()->params['gen2']['connectionString'];
    $username = Yii::app()->params['gen2']['username'];
    $password = Yii::app()->params['gen2']['password'];
    return odbcConnectElseErr($username, $password, $connectionString);
}
//------------------------------------------------------------------------------
function yGen2FetchCityStateInfo($zipCode) {
    $gen2Db = yGen2ConnectElseErr();
    $zipRow = odbcFetchRow($gen2Db, "select * from City_State_Reference_T where Zip_Code_VC = $zipCode;");
    return $zipRow;
}
//------------------------------------------------------------------------------
function yGen2CleanCityStateZipCountry(&$city, &$state, &$zip, &$countryNameToEncode) {
    $city = strtoupper($city);
    $state = strtoupper($state);
    $countryNameToEncode = strToUpper($countryNameToEncode);

    if($countryNameToEncode !== null) {
        $countryCode = yGen2FetchCountryCode($countryNameToEncode);
        if($countryCode !== false)
            $countryNameToEncode = $countryCode;
    }

    if((($countryNameToEncode === 'US') || ($countryNameToEncode === null)) &&
            (!isNullOrEmptyStr($zip))) {
        $zipRow = yGen2FetchCityStateInfo($zip); //todo: how slow is this
        if($zipRow !== false) {
            $state = arrayGet($zipRow, 'State_VC');
            $city = arrayGet($zipRow, 'City_Name_VC');
        }
    }
}
//------------------------------------------------------------------------------
function yGen2FindFedExCode($pamShippingMethod, $isResidential) {
    if(($pamShippingMethod === cMailNextDayPriority) || ($pamShippingMethod === cMailNextDayRound))
        return cGen2FedExPriorityOvernight;
    if($pamShippingMethod === cMailNextDayStandard)
        return cGen2FedExStandardOvernight;
    if($pamShippingMethod === cMailSecondDay)
        return cGen2FedEx2Day;
    if($pamShippingMethod === cMediaMail)
        return cGen2UspsMediaMail;
    if($pamShippingMethod === cMailIntlEconomy)
        return cGen2FedExInternationalEconomy;
    if($pamShippingMethod === cMailIntlPriority)
        return cGen2FedExInternationalPriority;
    if($pamShippingMethod === cMailStandard) {
        if($isResidential)
            return cGen2FedExHomeDelivery;
        else
            return cGen2FedExGround;
    }
    err($pamShippingMethod." doesn't map to a FedEx code");
}
//------------------------------------------------------------------------------
function yGen2FindDhlCode($pamMailCode, $isDocument) {
    if(($pamMailCode === cMailIntlPriority) ||
            ($pamMailCode === cMailIntlEconomy) ||
            ($pamMailCode === cMailStandard)) {
        if($isDocument)
            return cGen2DhlInternationalDocument;
        else
            return cGen2DhlWorldwidePriorityExpress;
    }
    err($pamMailCode." doesn't map to a DHL code");
}
//------------------------------------------------------------------------------
function yGen2FindServiceCode($countryCode, $pamMailCode, $isResidential, $isDocument) {
    if($countryCode == 'US')
        return yGen2FindFedExCode($pamMailCode, $isResidential);
    else
        return yGen2FindDhlCode($pamMailCode, $isDocument);
}
//------------------------------------------------------------------------------
function yGen2FetchFuelSurChargePercent($gen2CarrierCode) {
    $conn = yGen2ConnectElseErr();
    $fuelSurChargePercent = odbcFetchValue($conn, "select fuelsurcharge_fl from carrier_data_t where carrier_id = '$gen2CarrierCode'") / 100;
    ErrIfNull($fuelSurChargePercent, 'fuelsurcharge not found');
    return $fuelSurChargePercent;
}
//------------------------------------------------------------------------------
function yGen2CalculateFuelSurCharge($shippingCharge, $gen2CarrierCode) {
    $fuelSurChargePercent = yGen2FetchFuelSurChargePercent($gen2CarrierCode);
    return round($shippingCharge * $fuelSurChargePercent, 2);
}
//------------------------------------------------------------------------------
function yGen2DestTaxable($postalCode, $countryCode) {
    if($countryCode === 'US' && yGen2FetchStateCode($postalCode) == 'UT')
        return 1;
    else
        return 0;
}
//------------------------------------------------------------------------------
function yGen2CalculateExtraShippingCharges($shippingCharge, $gen2CarrierCode, $postalCode, $countryCode) {
    $conn = yGen2ConnectElseErr();
    $fuelSurCharge = yGen2CalculateFuelSurCharge($shippingCharge, $gen2CarrierCode);
    $handlingCharge = (float) odbcFetchValue($conn, "select flatratehandlingcharge_mn from carrier_data_t where carrier_id = '$gen2CarrierCode'");
    $destTaxable = yGen2DestTaxable($postalCode, $countryCode);
    return yBuildMailChargeInfo($shippingCharge, $fuelSurCharge, $handlingCharge, $destTaxable);
}
//------------------------------------------------------------------------------
//function yCalculateDomesticCharges($shippingCharge, $gen2CarrierCode, $zipCode)
//{
//  $fuelSurCharge = yCalculateFuelSurCharge($shippingCharge, $gen2CarrierCode);
//  $handlingCharge = odbcFetchValue($conn, "select flatratehandlingcharge_mn from carrier_data_t where carrier_id = '$serviceCode'");
//  if (yFetchStateCode($zipCode) == 'UT')
//    $destTaxable = 1;
//  else
//    $destTaxable = 0;
//  return yBuildMailChargeInfo($shippingCharge, $fuelSurCharge, $handlingCharge, $destTaxable);
//}
//------------------------------------------------------------------------------
function yGen2CalculateIntlCharges($shippingCharge, $fuelSurChargePercent) {
    $fuelSurCharge = $shippingCharge * $fuelSurChargePercent;
    $handlingCharge = 2;
    return yBuildMailChargeInfo($shippingCharge, $fuelSurCharge, $handlingCharge, 0);
}
//------------------------------------------------------------------------------
function yGen2FetchMailChargeFedExIntl($countryCode, $mailCode, $ounces, $isDocument) {
    errIf($ounces <= 0, "$ounces not a valid weight.");
    errIfNullOrEmptyStr($countryCode, "countryCode must be entered for international FedEx shipments");
    $fedexCode = yGen2FindFedExCode($mailCode, $isDocument);
    errIfNull($fedexCode, "Service '$mailCode' is not a valid service for international FedEx shipments. ".
            "Valid services are: ".
            cMailIntlPriority.','.
            cMailIntlEconomy);
    $conn = yGen2ConnectElseErr();
    $zoneSql = "SELECT FEDEX_CODE_VC FROM COUNTRY_CODES_T WHERE ISO_CODE_VC='$countryCode'";
    $zone = odbcFetchValue($conn, $zoneSql);
    errIf($zone == false, "countryCode $countryCode invalid for Fedex shipments");
    errIf($zone == 'XXX', "countryCode $countryCode invalid for Fedex shipments");
    $pounds = ceil(ouncesToPounds($ounces));
    $chargeSql = "SELECT $zone FROM FedEx_Intl_Custom_Rates_0_T WHERE Pkg_Weight_VC='$pounds' ".
            "AND CARRIER_CLASS_CODE_IN=".$fedexCode;
    $shippingCharge = (float) odbcFetchValue($conn, $chargeSql);
    return yGen2CalculateExtraShippingCharges($shippingCharge, $fedexCode, 'XX', $countryCode);
}
//------------------------------------------------------------------------------
function yGen2FetchMailChargeFedExDomestic($postalCode, $mailCode, $ounces, $isResidential = false) {
    errIf($ounces <= 0, "$ounces not a valid weight.");
    errIfNullOrEmptyStr($postalCode, "zipCode must be entered for domestic FedEx shipments");
    $fedexCode = yGen2FindFedExCode($mailCode, $isResidential);
    errIfNull($fedexCode, "Service '$mailCode' is not a valid service for domestic FedEx shipments. ".
            "Valid services are: ".
            cMailStandard.','.
            cMailSecondDay.','.
            cMailNextDay);

    $conn = yGen2ConnectElseErr();
    $threeDigitZip = substr($postalCode, 0, -2);
    if($fedexCode == cGen2FedExHomeDelivery || $fedexCode == cGen2FedExGround) {
        $zoneCodeSql = "SELECT FEDEX_GROUND_SI FROM ZIP_TO_ZONE_T WHERE DESTINATION_ZIP_VC = $postalCode";
        $zoneCode = ODBCFetchValue($conn, $zoneCodeSql);
        if(!$zoneCode) {
            $zoneCodeSql = "SELECT FEDEX_GROUND_SI FROM ZIP_TO_ZONE_T WHERE DESTINATION_ZIP_VC = $threeDigitZip";
            $zoneCode = ODBCFetchValue($conn, $zoneCodeSql);
        }
    }
    else if($fedexCode == cGen2FedEx2Day || $fedexCode == cGen2FedExPriorityOvernight) {
        $zoneCodeSql = "SELECT FEDEX_EXPRESS_SI FROM ZIP_TO_ZONE_T WHERE DESTINATION_ZIP_VC = $postalCode";
        $zoneCode = ODBCFetchValue($conn, $zoneCodeSql);
        if(!$zoneCode) {
            $zoneCodeSql = "SELECT FEDEX_EXPRESS_SI FROM ZIP_TO_ZONE_T WHERE DESTINATION_ZIP_VC = $threeDigitZip";
            $zoneCode = ODBCFetchValue($conn, $zoneCodeSql);
        }
    }
    $pounds = ceil(ouncesToPounds($ounces));
    errIf($zoneCode == false, "zipCode $postalCode invalid for fedEx shipments");
    errIf($zoneCode == 'XXX', "zipCode $postalCode invalid for fedEx shipments");
    $chargeSql = "SELECT \"$zoneCode\" FROM FedEx_Domestic_Custom_Rates_0_T WHERE Pkg_Weight_VC='$pounds'
              AND CARRIER_CLASS_CODE_IN=$fedexCode";
    $shippingCharge = (float)ODBCFetchValue($conn, $chargeSql);

    $areaChargeQuerySql = "SELECT * FROM FedEx_Delivery_Area_Zips_T WHERE Rual_Zip_VC = $postalCode";
    $areaChargeQuery = ODBCFetchValue($conn, $areaChargeQuerySql);

    if($areaChargeQuery !== false) {
        if($isResidential) {
            $areaChargeSql = "SELECT Grd_Del_Area_Surcharge_Res_MN FROM FedEx_Custom_Fees_0_T";
        }
        else {
            $areaChargeSql = "SELECT Grd_Del_Area_Surcharge_Com_MN FROM FedEx_Custom_Fees_0_T";
        }
        $areaCharge = (float)ODBCFetchValue($conn, $areaChargeSql);
        $shippingCharge = $shippingCharge + $areaCharge;
    }

    // TODO fix this so the error for maximum pounds exceeded is displayed instead of the zipCode one.
    errIf($shippingCharge == 0, 'FedEx does not offer the specified service to the given zipCode.');
    return yGen2CalculateExtraShippingCharges($shippingCharge, $fedexCode, $postalCode, 'US');
}
//------------------------------------------------------------------------------
function yGen2FetchMailChargeDhlIntl($countryCode, $mailCode, $ounces, $isDocument = true) {
    errIf($ounces <= 0, "$ounces not a valid weight.");
    errIf($countryCode == 'US' || $countryCode == null, '');
    errIfNullOrEmptyStr($mailCode, "serviceCode missing");
    $dhlCode = yGen2FindDhlCode($mailCode, $isDocument);
    errIfNull($dhlCode, "Service '$mailCode' is not a valid service for DHL. ".
            "Valid services are: ".
            cMailIntlPriority);
    $conn = yGen2ConnectElseErr();
    $zoneSql = "SELECT DHL_CODE_VC FROM COUNTRY_CODES_T WHERE ISO_CODE_VC='$countryCode'";
    $zone = odbcFetchValue($conn, $zoneSql);
    errIf($zone == false, "countryCode $countryCode invalid for DHL shipments");
    errIf($zone == 'XXX', "countryCode $countryCode invalid for DHL shipments");
    $pounds = ceil(ouncesToPounds($ounces));
    $chargeSql = "SELECT $zone FROM DHL_Intl_Custom_Rates_0_T WHERE Pkg_Weight_VC='$pounds' ".
            "AND CARRIER_CLASS_CODE_IN=".$dhlCode;
    $shippingCharge = (float)odbcFetchValue($conn, $chargeSql);
    return yGen2CalculateIntlCharges($shippingCharge, cGen2DhlInternationalFuelSurCharge);
}
//------------------------------------------------------------------------------
function yGen2FetchMailChargeInfo($countryCode, $zipCode, $mailCode, $ounces, $isResidential, $isDocument) {
    if(yMailCodeNeedsCarrier($mailCode)) {
        if($countryCode == 'US' || $countryCode == null)
            err("error processing mailtype $mailCode");
        //$result = yGen2FetchMailChargeFedExDomestic($zipCode, $mailCode, $ounces, $isResidential);
        //else if ($mailCode == 'INTLECONOMY')
        //err("error processing mailtype $mailCode");
        //$result = yGen2FetchMailChargeFedExIntl($countryCode, $mailCode, $ounces, $isDocument);
        else
            $result = yGen2FetchMailChargeDhlIntl($countryCode, $mailCode, $ounces, $isDocument);
    }
    else
        $result = yBuildMailChargeInfo(0, 0, 0, 1); // TODO find out if we should charge handling for this
    return $result;
}
//------------------------------------------------------------------------------
function yGen2FetchStateCode($zip) {
    $zip = substr((string)$zip, 0, 5);
    $conn = yGen2ConnectElseErr();
    $sql = "select State_VC from City_State_Reference_T where Zip_Code_VC = '$zip'";
    $state = odbcFetchValue($conn, $sql);
    odbcCloseElseErr($conn);
    return $state;
}
//------------------------------------------------------------------------------
class Address_Book {
    public $Number;
    public $Name;
    public $Address1;
    public $City;
    public $State;
    public $Zip_Code;
    public $Country;
    public $Residential_Flag;
    public $Order_Download;
    public $Phone;
    public $Address2;
    public $CompanyID;
    public $Address3;
    //public $Attn_Name;
    //public $Phone_Ext;
    //public $Fax_Number;
    //public $TaxID;
}
//------------------
function yRtsInsertIntoAddress_Book($conn, $obj) {
    yRtsFormatObject($obj);

    if($obj->Phone == '' && $obj->Country == 'UNITED STATES')
        $obj->Phone = '000-000-0000';

//  if ($obj->Country == 'UNITED STATES')
//  {
//    $sql = "INSERT INTO dbo.Address_Book_T
//             (Ship_To_Number_VC, Ship_To_Name_VC, Ship_To_Address_1_VC,
//                 Ship_To_City_VC, Ship_To_State_VC,Ship_To_Zip_Code_VC,
//                 Ship_To_Country_VC,Ship_to_Residential_Flag_BT,
//                 Order_Download_BT, Ship_To_Phone_VC, Ship_To_Address_2_VC,
//                 Company_ID)
//              VALUES
//              ('$obj->Number', '$obj->Name', '$obj->Address1', '$obj->City',
//                '$obj->State', '$obj->Zip_Code', '$obj->Country',
//                 $obj->Residential_Flag, $obj->Order_Download, '$obj->Phone',
//                 '$obj->Address2', $obj->CompanyID)";
//  }
//  else
//  {
    $sql = "INSERT INTO dbo.Address_Book_T
             (Ship_To_Number_VC, Ship_To_Name_VC, Ship_To_Address_1_VC,
                 Ship_To_Address_2_VC, Ship_To_Address_3_VC,Ship_To_City_VC,
                 Ship_To_Country_VC,Ship_to_Residential_Flag_BT,
                 Order_Download_BT, Ship_To_Phone_VC, Company_ID,
                 Ship_To_State_VC, Ship_To_Zip_Code_VC)
              VALUES
              ('$obj->Number', '$obj->Name', '$obj->Address1', '$obj->Address2',
                '$obj->Address3', '$obj->City', '$obj->Country',
                 $obj->Residential_Flag, $obj->Order_Download, '$obj->Phone'
                  , $obj->CompanyID, '$obj->State', '$obj->Zip_Code')";
//  }
    odbcRunSql($conn, $sql);
    $primaryKey = ODBCGetPrimaryKey($conn);
    return $primaryKey;
}
//------------------------------------------------------------------------------
class rtsTransaction_Main {
    public $Acct_ID;
    public $Ounces;
    public $Dimensional_Ounces;
    public $Carrier_ID;
    public $Number_Of_Pieces;
    public $Base_Rate;
    public $System_Date_Time;
    public $Manifest_Date_Time;
    public $Uploaded;
    public $Closed;
    public $Zone;
    public $Company_ID;
    public $ShipFrom_ID;
    public $Total_Charges;
    public $Carrier_Number_ID;
    public $Machine_Key;
}
//------------------
function yRtsInsertIntoRTS_Transaction_Main($Conn, $Obj) {
    yRtsFormatObject($Obj);
    $Sql = "INSERT INTO RTS_TRANSACTION_MAIN_T
                   (Acct_ID,Ounces_FL,Dimensional_Ounces_FL,Carrier_ID,
                    Number_Of_Pieces_IN,Base_Rate_MN,System_Date_Time_DT,
                    Manifest_Date_Time_DT,Uploaded_BT,Closed_IN,Zone_VC,
                    Company_ID,ShipFrom_ID,Total_Charges_MN,Carrier_Number_ID,
                    Machine_Key_VC)
                 VALUES
                   ($Obj->Acct_ID, $Obj->Ounces, $Obj->Dimensional_Ounces,
                    $Obj->Carrier_ID, $Obj->Number_Of_Pieces, $Obj->Base_Rate,
                    '$Obj->System_Date_Time', '$Obj->Manifest_Date_Time',
                    $Obj->Uploaded, $Obj->Closed, '$Obj->Zone', $Obj->Company_ID,
                    $Obj->ShipFrom_ID, $Obj->Total_Charges,
                    $Obj->Carrier_Number_ID, '$Obj->Machine_Key')";
    odbcRunSql($Conn, $Sql);
    $PrimaryKey = ODBCGetPrimaryKey($Conn);
    return $PrimaryKey;
}
//------------------------------------------------------------------------------
class Address_Book_FedEx_Billing {
    public $Ship_To_ID;
    public $S_Payment_Type;
    public $S_Account_Number;
    public $S_3RD_Country;
    public $D_Payment_Type;
    public $D_Account_Number;
    public $D_3RD_Country;
    public $Credit_Card_Type;
}
//------------------
function yRtsInsertIntoAddress_Book_FedEx_Billing($Conn, $Obj, $PKey) {
    rtsFormatObject($Obj);
    $Sql = "INSERT INTO dbo.Address_Book_FedEx_Billing_T
                  (Ship_To_ID, BST_Payment_Type_SI, BST_Account_Number_VC,
                   BST_3RD_Country_VC, BDT_Payment_Type_SI, BDT_Account_Number_VC,
                   BDT_3RD_Country_VC, Credit_Card_Type_VC)
                VALUES
                  ($PKey, '$Obj->S_Payment_Type', '$Obj->S_Account_Number',
                   '$Obj->S_3RD_Country', '$Obj->D_Payment_Type',
                   '$Obj->D_Account_Number', '$Obj->D_3RD_Country',
                   '$Obj->Credit_Card_Type')";
    odbcRunSql($Conn, $Sql);
}
//------------------------------------------------------------------------------
class Address_Book_UPS_Quantum_View {
    public $Ship_To_ID;
}
//------------------
function yRtsInsertIntoAddress_Book_UPS_Quantum_View($Conn, $Obj, $PKey) {
    yRtsFormatObject($Obj);
    $Sql = "INSERT INTO ADDRESS_BOOK_UPS_QUANTUM_VIEW_T
                   (Ship_To_ID)
                 VALUES
                   ($PKey)";
    odbcRunSql($Conn, $Sql);
}
//------------------------------------------------------------------------------
class rtsTransaction_Address_Book_Misc {
    public $Preferred_Carrier;
    public $COD_Only_Flag;
    public $COD_Cash_Only_Flag;
    public $Global_Email_Flag;
    public $Attn_Email_Flag;
    public $Address_Validation_SI;
}
//------------------
function yRtsInsertIntoRTS_Transaction_Address_Book_Misc($Conn, $Obj, $Ship_To_ID, $Trans_ID) {
    yRtsFormatObject($Obj);
    $Sql = "INSERT INTO RTS_TRANSACTION_ADDRESS_BOOK_MISC_T
                   (Trans_ID,Ship_To_ID,Preferred_Carrier_SI,COD_Only_Flag_BT,
                   COD_Cash_Only_Flag_BT,Global_Email_Flag_BT,
                   Attn_Email_Flag_BT,Address_Validation_SI)
                 VALUES
                   ($Trans_ID, $Ship_To_ID, $Obj->Preferred_Carrier,
                   $Obj->COD_Only_Flag, $Obj->COD_Cash_Only_Flag,
                   $Obj->Global_Email_Flag, $Obj->Attn_Email_Flag,
                   $Obj->Address_Validation_SI)";

    odbcRunSql($Conn, $Sql);
}
//------------------------------------------------------------------------------
class rtsTransaction_Address_Book_FedEx_Billing {
    public $BDT_Payment_Type;
    public $BST_Account_Number;
    public $BST_3RD_Country;
    public $BDT_Account_Number;
    public $BDT_3RD_Country;
    public $Credit_Card_Type;
}
//------------------
function yRtsInsertIntoRTS_Transaction_Address_Book_FedEx_Billing($Conn, $Obj, $Trans_ID, $Ship_To_ID) {
    rtsFormatObject($Obj);
    $Sql = "INSERT INTO RTS_TRANSACTION_ADDRESS_BOOK_FEDEX_BILLING_T
                   (Trans_ID, Ship_To_ID, BST_Payment_Type_SI,
                   BST_Account_Number_VC, BST_3RD_Country_VC,
                   BDT_Payment_Type_SI, BDT_Account_Number_VC,
                   BDT_3RD_Country_VC, Credit_Card_Type_VC)
                 VALUES
                   ($Trans_ID, $Ship_To_ID, '$Obj->BDT_Payment_Type', '$Obj->BST_Account_Number',
                    '$Obj->BST_3RD_Country', '$Obj->BDT_Payment_Type', '$Obj->BDT_Account_Number',
                    '$Obj->BDT_3RD_Country', '$Obj->Credit_Card_Type')";

    odbcRunSql($Conn, $Sql);
}
//------------------------------------------------------------------------------
class rtsTransaction_Address_Book_Attn_Email {
    public $Attn_Email_Event;
}
//------------------
function yRtsInsertIntoRTS_Transaction_Address_Book_Attn_Email($Conn, $Obj, $Trans_ID, $Ship_To_ID) {
    rtsFormatObject($Obj);
    $Sql = "INSERT INTO RTS_TRANSACTION_ADDRESS_BOOK_ATTN_EMAIL_T
                    (Trans_ID, Ship_To_ID, Attn_Email_Event_SI)
                 VALUES
                   ($Trans_ID, $Ship_To_ID, '$Obj->Attn_Email_Event')";

    odbcRunSql($Conn, $Sql);
}
//------------------------------------------------------------------------------
class rtsTransaction_Account {
    public $Acct_Number1;
    public $Acct_Number2;
    public $Acct_Name1;
    public $Acct_Name2;
}
//------------------
function yRtsInsertIntoRTS_Transaction_Account($Conn, $Obj, $Trans_ID) {
    yRtsFormatObject($Obj);
    $Sql = "INSERT INTO RTS_TRANSACTION_ACCOUNT_T
                   (Trans_ID, Acct_Number1_VC, Acct_Number2_VC,
                    Acct_Name1_VC, Acct_Name2_VC)
                 VALUES
                   ($Trans_ID, '$Obj->Acct_Number1', '$Obj->Acct_Number2',
                    '$Obj->Acct_Name1', '$Obj->Acct_Name2')";
    odbcRunSql($Conn, $Sql);
}
//------------------------------------------------------------------------------
class rtsTransaction_Special_Service {
    public $Handling_Charge;
    public $Fuel_Surcharge;
    public $Sat_Charge;
}
//------------------
function yRtsInsertIntoRTS_Transaction_Special_Service($Conn, $Obj, $Trans_ID) {
    yRtsFormatObject($Obj);
    $Sql = "INSERT INTO RTS_Transaction_Special_Service_T
                   (Trans_ID,Saturday_Delivery_Fee_MN)
                 VALUES
                   ($Trans_ID,'$Obj->Sat_Charge')";
    odbcRunSql($Conn, $Sql);
}
//------------------------------------------------------------------------------
class rtsTransaction_Ship_To {
    public $Number;
    public $Name;
    public $Address1;
    public $Address2;
    public $Address3;
    public $City;
    public $State;
    public $Zip_Code;
    public $Residential_Flag;
    public $Order_Download;
    public $Phone;
    public $Country;
}
//------------------
function yRtsInsertIntoRTS_Transaction_Ship_To($Conn, $Obj, $Trans_ID, $Ship_To_ID) {
    yRtsFormatObject($Obj);

    if($Obj->Phone == '' && $Obj->Country == 'UNITED STATES')
        $Obj->Phone = '000-000-0000';
//  if ($Obj->Country == 'UNITED STATES')
//  {
//    $Sql = "INSERT INTO RTS_TRANSACTION_SHIP_TO_T
//                   (Trans_ID,Ship_To_ID,Ship_To_Number_VC,Ship_To_Name_VC,
//                   Ship_To_Address_1_VC,Ship_To_City_VC,Ship_To_State_VC,
//                   Ship_To_Zip_Code_VC,Ship_To_Residential_Flag_BT,
//                   Order_Download_BT,Ship_To_Phone_VC,Ship_To_Country_VC,
//                   Ship_To_Address_2_VC)
//                VALUES
//                   ($Trans_ID, $Ship_To_ID, '$Obj->Number', '$Obj->Name',
//                   '$Obj->Address1', '$Obj->City', '$Obj->State',
//                   '$Obj->Zip_Code', $Obj->Residential_Flag,
//                    $Obj->Order_Download, '$Obj->Phone', '$Obj->Country',
//                    '$Obj->Address2')";
//  }
//  else
//  {
    $Sql = "INSERT INTO RTS_TRANSACTION_SHIP_TO_T
                   (Trans_ID,Ship_To_ID,Ship_To_Number_VC,Ship_To_Name_VC,
                   Ship_To_Address_1_VC,Ship_To_Address_2_VC,Ship_To_Address_3_VC,
                   Ship_To_City_VC,Ship_To_Residential_Flag_BT,
                   Order_Download_BT,Ship_To_Phone_VC,Ship_To_Country_VC, 
                   Ship_To_State_VC,Ship_To_Zip_Code_VC)
                VALUES
                   ($Trans_ID, $Ship_To_ID, '$Obj->Number', '$Obj->Name',
                   '$Obj->Address1', '$Obj->Address2', '$Obj->Address3',
                   '$Obj->City', $Obj->Residential_Flag,
                    $Obj->Order_Download, '$Obj->Phone', '$Obj->Country', '$Obj->State', '$Obj->Zip_Code')";
//  }
    odbcRunSql($Conn, $Sql);
}
//------------------------------------------------------------------------------
class rtsTransaction_Shipper {
    public $Shipper_ID;
}
//------------------
function yRtsInsertIntoRTS_Transaction_Shipper($Conn, $Obj, $Trans_ID) {
    yRtsFormatObject($Obj);
    $Sql = "INSERT INTO RTS_Transaction_Shipper_T
                   (Trans_ID,Shipper_ID)
                VALUES
                   ($Trans_ID, $Obj->Shipper_ID)";
    odbcRunSql($Conn, $Sql);
}
//------------------------------------------------------------------------------
class rtsTransaction_Strings {
    public $PackageNum;
    public $Destination_Zone;
    public $TransactionTimeBegin;
}
//------------------
function yRtsInsertIntoRTS_Transaction_Strings($Conn, $Obj, $Trans_ID) {
    yRtsFormatObject($Obj);
    $Sql = "INSERT INTO RTS_Transaction_Strings_T
                   (Trans_ID,Package_Number_VC,Machine_Operator_VC, Destination_Zone_VC,
                    Oversized_Type_VC, Signature_Confirmation_Type_VC, Package_Letter_Type_VC,
                    RTI_Reference_Offset_IN, Transaction_Begin_DT, Transaction_End_DT,
                    FedEx_Pickup_IN)
                 VALUES
                   ($Trans_ID, $Obj->PackageNum, 'ADMIN', '$Obj->Destination_Zone',
                   '0', '0', 'P-Customer Packaging',
                   '0', '$Obj->TransactionTimeBegin', '',
                   '0')";
    odbcRunSql($Conn, $Sql);
}
//------------------------------------------------------------------------------
class Address_Book_Attn_Email {
    public $Attn_Name;
    public $Attn_Email;
    public $Attn_Email_Event;
}
//------------------
function yRtsInsertIntoAddress_Book_Attn_Email($conn, $obj, $Ship_To_ID) {
    yRtsFormatObject($obj);
    $sql = "INSERT INTO Address_Book_Attn_Email_T
                   (Ship_To_ID, Attn_Name_VC,
                    Attn_Email_Address_VC, Attn_Email_Event_SI)
                 VALUES
                   ($Ship_To_ID, '$obj->Attn_Name',
                    '$obj->Attn_Email', '$obj->Attn_Email_Event')";
    odbcRunSql($conn, $sql);
}
//------------------------------------------------------------------------------
class rtsTransaction_Flags {
    public $Document;
    public $Intl_Filling_Page_BT;
}
//------------------
function yRtsInsertIntoRTS_Transaction_Flags($Conn, $obj, $Trans_ID) {
    yRtsFormatObject($obj);
    $sql = "INSERT INTO RTS_TRANSACTION_FLAGS_T
                   (Trans_ID, HundredWeight_Candidate_BT, HundredWeight_Shipped_BT, Document_BT, Intra_BMC_BT,
                    Mail_Manifested_BT, Delivery_Confirmation_BT, Modified_Record_BT, UPS_Shipper_Release_BT,
                    USPS_Waive_Sig_BT, USPS_No_Holiday_BT, USPS_No_Weekend_BT, MPI_BT, MPI_Identical_Pkgs_BT,
                    Intl_Billing_Page_BT, Need_SED_BT, Print_Com_Invoice_BT, SED_Not_Required_BT, Doc_CUst_Value_BT,
                    UPS_HundredWt_BT, FedEx_MultiWt_BT, Manual_Handling_BT, Cancel_Handling_BT)
                 VALUES
                   ($Trans_ID, 'False', 'False', '$obj->Document', 'False',
                    'False', 'False', 'False', 'False',
                    'False', 'False', 'False','False', 'False',
                    '$obj->Intl_Filling_Page_BT', 'False', 'False', 'False', 'False',
                    'False', 'False', 'False', 'False')";
    odbcRunSql($Conn, $sql);
}
//------------------------------------------------------------------------------
function yRtsException($exception) {
    echoLine("<center>An Error has occurred:<h2>Please call 422-3658 for your pickup</h2></center>".$exception->__toString());
    $toEMail = configGetGlobal('REDEMAIL');
    $subject = 'Error in Mail Pickup Request';
    $body = 'There was an error in Mail Pickup Request '.isoDateTime().
            'Error Message:'.$exception->__toString();
    ySendEmail('MailPickupRequest@byu.edu', $toEMail, $subject, $body);
}
//------------------------------------------------------------------------------
function yRtsFormatPhoneNumber($phoneNum) {
    //Correct Format: XXX-XXX-XXXX
    if(isNullOrEmptyStr($phoneNum))
        return $phoneNum;
    $phoneNum = strDelete("(", $phoneNum);
    $phoneNum = strDelete(")", $phoneNum);
    $phoneNum = strDelete(" ", $phoneNum);
    $phoneNum = strDelete("-", $phoneNum);
    if(strlen($phoneNum) == 10)
        $newPhoneNum = substr($phoneNum, 0, 3)."-".substr($phoneNum, 3, 3)."-".
                substr($phoneNum, 6, 4);
    else
        return $phoneNum;
    return $newPhoneNum;
}
//------------------
function yRtsTestFormatPhoneNumber() {
    testTrue(rtsFormatPhoneNumber("123-456-7890") == "123-456-7890", "FormatPhoneNumber: Correct Phone Number Format");
    testTrue(rtsFormatPhoneNumber("(123)456-7890") == "123-456-7890", "FormatPhoneNumber: Phone Number with Parenthesis");
    testTrue(rtsFormatPhoneNumber("( 123 ) 456 - 7890") == "123-456-7890", "FormatPhoneNumber: Phone Number space");
    testTrue(rtsFormatPhoneNumber("1234567890") == "123-456-7890", "FormatPhoneNumber: Phone Number w/o spaces or parenthesis");
    testTrue(rtsFormatPhoneNumber("456-7890") == "4567890", "FormatPhoneNumber: No Area Code");
    testTrue(rtsFormatPhoneNumber("") == "", "FormatPhoneNumber: Blank");
    testTrue(rtsFormatPhoneNumber(null) == null, "FormatPhoneNumber: Null");
}
//------------------------------------------------------------------------------
function yRtsIsCorrectPhoneFormat($PhoneNum) {
    return (preg_match("/^\d{3}-\d{3}-\d{4}$/", $PhoneNum) == 1);
}
//------------------------------------------------------------------------------
function yRtsTestIsCorrectPhoneFormat() {
    testTrue(rtsIsCorrectPhoneFormat("123-456-7890"), "IsCorrectPhoneFormat: Correct Phone Number Format");
    testFalse(rtsIsCorrectPhoneFormat("(123)456-7890"), "IsCorrectPhoneFormat: Phone Number with Parenthesis");
    testFalse(rtsIsCorrectPhoneFormat("( 123 ) 456 - 7890"), "IsCorrectPhoneFormat: Phone Number space");
    testFalse(rtsIsCorrectPhoneFormat("1234567890"), "IsCorrectPhoneFormat: Phone Number w/o spaces or parenthesis");
    testFalse(rtsIsCorrectPhoneFormat("456-7890"), "IsCorrectPhoneFormat: No Area Code");
    testFalse(rtsIsCorrectPhoneFormat(""), "IsCorrectPhoneFormat: Blank");
    testFalse(rtsIsCorrectPhoneFormat(null), "IsCorrectPhoneFormat: Null");
}
//------------------------------------------------------------------------------
function yRtsFormatObject($obj) {
    foreach($obj as $key=> $value) {
        $tempStr = strDelete("'", $value);
        $tempStr = strDelete('"', $tempStr);
        //$tempStr = strtoupper($tempStr);
        $obj->$key = $tempStr;
    }
}
//------------------------------------------------------------------------------
function pamRtsTest() {
    echoLine("RTS Toolbox Test:");
    rtsTestFormatPhoneNumber();
    rtsTestIsCorrectPhoneFormat();
}
//------------------------------------------------------------------------------
function insertIntoShipIrts($rtsRec) {
    $sql = "INSERT INTO SHIP.IRTS(RECSOURCE, TRANSID, STATUS, SHIPTOID, SERVICE, AUDITINFO) VALUES('".$rtsRec['RECSOURCE']."', '".$rtsRec['TRANSID']."', '".$rtsRec['STATUS']."', '".$rtsRec['SHIPTOID']."', '".$rtsRec['SERVICE']."','".$rtsRec['AUDITINFO']."')";
    executeSql($sql);
}
//------------------------------------------------------------------------------
function yGen2FetchCountryCodes() {
    $conn = yGen2ConnectElseErr();
    $sql = "SELECT COUNTRY_NAME_VC name, ISO_CODE_VC code FROM COUNTRY_CODES_T ORDER BY COUNTRY_NAME_VC";
    return odbcFetchAll($conn, $sql);
}
//------------------------------------------------------------------------------
function fetchCountryName($countryCode) {//TODO THIS WAS COPY/PASTED FROM PAMMAIL.PHP  CHECK AND VERIFY AND STUFF
    $conn = yGen2ConnectElseErr();
    $sql = "SELECT COUNTRY_NAME_VC name FROM COUNTRY_CODES_T WHERE ISO_CODE_VC = '$countryCode'";
    return odbcFetchValue($conn, $sql);
}
//------------------------------------------------------------------------------
function yCorrectCountryCodeIfNeeded(&$countryCode) {
    $countryName = yGen2FetchCountryName($countryCode);
    if(isNullOrEmptyStr($countryName) || $countryName === false) {
        $newCountryCode = yGen2FetchCountryCode($countryCode);
        if(!isNullOrEmptyStr($newCountryCode) && $newCountryCode !== false) {
            $countryCode = $newCountryCode;
        }
    }
}
//------------------------------------------------------------------------------
function yGen2FetchCountryName($countryCode) {//TODO THIS WAS COPY/PASTED FROM PAMMAIL.PHP  CHECK AND VERIFY AND STUFF
    $conn = yGen2ConnectElseErr();
    $sql = "SELECT COUNTRY_NAME_VC name FROM COUNTRY_CODES_T WHERE ISO_CODE_VC = '$countryCode'";
    return odbcFetchValue($conn, $sql);
}
//------------------------------------------------------------------------------
function yGen2FetchCountryCode($countryName) {
    $countryName = strtoupper($countryName);
    $conn = yGen2ConnectElseErr();
    $sql = "select iso_code_vc from country_codes_t where country_name_vc = '$countryName'";
    return odbcFetchValue($conn, $sql);
}
//------------------------------------------------------------------------------
function toCarrierService($code) {//TODO THIS WAS COPY/PASTED FROM PAMMAIL.PHP  CHECK AND VERIFY AND STUFF
    $carrierService = '';

    switch($code) {
        case cGen2FedExGround:
            $carrierService = 'FedEx Ground';
            break;
        case cGen2FedEx2Day:
            $carrierService = 'FedEx Second Day';
            break;
        case cGen2FedExPriorityOvernight:
            $carrierService = 'FedEx Priority';
            break;
        case cGen2FedExStandardOvernight:
            $carrierService = 'FedEx Priority';
            break;
        case cGen2FedExInternationalPriority:
            $carrierService = 'FedEx International Priority';
            break;
        case cGen2FedExInternationalEconomy:
            $carrierService = 'FedEx International Economy';
            break;
        case cGen2DhlWorldwidePriorityExpress:
            $carrierService = 'DHL International Priority';
            break;
        case cGen2FedExHomeDelivery:
            $carrierService = 'FedEx Standard Shipping'; //this is temp code to fix standardshipping errors
            break;
        default:
            err("Service Code Not Supported: $code");
    }
    return $carrierService;
}
//------------------------------------------------------------------------------
function ySubmitRtsToGen2($rtsRequest) {
    $conn = yGen2ConnectElseErr();
    $startTime = isoDateTime();
    //$isExam = arrayGet($rtsRequest,'exam',false);
    $ounces = arrayGet($rtsRequest, 'ounces', 0);
    if($ounces === null) {
        $ounces = 0;
    }
    $auditInfo = strDeleteCharSet(cStrInvalidSqlChars, arrayGet($rtsRequest, 'auditInfo', '')); //usually the link in ship.irts
    $acctName = strDeleteCharSet(cStrInvalidSqlChars, arrayGet($rtsRequest, 'acctName', '')); //who to bill
    $acctNum = strDeleteCharSet(cStrInvalidSqlChars, arrayGet($rtsRequest, 'acctNum', '')); //number to bill in bil.icust, usually 4, 8 digit
    $fromEmailName = strDeleteCharSet(cStrInvalidSqlChars, arrayGet($rtsRequest, 'fromEmailName', ''));
    $fromEmailAddress = strDeleteCharSet(cStrInvalidSqlChars, arrayGet($rtsRequest, 'fromEmailAddress', ''));
    $toEmail = strDeleteCharSet(cStrInvalidSqlChars, arrayGet($rtsRequest, 'toEmail', ''));

    $toName = strDeleteCharSet(cStrInvalidSqlChars, arrayGet($rtsRequest, 'toName'));
    $toName = substr($toName, 0, 50);

    $toAddr1 = strDeleteCharSet(cStrInvalidSqlChars, arrayGet($rtsRequest, 'toAddr1'));
    $toAddr1 = substr($toAddr1, 0, 50);

    $toAddr2 = strDeleteCharSet(cStrInvalidSqlChars, arrayGet($rtsRequest, 'toAddr2', ''));
    $toAddr2 = substr($toAddr2, 0, 50);

    $toAddr3 = strDeleteCharSet(cStrInvalidSqlChars, arrayGet($rtsRequest, 'toAddr3', ''));
    $toAddr3 = substr($toAddr3, 0, 31);

    $toAddr4 = strDeleteCharSet(cStrInvalidSqlChars, arrayGet($rtsRequest, 'toAddr4', ''));
    $toAddr4 = substr($toAddr4, 0, 50);

    $toCity = strDeleteCharSet(cStrInvalidSqlChars, arrayGet($rtsRequest, 'toCity', ' '));
    $toCity = substr($toCity, 0, 48);

    $toState = strDeleteCharSet(cStrInvalidSqlChars, arrayGet($rtsRequest, 'toState', ' '));

    $toZip = strDeleteCharSet(cStrInvalidSqlChars, arrayGet($rtsRequest, 'toZip', ' '));
    if(strlen($toZip) > 10)
        $toZip = strDeleteCharSet(cStrSymbolChars, $toZip);
    errIf(strlen($toZip) > 10, 'Zipcode '.$toZip.' too long '.strlen($toZip).'chars, max 10', 'EDataError');

    $recSource = arrayGet($rtsRequest, 'recSource');
    //yGen2FetchCountryCode();
    $toCountryCode = strDeleteCharSet(cStrInvalidSqlChars, arrayGet($rtsRequest, 'toCountryCode'));
    yCorrectCountryCodeIfNeeded($toCountryCode);
    $toCountryCodeAndName = "$toCountryCode-".yGen2FetchCountryName($toCountryCode);
    $toPhone = yRtsFormatPhoneNumber(arrayGet($rtsRequest, 'toPhone', ''));
    $pamShippingMethod = arrayGet($rtsRequest, 'service');
    $isResidential = arrayGet($rtsRequest, 'isResidential');
    $isDocument = arrayGet($rtsRequest, 'isDocument');

    $serviceCode = yGen2FindServiceCode($toCountryCode, $pamShippingMethod, $isResidential, $isDocument);
    //use FedEx RTS 
    //$serviceCode = yGen2FindFedExCode($pamShippingMethod, $isResidential);

    switch($serviceCode) {
        case cGen2FedExGround;
        case cGen2FedExHomeDelivery;
        case cGen2FedEx2Day;
        case cGen2FedExPriorityOvernight;
        case cGen2FedExStandardOvernight;
        case cGen2FedExInternationalFirst;
        case cGen2FedExInternationalPriority;
        case cGen2FedExInternationalEconomy;
            $carrierNum = cGen2FedExCode;
            break;
        case cGen2DhlInternationalDocument;
        case cGen2DhlWorldwidePriorityExpress;
            $carrierNum = cGen2DhlCode;
            break;
    }
//------------------------------------------------------------------------------
// Begin Inserting into tables
//------------------------------------------------------------------------------
    $tm = new rtsTransaction_Main();
    $tm->Acct_ID = "488";
    $tm->Ounces = '"'.$ounces.'"';
    $tm->Dimensional_Ounces = "0";
    $tm->Carrier_ID = "$serviceCode";
    $tm->Number_Of_Pieces = "1";
    $tm->Base_Rate = "0";
    $tm->System_Date_Time = $startTime;
    $tm->Manifest_Date_Time = "01-Jan-2000";
    $tm->Uploaded = "0";
    $tm->Closed = "0";
    $tm->Zone = "0";
    $tm->Company_ID = "0";
    $tm->ShipFrom_ID = "1";
    $tm->Total_Charges = "0.01";
    $tm->Carrier_Number_ID = "$carrierNum";
    $tm->Machine_Key = "0";
    $transId = yRtsInsertIntoRTS_Transaction_Main($conn, $tm);
    //------------------------------------------------------------------------------
    $ab = new Address_Book();
    $ab->Number = substr("$toName", 0, 10)."$transId";
    $ab->Name = $toName;
    $ab->Address1 = $toAddr1;
    $ab->Address2 = $toAddr2;
    $ab->Address3 = $toAddr3;
    $ab->City = $toCity;
    $ab->State = $toState;
    $ab->Zip_Code = $toZip;
    $ab->Country = $toCountryCodeAndName;
    $ab->Residential_Flag = "1";
    $ab->Order_Download = "0";
    $ab->Phone = $toPhone;
    $ab->CompanyID = "-1";
    $shipToId = yRtsInsertIntoAddress_Book($conn, $ab);
    //------------------------------------------------------------------------------
    $rtst = new rtsTransaction_Ship_To();
    $rtst->Number = substr("$toName", 0, 10)."$transId";
    $rtst->Name = $toName;
    $rtst->Address1 = $toAddr1;
    $rtst->Address2 = $toAddr2;
    $rtst->Address3 = $toAddr3;
    $rtst->City = $toCity;
    $rtst->State = substr($toState, 0, 10);
    $rtst->Zip_Code = $toZip;
    $rtst->Country = $toCountryCodeAndName;
    $rtst->Residential_Flag = "1";
    $rtst->Order_Download = "0";
    $rtst->Phone = $toPhone;
    yRtsInsertIntoRTS_Transaction_Ship_To($conn, $rtst, $transId, $shipToId);
    //------------------------------------------------------------------------------
    $rta = new rtsTransaction_Account();
    $rta->Acct_Name1 = substr($acctName, 0, 50);
    $rta->Acct_Number1 = $acctNum;
    $rta->Acct_Name2 = "";
    $rta->Acct_Number2 = "";
    yRtsInsertIntoRTS_Transaction_Account($conn, $rta, $transId);
    //------------------------------------------------------------------------------
    $rts = new rtsTransaction_Strings();
    $rts->PackageNum = $transId;
    $destinationZone = "0";
    $transactionTimeBegin = $startTime;
    yRtsInsertIntoRTS_Transaction_Strings($conn, $rts, $transId);
    //------------------------------------------------------------------------------
    $rtabm = new rtsTransaction_Address_Book_Misc();
    $rtabm->Preferred_Carrier = "9999";
    $rtabm->COD_Only_Flag = "0";
    $rtabm->COD_Cash_Only_Flag = "0";
    $rtabm->Global_Email_Flag = "0";
    $rtabm->Attn_Email_Flag = "0";
    $rtabm->Address_Validation_SI = "0";
    yRtsInsertIntoRTS_Transaction_Address_Book_Misc($conn, $rtabm, $shipToId, $transId);
    //------------------------------------------------------------------------------
    $rtf = new rtsTransaction_Flags();
    if($serviceCode == cGen2DhlWorldwidePriorityExpress) {
        $rtf->Document = 'true';
        $rtf->Intl_Filling_Page_BT = 'true';
    }
    else {
        $rtf->Document = 'false';
        $rtf->Intl_Filling_Page_BT = 'false';
    }
    yRtsInsertIntoRTS_Transaction_Flags($conn, $rtf, $transId);
    //------------------------------------------------------------------------------
    $sql = "INSERT INTO RTS_Transaction_Shipper_T (Trans_ID, Shipper_ID) VALUES($transId, 1)";
    $result = odbcRunSql($conn, $sql);
    //------------------------------------------------------------------------------ EMAIL
    $abae = new Address_Book_Attn_Email();
    $abae->Attn_Name = $fromEmailName;
    $abae->Attn_Email = $toEmail;
    $abae->Attn_Email_Event = "3";
    yRtsInsertIntoAddress_Book_Attn_Email($conn, $abae, $shipToId);
    //------------------------------------------------------------------------------
    // Close Connection
    odbcCloseElseErr($conn);
    $transId = (int)$transId;
    $_SESSION['Trans_ID'] = $transId;
    $rtsRec['RECSOURCE'] = $recSource;
    $rtsRec['TRANSID'] = $transId;
    $rtsRec['SHIPTOID'] = $shipToId;
    $rtsRec['STATUS'] = 'QUEUED';
    $rtsRec['SERVICE'] = toCarrierService($serviceCode);

    $rtsRec['AUDITINFO'] = $auditInfo;
    insertIntoShipIrts($rtsRec);
    return "RTS$transId";
}
//------------------------------------------------------------------------------
?>