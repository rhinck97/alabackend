<?php
//============================================================================================
function yPdfAnalyze($libraryDir, $fileName, $forceAnalyze = false) {
    global $gAnalyzedFileList;

    if(($gAnalyzedFileList !== null) && array_key_exists($fileName, $gAnalyzedFileList) && !$forceAnalyze) {
        $jsonOnyxResult = $gAnalyzedFileList[$fileName];
        return $jsonOnyxResult;
    }
    else {
        Yii::beginProfile("pdfAnalyzer");
        $site = Yii::app()->params['pdfAnalyzerUrl'];

        $dataToWrite['filePath'] = CJSON::encode($libraryDir);
        $dataToWrite['fileName'] = CJSON::encode($fileName);
        $jsonOnyxResult = httpLoad($site, $dataToWrite);
        $jsonPos = strpos($jsonOnyxResult, '{');
        $jsonOnyxResult = substr($jsonOnyxResult, $jsonPos);
        $jsonOnyxResult = strDeleteSuffix("\r\n0\r\n\r\n", $jsonOnyxResult);
        $jsonOnyxResult = CJSON::decode($jsonOnyxResult);
        valOnyxResult($jsonOnyxResult);

        $output = $jsonOnyxResult['output'];
        $gAnalyzedFileList[$fileName] = $output;
        Yii::endProfile("pdfAnalyzer");
        $fileInfo = pathinfo($fileName);
        $analyzerResultsFile = $fileInfo['filename'].".txt";
        $fullResultsPath = $libraryDir.$analyzerResultsFile;
//        file_put_contents("FILENAMETEST",print_r($fullResultsPath,true));
        file_put_contents($fullResultsPath, json_encode($output));
        return $output;
    }
}
//============================================================================================
function yLoginRouteY($username, $password) { //$recSource, $isAdmin = false  (possibly new name for function0)
    $rights = array();
    // yLogout();
    if(($username === null) || ($password === null))
        err();

    // yErrIfLoginInfoNull($username, $password);

    $conn = ldap_connect("ldap.byu.edu");
    errIf($conn === false, "Could not connect to LDAP server.");

    $loginValid = (arrayKeyExists($testUser, $username));  //Create arrayKeyExists function

    if(!$loginValid)
        $loginValid = @ldap_bind($conn, "uid=$username,ou=people,o=byu.edu", $password);
    verify($loginValid, 'Incorrect NetID or password.', 'ELoginFailed');
    $rights = yRouteYdetermineUserRights($conn, $username); //TODO: create yRouteYdetermineUserRights (Possibly new name)
    ldap_close($conn);

    if(count($rights) == 0) {
        global $gErrExposeStackTrace;
        $gErrExposeStackTrace = false;
        err("You are not authorized.");
        $gErrExposeStackTrace = true;
    }

    //write a validation and return true or false
    // yLoginSuccess($username, $rights);
}
//============================================================================================
//function orderHasEvent($orderLink, $orderstatus)
//{//keep
//  $sql = "select orderstatus from job.iorderstatusLog
//          where orderstatus = '$orderstatus' and
//          link = $orderLink";
//  $result = queryScalar($sql);
//  return $result !== null;
//}
//============================================================================================
function getCtrl($name, $appname = 'G') {
    $sql = "select value from g.ctrl where name = '$name' and appname = '$appname'";
    return queryScalar($sql);
}
//============================================================================================
/*function toBool($vBool)
{
  $firstLetter = $vBool[0];
  if(stristr('TY1', $firstLetter) !== false)
  return true;
  if(stristr('FN0', $firstLetter) !== false)
  return false;
  err("Unable to convert $vBool to boolean");
}*/
//============================================================================================
function newFileNameAndWorkspace($ext) {
    $ext = strEnsurePrefix(".", $ext);
    $currentWorkspaceDir = Yii::app()->params['workspaceDir'];
    $uniqueId = queryScalar("select job.filenameseq.nextval from dual");
    //?? CHECK IT OUT !! Yii::app()->params['currentWorkSpaceDir'] = $currentWorkspaceDir;
    return $currentWorkspaceDir.$uniqueId.$ext;
}
//============================================================================================
function archiveFile($fileName) {
    $sourceDir = Yii::app()->params['workspaceDir'];
    $fileName = fileExtractName($fileName);
    verify(file_exists($sourceDir.$fileName), "File doesn't exist in workspace");
    $archiveDir = Yii::app()->params['archiveDir'];
    $dtl = ProductDtl::model()->find("INFO = '$fileName'");
    $productCase = $dtl->product->PRODUCTCASE;

    errIf(($productCase !== 'CUSTOM'), "Non-custom Product");

    $productType = $dtl->product->PRODUCTTYPE;
    $productId = $dtl->product->PRODUCTID;
    // echoLine($productId);
    $job = Job::model()->find("PRODUCTID = $productId");
    errIfNull($job, "Job not found for file");
    $jobId = $job->JOBID;

    $destDir = $archiveDir."\\".$productType."\\".$jobId."\\";
    dirEnsureExists($destDir, true);
    fileMove($sourceDir.$fileName, $destDir);
}
//----------
function insertOrderTree($orderTree) {
    $order = new Order;
    if(array_key_exists("ORDERNOTES", $orderTree))
        $order->ORDERNOTES = $orderTree['ORDERNOTES'];
    $order->STEWARDSHIPLOC = $orderTree['STEWARDSHIPLOC'];
    $order->PAYMENTMETHOD = $orderTree['PAYMENTMETHOD'];
    $order->RECSOURCE = $orderTree['RECSOURCE'];
    $order->PAYMENTINFO = $orderTree['PAYMENTINFO'];
    $order->CONTACTEMAIL = $orderTree["CONTACTEMAIL"];
    $order->PAYMENTSTATUS = "PENDING";
    $order->BUNDLESHIPMENTS = "T";
    $order->ORDERSTATUS = "PLACED";
    $order->submit();

    $shippingInfo = new ShippingInfo;
    $shippingInfo->SHIPTONAME = $orderTree["SHIPTONAME"];
    $shippingInfo->ADDRESS1 = $orderTree["SHIPTOADDRESS1"];
    $shippingInfo->ADDRESS2 = $orderTree["SHIPTOADDRESS2"];
    $shippingInfo->ADDRESS3 = $orderTree["SHIPTOADDRESS3"];
    $shippingInfo->ADDRESS4 = $orderTree["SHIPTOADDRESS4"];
    $shippingInfo->CITY = $orderTree["SHIPTOCITY"];
    $shippingInfo->STATE = $orderTree["SHIPTOSTATE"];
    $shippingInfo->POSTALCODE = $orderTree["SHIPTOPOSTALCODE"];
    $shippingInfo->COUNTRY = $orderTree["SHIPTOCOUNTRY"];
    $shippingInfo->submit();

    $shipment = new Shipment;
    $shipment->LINK = $order->LINK;
    $shipment->SHIPPINGINFOID = $shippingInfo->SHIPPINGINFOID;
    $shipment->DELIVERYMETHOD = $orderTree["SHIPPINGMETHOD"];
    $shipment->NOTIFYSTATUS = "PENDING";
    $shipment->submit();

    $jobTrees = $orderTree['JOBS'];
    foreach($jobTrees as &$jobTree) {
        $job = new Job;
        $job->LINK = $order->LINK;
        $job->JOBNOTES = $jobTree['JOBNOTES'];
        $job->PRODUCTID = $jobTree['PRODUCTID'];
        $job->PRODUCTCOUNT = $jobTree['PRODUCTCOUNT'];
        $job->APPROVALSTATUS = 'NA';
        $job->STATUS = 'PENDING';
        $job->submit();

        $jobShipmentMap = new JobShipmentMap;
        $jobShipmentMap->PRODUCTCOUNT = $job->PRODUCTCOUNT;
        $jobShipmentMap->JOBID = $job->JOBID;
        $jobShipmentMap->SHIPMENTPKEY = $shipment->SHIPMENTPKEY;
        $jobShipmentMap->submit();
    }
    return $order->LINK;
}
//============================================================================================
function fetchOrderTree($orderLink) {
    $order = Order::model()->findByPk($orderLink);
    errIfNull($order, "Order $orderLink not found");

    $orderTree = $order->getAttributes();
    $jobTrees = fetchJobTrees($orderLink);
    $orderTree['jobs'] = $jobTrees;
    return $orderTree;
}
//============================================================================================
function fetchJobTrees($orderLink) {
    $jobActiveRecords = Job::model()->findAll("link = $orderLink");
    $jobTrees = array();
    foreach($jobActiveRecords as $jobActiveRecord) {
        $jobTree = $jobActiveRecord->getAttributes();
        //$jobTree['product'] = fetchProductTree($jobTree['PRODUCTID']);;
        $jobTree['shipments'] = fetchShipments($jobTree['JOBID']);
        $jobTrees[] = $jobTree;
    }
    return $jobTrees;
}
//============================================================================================
function fetchProductTree($productId) {
    $productActiveRecord = Product::model()->findByPk($productId);
    if($productActiveRecord === null)
        return null;
    else {
        $productTree = $productActiveRecord->getAttributes();
        $productDtlActiveRecords = ProductDtl::model()->findAll("productid = $productId");
        $productDtlRows = activeRecordsTo2dArray($productDtlActiveRecords);
        $productTree['productDtls'] = $productDtlRows;
    }
    return $productTree;
}
//------------------------------------------------------------------------------
function fetchShipments($jobId) {
    $shipmentsSql = "select
        shippingInfoId,
        deliveryMethod,
        productCount,
        m.shipmentpkey
    from
        job.ijobshipmentmap m join
        job.ishipment s on m.shipmentpkey = s.shipmentpkey
    where
        jobid = $jobId";
    $shipments = queryAll($shipmentsSql);
    return $shipments;
}
//-------------------------------------------------
function portOrderToBilling($link) {
    $order = Order::model()->findByPk($link);
    $jobs = $order->jobs;

    $transaction = new Transaction;
    $transaction->LINK = $order->LINK;
    $transaction->CUSTID = $order->CUSTID;
    $transaction->CATEGORY = 'JOB';
    $transaction->PMTMETHOD = $order->PAYMENTMETHOD;
    if($transaction->PMTMETHOD === 'BT') {
        $transaction->PFSNO = $order->PAYMENTINFO;
    }
    $transaction->NOTE1 = $order->ORDERNOTES;
    $transaction->RECSOURCE = $order->RECSOURCE;
    $transaction->EMODE = 'AUTHORIZE';
    $transaction->ENTRYLOC = $order->STEWARDSHIPLOC;
    $result = $transaction->save();
    echoValue($transaction->LINK);
    echoValue($result);
    echoValue($transaction->getErrors());

    foreach($jobs as $job) {
        $transactionDtl = new TransactionDtl;
        $transactionDtl->LINK = $job->LINK;
        $transactionDtl->AMT = $job->CHARGE;
        $transactionDtl->CNT = $job->PRODUCTCOUNT;
        $transactionDtl->ITEMTYPE = $job->PRODUCTTYPE;
        $transactionDtl->ITEM = ' ';
        $transactionDtl->RECSOURCE = $transaction->RECSOURCE;
        $transactionDtl->SRVLOC = $transaction->ENTRYLOC;
        $result = $transactionDtl->save();
    }
    if($order->SHIPPINGCHARGE > 0) {
        $transactionDtl = new TransactionDtl;
        $transactionDtl->LINK = $order->LINK;
        $transactionDtl->AMT = $order->SHIPPINGCHARGE;
        $transactionDtl->CNT = 1;
        $transactionDtl->ITEMTYPE = 'WEB SHIPPING';
        $transactionDtl->ITEM = ' ';
        $transactionDtl->RECSOURCE = $transaction->RECSOURCE;
        $transactionDtl->SRVLOC = 'WEB SHIPPING';
        $result = $transactionDtl->save();
    }
}
//============================================================================================
function processScLogic() {
    $daysBack = 14;
    $pendingShipmentsSql = "SELECT
        distinct
        shipmentPkey, shipmentid
      FROM
        job.ijob j join
        job.ijobshipmentmap m on j.jobid = m.jobid join
        job.ishipment s on m.shipmentpkey = s.shipmentpkey
      WHERE
        j.status not in('CANCELED', 'PENDING') and
        (s.deliverystatus is null or s.deliverystatus <> 'SHIPPED') and
        deliveryMethod = 'DELIVER' and
        j.recTime is not null and
        j.recTime >= sysdate-$daysBack
      order by
        shipmentPkey desc";

    $pendingShipments = queryAll($pendingShipmentsSql);

    foreach($pendingShipments as $pendingShipment) {
        $shipmentId = $pendingShipment['SHIPMENTID'];
        $assetHistory = SclAssetHistory::model()->findByAttributes(
                array('ASSETID'=>$shipmentId));
        if(!isNullOrEmptyStr($assetHistory)) {
            $hStatus = $assetHistory->HSTATUS;
            $shipmentPkey = $pendingShipment->SHIPMENTPKEY;
            $updateSql = 'update job.ishipment '.
                    "set shippedVia = 'Campus delivery', ".
                    "deliveryStatus = 'SHIPPED' ".
                    'where shipmentpkey = '.$shipmentPkey;
            executeSql($updateSql);
        }
    }
}
//------------------------------------------------------------------------------
/*function processRts()
{
  $pendingRtsShipmentsSql =
  "SELECT
  distinct
  shipmentPkey, rtsNumber
  FROM
  job.ijob j join
  job.ijobshipmentmap m on j.jobid = m.jobid join
  job.ishipment s on m.shipmentpkey = s.shipmentpkey
  WHERE
  j.status not in('CANCELED', 'PENDING') and
  (s.deliverystatus is null or s.deliverystatus <> 'SHIPPED') and
  rtsNumber is not null and
  j.recTime is not null and
  j.recTime >= sysdate-31
  order by
  shipmentPkey desc";

  $pendingRtsShipments = executeSql($pendingRtsShipmentsSql);
  foreach ($pendingRtsShipments as $pendingRtsShipment) {
  $rtsNumber = strDeletePrefix('RTS', $pendingRtsShipment['RTSNUMBER']);
  DBRefreshWithParams(gen2TrackNumSel, ['rtsNumber', rtsNumber]);
  trackingNumber := gen2TrackNumSel['Tracking_Number_VC'];
  if trackingNumber <> '' then
  begin
  DBRefreshWithParams(gen2CarrierSel, ['rtsNumber', rtsNumber]);
  mailCarrier := gen2CarrierSel['CarrierName'];
  shipmentPkey := pendingRtsShipmentsSel['shipmentPkey'];
  updateSql := 'update job.ishipment '+
  'set trackingnumber= '''+trackingNumber+''', '+
  'shippedVia = '''+mailCarrier+''', '+
  'deliveryStatus = ''SHIPPED'' '+
  'where shipmentpkey = '+shipmentPkey;//ok
  YRunSql(updateSql);
  Inc(fFoundCount);
  MainForm.updateCountLbl.Caption := ToStr(fFoundCount);
  end;
  pendingRtsShipmentsSel.Next;
  }
}*/
//============================================================================================
//function orderHasApprovalStatus($orderLink)
//{//keep
//  $sql = "select count(APPROVALSTATUS)
//          from job.ijob
//          where LINK = $orderLink and APPROVALSTATUS <> 'NA' and APPROVALSTATUS is not null";
//  $orderApprovalStatusCount = queryScalar($sql);
//  return $orderApprovalStatusCount > 0;
//}
//============================================================================================
//function logOrderStatusEvent($orderLink, $orderstatus)
//{//keep
//  $sql = "insert into job.iorderstatuslog (link, orderstatus) values ($orderLink, '$orderstatus')";
//  executeSql($sql);
//}
//============================================================================================
//function incrementJobStatus($jobId)
//{//keep
//  $nextJobStatus = queryScalar("select job.core.nextJobStatus($jobId) from dual");
//  executeSql("update job.ijob set status = '$nextJobStatus' where jobId = $jobId");
//  return $nextJobStatus;
//}
//============================================================================================
//function deliveryMethodInStateOnly($deliveryMethod)
//{//keep
//  return queryScalar("select inStateOnly from job.vDeliveryMethod where deliveryMethod = '$deliveryMethod'")
//          === 'T';
//}
//============================================================================================
//function deliveryMethodFedexable($deliveryMethod)
//{//keep
//  if($deliveryMethod == cMailStandard or
//      $deliveryMethod == cMailNextDay or//get rid of this in the future
//      $deliveryMethod == cMailNextDayPriority or
//      $deliveryMethod == cMailNextDayStandard or
//      $deliveryMethod == cMailNextDayRound or
//      $deliveryMethod == cMailSecondDay or
//      $deliveryMethod == cMailIntlPriority or
//      $deliveryMethod == cMailIntlEconomy)
//    return true;
//  else
//    return false;
//}
//function jobUsesProductTable($jobId)
//{//keep
//  $sql = "select inProductTable from job.vproducttype pt join job.ijob j on ".
//          "j.productType = pt.productType where jobid = $jobId";
//  $inProductTable = queryScalar($sql);
//  if($inProductTable == 0)
//    return false;
//  else
//    return true;
//}
//----------------------------------------------
function emailSendFromPam($subject, $body, $toEmail, $fromEmail = "", $ccEmail = "", $bccEmail = "") {
    if(isNullOrEmptyStr($fromEmail)) {
        $fromEmail = Yii::app()->params['adminEmail'];
    }
    $recsource = RecSourceExtra::model()->find("recsource = '".Yii::app()->getController()->recSource."'");
    fileLog(Yii::app()->getController()->recSource);
    if(!is_null($recsource) && !is_null($recsource->BCCEMAIL)) {
        $bccEmail .= ",".$recsource->BCCEMAIL;
        $bccEmail = strDeletePrefix(",", $bccEmail);
    }
    return emailSend($subject, $body, $fromEmail, $toEmail, $ccEmail, $bccEmail);
}
//----------------------------------------------
function emailAlert($subject, $body) {
    $toEmail = Yii::app()->params['alertEmail'];
    return emailSendFromPam($subject, $body, $toEmail);
}
//============================================================================================
function printFile($fileUrl, $printerName) {
    errIf(!file_exists($fileUrl), "File $fileUrl not found");
    $contents = file_get_contents($fileUrl);
    $printer = printer_open($printerName);
    errIf(!$printer, "Printer $printerName not found. Make sure it is the name of the printer as shown on your device list.");
    printer_set_option($printer, PRINTER_MODE, "raw");
    printer_set_option($printer, PRINTER_TITLE, $fileUrl);
    if (printer_write($printer, $contents)) {
        printer_close($printer);
        return true;
    }
    else {
        printer_close($printer);
        err("Unable to print file to printer $printer");
    }
    return false;
}
//------------------------------------------------------------------------------
function yTranslateOracleException($exception) {
    $errMsg = $exception->GetMessage();
    $oraErrPos = strpos($errMsg, 'ORA-');
    if($oraErrPos === false)
        return $exception;
    else {
        $currentErrMsg = substr($errMsg, $oraErrPos+11);
        $oraErrEndPos = strpos($currentErrMsg, chr(10));
        $currentErrMsg = substr($currentErrMsg, 0, $oraErrEndPos);
        fileLogDebug($currentErrMsg);
//        $oraErrCode = substr($errMsg, $oraErrPos, $oraErrEndPos - $oraErrPos);
//        $class = yErrCodeToExceptionClass($oraErrCode);
//        if($class !== null)
//            return new $class($errMsg);
//        else
        return $currentErrMsg;
    }
}
?>