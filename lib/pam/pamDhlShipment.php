<?php
    require_once 'protected/lib/onyx/onyxCore.php';
    require_once 'protected/lib/pam/pamCore.php';
    require_once 'protected/lib/DHL/init.php';

    use DHL\Entity\AM\GetQuote;
    use DHL\Datatype\AM\PieceType;
    use DHL\Client\Web as WebserviceClient;

function fetchDhlMailRate($city, $postalCode, $countryCode, $ounces, $depthInInches, $widthInInches, $heightInInches) {
    // DHL Settings
    $dhl = $config['dhl'];

    // Test a getQuote using DHL XML API
    $sample = new GetQuote();
    $sample->SiteID = 'brigham';
    $sample->Password = '638sMHTafO';

    // Set values of the request
    $sample->MessageTime = '2001-12-17T09:30:47-05:00';
    $sample->MessageReference = '1234567890123456789012345678901';
    $sample->BkgDetails->Date = date('Y-m-d');

    $pounds = round($ounces/16);
    $piece = new PieceType();
    $piece->PieceID = 1;
    $piece->PackageTypeCode = '';
    $piece->Height = $heightInInches;
    $piece->Depth = $depthInInches;
    $piece->Width = $widthInInches;
    $piece->Weight = $pounds;
    
    $sample->BkgDetails->addPiece($piece);
    $sample->BkgDetails->IsDutiable = 'Y';
    // $sample->BkgDetails->QtdShp->QtdShpExChrg->SpecialServiceType = 'WY';
    $sample->BkgDetails->ReadyTime = 'PT10H21M';
    $sample->BkgDetails->ReadyTimeGMTOffset = '+01:00';
    $sample->BkgDetails->DimensionUnit = 'IN';
    $sample->BkgDetails->WeightUnit = 'LB';
    $sample->BkgDetails->PaymentAccountNumber = '923546959';

    $sample->BkgDetails->PaymentCountryCode = 'US';
    $sample->BkgDetails->IsDutiable = 'Y';

    // Request Paperless trade
    // $sample->BkgDetails->QtdShp->QtdShpExChrg->SpecialServiceType = 'WY';

    $sample->From->CountryCode = 'US';
    $sample->From->Postalcode = '84602';
    $sample->From->City = 'Provo';

    $sample->To->CountryCode = $countryCode;
    $sample->To->Postalcode = $postalCode;
    $sample->To->City = $city;
    $sample->Dutiable->DeclaredValue = '5.00';
    $sample->Dutiable->DeclaredCurrency = 'USD';

    // Call DHL XML API
    //echo $sample->toXML();
    $client = new WebserviceClient('production');
    $xml = $client->call($sample);
    $first = strNthItem(2, $xml, '<LocalProductName>EXPRESS WORLDWIDE NONDOC</LocalProductName>');
    $afterFirst = $first;
    $expressWorldwideChunk = strNthItem(1, $afterFirst, '<TotalTaxAmount>');
    $shippingChargeField = strNthItem(2, $expressWorldwideChunk, '<ShippingCharge>');
    $afterShippingChargeField = $shippingChargeField;
    $result = strNthItem(1, $afterShippingChargeField, '</ShippingCharge>');

    errIf(isNullOrEmptyStr($result), 'DHL returned bad rate. Full output... '.$xml);
    return $result;
}

?>
