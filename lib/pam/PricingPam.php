<?php
require_once("protected/lib/onyx/onyxYii.php");
require_once("protected/lib/onyx/onyxCore.php");
class PricingPam extends Pricing {
    public function calcJobCharge($job) {//
        if(!strToBool($job->productType->INPRODUCTTABLE)) {
            return 0;
        }
        $productCount = $job->PRODUCTCOUNT;
        $productId = $job->PRODUCTID;
        $sql = "select job.core.calcProductCost($productId, $productCount) from dual";
        $dtlsPrice = queryScalar($sql);
        $sql = "select sum(sellingPrice) ".
                "from job.iproduct p join ".
                "job.iproductdtl d on p.productid = d.info and d.ITEM = 'ANOTHER PRODUCT' ".
                "where d.productid = $productId";
        $subProductsPrice = queryScalar($sql);

        return $dtlsPrice + $subProductsPrice;
    }
}