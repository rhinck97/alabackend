<?php
//needs a review
require Yii::app()->basePath.'/lib/onyx/onyxCore.php';
require Yii::app()->basePath.'/lib/CAS/CAS.php';
//=========================================================================
//CAS Setup
phpCAS::setDebug();
phpCAS::client(CAS_VERSION_2_0, 'cas.byu.edu', 443, 'cas'); //what is the last param?
//=========================================================================
class CasController extends YController {
    //--------------------------------------------------------------------------
    public function isLocalServer() {
        if(key_exists('HTTP_REFERER', $_SERVER) &&
                $_SERVER['HTTP_REFERER'] === 'http://127.0.0.1:9000/') {
            return true;
        }
        else {
            return false;
        }
    }
    //--------------------------------------------------------------------------
    public function isCasLoggedIn() {
        phpCAS::setNoCasServerValidation();
        $authenticated = phpCAS::isAuthenticated();
        return $authenticated;
    }
    //--------------------------------------------------------------------------
    public function actionIsLoggedIn() {
        if($this->isLocalServer()) {
            $authenticated = true;
        }
        else {
            $authenticated = $this->isCasLoggedIn();
        }
        $result['result'] = $authenticated;
        echo jsonEncodePlus($result);
    }
    //--------------------------------------------------------------------------
    public function actionLogin() {
        $redirectUrl = arrayGet($_GET, 'redirectUrl', '');

        $casLoggedIn = $this->isCasLoggedIn();

        if(!$casLoggedIn) {
            Yii::app()->user->handleCASUser("force");
        }
        $identity = new YUserIdentity(phpCAS::getUser(), "password");
        if($identity->authenticate()) {
            Yii::app()->user->login($identity);
        }

        if($redirectUrl !== '') {
            $this->redirect($redirectUrl);
        }
    }
    //--------------------------------------------------------------------------
    public function actionLogout() {
        $redirectUrl = arrayGet($_GET, 'redirectUrl', '');
        phpCAS::setNoCasServerValidation();
        phpCAS::logout();
//        Yii::app()->user->handleCASUser("logout");
//        Yii::app()->user->logout();
        if($redirectUrl !== '') {
            $this->redirect($redirectUrl);
        }
    }
    //--------------------------------------------------------------------------
    function actionGetLDAPInfo($routeY) {
        $conn = ldap_connect("ldap.byu.edu");
        errIf($conn === false, "Could not connect to LDAP server.");
        ldap_bind($conn);
        $attributes = array('displayName', 'employeeType', 'area', 'mail', 'roomNumber',
                'preferredFirstName', 'sn', 'postalAddress', 'telephoneNumber',
                'title', 'empClassification', 'empStanding', 'empStatus');
        $ldapResult = ldap_search($conn, 'ou=People,o=byu.edu', "uid=$routeY", $attributes, 0);
        $info = ldap_get_entries($conn, $ldapResult);

        ldap_close($conn);
        return $info;
    }
}