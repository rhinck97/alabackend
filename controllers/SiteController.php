<?php
require Yii::app()->basePath.'/lib/onyx/onyxCore.php';
require Yii::app()->basePath.'/lib/onyx/onyxYii.php';
require Yii::app()->basePath.'/lib/pam/pamCore.php';
//==============================================================================
class SiteController extends YController {
    //--------------------------------------------------------------------------
    /**
     * Returns the RECSOURCE of the application set in the app params. This
     * standardizes and makes convenient the getting of the RECSOURCE.
     */
    private function getRecsource() {
        return Yii::app()->params['RECSOURCE'];
    }
    //--------------------------------------------------------------------------
    /**
     * Queries the IPRODUCT table in the database for the record with the passed
     * productid. The record's productowner must match the app's recsource. This
     * will only fetch active products.
     *
     * @param productId a string of digits coresponding to the primary key of
     *                  the IPRODUCT table rows
     * @return          an array with keys corresponding to the IPRODUCT schema
     */
    private function fetchProduct($productId) {
        // queries the IPRODUCT table for a row with the passed PRODUCTID
        $recsource = $this->getRecsource();
        $sql = "SELECT
                    PRODUCTID,
                    PRODUCTOWNER,
                    PRODUCTTYPE,
                    DISPLAYNAME,
                    PRODUCTDESC,
                    SELLINGPRICE AS PRICE,
                    ORDERMIN,
                    ORDERINCREMENT,
                    LANGUAGE
                FROM
                    IPRODUCT
                WHERE
                    PRODUCTID = $productId AND
                    PRODUCTOWNER = '$recsource' AND
                    ACTIVE = 'T' AND
                    (PARAM IS NULL OR PARAM != 'DELETE')";
        $result = queryRow($sql);
        return $result;
    }
    //--------------------------------------------------------------------------
    /**
     * Queries the database for all the child poducts of the product with the
     * passed productId.
     *
     * @param productId a string of digits coresponding to the primary key of
     *                  the IPRODUCT table rows
     * @return          an array of products
     */
    private function fetchProductChildren($productId) {
        $recsource = $this->getRecsource();
        $sql = "SELECT
                    p.PRODUCTID,
                    p.PRODUCTOWNER,
                    p.PRODUCTTYPE,
                    p.DISPLAYNAME,
                    p.PRODUCTDESC,
                    p.SELLINGPRICE AS PRICE,
                    p.ORDERMIN,
                    p.ORDERINCREMENT,
                    p.LANGUAGE
                FROM
                    IPRODUCT p JOIN
                    IPRODUCTDTL pd ON p.PRODUCTID = pd.INFO
                WHERE
                    p.PRODUCTOWNER = '$recsource' AND
                    p.ACTIVE = 'T' AND
                    pd.PRODUCTID = $productId AND
                    (pd.FREETRAIT != 'DISABLED' OR pd.FREETRAIT IS NULL) AND
                    pd.ITEM = 'ANOTHER PRODUCT' AND
                    (p.PARAM IS NULL OR p.PARAM != 'DELETE')
                ORDER BY
                    pd.DISPLAYORDER,
                    p.LANGUAGE,
                    p.DISPLAYNAME";
        $result = queryAll($sql);
        return $result;
    }
    //--------------------------------------------------------------------------
    public function actionRootFetch() {
        valRequestMethodGet();
        $recsource = $this->getRecsource();
        $productRoot = queryScalar("
            SELECT
                PRODUCTROOT
            FROM
                VRECSOURCEEXTRA
            WHERE
                RECSOURCE = '$recsource'
        ");
        echo jsonEncodePlusYii($this->getProductInfo($productRoot));
    }
    //--------------------------------------------------------------------------
    /**
     * Determines whether the passed product is a branch node or a leaf node in
     * the product tree structure.
     *
     * @param $product  an array with keys corresponding to the IPRODUCT schema
     * @return          a boolean: true id branch node, false if leaf node
     */
    private function isGroupType($product) {
        // checks whether the product is a group type or
        // a non-leaf node in the product tree structure
        $productType = $product["PRODUCTTYPE"];
        if(stripos($productType, "GROUP") === false) {
            return false;
        }
        else {
            return true;
        }
    }
    //--------------------------------------------------------------------------
    /**
     * Modifies and returns the passed $product by adding the keys "MAINIMAGE",
     * "THUMBNAIL", and "PREVIEWS". These keys map to image paths (strings), the
     * last of which is an array of such strings.
     *
     * @param $product  an array with keys corresponding to the IPRODUCT schema
     * @return          a modified version of the passed $product
     */
    private function attachProductImages(&$product) {
        $productImageDir = dirEnsureSlash(Yii::app()->params["productImageDir"].$product["PRODUCTID"]);
        // defaults
        $missingImage = Yii::app()->params["missingImagePath"];
        $mainFile = $missingImage;
        $thumbFile = $missingImage;
        $previewFiles = [];
        // if product folder exists
        if(is_dir($productImageDir)) {
            $mainFindResults = fileOrDirFind($productImageDir."main.*");
            $thumbFindResults = fileOrDirFind($productImageDir."thumbnail.*");
            // if main does not exist, use thumb; or vice-versa; or default to missing
            if(empty($mainFindResults) && empty($thumbFindResults)) {
                $mainFile = $missingImage;
                $thumbFile = $missingImage;
            }
            if(empty($mainFindResults) && !empty($thumbFindResults)) {
                $thumbFile = $productImageDir.fileExtractName($thumbFindResults[0]);
                $mainFile = $thumbFile;
            }
            else if(!empty($mainFindResults) && empty($thumbFindResults)) {
                $mainFile = $productImageDir.fileExtractName($mainFindResults[0]);
                $thumbFile = $mainFile;
            }
            else {
                $mainFile = $productImageDir.fileExtractName($mainFindResults[0]);
                $thumbFile = $productImageDir.fileExtractName($thumbFindResults[0]);
            }
            // if there are no previews, previewFiles will be an empty array
            $previewFiles = fileOrDirFind($productImageDir."preview*.*");
            foreach($previewFiles as $key=>$image) {
                $previewFiles[$key] = $productImageDir.fileExtractName($image);
                $previewFiles[$key] = strDeletePrefix("..".DIRECTORY_SEPARATOR, $previewFiles[$key]);
            }
        }
        $product["MAINIMAGE"] = strDeletePrefix("..".DIRECTORY_SEPARATOR, $mainFile);
        $product["THUMBNAIL"] = strDeletePrefix("..".DIRECTORY_SEPARATOR, $thumbFile);
        $product["PREVIEWS"] = $previewFiles;
        return $product;
    }
    //--------------------------------------------------------------------------
    private function getProductInfo($productId) {
        $product = $this->fetchProduct($productId);
        $this->attachProductImages($product);
        $result = ["PRODUCT"=>$product];
        if($this->isGroupType($product)) {
            $children = $this->fetchProductChildren($productId);
            foreach($children as &$child) {
                $this->attachProductImages($child);
            }
            $result["CHILDREN"] = $children;
        }
        return $result;
    }
    //--------------------------------------------------------------------------
    /**
     * Handles AJAX call for productInfoFetch. The request method must be GET.
     * Fetches the product with the given PRODUCTID and its direct child
     * products, if they exist, from the IPRODUCT table in the databse. The
     * product and child products include paths to images.
     *
     * @param PRODUCTID a string of digits, the primary key of the desired
     *                  product
     * @return          a string in JSON format with keys "PRODUCT" and
     *                  "CHILDREN" (if there are any child products); "PRODUCT"
     *                  maps to the product object fetched from the database and
     *                  "CHILDREN" maps to an array of child product objects
     */
    public function actionProductInfoFetch() {
        valRequestMethodGet();
        $productId = arrayGet($_GET, 'PRODUCTID');
        echo jsonEncodePlusYii($this->getProductInfo($productId));
    }
    //--------------------------------------------------------------------------
    /**
     * Creates an order in the database.
     *
     * @return  a string of digits, the link number
     */
    public function actionOrderSubmit() {
        valRequestMethodPost();
        $order = new Order();
        $order->RECSOURCE = $this->getRecsource();
        $order->BUNDLESHIPMENTS = "T";
        $order->ORDERSTATUS = "PENDING";
        $order->PAYMENTMETHOD = "MISSING";
        $order->STEWARDSHIPLOC = "UPB";
        $order->submit();
        $order->refresh();

        $result = $order->LINK;
        echo jsonEncodePlus($result);
    }
    //--------------------------------------------------------------------------
    /**
     * Handles AJAX call for jobSubmit. The request method must be POST.
     * Creates or updates a job in the IJOB table. If a job does not exist with
     * the passed orderlink and productid, a new job is created, otherwise the
     * quantity is increased by the passed quantity amount.
     *
     * @param ORDERLINK a string of digits, the link number
     * @param PRODUCTID a string of digits coresponding to the primary key of
     *                  the IPRODUCT table rows
     * @param QUANTITY  a string of digits
     * @return          a string of digits, the JOBID
     */
    public function actionJobSubmit() {
        valRequestMethodPost();
        $jobInfo = inputGetJsonDecode();
fileLogDebugYii($jobInfo);

        $orderLink = arrayGet($jobInfo, "ORDERLINK");
        $productId = arrayGet($jobInfo, "PRODUCTID");
        $quantity = arrayGet($jobInfo, "QUANTITY");

        $product = Product::model()->findByPk($productId);
        errIfNull($product, "Product $productId is invalid");
        verify($product->PRODUCTOWNER === $this->getRecsource(), "Product $productId is invalid");
        verify($product->PRODUCTCASE === "PRODUCT", "Product $productId is invalid");
        verify($product->ACTIVE === "T", "Product $productId is invalid");
        verify(isNullOrEmptyStr($product->PARAM), "Product $productId is invalid");
        verify($quantity >= $product->ORDERMIN, "This product must be purchased in quantities of at least ".$product->ORDERMIN);
        verify(($quantity % $product->ORDERINCREMENT) === 0, "This product must be purchased in quantity increments of ".$product->ORDERINCREMENT);

        $job = Job::model()->findByAttributes(array("LINK"=>$orderLink, "PRODUCTID"=>$productId));
        if($job === null) {     // if job does not exist, create new job
            $job = new Job;
            $job->SERVICELOC = "UPB";
            $job->STATUS = "PENDING";
            $job->PRODUCTCOUNT = $quantity;
            $job->LINK = $orderLink;
            $job->PRODUCTID = $productId;
        }
        else {                  // otherwise update quantity
            $job->PRODUCTCOUNT = $job->PRODUCTCOUNT + $quantity;
        }
        $job->submit();
    }
    //--------------------------------------------------------------------------
    /**
     * Handles AJAX call for searchCatalog. The request method must be GET.
     * Searches the product tree in the database for products whose displaynames
     * contain the search words. Only returns leaf products, except in the case
     * of languagegroups, in which case the language group product is included
     * in the results and not the leaf products.
     *
     * @param search    an "&" delimited string of words (strings) used in the
     *                  search
     * @return          a string in JSON format with keys "searchParams" and
     *                  "searchResults"; "searchParams" maps to the parameter
     *                  search and "searchResults" maps to an array of product
     *                  objects
     */
    public function actionSearchCatalog() {
        valRequestMethodGet();
        $searchWords = explode(" ", arrayGet($_GET, "search"));
        $recsource = $this->getRecsource();
        $sql = "SELECT
                    PRODUCTID,
                    PRODUCTOWNER,
                    PRODUCTTYPE,
                    DISPLAYNAME,
                    PRODUCTDESC,
                    SELLINGPRICE AS PRICE,
                    ORDERMIN,
                    ORDERINCREMENT,
                    LANGUAGE
                FROM
                    IPRODUCT
                WHERE
                    PRODUCTOWNER = '$recsource' AND
                    ACTIVE = 'T' AND
                    PRODUCTTYPE != 'SUBGROUP' AND
                    PRODUCTID IN (
                        -- all PRODUCTIDS that have a parent in
                        SELECT CAST(INFO AS INT) FROM IPRODUCTDTL
                        WHERE PRODUCTID IN (
                            -- all SUBGROUP PRODUCTIDS in store
                            SELECT PRODUCTID FROM IPRODUCT
                            WHERE
                                PRODUCTOWNER = '$recsource' AND
                                PRODUCTTYPE = 'SUBGROUP' AND
                                ACTIVE = 'T' AND
                                (PARAM IS NULL OR PARAM != 'DELETE')
                        ) AND
                        (FREETRAIT != 'DISABLED' OR FREETRAIT IS NULL) AND
                        ITEM IN ('ANOTHER PRODUCT')
                    ) AND
                    (PARAM IS NULL OR PARAM != 'DELETE')";
        foreach($searchWords as $word) {
            $word = str_replace("'", "''", $word);
            $sql .= " AND REGEXP_LIKE(DISPLAYNAME, '$word', 'i')";
        }
        $sql .= " ORDER BY DISPLAYNAME";
        $results = queryAll($sql);
        foreach($results as &$item) {
            $this->attachProductImages($item);
        }
        echo jsonEncodePlusYii($results);
    }
    //--------------------------------------------------------------------------
    public function actionOptionGroupOptionsFetch() {
        valRequestMethodGet();
        $productId = arrayGet($_GET, 'PRODUCTID');  // parent product
        $sql = "SELECT
                    EM.DISPLAYNAME,
                    R.DISPLAYNAME AS ROLE,
                    PD.INFO
                FROM
                    IPRODUCTDTL PD JOIN
                    VELEMENTMAP EM ON
                        PD.ITEM = EM.ITEM AND PD.TRAIT = EM.TRAIT AND PD.EXTRATRAIT = EM.EXTRATRAIT LEFT JOIN
                    VROLE R ON
                        PD.ROLE = R.ROLE
                WHERE
                    PD.PRODUCTID = $productId AND
                    PD.ITEM != 'ANOTHER PRODUCT'
                ORDER BY
                    PD.DISPLAYORDER";
        $options = queryAll($sql);
        echo jsonEncodePlusYii($options);
    }
    //--------------------------------------------------------------------------
    public function actionOptionGroupValuesFetch() {
        valRequestMethodGet();
        $productId = arrayGet($_GET, 'PRODUCTID'); // parent product
        $selectedOptions = json_decode(arrayGet($_GET, 'SELECTEDOPTIONS', '[]'));
        $numSelectedOptions = count($selectedOptions);

        $sql = "SELECT PD.ITEM, EM.DISPLAYNAME
                FROM IPRODUCTDTL PD JOIN VELEMENTMAP EM ON
                    PD.ITEM = EM.ITEM AND PD.TRAIT = EM.TRAIT AND PD.EXTRATRAIT = EM.EXTRATRAIT
                WHERE PD.PRODUCTID = $productId AND PD.ITEM != 'ANOTHER PRODUCT'
                ORDER BY PD.DISPLAYORDER";
        $options = queryAll($sql);
        verify($numSelectedOptions < count($options), "All options have already been selected");

        // SELECT the next option values
        $sql = "SELECT DISTINCT
                    FIRST_VALUE(PD0.PRODUCTID) OVER(PARTITION BY EM$numSelectedOptions.DISPLAYNAME ORDER BY CORE.CALCPRODUCTCOST(PD0.PRODUCTID)) AS PRODUCTID,
                    EM$numSelectedOptions.DISPLAYNAME AS OPTIONVALUE,
                    FIRST_VALUE(PARENTDTL.DISPLAYORDER) OVER(PARTITION BY EM$numSelectedOptions.DISPLAYNAME ORDER BY CORE.CALCPRODUCTCOST(PD0.PRODUCTID)) AS DISPLAYORDER";
        // FROM the possible option values
        $sql .= "\nFROM
                    IPRODUCTDTL PARENTDTL JOIN
                    IPRODUCTDTL PD0 ON PARENTDTL.INFO = PD0.PRODUCTID JOIN
                    VELEMENTMAP EM0 ON
                        PD0.ITEM = EM0.ITEM AND
                        PD0.TRAIT = EM0.TRAIT AND
                        PD0.EXTRATRAIT = EM0.EXTRATRAIT";
        for($i = 1; $i < count($options); $i++) {
            $sql .= " JOIN
                    IPRODUCTDTL PD$i ON PD$i.PRODUCTID = PD0.PRODUCTID JOIN
                    VELEMENTMAP EM$i ON
                        PD$i.ITEM = EM$i.ITEM AND
                        PD$i.TRAIT = EM$i.TRAIT AND
                        PD$i.EXTRATRAIT = EM$i.EXTRATRAIT";
        }
        // WHERE certain option values have been chosen
        $sql .= "\nWHERE
                    PARENTDTL.PRODUCTID = $productId AND
                    PARENTDTL.ITEM = 'ANOTHER PRODUCT' AND
                    (PARENTDTL.FREETRAIT IS NULL OR PARENTDTL.FREETRAIT != 'DISABLED')";
        for($i = 0; $i < count($options); $i++) {
            $sql .= " AND\nPD$i.ITEM = '".$options[$i]["ITEM"]."'";
        }
        for($i = 0; $i < $numSelectedOptions; $i++) {
            $sql .= " AND\nEM$i.DISPLAYNAME = '".$selectedOptions[$i]."'"; // todo use PD$i.TRAIT instead
        }
        $sql .= "\nORDER BY
                    DISPLAYORDER,
                    OPTIONVALUE";

        $optionValues = queryAll($sql);
        $result = [
            "OPTION"=>$options[$numSelectedOptions]["DISPLAYNAME"],
            "OPTIONVALUES"=>$optionValues,
        ];
        echo jsonEncodePlusYii($result);
    }
}
