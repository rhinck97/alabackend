<?php
return CMap::mergeArray(require(dirname(__FILE__).DIRECTORY_SEPARATOR.'main.php'), array(
    'components'=>array(
        'db'=>array(
            'connectionString'=>'oci:dbname=Live;charset=UTF8',
        ),
    ),
    'params'=>array(
        'server'=>'live',
        'jobPrefix'=>'X',
    ),
));
