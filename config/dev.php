<?php
return CMap::mergeArray(require(dirname(__FILE__).DIRECTORY_SEPARATOR.'main.php'), array(
    'components'=>array(
        'db'=>array(
            'connectionString'=>'oci:dbname=Dev;charset=UTF8',
        ),
        array(
            'class'=>'CProfileLogRoute',
        ),
    ),
    'params'=>array(
        'server'=>'dev',
        'jobPrefix'=>'X',
    ),
));
