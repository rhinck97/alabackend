<?php
// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

return array(
    'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
    'name'=>'Print & Mail',
    'language'=>'en',
    'preload'=>array(
        'log',
        'db',
    ),
    'import'=>array(
        'application.models.*',
        'application.components.*',
        'application.lib.*',
        'application.lib.pam.*',
        'application.lib.onyx.*',
    ),
    'modules'=>array(
        'gii'=>array(
            'class'=>'system.gii.GiiModule',
            'password'=>'cup7shoe',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters'=>array('127.0.0.1', '::1', '*'),
            'generatorPaths'=>array(
                'bootstrap.gii',
            ),
        ),
    ),
    'components'=>array(
        'cache'=>array(
            'class'=>'system.caching.CFileCache',
        ),
        'user'=>array(
            'class'=>'YWebUser',
            // enable cookie-based authentication
            'allowAutoLogin'=>true,
        ),
        'clientScript'=>array(
            'class'=>'yClientScript',
        ),
        'urlManager'=>array(
            'urlFormat'=>'path',
            'showScriptName'=>true,
            // 'caseSensitive'=>false,
            'rules'=>array(
                '<controller:\w+>/<id:\d+>'=>'<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
            ),
        ),
        'db'=>array(
            'class'=>'CDbConnection',
            'connectionString'=>'oci:dbname=dev;charset=UTF8',
            'initSQLs'=>array(
                "ALTER SESSION SET NLS_DATE_FORMAT = 'DD-MM-YYYY HH24:MI:SS'",
            ),
            'charset'=>'utf8',
            'username'=>'job',
            'password'=>'cup7shoe',
            'schemaCachingDuration'=>86400, //Cache for one day
        ),
        'avanti'=>array(
            'class'=>'CDbConnection',
            'connectionString'=>'sqlsrv:Server=pmpcsd7.byu.edu;Database=BRIY',
            'autoConnect'=>false,
            'username'=>'sa',
            'password'=>'Georgia^hills',
        ),
        'gen2'=>array(
            'class'=>'CDbConnection',
            'autoConnect'=>false,
            'connectionString'=>'sqlsrv:Server=pmpcgen2.byu.edu;Database=MCMG2',
            'username'=>'MCMG2',
            'password'=>'findingbusylife',
        ),
        'errorHandler'=>array(
            'class'=>'YErrorHandler',
            'errorAction'=>'site/error',
        ),
        'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'error, profile',
                ),
            ),
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params'=>array(
        'shippingMultiplier'=>1.3,
        'handlingChange'=>2,
        'adminEmail'=>'printandmail-support@byu.edu',
        'alertEmail'=>'pmpccs@gmail.com',
        'gen2'=>array(
            'connectionString'=>'DRIVER={SQL Server};SERVER=pmpcgen2.byu.edu;DATABASE=MCMG2',
            'username'=>'MCMG2',
            'password'=>'findingbusylife',
        ),
        'productImageDir'=>'..'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'products'.DIRECTORY_SEPARATOR,
        'missingImagePath'=>'..'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'missing.jpg',
        'RECSOURCE'=>'ALA',
    ),
);
