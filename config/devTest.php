<?php
return CMap::mergeArray(require(dirname(__FILE__).DIRECTORY_SEPARATOR.'dev.php'), array(
    'components'=>array(
        'db'=>array(
            'enableProfiling'=>true,
        ),
        'log'=>array(
            'routes'=>array(
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'trace, error, warning',
                ),
                array(
                    'class'=>'CWebLogRoute',
                    'levels'=>'trace, error, warning',
                ),
                array(
                    'class'=>'CProfileLogRoute',
                ),
            ),
        ),
    ),
));

//require(dirname(__FILE__).'/testSettings.php')
