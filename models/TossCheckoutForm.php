<?php
class CheckoutForm extends CFormModel {
    public $firstName;
    public $lastName;
    public $email;
    public $phone;
    public $paymentMethod;
    public $paymentInfo;
    public $couponCode;
    public $poNumber;
    //----------------------------------------------
    public function rules() {
        return array(
                array('firstName, lastName, email, phone, paymentMethod', 'required',),
                array('email', 'email',),
                array('shipments', 'validateShipping'),
        );
    }
    //----------------------------------------------
    public function attributeLabels() {
        return array(
                'firstName'=>'First Name',
                'lastName'=>'Last Name',
                'email'=>'E-Mail',
                'phone'=>'Phone Number',
                'paymentMethod'=>'Select Payment Method...',
                'couponCode'=>'Coupon Code',
                'poNumber'=>''
        );
    }
    //----------------------------------------------
    public function validateShipping() {
        //look at shipments array
        //shipmentPkey
        //jobid
        //productCount
        return true;
    }
}