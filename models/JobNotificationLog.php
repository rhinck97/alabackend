<?php
/**
 * This is the model class for table "JOBNOTIFICATIONLOG".
 *
 * The followings are the available columns in table 'JOBNOTIFICATIONLOG':
 * @property double $ID
 * @property double $JOBID
 * @property string $CONTENT
 * @property string $RECIPIENT
 * @property string $RECTIME
 * @property string $VIEWED
 */
class JobNotificationLog extends YActiveRecord {
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return JobNotificationLog the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'JOB.JOBNOTIFICATIONLOG';
    }
    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
                array('CONTENT, RECIPIENT, RECTIME, VIEWED', 'required'),
                array('JOBID', 'numerical'),
                array('CONTENT', 'length', 'max'=>2000),
                array('RECIPIENT', 'length', 'max'=>40),
                array('VIEWED', 'length', 'max'=>1),
                // The following rule is used by search().
                // Please remove those attributes that should not be searched.
                array('ID, JOBID, CONTENT, RECIPIENT, RECTIME, VIEWED', 'safe', 'on'=>'search'),
        );
    }
    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'ID'=>'ID',
                'JOBID'=>'Jobid',
                'CONTENT'=>'Content',
                'RECIPIENT'=>'Recipient',
                'RECTIME'=>'Rectime',
                'VIEWED'=>'Viewed',
        );
    }
    public function getDisplayAttributes() {
        $attr = $this->getAttributes();
        $attr['RECTIME'] = date(DATE_ISO8601, strtotime($attr['RECTIME']));
        $attr['VIEWED'] = ($attr['VIEWED'] === 'T') ? true : false;
        $dispAttr = array("id"=>$attr["ID"],
                "jobId"=>$attr["JOBID"],
                "content"=>$attr["CONTENT"],
                "recipient"=>$attr["RECIPIENT"],
                "recTime"=>$attr["RECTIME"],
                "viewed"=>$attr["VIEWED"]);
        return $dispAttr;
    }
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID);
        $criteria->compare('JOBID', $this->JOBID);
        $criteria->compare('CONTENT', $this->CONTENT, true);
        $criteria->compare('RECIPIENT', $this->RECIPIENT, true);
        $criteria->compare('RECTIME', $this->RECTIME, true);
        $criteria->compare('VIEWED', $this->VIEWED, true);

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
        ));
    }
}