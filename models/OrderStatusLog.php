<?php
class OrderStatusLog extends YActiveRecord {
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    //----------------------------------------------
    public function tableName() {
        return 'JOB.IORDERSTATUSLOG';
    }
    //----------------------------------------------
    public function rules() {
        return array(
                array('LINK, ORDERSTATUS', 'required'),
                array('LINK', 'numerical'),
                array('ORDERSTATUS', 'length', 'max'=>25),
                array('LINK, ORDERSTATUS, ORDERSTATUSTIME, PKEY', 'safe'),
        );
    }
    //----------------------------------------------
    public function relations() {
        return array(
                'LINK'=>array(self::BELONGS_TO, 'Order', 'LINK'),
                'ORDERSTATUS'=>array(self::BELONGS_TO, 'OrderStatus', 'ORDERSTATUS'),
        );
    }
}