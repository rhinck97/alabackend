<?php
class DeliveryMethodDtl extends YActiveRecord {
    //----------------------------------------------
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    public function primaryKey() {
        return "DELIVERYMETHOD";
    }
    //----------------------------------------------
    public function tableName() {
        return 'JOB.VDELIVERYMETHODDTL';
    }
    //----------------------------------------------
    public function rules() {
        return array(
                array('DELIVERYMETHOD, RECSOURCE, DISPLAYORDER', 'required'),
                array('DISPLAYORDER, PRIORITY', 'numerical'),
                array('DELIVERYMETHOD, RECSOURCE', 'length', 'max'=>25),
                array('SHIPPREFIX', 'length', 'max'=>20),
                array('DELIVERYMETHOD, RECSOURCE, DISPLAYORDER, SHIPPREFIX, PRIORITY', 'safe', 'on'=>'search'),
        );
    }
    //----------------------------------------------
    public function relations() {
        return array(
                'deliveryMethod'=>array(self::BELONGS_TO, 'DeliveryMethod', 'DELIVERYMETHOD'),
                'recSource'=>array(self::BELONGS_TO, 'RecSource', 'RECSOURCE'),
                'shipPrefix'=>array(self::BELONGS_TO, 'VSHIPPREFIX', 'SHIPPREFIX'),
        );
    }
    //----------------------------------------------
    public function attributeLabels() {
        return array(
                'DELIVERYMETHOD'=>'Deliverymethod',
                'RECSOURCE'=>'Recsource',
                'DISPLAYORDER'=>'Displayorder',
                'SHIPPREFIX'=>'Shipprefix',
                'PRIORITY'=>'Priority',
        );
    }
    //----------------------------------------------
    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('DELIVERYMETHOD', $this->DELIVERYMETHOD, true);
        $criteria->compare('RECSOURCE', $this->RECSOURCE, true);
        $criteria->compare('DISPLAYORDER', $this->DISPLAYORDER);
        $criteria->compare('SHIPPREFIX', $this->SHIPPREFIX, true);
        $criteria->compare('PRIORITY', $this->PRIORITY);

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
        ));
    }
    //----------------------------------------------
    public function getAltAttributes() {
        $deliveryDtlArray = $this->getAttributes();
        $resultArray = array();
        $altKeyMap = array(
                "DELIVERYMETHOD"=>"deliveryMethod",
                "DISPLAYORDER"=>"displayOrder"
        );
        foreach($deliveryDtlArray as $key=> $value) {
            if(array_key_exists($key, $altKeyMap)) {
                $resultArray[$altKeyMap[$key]] = $value;
            }
        }
        return $resultArray;
    }
}