<?php
/**
 * This is the model class for table "INPUTTEMPLATEDTL".
 *
 * The followings are the available columns in table 'INPUTTEMPLATEDTL':
 * @property double $ID
 * @property double $TEMPLATEID
 * @property string $LABEL
 * @property string $WIDGETTYPE
 * @property string $ROLE
 * @property double $OPTIONSETID
 * @property double $DISPLAYORDER
 * @property double $GROUPNUMBER
 * @property string $GROUPLABEL
 * @property string $LABELSUBTEXT
 *
 * The followings are the available model relations:
 * @property WIDGETOPTIONSET $oPTIONSET
 * @property VROLE $rOLE
 * @property INPUTTEMPLATE $tEMPLATE
 * @property WIDGETTYPE $wIDGETTYPE
 */
class InputTemplateDtl extends YActiveRecord {
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return InputTemplateDtl the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'JOB.INPUTTEMPLATEDTL';
    }
    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
                array('TEMPLATEID, WIDGETTYPE, ROLE, GROUPNUMBER', 'required'),
                array('TEMPLATEID, OPTIONSETID, DISPLAYORDER, GROUPNUMBER', 'numerical'),
                array('LABEL, WIDGETTYPE, ROLE, GROUPLABEL, LABELSUBTEXT', 'length', 'max'=>20),
                // The following rule is used by search().
                // Please remove those attributes that should not be searched.
                array('ID, TEMPLATEID, LABEL, WIDGETTYPE, ROLE, OPTIONSETID, DISPLAYORDER, GROUPNUMBER, GROUPLABEL, LABELSUBTEXT', 'safe', 'on'=>'search'),
        );
    }
    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
                'template'=>array(self::BELONGS_TO, 'InputTemplate', 'TEMPLATEID'),
                'optionset'=>array(self::BELONGS_TO, 'WidgetOptionSet', array('ID'=>'OPTIONSETID')),
                'options'=>array(self::HAS_MANY, 'WidgetOption', array("ID"=>"OPTIONSETID"), "through"=>'optionset', 'order'=>"DISPLAYORDER"),
                'restrictedWidgets'=>array(self::HAS_MANY, 'InputRestrictionMap', array("ID"=>"PARENTDTLID"))
        );
    }
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'ID'=>'ID',
                'TEMPLATEID'=>'Templateid',
                'LABEL'=>'Label',
                'WIDGETTYPE'=>'Widgettype',
                'ROLE'=>'Role',
                'OPTIONSETID'=>'Optionsetid',
                'DISPLAYORDER'=>'Displayorder',
                'GROUPNUMBER'=>'Groupnumber',
                'GROUPLABEL'=>'Grouplabel',
                'LABELSUBTEXT'=>'Labelsubtext',
        );
    }
    public function getRestrictedSets() {
        //grab all the widgets restricted by current widget
        $restrictedWidgets = $this->restrictedWidgets;
        //this will contain the id of any widgets this widget will restrict
        foreach($restrictedWidgets as $key=> $widget) {//foreach widget that will be restricted
            //get the item trait extratrait combo from widget option
            $finalDtlAry = $this->options(array("condition"=>"ID='{$widget->ID}'", "select"=>"DTLITEM,DTLTRAIT,DTLEXTRATRAIT,DTLFREETRAIT"));
            //get itegroup
        }
    }
    public function getTargetAreas() {
        $roleEntry = Role::model()->findByPk($this->ROLE);
        return array_filter(explode(",", $roleEntry->TARGETAREAS), "strlen");
    }
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID);
        $criteria->compare('TEMPLATEID', $this->TEMPLATEID);
        $criteria->compare('LABEL', $this->LABEL, true);
        $criteria->compare('WIDGETTYPE', $this->WIDGETTYPE, true);
        $criteria->compare('ROLE', $this->ROLE, true);
        $criteria->compare('OPTIONSETID', $this->OPTIONSETID);
        $criteria->compare('DISPLAYORDER', $this->DISPLAYORDER);
        $criteria->compare('GROUPNUMBER', $this->GROUPNUMBER);
        $criteria->compare('GROUPLABEL', $this->GROUPLABEL, true);
        $criteria->compare('LABELSUBTEXT', $this->LABELSUBTEXT, true);

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
        ));
    }
}