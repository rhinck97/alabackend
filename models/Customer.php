<?php
class Customer extends YActiveRecord {
    //----------------------------------------------
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    //----------------------------------------------
    public function tableName() {
        return 'BIL.ICUST';
    }
    //----------------------------------------------
    public function rules() {
        return array(
                array('NAME, TYPE, CCRATETYPE, TAXSTATUS', 'required'),
                array('CSRNO, TAXABLE, UNITNUMBER', 'numerical', 'integerOnly'=>true),
                array('NAME', 'length', 'max'=>120),
                array('CONTACT', 'length', 'max'=>60),
                array('STMTADDR', 'length', 'max'=>70),
                array('STMTBLDG', 'length', 'max'=>10),
                array('RTNBLDG, TYPE, MANAGERBLDG', 'length', 'max'=>4),
                array('CITY, STMTADDR2, STMTTYPES, STMTADDR3, STMTADDR4, LOGIN', 'length', 'max'=>50),
                array('STATE, ZIP, TAXSTATUS, PARAM, MANAGERPHONE, ROUTEY, MANAGERROUTEY', 'length', 'max'=>20),
                array('RTNADDR', 'length', 'max'=>60),
                array('PHONE, MANAGERNAME, MANAGERADDR', 'length', 'max'=>30),
                array('MFSNO, PFSNO', 'length', 'max'=>23),
                array('ECOMMENT, COUNTRY', 'length', 'max'=>40),
                array('PMTTERMS', 'length', 'max'=>10),
                array('CCRATETYPE', 'length', 'max'=>3),
                array('EMAILADDR, MANAGEREMAIL', 'length', 'max'=>80),
                array('USERPASSWORD', 'length', 'max'=>25),
                array('UNITTYPE', 'length', 'max'=>1),
                array('MODIFIEDDATE', 'safe'),
                // The following rule is used by search().
                // Please remove those attributes that should not be searched.
                array('CUSTID, NAME, CONTACT, STMTADDR, STMTBLDG, CITY, STATE, ZIP, RTNADDR, RTNBLDG, PHONE, TYPE, MFSNO, PFSNO, ECOMMENT, STMTADDR2, PMTTERMS, CSRNO, CCRATETYPE, EMAILADDR, TAXABLE, STMTTYPES, USERPASSWORD, UNITNUMBER, TAXSTATUS, PARAM, MANAGERNAME, MANAGERADDR, MANAGEREMAIL, MANAGERPHONE, MANAGERBLDG, ROUTEY, MANAGERROUTEY, STMTADDR3, STMTADDR4, COUNTRY, LOGIN, MODIFIEDDATE, UNITTYPE', 'safe', 'on'=>'search'),
        );
    }
    //----------------------------------------------
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.

        return array(
                'transactions'=>array(self::HAS_MANY, 'Transaction', 'CUSTID'),
        );
    }
    //----------------------------------------------
    public function attributeLabels() {
        return array(
                'CUSTID'=>'Custid',
                'NAME'=>'Name',
                'CONTACT'=>'Contact',
                'STMTADDR'=>'Stmtaddr',
                'STMTBLDG'=>'Stmtbldg',
                'CITY'=>'City',
                'STATE'=>'State',
                'ZIP'=>'Zip',
                'RTNADDR'=>'Rtnaddr',
                'RTNBLDG'=>'Rtnbldg',
                'PHONE'=>'Phone',
                'TYPE'=>'Type',
                'MFSNO'=>'Mfsno',
                'PFSNO'=>'Pfsno',
                'ECOMMENT'=>'Ecomment',
                'STMTADDR2'=>'Stmtaddr2',
                'PMTTERMS'=>'Pmtterms',
                'CSRNO'=>'Csrno',
                'CCRATETYPE'=>'Ccratetype',
                'EMAILADDR'=>'Emailaddr',
                'TAXABLE'=>'Taxable',
                'STMTTYPES'=>'Stmttypes',
                'USERPASSWORD'=>'Userpassword',
                'UNITNUMBER'=>'Unitnumber',
                'TAXSTATUS'=>'Taxstatus',
                'PARAM'=>'Param',
                'MANAGERNAME'=>'Managername',
                'MANAGERADDR'=>'Manageraddr',
                'MANAGEREMAIL'=>'Manageremail',
                'MANAGERPHONE'=>'Managerphone',
                'MANAGERBLDG'=>'Managerbldg',
                'ROUTEY'=>'Routey',
                'MANAGERROUTEY'=>'Managerroutey',
                'STMTADDR3'=>'Stmtaddr3',
                'STMTADDR4'=>'Stmtaddr4',
                'COUNTRY'=>'Country',
                'LOGIN'=>'Login',
                'MODIFIEDDATE'=>'Modifieddate',
                'UNITTYPE'=>'Unittype',
        );
    }
    //----------------------------------------------
    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('CUSTID', $this->CUSTID, true);
        $criteria->compare('NAME', $this->NAME, true);
        $criteria->compare('CONTACT', $this->CONTACT, true);
        $criteria->compare('STMTADDR', $this->STMTADDR, true);
        $criteria->compare('STMTBLDG', $this->STMTBLDG, true);
        $criteria->compare('CITY', $this->CITY, true);
        $criteria->compare('STATE', $this->STATE, true);
        $criteria->compare('ZIP', $this->ZIP, true);
        $criteria->compare('RTNADDR', $this->RTNADDR, true);
        $criteria->compare('RTNBLDG', $this->RTNBLDG, true);
        $criteria->compare('PHONE', $this->PHONE, true);
        $criteria->compare('TYPE', $this->TYPE, true);
        $criteria->compare('MFSNO', $this->MFSNO, true);
        $criteria->compare('PFSNO', $this->PFSNO, true);
        $criteria->compare('ECOMMENT', $this->ECOMMENT, true);
        $criteria->compare('STMTADDR2', $this->STMTADDR2, true);
        $criteria->compare('PMTTERMS', $this->PMTTERMS, true);
        $criteria->compare('CSRNO', $this->CSRNO);
        $criteria->compare('CCRATETYPE', $this->CCRATETYPE, true);
        $criteria->compare('EMAILADDR', $this->EMAILADDR, true);
        $criteria->compare('TAXABLE', $this->TAXABLE);
        $criteria->compare('STMTTYPES', $this->STMTTYPES, true);
        $criteria->compare('USERPASSWORD', $this->USERPASSWORD, true);
        $criteria->compare('UNITNUMBER', $this->UNITNUMBER);
        $criteria->compare('TAXSTATUS', $this->TAXSTATUS, true);
        $criteria->compare('PARAM', $this->PARAM, true);
        $criteria->compare('MANAGERNAME', $this->MANAGERNAME, true);
        $criteria->compare('MANAGERADDR', $this->MANAGERADDR, true);
        $criteria->compare('MANAGEREMAIL', $this->MANAGEREMAIL, true);
        $criteria->compare('MANAGERPHONE', $this->MANAGERPHONE, true);
        $criteria->compare('MANAGERBLDG', $this->MANAGERBLDG, true);
        $criteria->compare('ROUTEY', $this->ROUTEY, true);
        $criteria->compare('MANAGERROUTEY', $this->MANAGERROUTEY, true);
        $criteria->compare('STMTADDR3', $this->STMTADDR3, true);
        $criteria->compare('STMTADDR4', $this->STMTADDR4, true);
        $criteria->compare('COUNTRY', $this->COUNTRY, true);
        $criteria->compare('LOGIN', $this->LOGIN, true);
        $criteria->compare('MODIFIEDDATE', $this->MODIFIEDDATE, true);
        $criteria->compare('UNITTYPE', $this->UNITTYPE, true);

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
        ));
    }
}