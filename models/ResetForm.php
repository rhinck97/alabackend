<?php
class ResetForm extends CFormModel {
    public $password;
    public $confirmPassword;
    //--------------------------------------
    public function rules() {
        return array(
                array('password, confirmPassword', 'required'),
                array('password, confirmPassword', 'confirmPassword')
        );
    }
    //--------------------------------------
    public function confirmPassword($attribute, $params) {
        if($this->$attribute != $this->confirmPassword) {
            $this->addError($attribute, 'The two supplied passwords do not match.');
        }
        return !$this->hasErrors();
    }
    //--------------------------------------
}