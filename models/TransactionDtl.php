<?php
class TransactionDtl extends YActiveRecord {
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    //------------------------------------------------
    public function tableName() {
        return 'BIL.ITRANDTL';
    }
    //------------------------------------------------
    public function rules() {
        return array(
                array('LINK, CNT, AMT, AMTEACH, AUXCNT2, AUXCNT3', 'numerical'),
                array('RECDATE, MODIFIED, RECTIME', 'safe'),
                // The following rule is used by search().
                // Please remove those attributes that should not be searched.
                array('LINK, CNT, AMT, RECSOURCE, RECDATE, PKEY, MODIFIED, EDESC, ITEMTYPE, SRVLOC, ITEM, JOT, AMTEACH, RECTIME, AUXCNT2, AUXCNT3, ITEMSTYLE, DTLMODE, DTLAUDITID', 'safe', 'on'=>'search'),
        );
    }
    //------------------------------------------------
    public function relations() {
        return array(
                'transaction'=>array(self::BELONGS_TO, 'Transaction', 'LINK'),
                'item'=>array(self::BELONGS_TO, 'Item', array('ITEM', 'ITEMTYPE'))
        );
    }
    //------------------------------------------------
    public function attributeLabels() {
        return array(
                'LINK'=>'Link',
                'CNT'=>'Cnt',
                'AMT'=>'Amt',
                'RECSOURCE'=>'Recsource',
                'RECDATE'=>'Recdate',
                'PKEY'=>'Pkey',
                'MODIFIED'=>'Modified',
                'EDESC'=>'Edesc',
                'ITEMTYPE'=>'Itemtype',
                'SRVLOC'=>'Srvloc',
                'ITEM'=>'Item',
                'JOT'=>'Jot',
                'AMTEACH'=>'Amteach',
                'RECTIME'=>'Rectime',
                'AUXCNT2'=>'Auxcnt2',
                'AUXCNT3'=>'Auxcnt3',
                'ITEMSTYLE'=>'Itemstyle',
                'DTLMODE'=>'Dtlmode',
                'DTLAUDITID'=>'Dtlauditid',
        );
    }
    //------------------------------------------------
    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('LINK', $this->LINK);
        $criteria->compare('CNT', $this->CNT);
        $criteria->compare('AMT', $this->AMT);
        $criteria->compare('RECSOURCE', $this->RECSOURCE, true);
        $criteria->compare('RECDATE', $this->RECDATE, true);
        $criteria->compare('PKEY', $this->PKEY);
        $criteria->compare('MODIFIED', $this->MODIFIED, true);
        $criteria->compare('EDESC', $this->EDESC, true);
        $criteria->compare('ITEMTYPE', $this->ITEMTYPE, true);
        $criteria->compare('SRVLOC', $this->SRVLOC, true);
        $criteria->compare('ITEM', $this->ITEM, true);
        $criteria->compare('JOT', $this->JOT, true);
        $criteria->compare('AMTEACH', $this->AMTEACH);
        $criteria->compare('RECTIME', $this->RECTIME, true);
        $criteria->compare('AUXCNT2', $this->AUXCNT2);
        $criteria->compare('AUXCNT3', $this->AUXCNT3);
        $criteria->compare('ITEMSTYLE', $this->ITEMSTYLE, true);
        $criteria->compare('DTLMODE', $this->DTLMODE, true);
        $criteria->compare('DTLAUDITID', $this->DTLAUDITID, true);

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
        ));
    }
}