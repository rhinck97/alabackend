<?php
class DocInfo extends YActiveRecord {
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    //--------------------------------------------
    public function tableName() {
        return 'JOB.IDOCINFO';
    }
    //--------------------------------------------
    public function rules() {
        return array(
                array('PAGECOUNT, ACTIVE', 'numerical'),
                array('PRODUCTTYPE, FILENAME, BWPAGECOUNT, COLORPAGECOUNT RECDATE, MODIFIEDDATE, ACTIVE, ', 'safe'),
        );
    }
    //--------------------------------------------
    public function relations() {
        return array(
                'productType'=>array(self::BELONGS_TO, 'ProductType', 'PRODUCTTYPE'),
        );
    }
    //--------------------------------------------
}