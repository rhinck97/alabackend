<?php
/**
 * This is the model class for table "INPUTTEMPLATE".
 *
 * The followings are the available columns in table 'INPUTTEMPLATE':
 * @property double $ID
 * @property string $DESCRIPTION
 *
 * The followings are the available model relations:
 * @property INPUTTEMPLATEDTLS[] $iNPUTTEMPLATEDTLSs
 */
class InputTemplate extends YActiveRecord {
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return InputTemplate the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'JOB.INPUTTEMPLATE';
    }
    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
                array('DESCRIPTION', 'length', 'max'=>20),
                // The following rule is used by search().
                // Please remove those attributes that should not be searched.
                array('ID, DESCRIPTION', 'safe', 'on'=>'search'),
        );
    }
    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
                'widgets'=>array(self::HAS_MANY, 'InputTemplateDtl', 'TEMPLATEID', 'order'=>"GROUPNUMBER ASC,DISPLAYORDER ASC"),
        );
    }
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'ID'=>'ID',
                'DESCRIPTION'=>'Description',
        );
    }
    public function roleToWidgetMap() {
        $dtls = $this->widgets;
        $map = array();
        foreach($dtls as $dtl)
            $map[$dtl->ROLE] = $dtl->WIDGETTYPE;
        return $map;
    }
    public function getDtlArray($role, $value) {

        $widgetDtl = $this->widgets(array("condition"=>"ROLE='$role'"));
        if(count($widgetDtl) < 1)
            return array();
        $widgetDtl = $widgetDtl[0];

        $optionDtls = $widgetDtl->options(array("condition"=>"VALUE='$value'", "select"=>"DTLITEM,DTLTRAIT,DTLEXTRATRAIT,DTLFREETRAIT"));
        if(count($optionDtls) < 1)
            return array();
        $optionDtls = $optionDtls[0];

        return array("item"=>$optionDtls->DTLITEM,
                "trait"=>$optionDtls->DTLTRAIT,
                "extraTrait"=>$optionDtls->DTLEXTRATRAIT,
                "freeTrait"=>$optionDtls->DTLFREETRAIT);
    }
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID);
        $criteria->compare('DESCRIPTION', $this->DESCRIPTION, true);

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
        ));
    }
}