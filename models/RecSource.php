<?php
class RecSource extends YActiveRecord {
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    //---------------------------------------------
    public function tableName() {
        return 'BIL.VRECSOURCE';
    }
    //---------------------------------------------
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
                array('IMPORT, UNIQUEAUDITID', 'required'),
                array('IMPORT, UNIQUEAUDITID, AUDITIDREQUIRED, ACTIVE, BILLIMMEDIATELY', 'numerical'),
                array('EMAILSUBJECT', 'length', 'max'=>25),
                array('EMAILBODY', 'length', 'max'=>500),
                array('EMAILCLOSE, EMAILFROM', 'length', 'max'=>100),
                array('URL', 'length', 'max'=>2000),
                array('SENDRECEIPTEMAIL, SENDSHIPMENTEMAIL', 'length', 'max'=>5),
                // The following rule is used by search().
                // Please remove those attributes that should not be searched.
                array('RECSOURCE, IMPORT, UNIQUEAUDITID, AUDITIDREQUIRED, ACTIVE, EMAILSUBJECT, EMAILBODY, EMAILCLOSE, EMAILFROM, URL, BILLIMMEDIATELY, SENDRECEIPTEMAIL, SENDSHIPMENTEMAIL', 'safe', 'on'=>'search'),
        );
    }
    //---------------------------------------------
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
                'iORDERs'=>array(self::HAS_MANY, 'IORDER', 'RECSOURCE'),
                'iPRODUCTs'=>array(self::HAS_MANY, 'IPRODUCT', 'ADMINGROUP'),
                'vDELIVERYMETHODDTLs'=>array(self::HAS_MANY, 'VDELIVERYMETHODDTL', 'RECSOURCE'),
                'vADMINGROUPs'=>array(self::HAS_MANY, 'VADMINGROUP', 'ADMINGROUP'),
                'iBATCHDATEs'=>array(self::HAS_MANY, 'IBATCHDATE', 'RECSOURCE'),
                'vPAYMENTMETHODSDTLs'=>array(self::HAS_MANY, 'VPAYMENTMETHODSDTL', 'RECSOURCE'),
                'vPAYEE'=>array(self::HAS_ONE, 'VPAYEE', 'PAYEE'),
                'iRIGHTSs'=>array(self::HAS_MANY, 'IRIGHTS', 'ENTITY'),
                'extra'=>array(self::HAS_ONE, 'RecSourceExtra', 'RECSOURCE'),
        );
    }
    //---------------------------------------------
    public function attributeLabels() {
        return array(
                'RECSOURCE'=>'Recsource',
                'IMPORT'=>'Import',
                'UNIQUEAUDITID'=>'Uniqueauditid',
                'AUDITIDREQUIRED'=>'Auditidrequired',
                'ACTIVE'=>'Active',
                'EMAILSUBJECT'=>'Emailsubject',
                'EMAILBODY'=>'Emailbody',
                'EMAILCLOSE'=>'Emailclose',
                'EMAILFROM'=>'Emailfrom',
                'URL'=>'Url',
                'BILLIMMEDIATELY'=>'Billimmediately',
                'SENDRECEIPTEMAIL'=>'Sendreceiptemail',
                'SENDSHIPMENTEMAIL'=>'Sendshipmentemail',
        );
    }
    //---------------------------------------------
    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('RECSOURCE', $this->RECSOURCE, true);
        $criteria->compare('IMPORT', $this->IMPORT);
        $criteria->compare('UNIQUEAUDITID', $this->UNIQUEAUDITID);
        $criteria->compare('AUDITIDREQUIRED', $this->AUDITIDREQUIRED);
        $criteria->compare('ACTIVE', $this->ACTIVE);
        $criteria->compare('EMAILSUBJECT', $this->EMAILSUBJECT, true);
        $criteria->compare('EMAILBODY', $this->EMAILBODY, true);
        $criteria->compare('EMAILCLOSE', $this->EMAILCLOSE, true);
        $criteria->compare('EMAILFROM', $this->EMAILFROM, true);
        $criteria->compare('URL', $this->URL, true);
        $criteria->compare('BILLIMMEDIATELY', $this->BILLIMMEDIATELY);
        $criteria->compare('SENDRECEIPTEMAIL', $this->SENDRECEIPTEMAIL, true);
        $criteria->compare('SENDSHIPMENTEMAIL', $this->SENDSHIPMENTEMAIL, true);

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
        ));
    }
}