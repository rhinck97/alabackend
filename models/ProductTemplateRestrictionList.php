<?php
require_once Yii::app()->basePath.'/lib/onyx/onyxCore.php';
require_once Yii::app()->basePath."/lib/onyx/onyxYii.php";
require_once Yii::app()->basePath.'/lib/pam/pamCore.php';
class ProductTemplateRestrictionList extends YActiveRecord {
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    //--------------------------------------
    public function tableName() {
        return 'JOB.IPRODTEMPLATERESTRICTIONLIST';
    }
    //--------------------------------------
    public function rules() {
        return array(
                array('PKEY, PRODTEMPLATERESTRICTIONID, PRODTEMPLATEDTLID', 'safe'),
        );
    }
    //--------------------------------------
    public function relations() {
        return array(
            'templateDtl'=>array(self::BELONGS_TO, 'ProductTemplateDtl', 'PKEY'),
            'templateRestriction'=>array(self::BELONGS_TO, 'ProductTemplateRestriction', 'PKEY'),
        );
    }
    //--------------------------------------
    public static function test($toss) {

    }
}
