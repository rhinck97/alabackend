<?php
/**
 * This is the model class for table "INPUTRESTRICTIONMAP".
 *
 * The followings are the available columns in table 'INPUTRESTRICTIONMAP':
 * @property string $SELECTIONROLE
 * @property string $SELECTIONVALUE
 * @property string $RESTRICTEDROLE
 * @property string $RESTRICTEDVALUE
 */
class InputRestrictionMap extends YActiveRecord {
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return InputRestrictionMap the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'JOB.INPUTRESTRICTIONMAP';
    }
    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
                // The following rule is used by search().
                // Please remove those attributes that should not be searched.
                array('SELECTIONROLE, SELECTIONVALUE, RESTRICTEDROLE, RESTRICTEDVALUE', 'safe', 'on'=>'search'),
        );
    }
    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'SELECTIONROLE'=>'Selectionrole',
                'SELECTIONVALUE'=>'Selectionvalue',
                'RESTRICTEDROLE'=>'Restrictedrole',
                'RESTRICTEDVALUE'=>'Restrictedvalue',
        );
    }
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('SELECTIONROLE', $this->SELECTIONROLE, true);
        $criteria->compare('SELECTIONVALUE', $this->SELECTIONVALUE, true);
        $criteria->compare('RESTRICTEDROLE', $this->RESTRICTEDROLE, true);
        $criteria->compare('RESTRICTEDVALUE', $this->RESTRICTEDVALUE, true);

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
        ));
    }
}