<?php
require_once Yii::app()->basePath.'/lib/onyx/onyxCore.php';
require_once Yii::app()->basePath."/lib/onyx/onyxYii.php";
require_once Yii::app()->basePath.'/lib/pam/pamCore.php';
class ProductTemplateRestriction extends YActiveRecord {
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    //--------------------------------------
    public function tableName() {
        return 'JOB.IPRODTEMPLATERESTRICTION';
    }
    //--------------------------------------
    public function rules() {
        return array(
                array('PKEY, PRODTEMPLATEID, TYPE', 'safe'),
        );
    }
    //--------------------------------------
    public function relations() {
        return array(
            'template'=>array(self::BELONGS_TO, 'ProductTemplate', 'PKEY'),
            'templateRestrictionLists'=>array(self::HAS_MANY, 'ProductTemplateRestrictionList', 'PRODTEMPLATERESTRICTIONID'),
        );
    }
    //--------------------------------------
    public static function test($toss) {

    }
}
