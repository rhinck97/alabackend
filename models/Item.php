<?php
class Item extends CActiveRecord {
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    //----------------------------
    public function tableName() {
        return 'BIL.VITEM';
    }
    //----------------------------
    public function rules() {
        return array(
          array('SUBCNT, PRICINGMETHOD', 'required'),
          array('SUBCNT', 'numerical', 'integerOnly'=>true),
          array('PRODUCTIONCOST', 'numerical'),
          array('EDESC', 'length', 'max'=>45),
          array('PRICINGMETHOD, DISPLAYGROUP', 'length', 'max'=>20),
          array('ITEMPART1, ITEMPART2', 'length', 'max'=>32),
          // The following rule is used by search().
          // Please remove those attributes that should not be searched.
          array('ITEMTYPE, ITEM, EDESC, SUBCNT, PRICINGMETHOD, PRODUCTIONCOST, ITEMPART1, ITEMPART2, DISPLAYGROUP', 'safe', 'on'=>'search'),
        );
    }
    //----------------------------
    public function relations() {
        return array(
        );
    }
    //----------------------------
    public function attributeLabels() {
        return array(
          'ITEMTYPE'=>'Itemtype',
          'ITEM'=>'Item',
          'EDESC'=>'Edesc',
          'SUBCNT'=>'Subcnt',
          'PRICINGMETHOD'=>'Pricingmethod',
          'PRODUCTIONCOST'=>'Productioncost',
          'ITEMPART1'=>'Itempart1',
          'ITEMPART2'=>'Itempart2',
          'DISPLAYGROUP'=>'Displaygroup',
        );
    }
    //----------------------------
    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('ITEMTYPE', $this->ITEMTYPE, true);
        $criteria->compare('ITEM', $this->ITEM, true);
        $criteria->compare('EDESC', $this->EDESC, true);
        $criteria->compare('SUBCNT', $this->SUBCNT);
        $criteria->compare('PRICINGMETHOD', $this->PRICINGMETHOD, true);
        $criteria->compare('PRODUCTIONCOST', $this->PRODUCTIONCOST);
        $criteria->compare('ITEMPART1', $this->ITEMPART1, true);
        $criteria->compare('ITEMPART2', $this->ITEMPART2, true);
        $criteria->compare('DISPLAYGROUP', $this->DISPLAYGROUP, true);

        return new CActiveDataProvider($this, array(
                  'criteria'=>$criteria,
                ));
    }
}