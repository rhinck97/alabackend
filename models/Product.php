<?php
require_once Yii::app()->basePath.'/lib/onyx/onyxCore.php';
require_once Yii::app()->basePath."/lib/onyx/onyxYii.php";
require_once Yii::app()->basePath.'/lib/pam/pamCore.php';
class Product extends YActiveRecord {
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    //--------------------------------------
    public function tableName() {//reviewed
        return 'JOB.IPRODUCT';
    }
    //--------------------------------------
    public function rules() {//reviewed
        return array(
                array('PRODUCTID, PRODUCTDESC, PRODUCTTYPE, DISPLAYNAME, VISUAL, SELLINGPRICE, '
                        .'PRODUCTVENDOR, PRODUCTOWNER, PRODUCTCASE, MAPKEY, OUNCESPERPRODUCT, SUBCOUNT, '
                        .'BANNER, APPROVALGROUP, LOGINMETHOD, SHIPPINGPRODUCTTYPE, TICKETLOCATIONS, '
                        .'COUNTPERBOX, PRODUCTCODE, PAYEE, ADMINGROUP, ORDERMIN, ORDERINCREMENT, PREVIEWFILE, '
                        .'TEMPLATECODE, VENDORSELLINGPRICE, LANGUAGE, LANGUAGEDESC', 'safe'),
        );
    }
    //--------------------------------------
    public function relations() {//reviewed
        return array(
                'productType'=>array(self::BELONGS_TO, 'ProductType', 'PRODUCTTYPE'),
                'productDtls'=>array(self::HAS_MANY, 'ProductDtl', 'PRODUCTID', 'order'=>'DISPLAYORDER ASC'),
                'parent'=>array(self::BELONGS_TO, 'ProductDtl', array('PRODUCTID'=>'INFO'), 'condition'=>"ITEM='ANOTHER PRODUCT'"),
                'template'=>array(self::BELONGS_TO, 'Product', array('TEMPLATECODE'=>'PRODUCTID')),
                'inputTemplate'=>array(self::BELONGS_TO, 'InputTemplate', array("TEMPLATECODE"=>"ID")),
        );
    }
    //--------------------------------------
    public static function test($toss) {//reviewed
        $testProduct = new Product();
        parent::test($testProduct);

        //Testing bad field type
        $testProduct = Product::model()->findByPk(1003890);
        $testProduct->PRODUCTTYPE = 'BADPRODUCTTYPE';
        try {
            $testProduct->save();
            errFailedToThrow();
        }
        catch(Exception $ex) {
            testErrWasThrown($ex, "error on save test");
        }

        $testProduct->PRODUCTID = '25';
        try {
            $testProduct->save();
            errFailedToThrow();
        }
        catch(Exception $ex) {
            testErrWasThrown($ex, "error on change product id");
        }

        try {
            $testProduct->delete();
            errFailedToThrow();
        }
        catch(Exception $ex) {
            testErrWasThrown($ex);
        }
    }
}