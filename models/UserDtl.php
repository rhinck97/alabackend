<?php
class UserDtl extends YActiveRecord {
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    //---------------------------------------------
    public function tableName() {
        return 'JOB.VUSERDTL';
    }
    //---------------------------------------------
    public function rules() {
        return array(
            array('USERNAME, RECSOURCE, ROLE, EXTRA', 'safe'),
        );
    }
    //---------------------------------------------
    public function relations() {
        return array(
          'user'=>array(self::BELONGS_TO, 'VUSER', 'USERNAME'),
        );
    }
}
