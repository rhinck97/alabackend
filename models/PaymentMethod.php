<?php
class PaymentMethod extends YActiveRecord {
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    //----------------------------------------------
    public function tableName() {
        return 'BIL.VPMTMETHODS';
    }
    //----------------------------------------------
    public function rules() {
        return array(
                array('EDESC', 'length', 'max'=>25),
                array('NEEDSINFO, CLOSEDBYBILLING', 'length', 'max'=>1),
                array('TYPE, COLLECTIONSYS', 'length', 'max'=>10),
                array('PMTMETHOD, EDESC, NEEDSINFO, TYPE, COLLECTIONSYS, CLOSEDBYBILLING', 'safe', 'on'=>'search'),
        );
    }
    //----------------------------------------------
    public function relations() {
        return array(
                'order'=>array(self::HAS_MANY, 'Order', 'PAYMENTMETHOD'),
                'paymentMethodsDtl'=>array(self::HAS_MANY, 'PaymentMethodDtl', 'PMTMETHOD'),
        );
    }
}