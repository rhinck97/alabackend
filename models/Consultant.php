<?php
class Consultant extends YActiveRecord {
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    //--------------------------------------------
    public function tableName() {
        return 'JOB.ICONSULTANT';
    }
    //--------------------------------------------
    public function rules() {
        return array(
                array('DISPLAYORDER, EMAIL, IMAGE, IMAGE, NAME, PHONE, ', 'safe'),
        );
    }
    //--------------------------------------------
}