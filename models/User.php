<?php
class User extends YActiveRecord {
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    //---------------------------------------------
    public function tableName() {
        return 'JOB.VUSER';
    }
    //---------------------------------------------
    public function rules() {
        return array(
            array('USERNAME, FIRSTNAME, LASTNAME, EMAIL', 'safe'),
        );
    }
    //---------------------------------------------
    public function relations() {
        return array(
          'userDtls'=>array(self::HAS_MANY, 'UserDtl', 'USERNAME'),
        );
    }
}
