<?php
class Element extends YActiveRecord {
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    public function tableName() {
        return 'JOB.VELEMENT';
    }
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
                array('DISPLAYNAME', 'length', 'max'=>100),
                // The following rule is used by search().
                // Please remove those attributes that should not be searched.
                array('ELEMENT, DISPLAYNAME', 'safe', 'on'=>'search'),
        );
    }
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
//			'vELEMENTDTL' => array(self::HAS_ONE, 'VELEMENTDTL', 'ELEMENT'),
//			'vELEMENTMAPs' => array(self::HAS_MANY, 'VELEMENTMAP', 'ITEM'),
//			'vELEMENTMAPs1' => array(self::HAS_MANY, 'VELEMENTMAP', 'TRAIT'),
//			'vELEMENTMAPs2' => array(self::HAS_MANY, 'VELEMENTMAP', 'EXTRATRAIT'),
//			'vELEMENTTAGs' => array(self::HAS_MANY, 'VELEMENTTAG', 'ELEMENT'),
        );
    }
}