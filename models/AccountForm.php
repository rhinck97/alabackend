<?php
class AccountForm extends CFormModel {
    public $firstName;
    public $lastName;
    public $email;
    public $phone;
    public $password;
    public $confirmPassword;
    //--------------------------------------
    public function rules() {
        return array(
                array('firstName, lastName, email, phone, password, confirmPassword', 'required'),
                array('email', 'email'),
                array('email', 'unique'),
                array('password, confirmPassword', 'confirmPassword')
        );
    }
    //--------------------------------------
    public function unique($attribute, $params) {
        if(Account::model()->findByPk($this->$attribute) != null) {
            $this->addError($attribute, 'This email address is already being used by another account.');
        }
        return !$this->hasErrors();
    }
    //--------------------------------------
    public function confirmPassword($attribute, $params) {
        if($this->$attribute != $this->confirmPassword) {
            $this->addError($attribute, 'The two supplied passwords do not match.');
        }
        return !$this->hasErrors();
    }
    //--------------------------------------
    public static function hashPassword($password) {
        return hash('sha256', $password);
    }
}
?>