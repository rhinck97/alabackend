<?php
class WebShepherdView extends YActiveRecord {
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    //---------------------------------------------
    public function primaryKey() {
        return array('LOGINID', 'STATUS');
    }
    //---------------------------------------------
    public function tableName() {
        return 'JOB.WEBSHEPHERDVIEW';
    }
    //---------------------------------------------
    public function rules() {
        return array(
                array('LOGINID, STATUS', 'safe'),
        );
    }
}