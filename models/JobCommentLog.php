<?php
/**
 * This is the model class for table "JOBCOMMENTLOG".
 *
 * The followings are the available columns in table 'JOBCOMMENTLOG':
 * @property double $ID
 * @property double $JOBID
 * @property string $CONTENT
 * @property string $AUTHOR
 * @property string $RECTIME
 * @property string $MODTIME
 *
 * The followings are the available model relations:
 * @property IJOB $jOB
 */
class JobCommentLog extends YActiveRecord {
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return JobCommentLog the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'JOB.JOBCOMMENTLOG';
    }
    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
                array('JOBID, CONTENT, AUTHOR', 'required'),
                array('JOBID', 'numerical'),
                array('CONTENT', 'length', 'max'=>2000),
                array('AUTHOR', 'length', 'max'=>40),
                array('MODTIME,RECTIME', 'unsafe'),
                // The following rule is used by search().
                // Please remove those attributes that should not be searched.
                array('ID, JOBID, CONTENT, AUTHOR, RECTIME, MODTIME', 'safe', 'on'=>'search'),
        );
    }
    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
                'job'=>array(self::BELONGS_TO, 'Job', 'JOBID'),
        );
    }
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'ID'=>'ID',
                'JOBID'=>'Jobid',
                'CONTENT'=>'Content',
                'AUTHOR'=>'Author',
                'RECTIME'=>'Rectime',
                'MODTIME'=>'Modtime',
        );
    }
    public function getDisplayAttributes() {
        $dispAttr = $this->getAttributes();
        $dispAttr['RECTIME'] = date(DATE_ISO8601, strtotime($dispAttr['RECTIME']));
        $dispAttr['MODTIME'] = (is_null($dispAttr['MODTIME'])) ? null : date(DATE_ISO8601, strtotime($dispAttr['MODTIME']));
        return $dispAttr;
    }
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID);
        $criteria->compare('JOBID', $this->JOBID);
        $criteria->compare('CONTENT', $this->CONTENT, true);
        $criteria->compare('AUTHOR', $this->AUTHOR, true);
        $criteria->compare('RECTIME', $this->RECTIME, true);
        $criteria->compare('MODTIME', $this->MODTIME, true);

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
        ));
    }
}