<?php
class PaymentMethodDtl extends YActiveRecord {
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    //----------------------------------------------
    public function tableName() {
        return 'JOB.VPAYMENTMETHODSDTL';
    }
    //----------------------------------------------
    public function primaryKey() {
        return "PMTMETHOD";
    }
    //----------------------------------------------
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
                array('PMTMETHOD, DISPLAYORDER', 'required'),
                array('DISPLAYORDER', 'numerical'),
                array('PMTMETHOD', 'length', 'max'=>10),
                array('RECSOURCE', 'length', 'max'=>25),
                // The following rule is used by search().
                // Please remove those attributes that should not be searched.
                array('PMTMETHOD, RECSOURCE, DISPLAYORDER', 'safe', 'on'=>'search'),
        );
    }
    //----------------------------------------------
    public function relations() {
        return array(
                'paymentMethod'=>array(self::BELONGS_TO, 'PaymentMethod', 'PMTMETHOD'),
                'recSource'=>array(self::BELONGS_TO, 'VRECSOURCE', 'RECSOURCE'),
        );
    }
    //----------------------------------------------
    public function attributeLabels() {
        return array(
                'PMTMETHOD'=>'Pmtmethod',
                'RECSOURCE'=>'Recsource',
                'DISPLAYORDER'=>'Displayorder',
        );
    }
    //----------------------------------------------
    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('PMTMETHOD', $this->PMTMETHOD, true);
        $criteria->compare('RECSOURCE', $this->RECSOURCE, true);
        $criteria->compare('DISPLAYORDER', $this->DISPLAYORDER);

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
        ));
    }
    public function getAltAttributes() {
        $deliveryMethodArray = $this->getAttributes();
        $resultArray = array();
        $altKeyMap = array(
                'PMTMETHOD'=>'paymentMethod',
                'RECSOURCE'=>'recSource',
//            'DISPLAYORDER'=>'Displayorder',
        );
        foreach($deliveryMethodArray as $key=> $value) {
            if(array_key_exists($key, $altKeyMap)) {
                $resultArray[$altKeyMap[$key]] = $value;
            }
        }
        return $resultArray;
    }
}