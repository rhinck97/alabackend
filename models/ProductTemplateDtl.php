<?php
require_once Yii::app()->basePath.'/lib/onyx/onyxCore.php';
require_once Yii::app()->basePath."/lib/onyx/onyxYii.php";
require_once Yii::app()->basePath.'/lib/pam/pamCore.php';
class ProductTemplateDtl extends YActiveRecord {
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    //--------------------------------------
    public function tableName() {
        return 'JOB.IPRODTEMPLATEDTL';
    }
    //--------------------------------------
    public function rules() {
        return array(
                array('PRODTEMPLATEDTLID, PRODTEMPLATEID, PRODUCTOPTION, VALUE, '
                        .'EXTRA1, EXTRA2, SORT, VISUAL', 'safe'),
        );
    }
    //--------------------------------------
    public function relations() {
        return array(
                'template'=>array(self::BELONGS_TO, 'ProductTemplate', 'PRODTEMPLATEDTLID'),
                'templateRestrictionLists'=>array(self::HAS_MANY, 'ProductTemplateRestrictionList', 'PRODTEMPLATEDTLID'),
        );
    }
    //--------------------------------------
    public static function test($toss) {

    }
}
