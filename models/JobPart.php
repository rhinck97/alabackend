<?php
require_once Yii::app()->basePath."/lib/onyx/onyxCore.php";
require_once Yii::app()->basePath."/lib/onyx/onyxYii.php";
class JobPart extends YActiveRecord {
    //------------------
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    //------------------
    public function tableName() {//
        return 'JOB.IJOBPART';
    }
    //------------------
    public function relations() {//
        return array(
                'job'=>array(self::BELONGS_TO, 'Job', 'JOBID'),
                'product'=>array(self::BELONGS_TO, 'Product', 'PRODUCTID')
        );
    }
}