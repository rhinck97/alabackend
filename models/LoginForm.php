<?php
class LoginForm extends CFormModel {
    public $username;
    public $password;
    public $rememberMe;
    private $_identity;
    //----------------------------------------------
    public function rules() {
        return array(
                array('username, password', 'required'),
                array('rememberMe', 'boolean'),
                array('password', 'authenticate'),
        );
    }
    //----------------------------------------------
    public function attributeLabels() {
        return array(
                'rememberMe'=>'Remember me next time',
        );
    }
    //----------------------------------------------
    public function authenticate($attribute, $params) {
        if(!$this->hasErrors()) {
            $this->_identity = new YUserIdentity($this->username, $this->password);
            if(!$this->_identity->authenticate()) {
                $this->addError('username', $this->_identity->errorMessage);
            }
        }
    }
    //----------------------------------------------
    public function login() {
        if($this->_identity === null) {
            $this->_identity = new YUserIdentity($this->username, $this->password);
            $this->_identity->authenticate();
        }
        if($this->_identity->errorCode === YUserIdentity::ERROR_NONE) {
            $duration = $this->rememberMe ? 60 * 60 * 24 * 30 : 0; // 30 days
            Yii::app()->user->login($this->_identity, $duration);
            return true;
        }
        else {
            $this->addError('username', 'Incorrect username or password.');
            return false;
        }
    }
}