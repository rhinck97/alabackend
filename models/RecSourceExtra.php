<?php
class RecSourceExtra extends YActiveRecord {
    //---------------------------------------------
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    //---------------------------------------------
    public function tableName() {
        return 'JOB.VRECSOURCEEXTRA';
    }
    //---------------------------------------------
    public function rules() {
        return array(
                array('SHOWCARTLIST, SHOWCONTINUE, SHIPPINGMULTIPLIER', 'required'),
                array('SHOWCARTLIST, SHIPPINGMULTIPLIER', 'numerical'),
                array('PRODUCTROOT, VENDORID, DEFAULTADDRESSTYPE', 'length', 'max'=>20),
                array('DISPLAYNAME', 'length', 'max'=>400),
                array('LOGINSECURITY', 'length', 'max'=>15),
                array('SHOWCONTINUE, SHOWPRINTABLE', 'length', 'max'=>50),
                array('BANNER', 'length', 'max'=>200),
                array('LOGO, RESOURCEPATH', 'length', 'max'=>100),
                array('SENDINGEMAIL', 'length', 'max'=>1),
                array('SHIPPINGBILLED', 'RECSOURCE, SHOWCARTLIST, PRODUCTROOT, DISPLAYNAME, VENDORID, SHOWCONTINUE, BANNER, SHOWPRINTABLE, SHIPPINGMULTIPLIER, LOGO, RESOURCEPATH, DEFAULTADDRESSTYPE, SENDINGEMAIL, LOGINSECURITY', 'safe', 'on'=>'search'),
        );
    }
    //---------------------------------------------
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
                'vENDOR'=>array(self::BELONGS_TO, 'IVENDOR', 'VENDORID'),
                'root'=>array(self::HAS_ONE, 'Product', array("PRODUCTID"=>"PRODUCTROOT")),
        );
    }
    //---------------------------------------------
    public function attributeLabels() {
        return array(
                'RECSOURCE'=>'Recsource',
                'SHOWCARTLIST'=>'Showcartlist',
                'PRODUCTROOT'=>'Productroot',
                'DISPLAYNAME'=>'Displayname',
                'VENDORID'=>'Vendorid',
                'SHOWCONTINUE'=>'Showcontinue',
                'BANNER'=>'Banner',
                'SHOWPRINTABLE'=>'Showprintable',
                'SHIPPINGMULTIPLIER'=>'Shippingmultiplier',
                'LOGO'=>'Logo',
                'RESOURCEPATH'=>'Resourcepath',
                'DEFAULTADDRESSTYPE'=>'Defaultaddresstype',
                'SENDINGEMAIL'=>'Sendingemail',
                'LOGINSECURITY'=>'LoginSecurity',
        );
    }
    //---------------------------------------------
    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('RECSOURCE', $this->RECSOURCE, true);
        $criteria->compare('SHOWCARTLIST', $this->SHOWCARTLIST);
        $criteria->compare('PRODUCTROOT', $this->PRODUCTROOT, true);
        $criteria->compare('DISPLAYNAME', $this->DISPLAYNAME, true);
        $criteria->compare('VENDORID', $this->VENDORID, true);
        $criteria->compare('SHOWCONTINUE', $this->SHOWCONTINUE, true);
        $criteria->compare('BANNER', $this->BANNER, true);
        $criteria->compare('SHOWPRINTABLE', $this->SHOWPRINTABLE, true);
        $criteria->compare('SHIPPINGMULTIPLIER', $this->SHIPPINGMULTIPLIER);
        $criteria->compare('LOGO', $this->LOGO, true);
        $criteria->compare('RESOURCEPATH', $this->RESOURCEPATH, true);
        $criteria->compare('DEFAULTADDRESSTYPE', $this->DEFAULTADDRESSTYPE, true);
        $criteria->compare('SENDINGEMAIL', $this->SENDINGEMAIL, true);
        $criteria->compare('LOGINSECURITY', $this->LOGINSECURITY, true);

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
        ));
    }
    //---------------------------------------------
    public function getAltAttributes() {
        $recSourceExtraArray = $this->getAttributes();
        $resultArray = array();
        $altKeyMap = array(
                'RECSOURCE'=>'recSource',
//      'SHOWCARTLIST'=>'Showcartlist',
                'PRODUCTROOT'=>'productRoot',
                'DISPLAYNAME'=>'displayname',
//      'VENDORID'=>'Vendorid',
//      'SHOWCONTINUE'=>'Showcontinue',
                'BANNER'=>'banner',
//      'SHOWPRINTABLE'=>'Showprintable',
//      'SHIPPINGMULTIPLIER'=>'Shippingmultiplier',
                'LOGO'=>'logo',
                'RESOURCEPATH'=>'resourcePath',
//      'DEFAULTADDRESSTYPE'=>'Defaultaddresstype',
//      'SENDINGEMAIL'=>'Sendingemail',
        );
        foreach($recSourceExtraArray as $key=> $value) {
            if(array_key_exists($key, $altKeyMap)) {
                $resultArray[$altKeyMap[$key]] = $value;
            }
        }
        return $resultArray;
    }
}