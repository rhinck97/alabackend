<?php
class VProductTypeExtra extends YActiveRecord {
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    //----------------------------------------------
    public function tableName() {
        return 'JOB.VPRODCUTTYPEEXTRA';
    }
    //----------------------------------------------
    public function primaryKey() {
        /*
         * Primary key is PRODUCTTYPE, ITEM, TRAIT, EXTRATRAIT, JOBSTATUS, ROLE
         */
        return array("PRODUCTTYPE", "ITEM", "TRAIT", "EXTRATRAIT", "JOBSTATUS", "ROLE");
    }
    //----------------------------------------------
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
                array('PRODUCTTYPE, ITEM, TRAIT, EXTRATRAIT, JOBSTATUS, ROLE', 'required'),
                array('SOURCE, DEST', 'safe'),
                array('SOURCE, DEST, ITEM, TRAIT, EXTRATRAIT, JOBSTATUS, ROLE', 'safe', 'on'=>'search'),
        );
    }
    //----------------------------------------------
    public function relations() {
        //These are for setting up foreign keys. Currently do not need them because I won't be creating them
        return array(
                /* 'paymentMethod'=>array(self::BELONGS_TO, 'PaymentMethod', 'PMTMETHOD'),
                  'recSource'=>array(self::BELONGS_TO, 'VRECSOURCE', 'RECSOURCE'), */
        );
    }
    //----------------------------------------------
    public function attributeLabels() {
        return array(
                'SOURCE'=>'Source',
                'DEST'=>'Dest',
                'PRODUCTTYPE'=>'ProductType',
                'ITEM'=>'Item',
                'TRAIT'=>'Trait',
                'EXTRATRAIT'=>'ExtraTrait',
                'JOBSTATUS'=>'JobStatus',
                'ROLE'=>'Role'
        );
    }
}