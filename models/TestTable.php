<?php
//-------------------------------------------
class TestTable extends YActiveRecord {
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    //----------------------------------------------
    public function tableName() {
        return 'JOB.TESTTABLE';
    }
    //----------------------------------------------
    public function rules() {
        return array(
                array('DISPLAYNAME, DISPLAYNAME2 ', 'safe'),
        );
    }
}