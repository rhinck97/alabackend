<?php
class OrderStatus extends YActiveRecord {
    //-----------------------------------------------------
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    //-----------------------------------------------------
    public function tableName() {
        return 'JOB.VORDERSTATUS';
    }
    //-----------------------------------------------------
    public function rules() {
        return array(
                array('BILLABLE, INPRODUCTION', 'length', 'max'=>1),
                array('DISPLAYNAME', 'length', 'max'=>20),
        );
    }
}