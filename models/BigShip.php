<?php
require_once Yii::app()->basePath."/lib/onyx/onyxCore.php";
require_once Yii::app()->basePath."/lib/onyx/onyxYii.php";
class BigShip extends YActiveRecord {
    //------------------
    public static function model($className = __CLASS__) {//
        return parent::model($className);
    }
    //------------------
    public function tableName() {//
        return 'JOB.BIGSHIP';
    }
    //------------------
    public function relations() {//
        return array();
    }
}