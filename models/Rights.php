<?php
class Rights extends YActiveRecord {
    //---------------------------------------------
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    //---------------------------------------------
    public function tableName() {
        return 'JOB.IRIGHTS';
    }
    //---------------------------------------------
    public function rules() {
        return array(
                array('USERNAME, ENTITY, ACTION', 'length', 'max'=>20),
                array('USERNAME, ENTITY, ACTION', 'safe', 'on'=>'search'),
        );
    }
    //---------------------------------------------
    public function relations() {
        return array(
                'User'=>array(self::BELONGS_TO, 'IUSER', 'USERNAME'),
                'Entity'=>array(self::BELONGS_TO, 'VRECSOURCE', 'ENTITY'),
        );
    }
    //---------------------------------------------
    public function attributeLabels() {
        return array(
                'USERNAME'=>'Username',
                'ENTITY'=>'Entity',
                'ACTION'=>'Action',
        );
    }
    //---------------------------------------------
    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('USERNAME', $this->USERNAME, true);
        $criteria->compare('ENTITY', $this->ENTITY, true);
        $criteria->compare('ACTION', $this->ACTION, true);

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
        ));
    }
}