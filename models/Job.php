<?php
require_once Yii::app()->basePath."/lib/onyx/onyxCore.php";
require_once Yii::app()->basePath."/lib/onyx/onyxYii.php";
class Job extends YActiveRecord {
    //------------------
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    //------------------
    public function tableName() {//
        return 'JOB.IJOB';
    }
    //------------------
    public function handleJobCharge() {//
        if(strToBool($this->productType->INPRODUCTTABLE)) {
            $productId = $this->PRODUCTID;
            $productCount = $this->PRODUCTCOUNT;
            $sql = "select job.core.calcProductCost($productId, $productCount) from dual";
            $this->CHARGE = queryScalar($sql);
        }
    }
    //------------------
    //before save should handle call logic to resolve values for this table
    //but should not call methods in other tables. Such calls should be in submit.
    //todo: this needs to calc the charges
    protected function beforeSave() {//
        parent::beforeSave();//result is always true else exception
        if(is_null($this->PRODUCTTYPE)) {
            $this->PRODUCTTYPE = $this->product->PRODUCTTYPE;//fix for indstudy
        }
        if($this->ACCESSCODE == null) {
            $this->ACCESSCODE = rand(0, 99999);
        }
        //check if changing number to less than currently trying to be shipped(essentially free shipping)
        $jobId = $this->JOBID;
        $shipCount = 0;
        if(!is_null($jobId)) {
            $shipCount = queryScalar("select sum(productcount) from ijobShipmentMap where jobid = $jobId");
        }
        errIf($shipCount > $this->PRODUCTCOUNT, 'The shipping quantity is greater than the cart quantity. '.
                                                'Go to the checkout and reduce the amount shipped');
        $this->handleRoutedId();
        $this->handleApprovalProcess();
        $this->handleJobCharge();
		$this->handleStatus();

        return true;//if there is a problem an error will be thrown instead of a false
        //returning true is sill important for other functions in the yii library to see however
    }
    //------------------
    //Submit will save the current table and call save on tables who's values have
    //a dependency on this table. Submit is responsible to call save for all
    //cascading dependencies. In other words a sumbit method should never call
    //submit methods in other models only save methods
    public function submit() {
        $this->save();
        $this->order->save();
        return true;
    }
    //------------------
    protected function beforeDelete() {//
        parent::beforeDelete();
        $statusLogs = $this->statusLogs();
        foreach($statusLogs as $statusLog) {
            $statusLog->delete();
        }
        $shipmentMaps = $this->jobShipmentMaps();
        foreach($shipmentMaps as $shipmentMap) {
            $shipmentMap->delete();
        }
        return true;
    }
    //------------------
    protected function afterDelete() {//
        $result = parent::afterDelete();
        $this->order->save();
        return $result;
    }
    //------------------
    public function handleStatus() {
        if($this->order->ORDERSTATUS === 'OPEN' && $this->STATUS === 'PENDING') {
            $this->incrementJobStatus('jobModel', 'PENDING');
        }
    }
    //------------------
    public function incrementJobStatus($username = null, $whatTheCurrentStatusShouldBe = null) {
        if($whatTheCurrentStatusShouldBe != null) {
            verify($this->STATUS == $whatTheCurrentStatusShouldBe, 
                    "Status mismatch. Current status $this->STATUS but expecting $whatTheCurrentStatusShouldBe");
        }
        
        $productType = ProductType::model()->find("PRODUCTTYPE = '$this->PRODUCTTYPE'");
        $workflowName = $productType->WORKFLOW;
        if($workflowName == null) {
            $workflowName = $this->PRODUCTTYPE;
        }
        $workflow = Workflow::model()->find("PRODUCTTYPE = '$workflowName' AND CURRENTSTATUS = '$this->STATUS' and part = 'F'");
        errIfNull($workflow, "The status '$this->STATUS' is not found in workflow '$workflowName' for this job");

        $this->STATUS = $workflow->NEXTSTATUS;
		$this->PARAM = "USERNAME=$username";
    }
    //------------------
    private function handleApprovalProcess() {//resolve value without app input
        $approvalStatus = $this->APPROVALSTATUS;
        if($approvalStatus == null) {
            $approvalGroup = $this->product->APPROVALGROUP;
            if($approvalGroup == null) {
                $this->APPROVALSTATUS = 'NA';
            }
            else {
                $this->APPROVALSTATUS = 'PENDING';
            }
        }
    }
    //------------------
    private function handleRoutedId() {//
        $needsRoutedId = (bool) $this->productType->NEEDSROUTEDID;
        if($needsRoutedId && $this->ROUTEDID == null) {
            $prefix = Yii::app()->params['jobPrefix'];
            $routedId = queryScalar("SELECT JOB.IAVANTIXJOBNOSEQ.NEXTVAL FROM DUAL");
            $routedId = $prefix.str_pad($routedId, 6, '0', STR_PAD_LEFT);
            $this->ROUTEDID = $routedId;
        }
    }
    //------------------
    public function rules() {//
        return array(
                array('LINK, PRODUCTCOUNT, PRODUCTID, CHARGE, ACCESSCODE, JOBID', 'numerical'),
                array('ACCESSCODE, APPROVALSTATUS, CHARGE, CURRENTLOC, JOBID, JOBNOTES, '.
                        'LINK, PRODUCTCOUNT, PRODUCTID, PRODUCTTYPE, ROUTEDID, '.
                        'SERVICELOC, STATUS', 'safe'),
        );
    }
    //------------------
    public function relations() {//
        return array(
                'order'=>array(self::BELONGS_TO, 'Order', 'LINK'),
                'product'=>array(self::BELONGS_TO, 'Product', 'PRODUCTID'),
                'shipments'=>array(self::MANY_MANY, 'Shipment', 'IJOBSHIPMENTMAP(JOBID, SHIPMENTPKEY)'),
                'jobShipmentMaps'=>array(self::HAS_MANY, 'JobShipmentMap', 'JOBID'),
                'statusLogs'=>array(self::HAS_MANY, 'StatusLog', 'JOBID'),
                'productType'=>array(self::BELONGS_TO, 'ProductType', "PRODUCTTYPE"),
        );
    }
    //------------------
    public static function test($toss) {
        $testJob = new Job();
        parent::test($testJob);
        $testJobId = 658769;
        $testJob = Job::model()->findByPk($testJobId);
        $testJob->STATUS = 'BADSTATUS';
        try {
            $testJob->save();
            errFailedToThrow();
        }
        catch(Exception $ex) {
            testErrWasThrown($ex, "error on save test");
        }

        $testJob = Job::model()->findByPk($testJobId);
        $testJob->ACCESSCODE = '';
        $testJob->save();
        $testJob->refresh();
        testTrue($testJob->ACCESSCODE != '', $testJob->ACCESSCODE." access code assigned test");

        $testJob->ROUTEDID = '';
        $testJob->save();
        $testJob->refresh();
        testTrue($testJob->ROUTEDID != '', $testJob->ROUTEDID.' ROUTEDID code assigned test');

        $testJob->LINK = '5';
        try {
            $testJob->save();
            errFailedToThrow();
        }
        catch(Exception $ex) {
            testErrWasThrown($ex, "error on change of link number");
        }
    }
}