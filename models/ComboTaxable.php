<?php
class ComboTaxable extends YActiveRecord {
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    //----------------------------------------------
    public function tableName() {
        return 'BIL.VCOMBOTAXABLE';
    }
    //----------------------------------------------
    public function rules() {
        return array(
                array('CHARGETAX', 'required'),
                array('CHARGETAX', 'numerical', 'integerOnly'=>true),
                array('PMTMETHOD, CHARGETAX, TAXSTATUS', 'safe', 'on'=>'search'),
        );
    }
    //----------------------------------------------
    public function relations() {
        return array(
        );
    }
    //----------------------------------------------
    public function attributeLabels() {
        return array(
                'PMTMETHOD'=>'Pmtmethod',
                'CHARGETAX'=>'Chargetax',
                'TAXSTATUS'=>'Taxstatus',
        );
    }
    //----------------------------------------------
    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('PMTMETHOD', $this->PMTMETHOD, true);
        $criteria->compare('CHARGETAX', $this->CHARGETAX);
        $criteria->compare('TAXSTATUS', $this->TAXSTATUS, true);

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
        ));
    }
}