<?php
require_once Yii::app()->basePath.'/lib/onyx/onyxCore.php';
require_once Yii::app()->basePath."/lib/onyx/onyxYii.php";
class Order extends YActiveRecord {
    //------------------
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    //------------------
    public function tableName() {
        return 'JOB.IORDER';
    }
    //------------------
    public function rules() {
        return array(
                array('CONTACTADDRESS1, CONTACTADDRESS2, CONTACTADDRESS3, CONTACTADDRESS4, '.
                        'CONTACTCITY, CONTACTCOUNTRY, CONTACTEMAIL, CONTACTFIRSTNAME, '.
                        'CONTACTLASTNAME, CONTACTNAME, CONTACTPHONE, CONTACTPOSTALCODE, '.
                        'CONTACTSTATE, LOGINID, ORDERNOTES, ORDERSTATUS, PAYMENTINFO, '.
                        'PAYMENTMETHOD, PAYMENTSTATUS, PO, RECSOURCE, SHIPPINGCHARGE,'.
                        'STEWARDSHIPLOC, JOBSCHARGE, SHIPPINGMETHOD, TAX, TOTALTAXABLECHARGES, '.
                        'GRANDTOTAL', 'safe'),
        );
    }
    //------------------
    public function relations() {
        return array(
                'jobs'=>array(self::HAS_MANY, 'Job', 'LINK'),
                'jobsByStatus'=>array(self::HAS_MANY, 'Job', 'LINK', "condition"=>"status in (:stati)"),
                'shipments'=>array(self::HAS_MANY, 'Shipment', 'LINK'),
                'coupon'=>array(self::BELONGS_TO, 'Coupon', 'COUPONCODE'),
                'recSource'=>array(self::BELONGS_TO, 'RecSource', 'RECSOURCE'),
                'recSourceExtra'=>array(self::BELONGS_TO, 'RecSourceExtra', 'RECSOURCE'),
                'orderStatusLogs'=>array(self::HAS_MANY, 'OrderStatusLog', 'LINK'),
                'paymentMethod'=>array(self::BELONGS_TO, 'PaymentMethod', 'PAYMENTMETHOD'),
                'customer'=>array(self::BELONGS_TO, 'Customer', 'CUSTID'),
                'deliveryMethodDtls'=>array(self::HAS_MANY, 'DeliveryMethodDtls', 'RECSOURCE'),
        );
    }
    //===================================================================
    //The beforeSave and Submit functions have the following rules
    //
    //BeforeSave should NEVER call SAVE on any of the other tables.
    //It may read data from other tables however.
    //
    //Submit should NEVER call SUBMIT on any of the other tabels.
    //It may read data from other tables and call SAVE on the other tables however.
    //
    //The reason for this is so saving behavior can be predicted.
    //If a save on table A can call a save on table B and a save on B can call a save on table A then
    //it would do a mutual recursion. So a save should not call another save.
    //A submit calling a submit is the same problem.
    //A submit calling a save on a second table is safe because the save will not call a save or
    //submit on the first table
    //
    //The Submit function then becomes responsible for its data and updates to itself after calls to saves on
    //other tables. It is also responsible to save its data that other tables may need before calling
    //save on those tables
    //===================================================================
    //Use beforeSave to do data validations and calculations on fields values without
    //changing values in other tables. If updates to other tables are needed do it in the submit function
    //===================================================================
    public function beforeSave() {
        parent::beforeSave();
        $this->handlePaymentStatus();
		$this->valOrderStatus();
        $this->MULTISHIP = "T"; //all things should be true in modern yii apps some old non yii apps are false
        $this->SHIPPINGBILLED = $this->recSourceExtra->SHIPPINGBILLED; //trigger could do this but keep here
        errIf($this->SHIPPINGBILLED == null, 'SHIPPINGBILLED value missing in recSourceExtra');
        $this->BUNDLESHIPMENTS = $this->recSourceExtra->BUNDLESHIPMENTS; //trigger could do this but keep here
        $this->LOGINID = Yii::app()->user->getState("loginId");
        $this->handleCustIdAndPaymentInfo();
        $this->handlePaymentStatus();
        $this->handleChargesAndTax();
        return true;
    }
    //============================================================================================
    //use submit to save data to the table where updates to other tables may be needed.
    //============================================================================================
    public function submit() {
fileLog('enter');
		errIf($this->ORDERSTATUS === 'OPEN', 'Open orders may not be modified');
        $this->save(); //
        $this->refresh();
		errIf($this->LINK == null, 'Link number missing');
fileLog('link '.$this->LINK);

fileLog('handle opening '.$this->ORDERSTATUS);
        $this->handleOpening();
        $this->save(); //
        if($this->ORDERSTATUS == 'OPEN') {
fileLog('order opened');
            $this->handleSplitShipments();
fileLog('save jobs to advance status');
			$jobs = $this->jobs;
			foreach($jobs as $job) {
				$job->save();
			}
fileLog('save shipments to create RTS');
			$shipments = $this->shipments;
			foreach($shipments as $shipment) {
				$shipment->afterOrderOpened();
			}
fileLog('expire coupon');
			if(!isNullorEmptyStr($this->COUPONCODE)) {
				$coupon = $this->coupon;
                $yesterday = time() - (24 * 60 * 60);
                $yesterdayStr = date("d-M-y", $yesterday);
                $yesterdayStr = strtoupper($yesterdayStr);
                $coupon->EXPIREDATE = $yesterdayStr;
                $coupon->save();
            }
        }
        $this->save(); //
    }
    //===================================================================
    public function beforeDelete() {//reviewed
        $deleteResult = parent::beforeDelete();
        $orderStatusLogs = $this->orderStatusLogs();
        foreach($orderStatusLogs as $orderStatusLog) {
            $orderStatusLog->delete();
        }
        return $deleteResult;
    }
    //============================================================================================
    public function valOrderStatus() {
        if($this->ORDERSTATUS == 'PLACED' || $this->ORDERSTATUS == 'OPEN') {
            errIf($this->PAYMENTMETHOD == 'MISSING', 'Payment method is missing');
			errIf($this->PAYMENTSTATUS == 'PENDING' && $this->PAYMENTMETHOD !== 'CASHNET',
					$this->PAYMENTMETHOD.' may not be pending');
			errIf($this->PAYMENTMETHOD == 'CASHNET' && $this->PAYMENTSTATUS !== 'PENDING' && $this->PAYMENTSTATUS !== 'AUTHORIZED',
					"CASHNET may not be $this->PAYMENTSTATUS");
			$jobWithShipmentCount = queryScalar("select count(*) from ijob where".
													" jobid in(select jobid from ijobshipmentmap) and ".
													"link = $this->LINK");
			$jobCount = queryScalar("select count(*) from ijob where link = $this->LINK");
			errIf($jobWithShipmentCount !== $jobCount, 'not all items assigned to an address');
			
			$jobShipmentMapProductCount = queryScalar("select sum(productcount) from ijobshipmentmap where ".
														"jobid in(select jobid from ijob where link = $this->LINK)");
			$orderProductCount = queryScalar("select sum(productcount) from ijob where link = $this->LINK");
			errIf($jobShipmentMapProductCount !== $orderProductCount, 'not all products assigned to an address');
			//does the delivery status say 'PENDING'??!!!
			//if it needs an RTS number does it have an RTS number
        }
    }
    //============================================================================================
    public function handleOpening() {//reviewed
        errIf($this->LINK == null, 'Link number missing');
        if($this->ORDERSTATUS == 'PLACED') {
            $this->valOrderStatus();
            $jobsPendingApprovalCount = queryScalar("select count(link) from job.ijob where ".
                    "link = $this->LINK and approvalStatus = 'PENDING'");
            if(($this->PAYMENTSTATUS !== 'PENDING') && ($jobsPendingApprovalCount == 0)) {
                $this->ORDERSTATUS = 'OPEN';
            }
        }
        //Testing database errors
        //$jobsPendingApprovalCount = queryScalar("select count(link) from job.ijob where ".
        //"link = $this->LINK and approvalStatus = 'fred'");
    }
    //============================================================================================
    public function logOrderStatusEvent($orderStatus) {//reviewed should we make an emailstatus field?
        $orderStatusLog = new OrderStatusLog;
        $orderStatusLog->LINK = $this->LINK;
        $orderStatusLog->ORDERSTATUS = $orderStatus;
        $orderStatusLog->save();
    }
    //============================================================================================
    private function calcTaxableJobChargesBasedOnShippingCriteria() {//reviewed
        //add up all jobcharges for taxable jobs
        $sql = "select nvl(sum(taxableamt), 0) from (
        select
          (j.charge/j.productcount)*m.productcount taxableamt
        from
          job.ijob j join
          job.ijobshipmentmap m on m.jobid = j.jobid join
          job.ishipment s on s.shipmentpkey = m.shipmentpkey
        where
          j.link = $this->LINK and
          s.taxable = 'T'
          )";
        return queryScalar($sql);
    }
    //============================================================================================
    private function calcShippingCharge() {//reviewed
        $sql = "select
              sum(charge)
            from
              job.ishipment
            where
              shipmentpkey in (select shipmentpkey
                                from
                                  job.ijobshipmentmap m join
                                  job.ijob j on j.jobid = m.jobid
                                where
                                  link = $this->LINK AND j.status <> 'CANCELED')";
        /*
          $shippingCharge = queryScalar($sql);
          $jobs = $this->Jobs;
          foreach($jobs as $job)
          {
          if($job->Product->PACKAGINGTYPE === 'TUBE')
          }

          $sql = "SELECT SUM(PACKAGINGCOST)
          FROM
          (SELECT
          UNIQUE T.PACKAGINGCOST, P.PACKAGINGTYPE, s.SHIPPINGINFOID
          FROM
          job.bigship s JOIN
          job.IPRODUCT p on s.productid = p.productid join
          VPACKAGINGTYPE t ON P.PACKAGINGTYPE = T.PACKAGINGTYPE
          WHERE s.productid   IN
          (SELECT productid FROM job.ijob WHERE LINK = 8586064)
          );";
         */
        return queryScalar($sql); // + $shippingCharge;
    }
    //===================================================================
    //may be a better way to thing about this, email status? should
    //this be called when the order is not open?
    public function timeToSendReceiptEmail() {//reviewed
        if($this->ORDERSTATUS !== 'OPEN') {
            return false;
        }
        if($this->hasOrderEvent('EMAILEDRECEIPT')) {//again should we have an email status?
            return false;
        }
        if(!toBool($this->recSourceExtra->SENDINGEMAIL)) {
            return false;
        }
        return true;
    }
    //------------------
    private function handleChargesAndTax() {//reviewed
		//if AUTHORIZED don't recalc charges. They need to be locked.
        //only calc charges when pending or placed. Should be locked otherwise
        if($this->PAYMENTSTATUS === 'AUTHORIZED' ||
                ($this->ORDERSTATUS != 'PENDING' && $this->ORDERSTATUS != 'PLACED')) {
fileLog("don't change charges. Order status ".$this->ORDERSTATUS);
fileLog("don't change charges. Payment status ".$this->PAYMENTSTATUS);
            return;
        }

        //zero out charges if no jobs exist and exit
        if(count($this->jobs) === 0) {
fileLog("zero out charges. No jobs");
            $this->JOBSCHARGE = 0;
            $this->SHIPPINGCHARGE = 0;
            $this->TOTALTAXABLECHARGES = 0;
            $this->TAX = 0;
            $this->GRANDTOTAL = 0;
            return;
        }
		//-----------------------------
        //check if shipping is billed and calc shipping charges
        //todo: this will not reflect a change to bundle shipment not sure what this means
        //todo: do the markup then discount or discount then markup?
        if(strToBool($this->SHIPPINGBILLED)) {
fileLog('shipping is billed');
            $preMarkupShippingCharge = $this->calcShippingCharge();
fileLog('premarkup charge '.$preMarkupShippingCharge);
            if($preMarkupShippingCharge > 0) {
                $markedupShippingCharge = $preMarkupShippingCharge * Yii::app()->params['shippingMultiplier'];
fileLog('marked up charge after shippingMultiplier '.$markedupShippingCharge);
                $markedupShippingCharge = $markedupShippingCharge + Yii::app()->params['handlingChange'];
fileLog('marked up charge after handlingChange '.$markedupShippingCharge);
            }
            else {
fileLog('shipping not billed');
                $markedupShippingCharge = 0;
            }
        }
        else {
fileLog('shipping not billed premarkup charge '.$preMarkupShippingCharge);
            $markedupShippingCharge = 0;
		}
fileLog('final marked up charge '.$markedupShippingCharge);
        $this->SHIPPINGCHARGE = $markedupShippingCharge;

        //job charges
        $preDiscoutedJobsCharge = queryScalar("select sum(charge) from job.ijob where status <> 'CANCELED' and link = $this->LINK");
        $this->JOBSCHARGE = $preDiscoutedJobsCharge;

		//handleCouponDiscounts must be called after $this->JOBSCHARGE is set
        $this->handleCouponDiscounts();

        //taxes
        errIfNull($this->customer, 'Customer invalid');
        $custTaxStatus = $this->customer->TAXSTATUS;
        $custTaxable = ComboTaxable::model()->find("TAXSTATUS = '$custTaxStatus' AND PMTMETHOD = '$this->PAYMENTMETHOD'")->CHARGETAX; //todo: make this a relation?
        if(strToBool($custTaxable)) {
            $origTaxableCharges = $this->calcTaxableJobChargesBasedOnShippingCriteria();
            $percentTaxable = $origTaxableCharges / $preDiscoutedJobsCharge;
            //discount affects taxes
            $finalTaxableCharges = $origTaxableCharges - ($this->JOBDISCOUNT * $percentTaxable);
            //this is right I am sorta sure. I asked Joe who is now longer here need to explain this better.
            //It could still maybe not be exacty right but is has to do with discounts for things going to the stat vs out of the state and a bunch of stuff like that
            if($finalTaxableCharges < 0) {
                $finalTaxableCharges = 0;
            }
        }
        else {
            $finalTaxableCharges = 0;
        }
        $salesTaxRate = queryScalar("select value from g.ctrl where name = 'SALESTAX' and appname = 'BILLING'");
        $this->TOTALTAXABLECHARGES = $finalTaxableCharges;
        $tax = $finalTaxableCharges * $salesTaxRate;
        $this->TAX = $tax;

        $grandTotal = $this->JOBSCHARGE + $this->SHIPPINGCHARGE + $this->TAX;
        $this->GRANDTOTAL = $grandTotal;
    }
    //------------------------------------------------------------------------------
    public function handleCouponDiscounts() {//reviewed
        $this->JOBDISCOUNT = 0;
        $this->SHIPPINGDISCOUNT = 0;

        if(!isNullOrEmptyStr($this->COUPONCODE)) {
            $couponRec = $this->coupon;
            errIfNull($couponRec, "Coupon code '$this->COUPONCODE' not found");

            $currDateTimeObj = new DateTime();
            $currDateTimeStamp = $currDateTimeObj->getTimestamp();
            $expireDateTimeStr = $couponRec['EXPIREDATE'];
            $expireDay = strNthItem(1, $expireDateTimeStr, '-');
            $expireMonth = strNthItem(2, $expireDateTimeStr, '-');
            $expireYear = strNthItem(3, $expireDateTimeStr, '-');
            $expireYear = strNthItem(1, $expireYear, ' ');
            $expireDateTimeObj = new DateTime();
            $expireDateTimeObj->setDate($expireYear, $expireMonth, $expireDay);
            $expireDateTimeStamp = $expireDateTimeObj->getTimestamp();

            errIf($currDateTimeStamp > $expireDateTimeStamp, 'The coupon has expired');

            $couponDtls = queryAll("select * from JOB.icoupondtl
                                    where upper(couponcode) = upper('$this->COUPONCODE')"); //todo make a coupon dtls model?
            foreach($couponDtls as $couponDtl) {
                $item = $couponDtl['ITEM'];
                $discountType = $couponDtl['DISCOUNTTYPE'];
                errIf($discountType === 'DISCOUNT PRICE', 'DISCOUNT PRICE not longer supported for coupons');
                $discountValue = $couponDtl['VALUE'];
                if($item === 'JOBSCHARGE') {
                    $this->JOBDISCOUNT = $discountValue;
                    $finalJobsCharge = $this->JOBSCHARGE - $this->JOBDISCOUNT;
                    if($finalJobsCharge < 0) {
                        $finalJobsCharge = 0;
                    }
                    $this->JOBSCHARGE = $finalJobsCharge;
                }
                elseif($item === 'SHIPPINGCHARGE') {
                    $this->SHIPPINGDISCOUNT = $discountValue;
                    $finalShippingCharge = $this->SHIPPINGCHARGE - $this->SHIPPINGDISCOUNT;
                    if($finalShippingCharge < 0) {
                        $finalShippingCharge = 0;
                    }
                    $this->SHIPPINGCHARGE = $finalShippingCharge;
                }
                else {
                    err("$item not a supported coupon type");
                }
            }
        }
    }
    //------------------
    private function handlePaymentStatus() {//reviewed
		if($this->PAYMENTMETHOD === 'CASHNET') {
			if($this->PAYMENTSTATUS !== 'AUTHORIZED') {
				$this->PAYMENTSTATUS = 'PENDING';
			}
		}
        else if($this->PAYMENTMETHOD === 'MISSING') {
            $this->PAYMENTSTATUS = 'PENDING';
        }
        else {
            $this->PAYMENTSTATUS = 'ROUTED';
        }
    }
    //------------------
    private function handleCustIdAndPaymentInfo() {//reviewed
        switch($this->PAYMENTMETHOD) {
            case "CASHNET": //credit card
                $this->PAYMENTINFO = null;
                $custId = '1470'; //Cash customer taxable
                break;
            case "COUPON": //credit card
                $this->PAYMENTINFO = null;
                $custId = '1470'; //Cash customer taxable
                break;
            case "AR": //accounts receivable
                //errIf($this->ORDERSTATUS !== 'OPEN' && $this->ORDERSTATUS !== 'BILLING',
				//		"Payment Method "."{$this->paymentMethod->EDESC} only available when logged in.");
                $this->PAYMENTINFO = null;
                $custId = $this->CUSTID;
                break;
            case "BT": //budget transfer
                //errIf(/*Yii::app()->user->isGuest && */
                //$this->ORDERSTATUS !== 'OPEN' &&
                //      $this->ORDERSTATUS !== 'BILLING',
                //    "Payment Method ".
                //  "{$this->paymentMethod->EDESC} only available when logged in.");
                errIfNull($this->PAYMENTINFO, 'Payment info missing');
                $custId = strNthItem(1, $this->PAYMENTINFO, '-');
                break;
            case "CF": //church unit number
                errIfNull($this->PAYMENTINFO, 'Payment info missing');
                $unitNumber = strNthItem(1, $this->PAYMENTINFO, ' ');
                $custId = 'CH'.$unitNumber;
//	don't check the leader name anymore
//                $leaderLastName = strtoupper(strNthItem(2, $this->PAYMENTINFO, ' '));
//                $contactNames = $this->customer->CONTACT;
//                $names = explode(' ', $contactNames);
//                foreach($names as $name) {
//                    $upperName = strtoupper($name);
//                    $compareResult = strcmp($upperName, $leaderLastName);
//                    if($compareResult === 0) {
//                        break;
//                    }
//                }
//                verify($compareResult === 0, "Leader name '$leaderLastName' and Unit number '$unitNumber' do not match. Is the leader lastname spelled correctly?");
                break;
            case "AC": //account code
                $custId = '1027';
                break;
            case "BUN": //business unit number
                $custId = '1027';
                break;
            case "CASH"://pay on pickup
                $this->PAYMENTINFO = null;
                $custId = '1027';
                break;
            case "MISSING":
                $this->PAYMENTINFO = null;
                $custId = '1027';
                break;
            default:
                throw new Exception("Payment method $this->PAYMENTMETHOD not supported.");
        }

        //Check the payment number to make sure that it is being used for the right type
        //EX: make sure CF number isn't used as an AC
        //$paymentType = queryScalar("SELECT TYPE FROM BIL.ICUST WHERE CUSTID = '$custId'");
        //errIf($this->PAYMENTMETHOD !== $paymentType, "The payment type does not match the payment number.");
        $this->CUSTID = $custId;
        errIfNull($this->customer, "Invalid Customer Number. $custId");
    }
    //------------------
    public function handleSplitShipments() {//reviewed.
        $this->BUNDLESHIPMENTS = $this->recSourceExtra->BUNDLESHIPMENTS;
        if(!strToBool($this->BUNDLESHIPMENTS)) {
            $shipments = $this->shipments;
            foreach($shipments as $shipment) {
                $jobs = $shipment->jobs;
                if(empty($jobs)) {
                    continue;
                }
                if(count($jobs) > 1) {
                    for($i = 1; $i < count($jobs); $i++) {
                        $newShipment = new Shipment;
                        $newShipment->LINK = $shipment->LINK;
                        $newShipment->DELIVERYMETHOD = $shipment->DELIVERYMETHOD;
                        $newShipment->NOTIFYSTATUS = 'PENDING';
                        $newShipment->SHIPPINGINFOID = $shipment->SHIPPINGINFOID;
                        $newShipment->TAXABLE = $shipment->TAXABLE;
                        $newShipment->save(); //todo: maybe not save here in the future

                        $currentJobMap = JobShipmentMap::model()->find('JOBID='.$jobs[$i]->JOBID.' AND SHIPMENTPKEY = '.$shipment->SHIPMENTPKEY);
                        $oldShipmentPkey = $currentJobMap->SHIPMENTPKEY;
                        $currentJobMap->SHIPMENTPKEY = $newShipment->SHIPMENTPKEY;
                        $currentJobMap->save(false);
                        Shipment::model()->deleteByPK($oldShipmentPkey);
                    }
                }
            }
        }
    }
    //------------------
    public function hasOrderEvent($orderStatus) {//reviewed
        $orderStatusLog = OrderStatusLog::model()->find("LINK=$this->LINK AND ORDERSTATUS = '$orderStatus'");
        return !is_null($orderStatusLog);
    }
    //------------------
    public static function test($toss) {
        $testOrder = new Order();
        parent::test($testOrder);

        $testOrder = Order::model()->findByPk(8522604);
        $testOrder->ORDERSTATUS = 'BADSTATUS';
        try {
            $testOrder->save();
            errFailedToThrow();
        }
        catch(Exception $ex) {
            testErrWasThrown($ex, "error on save test");
        }

        $testOrder->RECDATE = '01-JAN-2000';
        try {
            $testOrder->save();
            errFailedToThrow();
        }
        catch(Exception $ex) {
            testErrWasThrown($ex, "error on change rec date test");
        }

        try {
            $testOrder->delete();
            errFailedToThrow();
        }
        catch(Exception $ex) {
            testErrWasThrown($ex, 'error on delete test');
        }
    }
}