<?php
class ShippingInfo extends YActiveRecord {
    //----------------------
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    //----------------------
    public function tableName() {
        return 'JOB.ISHIPPINGINFO';
    }
    //----------------------
    public function rules() {
        return array(
                array('SHIPTONAME, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, CITY, EMAIL', 'length', 'max'=>100),
                array('STATE, POSTALCODE, COUNTRY, NETID', 'length', 'max'=>50),
                array('PHONE', 'length', 'max'=>25),
                array('ADDRESSTYPE', 'length', 'max'=>20),
                array('NOTES', 'length', 'max'=>200),
                array('INSTATE, RESIDENTIAL', 'length', 'max'=>1),
                array('SHIPTONAME, ADDRESS1, CITY, STATE, POSTALCODE, RESIDENTIAL', 'required', 'on'=>'addDomestic'),
                array('SHIPTONAME, ADDRESS1, CITY, COUNTRY, POSTALCODE', 'required', 'on'=>'addInternational'),
                array('SHIPTONAME, ADDRESS1', 'required', 'on'=>'addCampus'),
                array('SHIPPINGINFOID, SHIPTONAME, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, CITY, STATE, POSTALCODE, COUNTRY, PHONE, EMAIL, ADDRESSTYPE, NOTES, INSTATE, NETID, RESIDENTIAL', 'safe', 'on'=>'search'),
                array('SHIPTONAME', 'ensureUnicode')
        );
    }
    //----------------------
    public function ensureUnicode($attributes, $params) {
        $this->SHIPTONAME = htmlentities($this->SHIPTONAME, ENT_COMPAT | ENT_HTML401, 'UTF-8', false);
    }
    //----------------------
    public function relations() {
        return array(
                'shipments'=>array(self::HAS_MANY, 'Shipment', 'SHIPPINGINFOID'),
        );
    }
    //----------------------
    public function submit() {//touch pending shipments and orders to update charges
        $this->save();
        $shipments = $this->shipments;
        if(!is_null($shipments)) {
            foreach($shipments as $shipment) {
                $order = $shipment->order;
                if($order->ORDERSTATUS === 'PENDING') {
                    $shipment->save();
                    $order->save();
                }
            }
        }
    }
}