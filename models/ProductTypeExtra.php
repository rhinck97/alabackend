<?php
class ProductTypeExtra extends YActiveRecord {
    //---------------------------------------------
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    //---------------------------------------------
    public function tableName() {
        return 'JOB.VPRODUCTTYPEEXTRA';
    }
    //---------------------------------------------
    public function rules() {
        return array(
        );
    }
    //---------------------------------------------
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }
}