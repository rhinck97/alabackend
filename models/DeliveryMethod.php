<?php
class DeliveryMethod extends YActiveRecord {
    //----------------------------------------------
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    //----------------------------------------------
    public function tableName() {
        return 'JOB.VDELIVERYMETHOD';
    }
    //----------------------------------------------
    public function rules() {
        return array(
                array('DISPLAYNAME, SUBMITRTS', 'required'),
                array('PRIORITY', 'numerical'),
                array('DISPLAYNAME', 'length', 'max'=>50),
                array('SUBMITRTS, DEFAULTSHIPPREFIX, DELIVERYTYPE', 'length', 'max'=>20),
                array('INSTATEONLY', 'length', 'max'=>1),
                array('DELIVERYMETHOD, DISPLAYNAME, PRIORITY, SUBMITRTS, INSTATEONLY, DEFAULTSHIPPREFIX, DELIVERYTYPE', 'safe', 'on'=>'search'),
        );
    }
    //----------------------------------------------
    public function relations() {
        return array(
                'shipment'=>array(self::HAS_MANY, 'Shipment', 'DELIVERYMETHOD'),
                'deliveryType'=>array(self::BELONGS_TO, 'DeliveryType', 'DELIVERYTYPE'),
                'deliveryDtl'=>array(self::HAS_MANY, 'DeliveryMethodDtl', 'DELIVERYMETHOD'),
        );
    }
}