<?php
//-------------------------------------------
class ProductDtl extends YActiveRecord {
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    //----------------------------------------------
    public function tableName() {
        return 'JOB.IPRODUCTDTL';
    }
    //----------------------------------------------
    public function rules() {
        return array(
                array('PKEY, TRAIT, EXTRATRAIT, FREETRAIT, UNITCOUNT, SERVICELOC, '.
                        'PRODUCTID, INFO, ITEMTAG, ITEM, TICKETINFO, DISPLAYORDER, '.
                        'ROLE, PARENTROLE, RATIOTOPARENT', 'safe'),
        );
    }
    //----------------------------------------------
    public function relations() {
        return array(
                'product'=>array(self::BELONGS_TO, 'Product', 'PRODUCTID'),
                'getRole'=>array(self::BELONGS_TO, 'Role', 'ROLE'),
                'designer'=>array(self::BELONGS_TO, 'Product', array('INFO'=>'PRODUCTID')),
                'parentRole'=>array(self::BELONGS_TO, 'ProductDtl', array('PARENTROLE'=>'ROLE', 'PRODUCTID'=>'PRODUCTID')),
                'elementMap'=>array(self::BELONGS_TO, 'ElementMap', array('ITEM', 'TRAIT', 'EXTRATRAIT')),
        );
    }
}