<?php
/**
 * This is the model class for table "ICOUPON".
 *
 * The followings are the available columns in table 'ICOUPON':
 * @property string $COUPONCODE
 * @property string $EXPIREDATE
 *
 * The followings are the available model relations:
 * @property ICOUPONDTL[] $iCOUPONDTLs
 */
class Coupon extends YActiveRecord {
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Coupon the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'JOB.ICOUPON';
    }
    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
                array('EXPIREDATE', 'safe'),
                // The following rule is used by search().
                // Please remove those attributes that should not be searched.
                array('COUPONCODE, EXPIREDATE', 'safe', 'on'=>'search'),
        );
    }
    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
                'iCOUPONDTLs'=>array(self::HAS_MANY, 'ICOUPONDTL', 'COUPONCODE'),
        );
    }
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'COUPONCODE'=>'Couponcode',
                'EXPIREDATE'=>'Expiredate',
        );
    }
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('COUPONCODE', $this->COUPONCODE, true);
        $criteria->compare('EXPIREDATE', $this->EXPIREDATE, true);

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
        ));
    }
}