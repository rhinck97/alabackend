<?php
class Location extends YActiveRecord {
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    public function rules() {
        
    }
    public function tableName() {
        return 'BIL.VLOC';
    }
    public function getDisplayAttributes() {
        $attributes = array("centerName"=>$this->EDESC,
                "centerId"=>$this->LOCCODE);
        return $attributes;
    }
    public function getAltAttributes() {
        $locationArray = $this->getAttributes();
        $resultArray = array();
        $altKeyMap = array(
                "LOC"=>"location",
                "LOCCODE"=>"locationCode",
                "EDESC"=>"description",
                "PHONE"=>"phone",
                "ADDRESS"=>"address",
                "HOURS"=>"hours",
        );
        foreach($locationArray as $key=> $value) {
            if(array_key_exists($key, $altKeyMap)) {
                $resultArray[$altKeyMap[$key]] = $value;
            }
        }
        return $resultArray;
    }
}
?>
