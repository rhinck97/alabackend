<?php
class Transaction extends YActiveRecord {
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    //------------------------------------------------
    public function tableName() {
        return 'BIL.ITRAN';
    }
    //------------------------------------------------
    public function rules() {
        return array(
                array('CUSTID, CATEGORY, RECSOURCE, EMODE, ENTRYLOC, PMTMETHOD', 'required'),
                array('TAX, TAXABLEAMT, TRANAMT', 'numerical'),
                array('TRANDATE, MODEDATE, RECDATE, STARTDATE, MODIFIED, RECTIME, ACCTPERIOD, INVOICEDATE, DUEDATE', 'safe'),
        );
    }
    //------------------------------------------------
    public function relations() {
        return array(
                'customer'=>array(self::BELONGS_TO, 'Customer', 'CUSTID'),
                'transactionDtls'=>array(self::HAS_MANY, 'TransactionDtl', 'LINK'),
        );
    }
    //------------------------------------------------
    public function attributeLabels() {
        return array(
                'CUSTID'=>'Custid',
                'CATEGORY'=>'Category',
                'LINK'=>'Link',
                'TRANDATE'=>'Trandate',
                'MFSNO'=>'Mfsno',
                'PFSNO'=>'Pfsno',
                'NOTE1'=>'Note1',
                'NOTE2'=>'Note2',
                'NOTE3'=>'Note3',
                'MODEDATE'=>'Modedate',
                'RECDATE'=>'Recdate',
                'RECSOURCE'=>'Recsource',
                'STARTDATE'=>'Startdate',
                'DISPLAYID'=>'Displayid',
                'MODIFIED'=>'Modified',
                'EMPNO'=>'Empno',
                'CONTACTPHONE'=>'Contactphone',
                'ECOMMENT'=>'Ecomment',
                'EMODE'=>'Emode',
                'ENTRYLOC'=>'Entryloc',
                'AUDITID'=>'Auditid',
                'JOT'=>'Jot',
                'RECTIME'=>'Rectime',
                'SUPADDR'=>'Supaddr',
                'ACCTPERIOD'=>'Acctperiod',
                'ROUTEDID'=>'Routedid',
                'PMTMETHOD'=>'Pmtmethod',
                'PMTINFO'=>'Pmtinfo',
                'CUSTTYPE'=>'Custtype',
                'TAX'=>'Tax',
                'TAXABLEAMT'=>'Taxableamt',
                'INVOICEDATE'=>'Invoicedate',
                'TRANAMT'=>'Tranamt',
                'DUEDATE'=>'Duedate',
                'PARAM'=>'Param',
                'FSNOTYPE'=>'Fsnotype',
                'CUSTNAME'=>'Custname',
        );
    }
    //------------------------------------------------
    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('CUSTID', $this->CUSTID, true);
        $criteria->compare('CATEGORY', $this->CATEGORY, true);
        $criteria->compare('LINK', $this->LINK);
        $criteria->compare('TRANDATE', $this->TRANDATE, true);
        $criteria->compare('MFSNO', $this->MFSNO, true);
        $criteria->compare('PFSNO', $this->PFSNO, true);
        $criteria->compare('NOTE1', $this->NOTE1, true);
        $criteria->compare('NOTE2', $this->NOTE2, true);
        $criteria->compare('NOTE3', $this->NOTE3, true);
        $criteria->compare('MODEDATE', $this->MODEDATE, true);
        $criteria->compare('RECDATE', $this->RECDATE, true);
        $criteria->compare('RECSOURCE', $this->RECSOURCE, true);
        $criteria->compare('STARTDATE', $this->STARTDATE, true);
        $criteria->compare('DISPLAYID', $this->DISPLAYID, true);
        $criteria->compare('MODIFIED', $this->MODIFIED, true);
        $criteria->compare('EMPNO', $this->EMPNO, true);
        $criteria->compare('CONTACTPHONE', $this->CONTACTPHONE, true);
        $criteria->compare('ECOMMENT', $this->ECOMMENT, true);
        $criteria->compare('EMODE', $this->EMODE, true);
        $criteria->compare('ENTRYLOC', $this->ENTRYLOC, true);
        $criteria->compare('AUDITID', $this->AUDITID, true);
        $criteria->compare('JOT', $this->JOT, true);
        $criteria->compare('RECTIME', $this->RECTIME, true);
        $criteria->compare('SUPADDR', $this->SUPADDR, true);
        $criteria->compare('ACCTPERIOD', $this->ACCTPERIOD, true);
        $criteria->compare('ROUTEDID', $this->ROUTEDID, true);
        $criteria->compare('PMTMETHOD', $this->PMTMETHOD, true);
        $criteria->compare('PMTINFO', $this->PMTINFO, true);
        $criteria->compare('CUSTTYPE', $this->CUSTTYPE, true);
        $criteria->compare('TAX', $this->TAX);
        $criteria->compare('TAXABLEAMT', $this->TAXABLEAMT);
        $criteria->compare('INVOICEDATE', $this->INVOICEDATE, true);
        $criteria->compare('TRANAMT', $this->TRANAMT);
        $criteria->compare('DUEDATE', $this->DUEDATE, true);
        $criteria->compare('PARAM', $this->PARAM, true);
        $criteria->compare('FSNOTYPE', $this->FSNOTYPE, true);
        $criteria->compare('CUSTNAME', $this->CUSTNAME, true);

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
        ));
    }
}