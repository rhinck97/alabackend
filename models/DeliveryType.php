<?php
class DeliveryType extends YActiveRecord {
    //----------------------------------------------
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    //----------------------------------------------
    public function tableName() {
        return 'JOB.VDELIVERYTYPE';
    }
    //----------------------------------------------
    public function rules() {
        return array(
                array('DISPLAYNAME', 'length', 'max'=>100),
                // The following rule is used by search().
                // Please remove those attributes that should not be searched.
                array('DELIVERYTYPE, DISPLAYNAME', 'safe', 'on'=>'search'),
        );
    }
    //----------------------------------------------
    public function relations() {
        return array(
                'deliveryMethod'=>array(self::HAS_MANY, 'VDELIVERYMETHOD', 'DELIVERYTYPE'),
        );
    }
}