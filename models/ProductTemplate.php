<?php
require_once Yii::app()->basePath.'/lib/onyx/onyxCore.php';
require_once Yii::app()->basePath."/lib/onyx/onyxYii.php";
require_once Yii::app()->basePath.'/lib/pam/pamCore.php';
class ProductTemplate extends YActiveRecord {
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    //--------------------------------------
    public function tableName() {
        return 'JOB.IPRODTEMPLATE';
    }
    //--------------------------------------
    public function rules() {
        return array(
                array('PRODTEMPLATEID, PRODUCTTYPE, DISPLAYNAME, PRODUCTDESC, RECSOURCE, BASEPRICE, BASEVISUAL, '
                        .'TICKETLOCATIONS, TEMPLATECODE, ACTIVE', 'safe'),
        );
    }
    //--------------------------------------
    public function relations() {
        return array(
                'baseTemplate'=>array(self::BELONGS_TO, 'ProductTemplate', 'PRODTEMPLATEID'),
                'templateCopies'=>array(self::HAS_MANY, 'ProductTemplate', 'TEMPLATECODE'),
                'templateDtlOptions'=>array(self::HAS_MANY, 'ProductTemplateDtl', 'PRODTEMPLATEID', 'order'=>'SORT', 'condition'=>'VALUE is null'),
                'templateDtls'=>array(self::HAS_MANY, 'ProductTemplateDtl', 'PRODTEMPLATEID', 'order'=>'PRODUCTOPTION, SORT', 'condition'=>'VALUE is not null'),
                'templateRestrictions'=>array(self::HAS_MANY, 'ProductTemplateRestriction', 'PRODTEMPLATEID'),
        );
    }
    //--------------------------------------
    public static function test($toss) {

    }
}
