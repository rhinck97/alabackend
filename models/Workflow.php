<?php
class Workflow extends YActiveRecord {
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    //---------------------------------------------
    public function primaryKey() {
        return array('PRODUCTTYPE', 'CURRENTSTATUS');
    }
    //---------------------------------------------
    public function tableName() {
        return 'JOB.SWORKFLOW';
    }
    //---------------------------------------------
    public function rules() {
        return array(
                array('PRODUCTTYPE, CURRENTSTATUS, NEXTSTATUS', 'safe'),
        );
    }
    //---------------------------------------------
    public function relations() {
        return array(
                'CURRENTSTATUS'=>array(self::BELONGS_TO, 'VSTATUS', 'CURRENTSTATUS'),
                'NEXTSTATUS'=>array(self::BELONGS_TO, 'VSTATUS', 'NEXTSTATUS'),
        );
    }
}