<?php
class ProductForm extends CFormModel {
    //Form Variables
    public $displayName;
    public $productDesc;
    public $visual;
    public $sellingPrice;
    //Template Variables
    public $templateId;
    public $productType;
    //public $subcount; <- who knows? countperbox...?? wtf?
    //public $ticketLocations; ???
    public $orderMin = 1;
    public $orderIncrement = 1;
    //public $details; // <- filled by template* to populate the productDtls table
    //System
    public $productId;
    public $productOwner; //$this->recSource
    public $productCase = "PRODUCT";
    //$OUNCESPERPRODUCT <- maybe?
    public $approvalGroup; //$this->recSource
    public $details = array();
    /**
     *  
     * 
     */
    public $group;
    //Details

    public function rules() {
        return array(
                //form
                array('templateId, displayName, productDesc, visual', 'required'),
                array('productId', 'numerical'),
                array('details', 'validateDetails')
                // array('gutsFile, coverFile','fileValidation'),
                // array('','selectorValidation'),
                // array('','')
                //Template
                //System
                //Details
                //array('gutsFile, coverFile', 'file', 'types'=>'pdf')
        );
    }
    //---------------------------------------------
    public function attributeLabels() {
        return array(
                //Form
                "displayName"=>"Product Display Name",
                "productDesc"=>"Product Description",
                "visual"=>"Thumbnail",
                //Template
                //System
                //Details
                "GUTS FILE"=>"Pages File",
                "COVER FILE"=>"Cover File"
        );
    }
    //---------------------------------------------
    public function validateDetails() {
        $ret = true;
        foreach($this->details as $key=> $value) {
            if($value === "") {
                $labels = $this->attributeLabels();
                $this->addError('displayName', $labels[$key]." is not set");
                $ret = false;
            }
        }
        return $ret;
    }
}
?>
