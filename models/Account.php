<?php
/**
 * This is the model class for table "ACCOUNT".
 *
 * The followings are the available columns in table 'ACCOUNT':
 * @property string $LOGINID
 * @property string $PASSWORDHASH
 * @property string $FIRSTNAME
 * @property string $LASTNAME
 * @property string $EMAIL
 * @property string $PHONE
 */
class Account extends YActiveRecord {
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Account the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'JOB.ACCOUNT';
    }
    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
                array('PASSWORDHASH, FIRSTNAME, LASTNAME, EMAIL', 'length', 'max'=>100),
                array('PHONE, VALIDATIONREQUESTTIME', 'length', 'max'=>50),
                array('EMAIL', 'email'),
                array('VALIDATED', 'length', 'max'=>5),
                // The following rule is used by search().
                // Please remove those attributes that should not be searched.
                array('LOGINID, PASSWORDHASH, FIRSTNAME, LASTNAME, EMAIL, PHONE, VALIDATED, VALIDATIONREQUESTTIME', 'safe', 'on'=>'search'),
        );
    }
    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
                'location'=>array(self::MANY_MANY, 'Location', 'ACCOUNTLOCMAP(LOGINID, LOC)'),
        );
    }
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'LOGINID'=>'Loginid',
                'PASSWORDHASH'=>'Passwordhash',
                'FIRSTNAME'=>'Firstname',
                'LASTNAME'=>'Lastname',
                'EMAIL'=>'Email',
                'PHONE'=>'Phone',
                'VALIDATED'=>'Validated',
                'VALIDATIONREQUESTTIME'=>'ValidationRequestTime',
        );
    }
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('LOGINID', $this->LOGINID, true);
        $criteria->compare('PASSWORDHASH', $this->PASSWORDHASH, true);
        $criteria->compare('FIRSTNAME', $this->FIRSTNAME, true);
        $criteria->compare('LASTNAME', $this->LASTNAME, true);
        $criteria->compare('EMAIL', $this->EMAIL, true);
        $criteria->compare('PHONE', $this->PHONE, true);
        $criteria->compare('VALIDATED', $this->VALIDATED, true);
        $criteria->compare('VALIDATIONREQUESTTIME', $this->VALIDATIONREQUESTTIME, true);

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
        ));
    }
}