<?php
// include(Yii::app()->basePath."/../3rdParty/FedEx/php/library/fedex-common.php5");

class ShipmentForm extends CFormModel {
    public $name;
    public $address1;
    public $address2;
    public $address3;
    public $address4;
    public $city;
    public $state;
    public $postalCode;
    public $country;
    public $email;
    public $phone;
    public $deliveryMethod;
    public $deliveryType;
    public $shipmentpkey;
    public function __construct($type = '') {
        parent::__construct();
        $this->deliveryType = $type;
    }
    //-------------------  
    public function getDeliveryType() {
        if($this->deliveryType) {
            return $this->deliveryType;
        }
        else {
            $deliveryMethod = $this->deliveryMethod;
            $command = Yii::app()->db->createCommand("select DELIVERYTYPE FROM 
                                                VDELIVERYMETHOD WHERE 
                                                DELIVERYMETHOD = '$deliveryMethod'");
            $this->deliveryType = $command->queryScalar();
            return $this->deliveryType;
        }
    }
    //-------------------  
    public function rules() {
        switch($this->deliveryType) {
            case "DOMESTIC":
                return array(
                        array('deliveryMethod, name, address1, city, state, postalCode', 'required'),
//            array('address', 'validateAddress')     //we'll do this later.
                );
                break;
            case "INTERNATIONAL":
                return array(
                        array('deliveryMethod, name, address1, city, country, postalCode', 'required')
                );
                break;
            case "DELIVER":
                return array(
                        array('name, address1', 'required')
                );
                break;
            case "PICKUP":
                return array(
                        array('name, address1, deliveryMethod', 'required')
                );
                break;
            default:
                throw new CHttpException(500, "INVALID DELIVERY TYPE".$this->deliveryType);
                break;
        }
    }
    //-------------------  
    public function attributeLabels() {
        //Default Labels
        $labels = array(
                'name'=>'Name',
                'address1'=>'Street Address',
                'address2'=>'Address Line (Optional)',
                'address3'=>'Address Line (Optional)',
                'address4'=>'Address Line (Optional)',
                'city'=>'City',
                'state'=>'State',
                'country'=>'Country',
                'postalCode'=>'Postal Code',
                'email'=>'E-mail',
                'phone'=>'Phone Number'
        );
        //Special Differences...
        switch($this->getDeliveryType()) {
            case "DELIVER":
                $labels['address1'] = "Campus Address";
                break;
            case "PICKUP";
                $labels['name'] = "Name (who will pickup)";
                $labels['address1'] = "Pickup Location";
                break;
        }
        return $labels;
    }
    //-------------------  
}
?>