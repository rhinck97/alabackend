<?php
class StatusLog extends YActiveRecord {
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    //---------------------------------------------
    public function tableName() {
        return 'JOB.ISTATUSLOG';
    }
    //---------------------------------------------
    public function rules() {
        return array(
                array('JOBID, STATUS, STATUSTIME', 'required'),
                array('JOBID', 'numerical'),
                array('STATUS', 'length', 'max'=>25),
                array('JOBID, STATUS, STATUSTIME, PKEY', 'safe', 'on'=>'search'),
        );
    }
    //---------------------------------------------
    public function relations() {
        return array(
                'STATUS'=>array(self::BELONGS_TO, 'VSTATUS', 'STATUS'),
                'JOB'=>array(self::BELONGS_TO, 'IJOB', 'JOBID'),
        );
    }
}