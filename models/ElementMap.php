<?php
/**
 * This is the model class for table "VELEMENTMAP".
 *
 * The followings are the available columns in table 'VELEMENTMAP':
 * @property string $ITEM
 * @property string $TRAIT
 * @property string $FREETRAITSET
 * @property double $OUNCES
 * @property string $EXTRATRAIT
 * @property string $UNIT
 * @property string $PRICINGMETHOD
 * @property string $SHOWONTICKET
 * @property string $DISPLAYNAME
 *
 * The followings are the available model relations:
 * @property IPRODUCTDTL[] $iPRODUCTDTLs
 * @property IPRODUCTDTL[] $iPRODUCTDTLs1
 * @property IPRODUCTDTL[] $iPRODUCTDTLs2
 * @property VELEMENT $iTEM
 * @property VELEMENT $tRAIT
 * @property VELEMENT $eXTRATRAIT
 * @property VPRICINGMETHOD $pRICINGMETHOD
 */
class ElementMap extends YActiveRecord {
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return ElementMap the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'JOB.VELEMENTMAP';
    }
    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
                array('DISPLAYNAME', 'required'),
                array('OUNCES', 'numerical'),
                array('FREETRAITSET, UNIT, PRICINGMETHOD', 'length', 'max'=>25),
                array('SHOWONTICKET, DISPLAYNAME', 'length', 'max'=>50),
                // The following rule is used by search().
                // Please remove those attributes that should not be searched.
                array('ITEM, TRAIT, FREETRAITSET, OUNCES, EXTRATRAIT, UNIT, PRICINGMETHOD, SHOWONTICKET, DISPLAYNAME', 'safe', 'on'=>'search'),
        );
    }
    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
//			'iPRODUCTDTLs' => array(self::HAS_MANY, 'IPRODUCTDTL', 'ITEM'),
//			'iPRODUCTDTLs1' => array(self::HAS_MANY, 'IPRODUCTDTL', 'TRAIT'),
//			'iPRODUCTDTLs2' => array(self::HAS_MANY, 'IPRODUCTDTL', 'EXTRATRAIT'),
//			'iTEM' => array(self::BELONGS_TO, 'VELEMENT', 'ITEM'),
//			'tRAIT' => array(self::BELONGS_TO, 'VELEMENT', 'TRAIT'),
//			'eXTRATRAIT' => array(self::BELONGS_TO, 'VELEMENT', 'EXTRATRAIT'),
//			'pRICINGMETHOD' => array(self::BELONGS_TO, 'VPRICINGMETHOD', 'PRICINGMETHOD'),
        );
    }
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'ITEM'=>'Item',
                'TRAIT'=>'Trait',
                'FREETRAITSET'=>'Freetraitset',
                'OUNCES'=>'Ounces',
                'EXTRATRAIT'=>'Extratrait',
                'UNIT'=>'Unit',
                'PRICINGMETHOD'=>'Pricingmethod',
                'SHOWONTICKET'=>'Showonticket',
                'DISPLAYNAME'=>'Displayname',
        );
    }
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ITEM', $this->ITEM, true);
        $criteria->compare('TRAIT', $this->TRAIT, true);
        $criteria->compare('FREETRAITSET', $this->FREETRAITSET, true);
        $criteria->compare('OUNCES', $this->OUNCES);
        $criteria->compare('EXTRATRAIT', $this->EXTRATRAIT, true);
        $criteria->compare('UNIT', $this->UNIT, true);
        $criteria->compare('PRICINGMETHOD', $this->PRICINGMETHOD, true);
        $criteria->compare('SHOWONTICKET', $this->SHOWONTICKET, true);
        $criteria->compare('DISPLAYNAME', $this->DISPLAYNAME, true);

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
        ));
    }
}