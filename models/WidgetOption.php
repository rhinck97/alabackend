<?php
/**
 * This is the model class for table "WIDGETOPTION".
 *
 * The followings are the available columns in table 'WIDGETOPTION':
 * @property double $ID
 * @property double $OPTIONSETID
 * @property double $DISPLAYORDER
 * @property string $TRAIT
 * @property string $VALUE
 * @property string $DISPLAYNAME
 * @property string $DTLITEM
 * @property string $DTLTRAIT
 * @property string $DTLEXTRATRAIT
 * @property string $DTLFREETRAIT
 *
 * The followings are the available model relations:
 * @property VELEMENTMAP $dTLITEM
 * @property VELEMENTMAP $dTLTRAIT
 * @property VELEMENTMAP $dTLEXTRATRAIT
 * @property WIDGETOPTIONSET $oPTIONSET
 */
class WidgetOption extends YActiveRecord {
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return WidgetOption the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'JOB.WIDGETOPTION';
    }
    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
                array('OPTIONSETID, VALUE', 'required'),
                array('OPTIONSETID, DISPLAYORDER', 'numerical'),
                array('TRAIT, DISPLAYNAME, DTLFREETRAIT', 'length', 'max'=>20),
                array('VALUE', 'length', 'max'=>100),
                array('DTLITEM, DTLTRAIT, DTLEXTRATRAIT', 'length', 'max'=>40),
                // The following rule is used by search().
                // Please remove those attributes that should not be searched.
                array('ID, OPTIONSETID, DISPLAYORDER, TRAIT, VALUE, DISPLAYNAME, DTLITEM, DTLTRAIT, DTLEXTRATRAIT, DTLFREETRAIT', 'safe', 'on'=>'search'),
        );
    }
    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
                'dTLITEM'=>array(self::BELONGS_TO, 'VELEMENTMAP', 'DTLITEM'),
                'dTLTRAIT'=>array(self::BELONGS_TO, 'VELEMENTMAP', 'DTLTRAIT'),
                'dTLEXTRATRAIT'=>array(self::BELONGS_TO, 'VELEMENTMAP', 'DTLEXTRATRAIT'),
                'oPTIONSET'=>array(self::BELONGS_TO, 'WIDGETOPTIONSET', 'OPTIONSETID'),
        );
    }
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
                'ID'=>'ID',
                'OPTIONSETID'=>'Optionsetid',
                'DISPLAYORDER'=>'Displayorder',
                'TRAIT'=>'Trait',
                'VALUE'=>'Value',
                'DISPLAYNAME'=>'Displayname',
                'DTLITEM'=>'Dtlitem',
                'DTLTRAIT'=>'Dtltrait',
                'DTLEXTRATRAIT'=>'Dtlextratrait',
                'DTLFREETRAIT'=>'Dtlfreetrait',
        );
    }
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('ID', $this->ID);
        $criteria->compare('OPTIONSETID', $this->OPTIONSETID);
        $criteria->compare('DISPLAYORDER', $this->DISPLAYORDER);
        $criteria->compare('TRAIT', $this->TRAIT, true);
        $criteria->compare('VALUE', $this->VALUE, true);
        $criteria->compare('DISPLAYNAME', $this->DISPLAYNAME, true);
        $criteria->compare('DTLITEM', $this->DTLITEM, true);
        $criteria->compare('DTLTRAIT', $this->DTLTRAIT, true);
        $criteria->compare('DTLEXTRATRAIT', $this->DTLEXTRATRAIT, true);
        $criteria->compare('DTLFREETRAIT', $this->DTLFREETRAIT, true);

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
        ));
    }
}