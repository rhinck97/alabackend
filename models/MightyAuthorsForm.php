<?php
class MightyAuthorsForm extends CFormModel {
    public $BINDING;
    public $IMPRESSIONS;
    public $PRINTING_TYPE;
    public $QUANTITY;
    public $allowedBinding = array("HARD BOUND", "SOFT BOUND");
    public $allowedImpressions = array("BW", "PRODUCTION COLOR");
    public $allowedPrintTypes = array("SIMPLEX", "DUPLEX");
    public function rules() {

        $rules = array(
                //form
                array("QUANTITY", "numerical", "message"=>'Please enter a number greater than zero.'),
                array('BINDING,IMPRESSIONS,PRINTING_TYPE', 'required', 'message'=>'This is a required field.'),
                array('BINDING', 'validateBinding'),
                array('IMPRESSIONS', 'validateImpressions'),
                array('PRINTING_TYPE', 'validatePrintingType'),
                array('QUANTITY', 'validateQuantity'),
        );

        return $rules;
    }
    //---------------------------------------------
    public function attributeLabels() {
        return array(
                //Form
                "BINDING"=>"Binding",
                "IMPRESSIONS"=>"Print Color",
                "PRINTING_TYPE"=>"Print Type",
                "QUANTITY"=>"Number of Copies",
        );
    }
    public function validateBinding($attribute, $params) {
        //make sure they chose a place to pick it up.
        if(!in_array($this->$attribute, $this->allowedBinding))
            $this->addError($attribute, "Please select a valid item from the list.");
        return !$this->hasErrors();
    }
    public function validateImpressions($attribute, $params) {//must return true or false.
        //make sure they chose a place to pick it up.
        if(!in_array($this->$attribute, $this->allowedImpressions))
            $this->addError($attribute, "Please select a valid item from the list.");
        return !$this->hasErrors();
    }
    public function validatePrintingType($attribute, $params) {//must return true or false.
        //make sure they chose a place to pick it up.
        if(!in_array($this->$attribute, $this->allowedPrintTypes))
            $this->addError($attribute, "Please select a valid item from the list.");
        return !$this->hasErrors();
    }
    public function validateQuantity($attribute, $params) {//must return true or false.
        //make sure they chose a place to pick it up.
        if($this->$attribute < 1)
            $this->addError($attribute, "Please input a number greater than zero.");
        return !$this->hasErrors();
    }
}
?>