<?php
class JobShipmentMap extends YActiveRecord {
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    //-------------------------
    public function tableName() {
        return 'JOB.IJOBSHIPMENTMAP';
    }
    //-------------------------
    public function rules() {
        return array(
                array('JOBID, PRODUCTCOUNT, SHIPMENTPKEY', 'safe'),
        );
    }
    //-------------------------
    public function relations() {
        return array(
                'job'=>array(self::BELONGS_TO, 'Job', 'JOBID'),
                'shipment'=>array(self::BELONGS_TO, 'Shipment', 'SHIPMENTPKEY'),
        );
    }
    //-------------------------
    public function submit() {
        $this->save();
        $this->shipment->save();
        $this->job->order->save();
    }
    //-------------------------
    public function afterDelete() {
        parent::afterDelete();
        $this->job->order->save();
//        $this->shipment.save();
    }
}