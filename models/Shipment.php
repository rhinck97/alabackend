<?php
require_once 'protected/lib/pam/pamShipping.php';
require_once 'protected/lib/pam/pamGen2.php';
require_once 'protected/lib/pam/pamFedExShipment2.php';
require_once 'protected/lib/onyx/onyxYii.php';
class Shipment extends YActiveRecord {
    //------------------
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    //------------------
    public function tableName() {
        return 'JOB.ISHIPMENT';
    }
    //------------------
    public function rules() {
        return array(
                array('LINK, SHIPPINGINFOID, ACTUALOUNCES, ACTUALSHIPPINGCOST, CHARGE, OUNCES', 'numerical'),
                array('RETURNTRACKINGNUMBER, TRACKINGNUMBER', 'length', 'max'=>100),
                array('SHIPPEDVIA, SHIPMENTID, DELIVERYMETHOD, RTSNUMBER', 'length', 'max'=>50),
                array('DELIVERYMETHOD', 'required'),
                array('NOTIFYSTATUS, DELIVERYSTATUS', 'length', 'max'=>20),
                array('TAXABLE', 'length', 'max'=>1),
                array('ESTIMATEDDELIVERY, RECTIME, DELIVERYSTATUSTIME', 'safe'),
        );
    }
    //------------------
    public function relations() {
        return array(
                'order'=>array(self::BELONGS_TO, 'Order', 'LINK'),
                'jobs'=>array(self::MANY_MANY, 'Job', 'IJOBSHIPMENTMAP(SHIPMENTPKEY, JOBID)'),
                'jobShipmentMaps'=>array(self::HAS_MANY, 'JobShipmentMap', 'SHIPMENTPKEY'),
                'shippingInfo'=>array(self::BELONGS_TO, 'ShippingInfo', 'SHIPPINGINFOID'),
                'deliveryMethod'=>array(self::BELONGS_TO, 'DeliveryMethod', 'DELIVERYMETHOD'),
        );
    }
    //------------------
    protected function beforeSave() {
        parent::beforeSave();
        //errIf($this->LINK, 'order id missing in shipment record');
        $shippingInfo = $this->shippingInfo;
        errIf($shippingInfo == null, 'shipping info record not found in database');
        errIf($this->deliveryMethod->DELIVERYTYPE == "INTERNATIONAL" &&
                $shippingInfo->COUNTRY == 'US', 'International shipments may not go to US');
		//TODO: we may wish to check for 'DELIVER' status going off campus
        $this->handleOunces();
        $this->TAXABLE = $this->shippingInfo->INSTATE;
        $this->handleCharge(); //this needs to be code reviewed to the old libraries and maybe port to yii
        return true;
    }
    //------------------
    public function submit() {
        $this->save();
        $this->order->save();
        return true;
    }
    //------------------
    public function shipmentInUse() {//
        $jobs = $this->jobs;
fileLogDebug($jobs);
        if(!empty($jobs)) {
            foreach($jobs as $job) {
                if($job->STATUS !== 'CANCELED') {
                    return true;
                }
            }
        }
        return false;
    }
    //------------------ todo why do we not do it this way
    // public function shipmentInUse() {
    //     $shipmentPkey = $this->SHIPMENTPKEY
    //     $count = queryScaler("select COUNT(*) from IJOBSHIPMENTMAP m join ijob j on m.jobid = j.jobid
    //                     where SHIPMENTPKEY = $shipmentPkey and status <> 'CANCELED'");
    //     return $count > 0;
    // }
    //------------------
    public function afterOrderOpened() {//
fileLog('rts number '.$this->RTSNUMBER);
fileLog('submit rts '.$this->deliveryMethod->SUBMITRTS);
fileLog('shipment in use '.$this->shipmentInUse());
        if($this->RTSNUMBER === null &&
				strToBool($this->deliveryMethod->SUBMITRTS) &&
                $this->shipmentInUse()) {
            $this->submitRtsToGen2();
            $this->save();
        }
    }
    //------------------
    private function handleOunces() {//todo should this just be an sql instead? what about canceled jobs
        $sumOunces = 0;
        $jobShipmentMaps = $this->jobShipmentMaps;
        foreach($jobShipmentMaps as $jobShipmentMap) {
            $product = $jobShipmentMap->job->product;
            if(!is_null($product)) {
                $sumOunces += $jobShipmentMap->PRODUCTCOUNT * $product->OUNCESPERPRODUCT;
            }
        }
        if($sumOunces > 0) {
            $this->OUNCES = $sumOunces;
        }
    }
    //------------------
    //The SHIPPINGBILLED is also checked in the order model should just be in one place. <-(not sure what this first sentance means)
    //Do not try to calulate the charge if the ounces are zero. The calculator will crash
    //Why would the ounces ever be zero you may ask? If the jobshipmentmaps haven't been
    //put into the database yet your ounces will be zero so you have to let the jobshipmentmap->submit()
    //call this shipment->save again to get corrected ounces
    private function handleCharge() {
        $jobShipmentMaps = $this->jobShipmentMaps;
fileLogDebugYii($jobShipmentMaps != null);
        if($jobShipmentMaps != null) {
            $order = $jobShipmentMaps[0]->job->order;
            if(($order->ORDERSTATUS == 'PENDING' || $order->ORDERSTATUS == 'PLACED')) {
fileLog('resolve shipment charge');
fileLogDebugYii($order->SHIPPINGBILLED);
fileLogDebugYii($this->deliveryMethod->HASCHARGE);
                if(strToBool($order->SHIPPINGBILLED) && strToBool($this->deliveryMethod->HASCHARGE)) {
                    $this->CHARGE = $this->calcMailRate();
fileLog('calculate shipment charge '.$this->CHARGE);
                }
                else {
                    $this->CHARGE = 0;
fileLog('dont calculate shipment charge. set to 0');
                }
            }
        }
    }
    //=================================
    //if no maps we don't want to die...
    //but then what do we do then????
    //============================================================================
    public function calcMailRate() {
        $shippingInfo = $this->shippingInfo;
fileLogDebugYii($shippingInfo);
        errIfNull($shippingInfo);

        $address1 = $shippingInfo->ADDRESS1;
        $address2 = $shippingInfo->ADDRESS2;
        $city = $shippingInfo->CITY;
        $stateCode = $shippingInfo->STATE;
        $postalCode = $shippingInfo->POSTALCODE;
        $countryCode = $shippingInfo->COUNTRY;
        $deliveryMethod = $this->DELIVERYMETHOD;
        $isResidential = true; //TODO MAKE THIS MORE DYNAMIC?
        $ounces = $this->OUNCES;
fileLog('shipment ounces '.$ounces);
fileLog('country code '.$countryCode);
        $lengthInInches = 0; //Length is unavailable, but 0 is allowed.
        $widthInInches = 0; //Width is unavailable, but 0 is allowed.
        $heightInInches = 0; //Height is unavailable, but 0 is allowed.
        return fetchMailRate($address1, $address2, $city, $stateCode, $postalCode, $countryCode,
                        $deliveryMethod, $isResidential, $ounces, $lengthInInches, $widthInInches, $heightInInches);
    }
    //============================================================================
    public function submitRtsToGen2() {
fileLog('made it to submitRtsToGen2 ');
        $rtsInfo['acctNum'] = $this->order->CUSTID;
        $rtsInfo['acctName'] = $this->order->customer->NAME;
        //we have a blank fromEmailName and fromEmailAddress right now...
        $shippingInfo = $this->shippingInfo;
        $rtsInfo['toEmail'] = $shippingInfo->EMAIL;
        $rtsInfo['toName'] = $shippingInfo->SHIPTONAME;
        $rtsInfo['toAddr1'] = $shippingInfo->ADDRESS1;
        $rtsInfo['toAddr2'] = $shippingInfo->ADDRESS2;
        $rtsInfo['toAddr3'] = $shippingInfo->ADDRESS3;
        $rtsInfo['toAddr4'] = $shippingInfo->ADDRESS4;
        $rtsInfo['toCity'] = $shippingInfo->CITY;
        $rtsInfo['toState'] = $shippingInfo->STATE;
        $rtsInfo['toZip'] = $shippingInfo->POSTALCODE;
        $rtsInfo['recSource'] = $this->order->RECSOURCE;
        $rtsInfo['toCountryCode'] = $shippingInfo->COUNTRY;
        $rtsInfo['toPhone'] = $shippingInfo->PHONE;
        $rtsInfo['service'] = $this->DELIVERYMETHOD;
        $rtsInfo['ounces'] = $this->OUNCES;
        $rtsInfo['isResidential'] = true;
        $rtsInfo['isDocument'] = false;
        $rtsNumber = ySubmitRtsToGen2($rtsInfo);
        $this->RTSNUMBER = $rtsNumber;
    }
}