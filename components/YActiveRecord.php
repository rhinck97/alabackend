<?php
require_once dirname(__FILE__)."/../lib/onyx/onyxCore.php";
class YActiveRecord extends CActiveRecord {
    //------------------override setAttributes to raise an error if a key is not found in the model
    public function setAttributes($givenAttributes, $safeOnly = true) {
        $tableAttributes = $this->getAttributes();
        valArrayKeysExist($givenAttributes, $tableAttributes, ' Key not in '.$this->tableName());
        return parent::setAttributes($givenAttributes, $safeOnly);
    }
    //------------------raise error if save returns false
    public function save($runValidation = true, $attributes = null) {
        $result = parent::save($runValidation, $attributes);
        if($result === false) {
            $errors = $this->getErrors();
            if(count($errors) === 0) {
                $errMsg = 'Unspecified error on save to model. Perhaps a query returned null in a trigger?';
                fileLogDebug($errMsg);
                err($errMsg);
            }
            else {
                fileLogDebug($errors);
                err(toDebugStr($errors));
            }
        }
        return $result;
    }
    //------------------raise error if delete returns false
    public function delete() {
        $result = parent::delete();
        if($result === false) {
            $errors = $this->getErrors();
            err(toDebugStr($errors));
        }
        return $result;
    }
    //------------------
    public static function test($testRecord) {
        $badAttr['thisWillNeverBeAField'] = '';
        try {
            $testRecord->setAttributes($badAttr);
            errFailedToThrow();
        }
        catch(Exception $ex) {
            testCorrectErrWasThrown($ex, cEKeyNotFound, "fail on bad attribute test");
        }
    }
}