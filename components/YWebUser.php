<?php
include_once Yii::app()->basePath.'/lib/CAS/CAS.php';
//CAS Setup
phpCAS::setDebug();
phpCAS::client(CAS_VERSION_2_0, 'cas.byu.edu', 443, 'cas');
class YWebUser extends CWebUser {
    //--------------------------------------------------------------------------
    public function getId() {
        if($this->isLoggedIn()) {
            return phpCAS::getUser();
        }
        return null;
    }
    //--------------------------------------------------------------------------
    public function login($identity = null, $duration = 0) {
        if(!$this->isLoggedIn()) {
        	phpCAS::forceAuthentication();
		}
    }
    //--------------------------------------------------------------------------
    public function isLoggedIn() {
        //TODO: figure out server validation
        phpCAS::setNoCasServerValidation();
        return phpCAS::isAuthenticated();
    }
    //--------------------------------------------------------------------------
    public function logout($destroySession = true) {
        if(phpCAS::isAuthenticated()) {
            $params['service'] = Yii::app()->params['authRedirect'];
            phpCAS::logout($params);
        }
    }
    //--------------------------------------------------------------------------

}
