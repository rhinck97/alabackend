<?php
require_once Yii::app()->basePath.'/lib/onyx/onyxCore.php';
require_once Yii::app()->basePath.'/lib/onyx/onyxYii.php';
//--------------------------------------------------------------------------
class YController extends CController {
    //--------------------------------------------------------------------------
    public static function accessDeniedError() {
        errHttp('Access Denied', 403);
    }
}