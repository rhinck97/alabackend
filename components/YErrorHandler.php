<?php
require_once dirname(__FILE__)."/../lib/onyx/onyxCore.php";
Yii::import('CHtml', true);
//----------------------------------------------------------
class YErrorHandler extends CErrorHandler {
    //--------------------------------------------------------------------------
    protected function handleException($exception) {//
        if($exception instanceof CHttpException) {
            $httpErrCode = $exception->statusCode;
        }
        else {
            $httpErrCode = 400;
        }

        $errMessage = $exception->getMessage();
        $errClass = get_class($exception);

        if($exception instanceof CDbException) {
            $errMessage = $this->extractDbErrMessage($errMessage);
        }

        errIf(headers_sent(), "Headers already sent in exception handler. Did you ".
                "possibly echo something for debugging? If so the echo should now be erased. ".
                "It creates a header. The original error is $httpErrCode $errMessage $errClass");
        header("HTTP/1.1 $httpErrCode");
        $response['class'] = $errClass;
        $response['message'] = $errMessage;
        echo jsonEncodePlus($response);
        //the following lines currently are not compatible with polymers ajax object expecting to see
        //json formatted results.
        if(YII_DEBUG) {
            echoLine("~##~");
            parent::handleException($exception);
        }
    }
    //--------------------------------------------------------------------------
    public function extractDbErrCodeStr($dbMessage) {//
        $temp = strNthItem(2, $dbMessage, 'ORA');
        $dbErrCode = strNthItem(1, $temp, ':');
        if(isNullOrEmptyStr($dbErrCode)) {
            $dbErrCode = false;
        }
        return $dbErrCode;
    }
	//--------------------------------------------------------------------------
    public function extractDbErrMessage($dbMessage) {//
        $dbErrCodeNum = $this->extractDbErrCodeStr($dbMessage);
        if($dbErrCodeNum === false) {
            return $dbMessage;
        }
		$dbErrCodeNum = intval($dbErrCodeNum);
		if(-20999 <= $dbErrCodeNum && $dbErrCodeNum <= -20000) {
            $errMessage = $this->extractCustomDbErrMessage($dbMessage);
        }
        else {
            $errMessage = $this->extractStandardDbErrMessage($dbMessage);
        }
        return $errMessage;
    }
    //--------------------------------------------------------------------------
    public function extractCustomDbErrMessage($dbMessage) {//
        $temp = strNthItem(2, $dbMessage, "ORA");
        $temp = strNthItem(2, $temp, ":");
        errIfNullOrEmptyStr($temp, "could not find message in db message");
        return trim($temp);
    }
    //--------------------------------------------------------------------------
    //Uses the exception code to find the index of where to begin parsing.
    //The function then parses according to what is needed for the given code
    //--------------------------------------------------------------------------
    public function extractStandardDbErrMessage($dbMessage) {
        $dbErrCodeStr = $this->extractDbErrCodeStr($dbMessage);
        errIf($dbErrCodeStr === false, "error code not found");

        $parceToken = "ORA$dbErrCodeStr:";
        $dbErrCodeNum = intval($dbErrCodeStr);
        if($dbErrCodeNum === -904 || $dbErrCodeNum === -936 ||
                $dbErrCodeNum === -1722 || $dbErrCodeNum === -911) {//Trying to reference a column that does not exist
            $afterErrCode = strNthItem(2, $dbMessage, $parceToken);
            $firstPart = strNthItem(2, $afterErrCode, ':');
            //$secondPart = strNthItem(3, $afterErrCode, ':');
            //return "$firstPart $secondPart";
            $secondPart = trim(strNthItem(1, $afterErrCode, '('));
            return "$secondPart";
        }
        else if($dbErrCodeNum === -1) {//Trying to add an item that already exists
            /*
              CDbCommand failed to execute the SQL statement: SQLSTATE[HY000]: General error: 1 OCIStmtExecute:
              ORA-00001: unique constraint (JOB.IORDER_PK) violated (ext\pdo_oci\oci_statement.c:148).
              The SQL statement executed was: update iorder set link = 848
            */
            $afterErrCode = strNthItem(2, $dbMessage, $parceToken);
            $firstPart = strNthItem(1, $afterErrCode, '. ');
            return trim($firstPart);
        }
        else if($dbErrCodeNum === -2291) {//Violating foreign key
            /*
              CDbCommand failed to execute the SQL statement: SQLSTATE[HY000]: General error: 2291 OCIStmtExecute: ORA-02291: integrity constraint (JOB.IORDER_VRECSOURCE_FK1) violated - parent key not found
              (ext\pdo_oci\oci_statement.c:148). The SQL statement executed was: update iorder set recsource = 'null' where link = 8585300
            */
            $afterErrCode = strNthItem(2, $dbMessage, $parceToken);
            $firstHalfofMessage = strNthItem(1, $afterErrCode, ')');
            $errMessage = $firstHalfofMessage.trim(strNthItem(2, $afterErrCode, '('));
            return $errMessage.')';
        }
        else if($dbErrCodeNum === -1400) {//Violating foreign key
            /*
                CDbCommand failed to execute the SQL statement: SQLSTATE[HY000]: General error: 1400 OCIStmtExecute: ORA-01400:
                cannot insert NULL into ("JOB"."IJOB"."JOBID")
                (ext\pdo_oci\oci_statement.c:148).
                The SQL statement executed was: INSERT INTO "IJOB"
                ("STATUS", "LINK", "PRODUCTID", "PRODUCTCOUNT", "SERVICELOC", "APPROVALSTATUS", "PRODUCTTYPE", "ACCESSCODE", "CHARGE")
                VALUES (:yp0, :yp1, :yp2, :yp3, :yp4, :yp5, :yp6, :yp7, :yp8) RETURNING "JOBID" INTO :RETURN_ID
            */
            $afterErrCode = strNthItem(2, $dbMessage, $parceToken);
            $errorWithNullColName = strNthItem(1, $afterErrCode, ')');
            return $errorWithNullColName.')';
        }
        else if($dbErrCodeNum === -12899) {//value too large
            /*
              CDbCommand failed to execute the SQL statement: SQLSTATE[HY000]: General error: 12899 OCIStmtExecute:
              ORA-12899: value too large for column "JOB"."IORDER"."CUSTID" (actual: 73, maximum: 20) (ext\pdo_oci\oci_statement.c:148).
              The SQL statement executed was: update iorder set custid = 'ddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd'
            */
            $afterErrCode = strNthItem(2, $dbMessage, $parceToken);
            $errMessage = strNthItem(1, $afterErrCode, ')');
            return $errMessage.')';
        }
        else if($dbErrCodeNum === -1407) {//value may no be null
            /*
              ORA-01407: cannot update ("JOB"."IJOB"."STATUS") to NULL
            */
            $afterErrCode = strNthItem(2, $dbMessage, $parceToken);
            return $afterErrCode;
        }
        else {
            $afterErrCode = strNthItem(2, $dbMessage, $parceToken);
            return $afterErrCode;
        }
    }
}
