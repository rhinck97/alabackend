<?php
//needs a code review
class YUserIdentity extends CUserIdentity {
    const ERROR_CONNECTION_FAILED = 101;
    const ERROR_UNVERIFIED_ACCOUNT = 102;
    public function authenticate() {
        $account = Account::model()->findByPk($this->username);
        if($account !== null && $account->VALIDATED == "T" && $account->PASSWORDHASH === AccountForm::hashPassword($this->password)) {//how is $this password set
            Yii::app()->user->setState('firstName', $account->FIRSTNAME);
            Yii::app()->user->setState('name', $account->FIRSTNAME." ".$account->LASTNAME);
            Yii::app()->user->setState('loginId', $this->username);
            Yii::app()->user->setState('email', $account->EMAIL);
            $this->errorCode = self::ERROR_NONE;
            return true;
        }
        if(!isNullOrEmptyStr($account) && $account->VALIDATED == "F") {
            $this->errorCode = self::ERROR_UNVERIFIED_ACCOUNT;
            $link = Yii::app()->createUrl(Yii::app()->getController()->site)."/site/ResetAndSendValidationEmail/email/".$this->username;
            $this->errorMessage = "This account has not yet been verified. Please login to your email account and use the provided link to verify your account. If you need to resend your verification email <a href='$link'>click here</a>.";
        }
        else {
            $this->errorCode = self::ERROR_UNKNOWN_IDENTITY;
            $this->errorMessage = "The username and/or password provided is incorrect please try again.";
        }
        return false;
    }
    //-----------------------------------------------------
    public function setRoles() {
        $empArea = Yii::app()->user->getState('area');
        $auth = Yii::app()->authManager;
        $auth->createRole('PMEmployee');

        if($empArea === "Print and Mail Production Ctr") {
            $auth->assign('PMEmployee', Yii::app()->user->Id);
        }
    }
}