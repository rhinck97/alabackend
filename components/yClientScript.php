<?php
class YClientScript extends CClientScript {
    function registerCssFile($url, $media = '') {
        if(file_exists("css/".$url) || (Yii::app()->theme && file_exists(Yii::app()->theme->basePath."/css/".$url))) {
            if(file_exists(Yii::app()->request->baseUrl."/css/".$url)) {
                parent::registerCssFile(Yii::app()->request->baseUrl."/css/".$url, $media);
            }
            if(Yii::app()->theme && file_exists(Yii::app()->theme->basePath."/css/".$url)) {
                parent::registerCssFile(Yii::app()->theme->baseUrl."/css/".$url, $media);
            }
        }
        else {
            parent::registerCssFile($url, $media);
        }
        return $this;
    }
//  function registerScriptFile($url, $position = NULL)
//  {
//    if(file_exists("lib/".$url)){
//      parent::registerScriptFile(Yii::app()->request->baseUrl."/lib/".$url, $position); 
//    }
//    else
//      parent::registerScriptFile($url, $position);
//  }
}